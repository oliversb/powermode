#tag Class
Protected Class MyVariant
	#tag Method, Flags = &h0
		Sub AngleChanged(sender as Angle)
		  #pragma unused sender
		  if Parent <> nil then
		    Parent.Changed = true
		  end if
		  
		  RaiseEvent AngleChanged()
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AngleValue() As Angle
		  return AngleProp
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AngleValue(assigns value as Angle)
		  Type = "Angle"
		  AngleProp = value
		  AddHandler value.AngleChanged, AddressOf AngleChanged
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function BooleanValue() As Boolean
		  return BooleanProp
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub BooleanValue(assigns value as boolean)
		  Type = "Boolean"
		  BooleanProp = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ColorValue() As color
		  return ColorProp
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ColorValue(assigns value as color)
		  Type = "Color"
		  ColorProp = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Copy(assigns toCopy as ValueChangeTracker)
		  dim value as MyVariant = toCopy.Value
		  
		  Type(false) = value.Type
		  select case value.Type
		  case "Boolean"
		    BooleanProp = value.BooleanProp
		  case "Color"
		    ColorProp = value.ColorProp
		  case "Double"
		    DoubleProp = value.DoubleProp
		  case "Integer"
		    IntProp = value.IntProp
		    'case "ObjectVariablePathArray"
		    'ObjectVariablePathArrayValue = value.ObjectVariablePathArrayValue
		  case "Resource"
		    ResourceValue = value.ResourceValue
		  case "String"
		    StringValue = value.StringProp
		  case "Angle"
		    if AngleProp = nil then
		      AngleProp = new Angle
		    end if
		    AngleValue.Clone(value.AngleProp)
		  end select
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function DoubleValue() As Double
		  select case Type
		  case "String"
		    return val(stringProp)
		  case "Integer"
		    return intProp
		  case "Double"
		    return doubleProp
		  else
		    raise new UnsupportedFormatException
		  end
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DoubleValue(assigns d as double)
		  Type = "Double"
		  doubleProp = d
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DoubleValue(assigns value as string)
		  Type = "Double"
		  doubleProp = val(value)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function IntValue() As integer
		  return IntProp
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub IntValue(setChanged as boolean = true, assigns value as integer)
		  Type(setChanged) = "Integer"
		  IntProp = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ObjectVariablePathArrayValue() As ObjectVariablePath()
		  return ObjectVariablePathArrayProp
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ObjectVariablePathArrayValue(assigns value() as ObjectVariablePath)
		  Type = "ObjectVariablePathArray"
		  ObjectVariablePathArrayProp = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ResourceValue() As Resource
		  select case Type
		  case "Resource"
		    return ResourceValue
		  else
		    return nil
		  end
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ResourceValue(assigns value as Resource)
		  Type = "Resource"
		  ResourceValue = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function StringValue() As string
		  select case Type
		  case "String"
		    return stringProp
		  case "Integer"
		    return str(intProp)
		  case "Double"
		    return str(doubleProp)
		  case "Boolean"
		    return str(BooleanProp)
		  case "Color"
		    return str(ColorProp)
		  case "Angle"
		    return str(AngleProp.Value)
		  else
		    raise new UnsupportedFormatException
		  end
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub StringValue(assigns value as string)
		  Type = "String"
		  StringProp = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Type() As string
		  return mType
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Type(assigns value as string)
		  Type(true) = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Type(setChanged as boolean, assigns value as string)
		  //clear value before setting a new one
		  select case mType
		  case "Boolean"
		    BooleanProp = false
		  case "Color"
		    ColorProp = &c000000
		  case "Double"
		    DoubleProp = 0
		  case "Integer"
		    IntProp = 0
		  case "ObjectVariablePathArray"
		    ObjectVariablePathArrayProp = nil
		  case "Resource"
		    ResourceValue = nil
		  case "String"
		    StringProp = ""
		  case "Angle"
		    AngleProp = nil
		  end select
		  
		  if setChanged and Parent <> nil then
		    Parent.Changed = true
		  end if
		  
		  mType = value
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event AngleChanged()
	#tag EndHook


	#tag Property, Flags = &h21
		Private AngleProp As Angle
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected BooleanProp As Boolean
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected ColorProp As Color
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected DoubleProp As Double
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected IntProp As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mType As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected ObjectVariablePathArrayProp() As ObjectVariablePath
	#tag EndProperty

	#tag Property, Flags = &h0
		Parent As ValueChangeTracker
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected ResourceValue As Resource
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected StringProp As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
