#tag Class
Protected Class ShortcutKey
	#tag Method, Flags = &h0
		Sub Constructor()
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(keyCode as integer)
		  me.KeyCode = keyCode
		  UseKeyCode = true
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function TestIndividualKeyPress() As boolean
		  if UseKeyCode then
		    //use keycode specified
		    return Keyboard.AsyncKeyDown(KeyCode)
		  else
		    //use custom test for key being pressed
		    return TestIndividualKeyPressEvent
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function TestPress() As boolean
		  dim pressed as boolean
		  
		  if JustPressedControlKey and Keyboard.AsyncControlKey and TestIndividualKeyPress then
		    pressed = true
		    PressedControlKey = false
		  elseif TestIndividualKeyPress then
		    JustPressedControlKey = false
		  end if
		  
		  PressedControlKey = Keyboard.AsyncControlKey
		  
		  return pressed
		End Function
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event TestIndividualKeyPressEvent() As boolean
	#tag EndHook


	#tag Property, Flags = &h21
		Private JustPressedControlKey As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		KeyCode As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mPressedControlKey As Boolean
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mPressedControlKey
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if (mPressedControlKey = false) and (TestIndividualKeyPress = false) and (value = true) then
			    JustPressedControlKey = true
			  end if
			  mPressedControlKey = value
			End Set
		#tag EndSetter
		Private PressedControlKey As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		UseKeyCode As boolean
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="KeyCode"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="UseKeyCode"
			Group="Behavior"
			Type="boolean"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
