#tag Class
Protected Class Parser
	#tag Method, Flags = &h21
		Private Function AddMatchState() As integer
		  //append a new match state to the matchTable
		  //called as part of the BuildMatchTable process
		  dim start as integer = ubound(matchTable) + 1
		  redim matchTable(start + lookup.Count)
		  return start
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AddStructureType(name as string, pattern as string, precedence as integer = 0, leftAssociative as boolean = true)
		  //clear match table
		  redim matchTable(-1)
		  
		  //expand substructure table to hold substructures
		  structureNames.Append name
		  me.leftAssociative.Append leftAssociative
		  precedences.Append Precedence
		  dim structureBound as integer = ubound(structureNames)
		  dim subStructures() as String = pattern.Split
		  dim subStructureBound as integer = ubound(subStructures)
		  subStructureBounds.Append subStructureBound
		  redim me.subStructures(structureBound,max(subStructureBound,UBound(me.subStructures,2)))
		  
		  //get substructures of specified pattern, and hash any tokens or structures therein
		  for i as integer = 0 to ubound(subStructures)
		    //add structure names to hash lookup
		    me.subStructures(structureBound,i) = Hash(subStructures(i))
		  next
		  
		  //add structure name to hash lookup
		  call Hash(name)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function BuildMatchTable() As boolean
		  //this method generates a finite state machine from the specified structures
		  //and tokens, which allows for a very fast lookup process when parsing
		  dim tablepos as integer = AddMatchState
		  for i as integer = ubound(structureNames) downto 0
		    tablepos = 0
		    for j as integer = 0 to subStructureBounds(i)
		      dim index as integer = tablepos + subStructures(i,j)
		      if matchTable(index) < 1 then
		        //undefined
		        tablepos = AddMatchState
		        matchTable(index) = tablepos
		      else
		        //go to next state in chain
		        tablepos = matchTable(index)
		      end
		    next
		    //set return structure index
		    for j as integer = tablepos + lookup.Count DownTo tablepos
		      if matchTable(j) = 0 then
		        matchTable(j) = - (i + 1)
		      elseif matchTable(j) < 0 then
		        return false //conflict detected
		      end
		    next
		  next
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Clear()
		  redim matchTable(-1)
		  redim strids(-1)
		  redim structures(-1)
		  redim structureNames(-1)
		  redim matchTable(-1)
		  redim precedences(-1)
		  redim leftAssociative(-1)
		  Lookup.Clear
		  index = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor()
		  lookup = new Dictionary
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function GetMatch(byref start as integer, byref finish as integer) As integer
		  //return the index of the structure type matched
		  dim strtype as integer
		  dim i, j, index as integer
		  dim upto as integer
		  
		  #pragma unused strtype
		  #pragma unused upto
		  
		  for i = start+1 to ubound(strids)
		    index = 0
		    for j = i to ubound(strids)
		      index = matchTable(index + strids(j))
		      if index < 1 then exit
		    next
		    if index > 1 then index = matchTable(index)
		    if index < 0 then
		      start = i
		      dim structureIndex as integer = -(index+1)
		      if start + subStructureBounds(structureIndex) > finish then
		        finish = start + subStructureBounds(structureIndex)
		        return structureIndex
		      end
		    end
		  next
		  
		  Return -1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function GetToken() As boolean
		  dim token as Token = FetchToken
		  if token = nil then
		    //end of input reached (or an error has occured)
		    return true
		  else
		    //add token to structure stack
		    structures.Append token
		    //add token id to structure id stack
		    strids.Append Hash(token.name)
		    return false
		  end
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function Hash(name as string) As integer
		  //hash the given structure or token name and return a unique id
		  if not lookup.HasKey(name) then
		    index = index + 1 //name indices start at 1
		    lookup.Value(name) = index
		  end
		  return lookup.Value(name)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Parse()
		  if ubound(matchTable) = -1 then
		    //build matching table
		    if not BuildMatchTable then
		      //conflict detected in StructureType patterns
		      raise new UnsupportedFormatException
		      return
		    end
		  end
		  
		  //clear results
		  redim structures(-1)
		  redim strids(-1)
		  
		  dim eof as boolean
		  dim prevmatch, prevstart, prevfinish as integer
		  do
		    dim start as integer = -1
		    dim finish as integer = -1
		    //get first match
		    dim match as integer = GetMatch(start,finish)
		    while match = -1 and eof = false
		      eof = GetToken
		      match = GetMatch(start,finish)
		    wend
		    if match = -1 then exit //program cannot be parsed any further
		    do
		      //find next match so precedence can be compared
		      prevstart = start
		      prevfinish = finish
		      prevmatch = match
		      match = GetMatch(start,finish)
		      while match = -1 and eof = false
		        eof = GetToken
		        match = GetMatch(start,finish)
		      wend
		      //if precedence of first match is higher, reduce and continue
		      if Precedence(prevmatch, match) then
		        ReduceStructures(prevmatch, prevstart)
		        exit
		      end
		    loop
		  loop
		  
		  Finished(structures)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function Precedence(index1 as integer, index2 as integer) As boolean
		  //return true if structure 1 has higher precedence than structure 2
		  //if precedence is the same, return true only if 2 is left-associative
		  if index2 = -1 then return true
		  if precedences(index2) < precedences(index1) then
		    return true
		  elseif precedences(index2) = precedences(index1) then
		    return leftAssociative(index2)
		  else
		    return false
		  end
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ReduceStructures(index as integer, position as integer)
		  dim strcts() as Token
		  dim substr as integer
		  
		  #pragma unused substr
		  
		  //remove matched stuctures or tokens from the stack
		  for i as integer = 0 to subStructureBounds(index)
		    strcts.Append structures(position)
		    structures.Remove position
		    strids.Remove position
		  next
		  //get replacement structure
		  dim strct as Token = Reduce(structureNames(index), strcts)
		  if strct <> nil then
		    if position > ubound(structures) then
		      structures.Append strct
		      strids.Append Hash(strct.name)
		    else
		      structures.Insert position, strct
		      strids.Insert position, Hash(strct.name)
		    end
		  end
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event FetchToken() As Token
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Finished(structures() as Token)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Reduce(name as String, subStructures() as Token) As Token
	#tag EndHook


	#tag Property, Flags = &h21
		Private comment As integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private error As integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private index As integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private leftAssociative() As boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private lookup As dictionary
	#tag EndProperty

	#tag Property, Flags = &h21
		Private matchTable() As integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private precedences() As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private strids() As integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private structureNames() As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private structures() As Token
	#tag EndProperty

	#tag Property, Flags = &h21
		Private subStructureBounds() As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private subStructures(-1,-1) As Integer
	#tag EndProperty


	#tag Constant, Name = version, Type = Double, Dynamic = False, Default = \"1.0", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
