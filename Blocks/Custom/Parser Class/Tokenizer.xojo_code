#tag Class
Protected Class Tokenizer
	#tag Method, Flags = &h0
		Sub AddTokenType(name as string, pattern as string, replacement as string = "\0")
		  tokenNames.Append name
		  tokenPatterns.Append pattern
		  subExpressionCount = subExpressionCount + 1
		  tknExpressionIndex.Append subExpressionCount
		  
		  // add pattern to search string
		  if regex.SearchPattern <> "" then regex.SearchPattern = regex.SearchPattern + "|"
		  regex.SearchPattern = regex.SearchPattern + "(" + pattern + ")"
		  
		  // increment replacement backreferences
		  dim result, number as string, escaped as boolean
		  for i as integer = 1 to replacement.Len
		    dim char as string = replacement.mid(i,1)
		    select case char
		    case "\", "$"
		      if escaped then
		        if val(number) = 0 then
		          result = result + "0"
		        else
		          result = result + str(val(number) + subExpressionCount)
		        end
		      end
		      result = result + char
		      escaped = true
		      number = ""
		    else
		      if escaped then
		        if IsNumeric(char) then
		          number = number + char
		        else
		          if val(number) = 0 then
		            result = result + "0" + char
		          else
		            result = result + str(val(number) + subExpressionCount) + char
		          end
		          escaped = false
		        end
		      else
		        result = result + char
		      end
		    end
		  next
		  if escaped and number <> "" then
		    if val(number) = 0 then
		      result = result + "0"
		    else
		      result = result + str(val(number) + subExpressionCount)
		    end
		  end
		  tokenReturnPatterns.Append result
		  
		  // determine subexpression count
		  dim inCharClass, prevBracket as Boolean = false
		  escaped = false
		  for i as integer = 1 to pattern.Len
		    select case pattern.mid(i,1)
		    case "\"
		      escaped = true
		      prevBracket = false
		    case "("
		      if not inCharClass and not escaped then subExpressionCount = subExpressionCount + 1
		      prevBracket = false
		      escaped = false
		    case "["
		      if inCharClass or escaped then
		        prevBracket = false
		        escaped = false
		      else
		        inCharClass = true
		        prevBracket = true
		      end
		    case "]"
		      if not prevBracket then inCharClass = false
		      prevBracket = false
		    else
		      prevBracket = false
		      escaped = false
		    end
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Clear()
		  //clear out all token types
		  redim tokenNames(-1)
		  redim tokenPatterns(-1)
		  redim tokenReturnPatterns(-1)
		  redim tknExpressionIndex(-1)
		  subExpressionCount = 0
		  regex.SearchPattern = ""
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor()
		  regex = new RegEx
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub GetNextToken(source as string, byref position as integer)
		  // get next token from input and advance pointer
		  dim match as RegExMatch = regex.Search(source,position-1)
		  
		  if match = nil then
		    
		    // no tokens found (end of file?)
		    if position <= source.len then
		      UnexpectedToken source.mid(position), position
		      position = source.len + 1
		    else
		      EOF
		    end
		    
		  else
		    
		    //check if token starts at next character
		    dim start as integer = match.SubExpressionStartB(0)
		    if start > position - 1 then
		      // no tokens matched at pos, return error
		      UnexpectedToken source.mid(position,start + 1 - position), position
		      // increment pointer
		      position = start + 1
		    end
		    
		    // determine which token was matched
		    dim tknIndex as integer
		    dim tknText as string = match.SubExpressionString(0)
		    for i as integer = 1 to match.SubExpressionCount - 1
		      if match.SubExpressionString(i) = tknText then
		        tknIndex = tknExpressionIndex.IndexOf(i)
		        exit
		      end
		    next
		    
		    // return value
		    dim length as integer = tknText.len
		    TokenMatched tokenNames(tknIndex), match.Replace(tokenReturnPatterns(tknIndex)), position, length
		    
		    // increment pointer
		    position = position + length
		    
		  end
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Tokenize(source as string)
		  // loop through the source string extracting all tokens
		  dim pos as integer = 1
		  while pos <= source.Len
		    // get next token from input and advance pointer
		    GetNextToken source, pos
		  wend
		  EOF
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event EOF()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event TokenMatched(name as string, text as string, position as integer, length as integer)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event UnexpectedToken(text as string, position as integer)
	#tag EndHook


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return regex.Options
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  regex.Options = value
			End Set
		#tag EndSetter
		Options As RegExOptions
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private regex As RegEx
	#tag EndProperty

	#tag Property, Flags = &h21
		Private subExpressionCount As integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private tknExpressionIndex() As integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private tokenNames() As string
	#tag EndProperty

	#tag Property, Flags = &h21
		Private tokenPatterns() As string
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected tokenReturnPatterns() As string
	#tag EndProperty


	#tag Constant, Name = version, Type = Double, Dynamic = False, Default = \"1.2", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
