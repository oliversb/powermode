#tag Class
Protected Class BlockSetVariable
Inherits BlockCustom
	#tag Event
		Sub GenerateHTML5Code(byref s as string, res as ResourceObject)
		  #pragma unused res
		  
		  //inject code from script
		  if VariablePath1 isa EngineVariable then
		    s = s + EngineVariable(VariablePath1).GetHTML5Setter(CustomMessage) + ";" + EndOfLine
		  else
		    s = s + VariablePath1.GetHTML5CodeName + " = " + CustomMessage + ";" + EndOfLine
		  end if
		  s = s + "//" + GetCodeName + "Done" + EndOfLine
		  
		  setCodeToCallFunction("Set", GetHTML5CodeName)
		  
		  
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h1000
		Sub Constructor()
		  Super.Constructor
		  
		  me.CodeNameBase = "pmSetVariable"
		  me.DisplayName = "Set Variable"
		  me.Title = "Set Variable"
		  me.Width = 150
		  
		  me.ObjectItems.Append "Set"
		  me.ObjectShowInput.Append true
		  me.ObjectShowOutput.Append false
		  
		  me.ObjectItems.Append "Done"
		  me.ObjectShowInput.Append false
		  me.ObjectShowOutput.Append true
		  
		  RequireVariablePath1 = true
		  
		  CustomMessage = "//Javascript Expression"
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h1
		Protected eof As boolean
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected position As Integer = 1
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected token As token
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected tokenFound As boolean
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected Tokenizer1 As Tokenizer
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="backupVariablePath1"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="backupVariablePath2"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="backupVariablePath3"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="BGColor"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CodeNameBase"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CustomMessage"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CustomPicture"
			Group="Behavior"
			Type="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DisplayCategory"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DisplayName"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IsEvent"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="OriginalPicture"
			Group="Behavior"
			Type="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RequireVariablePath1"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RequireVariablePath2"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RequireVariablePath3"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RequireVariablePath4"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Title"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="UniqueID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
