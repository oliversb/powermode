#tag Class
Protected Class BlockExecuteScript
Inherits BlockCustom
	#tag Event
		Sub GenerateHTML5Code(byref s as string, res as ResourceObject)
		  #pragma unused res
		  
		  //inject code from script
		  s = s + CompiledCode + EndOfLine
		  s = s + "//" + GetCodeName + "Done" + EndOfLine
		  
		  setCodeToCallFunction("Execute", GetHTML5CodeName)
		  
		  
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Sub AddJSCode(sender as tokenizer, name as string, text as string, position as integer, length as integer)
		  #pragma unused sender
		  #pragma unused position
		  #pragma unused length
		  
		  dim s as string
		  
		  if LastToken = "" then
		    select case name
		    case "identifier", "integer_literal", "string_literal", _
		      "+", "-", "*", "/", "%", _
		      "=", "==", "<", ">", "<=", ">=", "<>", _
		      ")", "(", _
		      "true", "false"
		      s = GetJSValue(name, text)
		    case "+=", "-=", "*=", "/=", "%=", _
		      "++", "--", _
		      "return", "break"
		      s = name
		      //start
		    case "for", "while", "loop", "method", "else if", "case", "break", "continue"
		      SetLastToken name
		      return
		    case "if", "select"
		      SetLastToken name
		      StatementTokens.Append name
		      return
		      //else
		    case "else"
		      if StatementTokens.Ubound <> -1 then
		        select case StatementTokens(StatementTokens.Ubound)
		        case "if"
		          s = "} else {"
		        case "select"
		          SelectElseCode s
		        end select
		        StatementTokens.Remove(StatementTokens.Ubound)
		      end if
		      //end
		    case "case else"
		      SelectElseCode s
		    case "end"
		      s = "}"
		    end select
		  else
		    select case LastToken
		    case "if"
		      //if val then
		      select case name
		      case "then", "end_line"
		        FinalStageIf s
		      else
		        ExpressionRecord.Append GetJSValue(name, text)
		        return
		      end select
		    case "else if"
		      //else if val then
		      select case name
		      case "then", "end_line"
		        FinalStageElseIf s
		      else
		        ExpressionRecord.Append GetJSValue(name, text)
		        return
		      end select
		    case "while"
		      //while val
		      select case name
		      case "end_line"
		        FinalStageWhile s
		      else
		        ExpressionRecord.Append GetJSValue(name, text)
		        return
		      end select
		    case "for"
		      select case TokenStage
		      case 0
		        //for var
		        TokenRecord.Append GetJSValue(name, text)
		        SetLastToken
		        return
		      case 1
		        //for var =
		        if name = "=" then
		          SetLastToken
		        else
		          ResetLastToken
		        end if
		        return
		      case 2
		        //for var = val
		        select case name
		        case "to", "downto"
		          NextToken name
		        else
		          ExpressionRecord.Append GetJSValue(name, text)
		        end select
		        return
		      case 3
		        select case name
		        case "end_line"
		          FinalStageFor s
		        case "step"
		          NextToken
		          return
		        case else
		          ExpressionRecord.Append GetJSValue(name, text)
		          return
		        end select
		      case 4
		        //for var = val1 to val2 - step val3
		        if name = "end_line"  then
		          FinalStageFor s
		        else
		          ExpressionRecord.Append GetJSValue(name, text)
		          return
		        end if
		      end select
		    case "loop"
		      //loop val
		      select case name
		      case "end_line"
		        FinalStageLoop s
		      case else
		        ExpressionRecord.Append GetJSValue(name, text)
		      end select
		    case "method"
		      select case name
		      case "end_line"
		        FinalStageMethod s
		      case else
		        ExpressionRecord.Append GetJSValue(name, text)
		      end select
		    case "select"
		      //select case var
		      select case name
		      case "end_line"
		        FinalStageSelect s
		      case else
		        ExpressionRecord.Append GetJSValue(name, text)
		      end select
		    case "case"
		      select case name
		      case "end_line"
		        FinalStageCase s
		      case else
		        ExpressionRecord.Append GetJSValue(name, text)
		      end select
		    case "break"
		      //break - val
		      select case name
		      case "identifier"
		        //directly using text instead of GetJSValue because it contains a label name
		        FinalStageBreak s, text
		      case "end_line"
		        FinalStageBreak s
		      case else
		        ResetLastToken
		        return
		      end select
		    case "continue"
		      //continue - val
		      select case name
		      case "identifier"
		        FinalStageContinue s, text
		      case "end_line"
		        FinalStageContinue s
		      case else
		        ResetLastToken
		        return
		      end select
		    end select
		  end if
		  
		  CompiledCode = CompiledCode + s
		  if name = "end_line" then
		    CompiledCode = CompiledCode + EndOfLine
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1000
		Sub Constructor()
		  Super.Constructor
		  
		  me.CodeNameBase = "pmScript"
		  me.DisplayName = "Execute Script"
		  me.Title = "Execute Script"
		  me.Width = 150
		  
		  me.ObjectItems.Append "Execute"
		  me.ObjectShowInput.Append true
		  me.ObjectShowOutput.Append false
		  
		  me.ObjectItems.Append "Done"
		  me.ObjectShowInput.Append false
		  me.ObjectShowOutput.Append true
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub EOF(sender as Tokenizer)
		  #pragma unused sender
		  
		  eof = true
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function FetchToken(sender as parser) As token
		  #pragma unused sender
		  //fetch next token using tokenizer
		  //ignore white space and comments
		  tokenFound = false
		  token = nil
		  while not (tokenFound or eof)
		    Tokenizer1.GetNextToken CustomMessage, position
		  wend
		  return token
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub FinalStageBreak(byref s as string, text as string = "")
		  NextToken
		  
		  s = "break"
		  //break specific loop
		  if text <> "" then
		    s = s + " " + text
		  end if
		  
		  ResetLastToken
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub FinalStageCase(byref s as string)
		  NextToken
		  
		  if BreakReady then
		    s = "break" + EndOfLine
		  end if
		  BreakReady = true
		  s = s + "case " + TokenRecord(0)+ ":"
		  
		  ResetLastToken
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub FinalStageContinue(byref s as string, text as string = "")
		  NextToken
		  
		  s = "continue"
		  //break specific loop
		  if text <> "" then
		    s = s + " " + text
		  end if
		  
		  ResetLastToken
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub FinalStageElseIf(byref s as string)
		  NextToken
		  
		  s = "} else if (" + TokenRecord(0) + ") {"
		  
		  ResetLastToken
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub FinalStageFor(byref s as string)
		  dim useDownTo as boolean = (TokenRecordName(0) = "downto")
		  dim compareCode as string
		  dim incrementCode as string
		  
		  NextToken
		  
		  compareCode = If(useDownTo, " > ", " < ")
		  
		  s = "for (" + TokenRecord(0) + " = " + TokenRecord(1) + "; " + TokenRecord(0) + compareCode + TokenRecord(2) + "; "
		  
		  if TokenRecord.Ubound = 3 then
		    //step
		    //downto = "-="
		    //to = "+="
		    incrementCode = If(useDownTo,  " -= ", " += ")
		    s = s + TokenRecord(0) + incrementCode + TokenRecord(3)
		  else
		    //step not specified (increment by 1 as default)
		    //downto = "--"
		    //to = "++"
		    incrementCode = If(useDownTo, "--", "++")
		    s = s + TokenRecord(0) + incrementCode
		  end if
		  
		  s = s + ") {"
		  
		  ResetLastToken
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub FinalStageIf(byref s as string)
		  NextToken
		  
		  s = "if (" + TokenRecord(0) + ") {"
		  
		  ResetLastToken
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub FinalStageLoop(byref s as string)
		  NextToken
		  
		  //create a loop without the user having to manually write a for loop
		  s = "for (var loop = 0; loop < " + TokenRecord(0) + "; loop++) {"
		  
		  ResetLastToken
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub FinalStageMethod(byref s as string)
		  NextToken
		  
		  s = "function pmMETHOD" + TokenRecord(0) + "() {"
		  
		  ResetLastToken
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub FinalStageSelect(byref s as string)
		  NextToken
		  
		  s = "switch (" + TokenRecord(0) + ") {"
		  BreakReady = false
		  
		  ResetLastToken
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub FinalStageTEMPLATE(byref s as string)
		  NextToken
		  
		  s = ""
		  
		  ResetLastToken
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub FinalStageWhile(byref s as string)
		  NextToken
		  
		  s = "while (" + TokenRecord(0) + ") {"
		  
		  ResetLastToken
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub Finished(sender as parser, structures() as token)
		  #pragma unused sender
		  
		  #pragma unused structures
		  
		  'if ubound(structures) <> 0 or structures(0).name <> "program" then
		  'ErrorList.Append "*** Parsing failed: Unable to reduce input to a single program structure ***"
		  'else
		  'ErrorList.Append "*** Parsing succeeded ***"
		  'end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function GetJSValue(name as string, text as string) As string
		  select case name
		  case "identifier"
		    return "this.pmVAR" + text
		  case "integer_literal"
		    return text
		  case "string_literal"
		    return """" + text + """"
		  case "+", "-", "*", "/", "%", _
		    "=", "==", "<", ">", "<=", ">=", _
		    ")", "(", _
		    "true", "false"
		    return name
		  case "<>"
		    return "!="
		  else
		    ResetLastToken
		  end select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1021
		Private Sub NextToken(name as string = "")
		  //append ExpressionRecords to TokenRecord and then empty array for next time's use
		  TokenRecord.Append Join(ExpressionRecord, "")
		  redim ExpressionRecord(-1)
		  //setup for next stage
		  SetLastToken
		  //record name even if it's not given
		  if name <> "" then
		    TokenRecordName.Append name
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ParseCode(text as string)
		  CustomMessage = text
		  CompiledCode = ""
		  
		  redim ErrorList(-1)
		  
		  eof = false
		  position = 1
		  
		  Tokenizer1 = new Tokenizer
		  Parser1 = new Parser
		  
		  //add tokenizer1's event handlers
		  AddHandler Tokenizer1.EOF, AddressOf EOF
		  AddHandler Tokenizer1.TokenMatched, AddressOf TokenMatched
		  AddHandler Tokenizer1.UnexpectedToken, AddressOf UnexpectedToken
		  //add parser1's event handlers
		  AddHandler Parser1.FetchToken, AddressOf FetchToken
		  AddHandler Parser1.Finished, AddressOf Finished
		  AddHandler Parser1.Reduce, AddressOf Reduce
		  
		  //setup tokens
		  Tokenizer1.AddTokenType "end_line","\r",""
		  //not compiled
		  Tokenizer1.AddTokenType "whitespace","[ \n\r]",""
		  Tokenizer1.AddTokenType "comment","//([^\n\r]*)","\1"
		  //end
		  Tokenizer1.AddTokenType "end","end if",""
		  Tokenizer1.AddTokenType "end","wend",""
		  Tokenizer1.AddTokenType "end","end while",""
		  Tokenizer1.AddTokenType "end","next",""
		  Tokenizer1.AddTokenType "end","end for",""
		  Tokenizer1.AddTokenType "end","end select",""
		  Tokenizer1.AddTokenType "end","end loop",""
		  Tokenizer1.AddTokenType "end","end method",""
		  Tokenizer1.AddTokenType "end","end",""
		  //break
		  Tokenizer1.AddTokenType "break","exit",""
		  Tokenizer1.AddTokenType "continue","continue",""
		  Tokenizer1.AddTokenType "return","return",""
		  //select case
		  Tokenizer1.AddTokenType "case else","case else",""
		  Tokenizer1.AddTokenType "select","select case",""
		  Tokenizer1.AddTokenType "case","case",""
		  //if
		  Tokenizer1.AddTokenType "else if","elseif",""
		  Tokenizer1.AddTokenType "else if","else if",""
		  Tokenizer1.AddTokenType "if","if",""
		  Tokenizer1.AddTokenType "then","then",""
		  Tokenizer1.AddTokenType "else","else",""
		  //for
		  Tokenizer1.AddTokenType "for","for",""
		  Tokenizer1.AddTokenType "step","step",""
		  Tokenizer1.AddTokenType "to","to",""
		  Tokenizer1.AddTokenType "downto","downto",""
		  //while
		  Tokenizer1.AddTokenType "while","while",""
		  //loop
		  Tokenizer1.AddTokenType "loop","loop",""
		  //method
		  Tokenizer1.AddTokenType "method","method",""
		  //boolean
		  Tokenizer1.AddTokenType "true","true",""
		  Tokenizer1.AddTokenType "false","false",""
		  //maths operators
		  Tokenizer1.AddTokenType "+","[+]",""
		  Tokenizer1.AddTokenType "-","[-]",""
		  Tokenizer1.AddTokenType "*","[*]",""
		  Tokenizer1.AddTokenType "/","[/]",""
		  //comparision operators
		  Tokenizer1.AddTokenType "<=","<=",""
		  Tokenizer1.AddTokenType ">=",">=",""
		  Tokenizer1.AddTokenType "<>","<>",""
		  Tokenizer1.AddTokenType ">","[>]",""
		  Tokenizer1.AddTokenType "<","[<]",""
		  Tokenizer1.AddTokenType "==","==",""
		  Tokenizer1.AddTokenType "=","[=]",""
		  //
		  Tokenizer1.AddTokenType "(","[(]",""
		  Tokenizer1.AddTokenType ")","[)]",""
		  Tokenizer1.AddTokenType "integer_literal","[0-9]+"
		  Tokenizer1.AddTokenType "real_literal","[0-9]*[.][0-9]+|[0-9]+[.][0-9]*"
		  Tokenizer1.AddTokenType "string_literal","[""]([^""]*)[""]","\1"
		  Tokenizer1.AddTokenType "type","string|integer|boolean"
		  Tokenizer1.AddTokenType "identifier","[a-z_][a-z0-9_]*"
		  
		  
		  //set up operands
		  Parser1.AddStructureType "operand","identifier",1000
		  Parser1.AddStructureType "operand","string_literal",1000
		  Parser1.AddStructureType "operand","real_literal",1000
		  Parser1.AddStructureType "operand","integer_literal",1000
		  Parser1.AddStructureType "operand","expression",1003
		  Parser1.AddStructureType "operand","( operand )",1001
		  //set up prefix expressions
		  Parser1.AddStructureType "expression","- operand",1002
		  //set up infix expressions
		  Parser1.AddStructureType "expression","operand + operand",2
		  Parser1.AddStructureType "expression","operand - operand",2
		  Parser1.AddStructureType "expression","operand * operand",3
		  Parser1.AddStructureType "expression","operand / operand",3
		  Parser1.AddStructureType "expression","operand <= operand",4
		  Parser1.AddStructureType "expression","operand >= operand",4
		  Parser1.AddStructureType "expression","operand <> operand",4
		  Parser1.AddStructureType "expression","operand == operand",5,false
		  Parser1.AddStructureType "expression","operand < operand",4
		  Parser1.AddStructureType "expression","operand > operand",4
		  Parser1.AddStructureType "assignment","operand = operand",1,false
		  //statements
		  Parser1.AddStructureType "statement","print operand",-1
		  Parser1.AddStructureType "statement","if operand then program end",-1
		  Parser1.AddStructureType "statement","if operand then program else program end",-1
		  //loops
		  Parser1.AddStructureType "loop","for assignment to operand program next",-1
		  Parser1.AddStructureType "loop","for assignment downto operand program next",0
		  //program
		  Parser1.AddStructureType "program","declaration",-1
		  Parser1.AddStructureType "program","statement",-1
		  Parser1.AddStructureType "program","loop",-1
		  Parser1.AddStructureType "program","program program",0
		  
		  Parser1.Parse
		  redim StatementTokens(-1)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function Reduce(sender as parser, name as string, subStructures() as token) As token
		  #pragma unused sender
		  
		  //add a description to the list box
		  dim description as string = name + "(" + subStructures(0).name
		  for i as integer = 1 to ubound(subStructures)
		    description = description + "," + subStructures(i).name
		  next
		  description = description + ")"
		  
		  ErrorList.Append description
		  
		  
		  //create a token to replace the tokens or structures that have been collapsed
		  return new Token(name)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ResetLastToken()
		  LastToken = ""
		  TokenStage =  -1
		  redim TokenRecord(-1)
		  redim TokenRecordName(-1)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SelectElseCode(byref s as string)
		  if BreakReady then
		    s = "break" + EndOfLine
		  end if
		  s = s + "default:"
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SetLastToken(name as string = "")
		  TokenStage = TokenStage + 1
		  if name <> "" then
		    LastToken = name
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub TokenMatched(sender as Tokenizer, name as string, text as string, position as integer, length as integer)
		  #pragma unused sender
		  #pragma unused position
		  #pragma unused length
		  
		  if name <> "whitespace" then
		    if text = "" then
		      ErrorList.Append name
		    else
		      ErrorList.Append name + "(" + text + ")"
		    end
		    AddJSCode(sender, name, text, position, length)
		    
		    if name <> "comment" then
		      tokenFound = true
		      token = new Token(name)
		      return
		    end
		  end
		  tokenFound = false
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UnexpectedToken(sender as tokenizer, text as string, position as integer)
		  #pragma unused sender
		  #pragma unused position
		  
		  ErrorList.Append "*** Unexpected Token: """ + text + """ ***"
		  tokenFound = true
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h1
		Protected BreakReady As Boolean
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected CompiledCode As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected eof As boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		ErrorList() As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected ExpressionRecord() As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected LastToken As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected Parser1 As Parser
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected position As Integer = 1
	#tag EndProperty

	#tag Property, Flags = &h21
		#tag Note
			This variable is used to work with tokens used after 'else'.
		#tag EndNote
		Private StatementTokens() As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected token As token
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected tokenFound As boolean
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected Tokenizer1 As Tokenizer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected TokenRecord() As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected TokenRecordName() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		TokenStage As Integer = -1
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="backupVariablePath1"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="backupVariablePath2"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="backupVariablePath3"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="BGColor"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CodeNameBase"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CustomMessage"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CustomPicture"
			Group="Behavior"
			Type="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DisplayCategory"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DisplayName"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IsEvent"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="OriginalPicture"
			Group="Behavior"
			Type="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RequireVariablePath1"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RequireVariablePath2"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RequireVariablePath3"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RequireVariablePath4"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Title"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TokenStage"
			Group="Behavior"
			InitialValue="-1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="UniqueID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
