#tag Class
Protected Class BlockBase
	#tag Method, Flags = &h0
		Sub CallGenerateHTML5Code(res as ResourceObject, byref s as string)
		  GenerateHTML5Code(s,res)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ConnectToBorn(res as ResourceObject)
		  if res <> nil then
		    //find the BlockBorn and connect it to this block
		    for each block as contentframe in res.Blocks
		      if block.tag isa BlockBorn then
		        block.contents(0).outputs.Append me.CodeFrame.contents(0)
		        exit for
		      end if
		    next
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor()
		  DocFile.UniqueIDCount = DocFile.UniqueIDCount + 1
		  UniqueID = Str(DocFile.UniqueIDCount)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetCodeName() As string
		  return CodeNameBase + UniqueID
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetCurrentObjectFromName(theName as String) As contentObject
		  for each content as contentObject in CodeFrame.contents
		    if Lowercase(theName) = Lowercase(TextObject(content).text) then
		      Return content
		    end if
		  next
		  Return nil
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetHTML5CodeName() As string
		  return GetHTML5ValueCodeName + "();"
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetHTML5ValueCodeName() As string
		  return "this." + GetCodeName
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		 Shared Sub HelperGenerateHTML5CodePhase2(theSourceObject as contentObject, theDestinationObject as contentObject)
		  dim sCommentTag as string
		  dim sReplacementCode as string
		  
		  if theSourceObject.parent.tag isa BlockBorn then
		    sCommentTag = BlockBorn(theSourceObject.parent.tag).ResParent.GetCodeCommentConstructor
		  else
		    sCommentTag = "//" + BlockBase(theSourceObject.parent.tag).GetCodeName + TextObject(theSourceObject).text
		  end if
		  
		  sReplacementCode = theDestinationObject.CodeToCallThisObject
		  
		  HTML5InsertCode(sReplacementCode,sCommentTag)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		 Shared Sub HTML5InsertCode(s as string, marker as string)
		  HTML5InsertCodeAtMarker(s, marker)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RefreshContents(cf as contentframe)
		  redim cf.contents(-1)
		  
		  dim tempObj as BlockBase = self
		  
		  for i as integer = 0 to tempObj.ObjectItems.Ubound
		    cf.AppendObject new TextObject(tempObj.ObjectItems(i))
		    cf.contents(cf.contents.Ubound).ShowInput = tempObj.ObjectShowInput(i)
		    cf.contents(cf.contents.Ubound).ShowOutput = tempObj.ObjectShowOutput(i)
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetCodeToCallFunction(forName as string, withCodeToCall as string)
		  GetCurrentObjectFromName(forName).CodeToCallThisObject = withCodeToCall
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event GenerateHTML5Code(byref s as string, res as ResourceObject)
	#tag EndHook


	#tag Property, Flags = &h0
		#tag Note
			Backup of math variable before it can be referenced
		#tag EndNote
		backupVariablePath1 As String
	#tag EndProperty

	#tag Property, Flags = &h0
		backupVariablePath2 As String
	#tag EndProperty

	#tag Property, Flags = &h0
		backupVariablePath3 As String
	#tag EndProperty

	#tag Property, Flags = &h0
		backupVariablePathArray() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		BGColor As Color
	#tag EndProperty

	#tag Property, Flags = &h0
		#tag Note
			The corrosponding ContentFrame.
		#tag EndNote
		CodeFrame As ContentFrame
	#tag EndProperty

	#tag Property, Flags = &h0
		#tag Note
			Base for reference to this variable in the compiled code
		#tag EndNote
		CodeNameBase As String
	#tag EndProperty

	#tag Property, Flags = &h0
		#tag Note
			Special value. For example:
			<
			>
			<>
			=
		#tag EndNote
		CustomMessage As String
	#tag EndProperty

	#tag Property, Flags = &h0
		#tag Note
			Altered version of original picture (depends on size of picture)
		#tag EndNote
		CustomPicture As Picture
	#tag EndProperty

	#tag Property, Flags = &h0
		DisplayCategory As string
	#tag EndProperty

	#tag Property, Flags = &h0
		DisplayName As string
	#tag EndProperty

	#tag Property, Flags = &h0
		IsEvent As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		ObjectItems() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		ObjectShowInput() As boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		ObjectShowOutput() As boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		#tag Note
			Kept reference of the original picture to fill the ContentObject with.
		#tag EndNote
		OriginalPicture As Picture
	#tag EndProperty

	#tag Property, Flags = &h0
		RequireVariablePath1 As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		RequireVariablePath2 As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		RequireVariablePath3 As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		RequireVariablePath4 As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		#tag Note
			Examples:
			If
			While
			Note
		#tag EndNote
		Title As string
	#tag EndProperty

	#tag Property, Flags = &h0
		#tag Note
			The unique identifier for this block (so it is unique in the compiled code).
		#tag EndNote
		UniqueID As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Variable As ObjectVariable
	#tag EndProperty

	#tag Property, Flags = &h0
		VariablePath1 As ObjectVariablePath
	#tag EndProperty

	#tag Property, Flags = &h0
		VariablePath2 As ObjectVariablePath
	#tag EndProperty

	#tag Property, Flags = &h0
		VariablePath3 As ObjectVariablePath
	#tag EndProperty

	#tag Property, Flags = &h0
		VariablePath4 As ObjectVariablePath
	#tag EndProperty

	#tag Property, Flags = &h0
		VariablePathArray() As ObjectVariablePath
	#tag EndProperty

	#tag Property, Flags = &h0
		Width As UInt32
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="backupVariablePath1"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="backupVariablePath2"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="backupVariablePath3"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="BGColor"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CodeNameBase"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CustomMessage"
			Group="Behavior"
			InitialValue="="
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CustomPicture"
			Group="Behavior"
			Type="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DisplayCategory"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DisplayName"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IsEvent"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="OriginalPicture"
			Group="Behavior"
			Type="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RequireVariablePath1"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RequireVariablePath2"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RequireVariablePath3"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RequireVariablePath4"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Title"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="UniqueID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
