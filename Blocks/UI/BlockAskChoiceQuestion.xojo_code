#tag Class
Protected Class BlockAskChoiceQuestion
Inherits BlockUI
	#tag Event
		Sub GenerateHTML5Code(byref s as string, res as ResourceObject)
		  #pragma unused s
		  #pragma unused res
		  
		  's = s + me.VariablePath1.GetHTML5CodeName + " = askChoiceQuestion(" + me.VariablePath2.GetHTML5CodeName + ", " + VariablePath3.GetHTML5CodeName + ");" + EndOfLine
		  's = s + "switch(" + me.VariablePath1.GetHTML5CodeName + ") {" + EndOfLine
		  'dim arr() as ObjectVariable = VariablePath3.Value.ObjectVariablePathArrayValue
		  'for each v as ObjectVariable in arr
		  'UniqueIDCount = UniqueIDCount + 1
		  's = s + "case " + v.GetHTML5CodeName + ":" + EndOfLine
		  's = "//Choice" + str(UniqueIDCount)
		  'next
		  '
		  '
		  'setCodeToCallFunction("Ask", GetHTML5CodeName)
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h1000
		Sub Constructor()
		  Super.Constructor
		  
		  me.DisplayName = "Ask Choice Question"
		  me.Title = "Ask Choice Question"
		  me.CodeNameBase = "AskChoiceQuestion"
		  me.Width = 120
		  
		  ListChoices
		  
		  //variable to store answer
		  me.RequireVariablePath1 = true
		  //question to ask
		  me.RequireVariablePath2 = true
		  //choices
		  me.RequireVariablePath3 = true
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ListChoices()
		  'redim me.ObjectItems(-1)
		  'redim me.ObjectShowInput(-1)
		  'redim me.ObjectShowOutput(-1)
		  '
		  'me.ObjectItems.Append "Ask"
		  'me.ObjectShowInput.Append true
		  'me.ObjectShowOutput.Append false
		  '
		  'if VariablePath3 <> nil and VariablePath3.Value <> nil then
		  '//show choices
		  'dim arr() as ObjectVariable = VariablePath3.Value.ObjectVariablePathArrayValue
		  'for each v as ObjectVariable in arr
		  'if v <> nil and v.Type = "Text" then
		  'me.ObjectItems.Append "- " + v.Value.StringValue
		  'me.ObjectShowInput.Append false
		  'me.ObjectShowOutput.Append true
		  'end if
		  'next
		  'end if
		  '
		  'me.ObjectItems.Append "Done"
		  'me.ObjectShowInput.Append false
		  'me.ObjectShowOutput.Append true
		End Sub
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="backupVariablePath1"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="backupVariablePath2"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="backupVariablePath3"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="BGColor"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CodeNameBase"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CustomMessage"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CustomPicture"
			Group="Behavior"
			Type="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DisplayCategory"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DisplayName"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IsEvent"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="OriginalPicture"
			Group="Behavior"
			Type="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RequireVariablePath1"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RequireVariablePath2"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RequireVariablePath3"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RequireVariablePath4"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Title"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="UniqueID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
