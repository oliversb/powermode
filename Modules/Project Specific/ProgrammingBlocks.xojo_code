#tag Module
Protected Module ProgrammingBlocks
	#tag Method, Flags = &h0
		Sub AddBlockRows(catagory as CatagoryEnum, ByVal Blocks as listbox)
		  select case catagory
		  case CatagoryEnum.Logic
		    blocks.AddRow "If... Then..."
		    blocks.AddRow "Do While"
		    blocks.AddRow "Timer"
		    blocks.AddRow "If... OR... Then..."
		  case CatagoryEnum.Numbers
		    blocks.AddRow "Add"
		    blocks.AddRow "Subtract"
		    blocks.AddRow "Multiply"
		    blocks.AddRow "Divide"
		    blocks.AddRow "Remainder"
		    blocks.AddRow "Random"
		    blocks.AddRow "Abs (convert to positive)"
		    blocks.AddRow "Round"
		    blocks.AddRow "Floor (round down)"
		    blocks.AddRow "Ceil (round up)"
		    blocks.AddRow "Square Root"
		    blocks.AddRow "Sin"
		    blocks.AddRow "Cos"
		    blocks.AddRow "Tan"
		    blocks.AddRow "Asin"
		    blocks.AddRow "Acos"
		    blocks.AddRow "Atan"
		    blocks.AddRow "Atan2"
		  case CatagoryEnum.Text
		    blocks.AddRow "Join Text"
		    blocks.AddRow "Lowercase"
		    blocks.AddRow "Uppercase"
		    blocks.AddRow "Replace Text"
		    blocks.AddRow "Text Length"
		  case CatagoryEnum.Physics
		    blocks.AddRow "Set X"
		    blocks.AddRow "Set Y"
		    blocks.AddRow "Set Velocity X"
		    blocks.AddRow "Set Velocity Y"
		    blocks.AddRow "Set Acceleration X"
		    blocks.AddRow "Set Acceleration Y"
		    blocks.AddRow "Set Max Velocity X"
		    blocks.AddRow "Set Max Velocity Y"
		    blocks.AddRow "Set Gravity"
		    blocks.AddRow "Set Mass"
		    blocks.AddRow "Set Max Angular Velocity"
		    blocks.AddRow "Set Rotation"
		    blocks.AddRow "Reset"
		    blocks.AddRow "Set Collision Movement?"
		  case CatagoryEnum.Graphics
		    blocks.AddRow "Pen Down"
		    blocks.AddRow "Pen Up"
		    blocks.AddRow "Set Draw Point"
		    blocks.AddRow "Set Draw Colour"
		    blocks.AddRow "Set Draw Opacity"
		    blocks.AddRow "Set Line Thickness"
		    blocks.AddRow "Clear"
		    blocks.AddRow "Draw Rectangle"
		    blocks.AddRow "Draw Line"
		    blocks.AddRow "Draw Circle"
		    blocks.AddRow "Draw Ellipse"
		    blocks.AddRow "Begin Fill"
		    blocks.AddRow "End Fill"
		    blocks.AddFolder "Sprite/Animation"
		  case CatagoryEnum.UI
		    blocks.AddRow "Show Message"
		    blocks.AddRow "Ask Question"
		    blocks.AddRow "Ask Yes/No Question"
		    blocks.AddRow "Ask Choice Question"
		  case CatagoryEnum.Sound
		    blocks.AddRow "Play Music"
		    blocks.AddRow "Play Sound"
		    blocks.AddRow "Pause Sound"
		    blocks.AddRow "Resume Sound"
		    blocks.AddRow "Stop Sound"
		    blocks.AddRow "Set Volume"
		    blocks.AddRow "Set Pan"
		  case CatagoryEnum.Input
		    blocks.AddFolder "Events"
		    blocks.Expanded(0) = true
		  case CatagoryEnum.Test
		    blocks.AddRow "Log"
		    'blocks.AddRow "Display Score"
		    'blocks.AddRow "Display Lives"
		    'blocks.AddRow "Display Health"
		    'blocks.AddRow "Display Healthbar"
		    'blocks.AddRow "Hide Score"
		    'blocks.AddRow "Hide Lives"
		    'blocks.AddRow "Hide Health"
		    'blocks.AddRow "Hide Healthbar"
		  case CatagoryEnum.Misc
		    blocks.AddRow "Spawn"
		    blocks.AddRow "Spawn At"
		    blocks.AddRow "Kill"
		    blocks.AddRow "Cause Extinction"
		  case CatagoryEnum.Custom
		    blocks.AddRow "Set Variable"
		    blocks.AddRow "Note"
		    blocks.AddRow "Picture Note"
		    blocks.AddRow "Execute Script"
		  end select
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetBlockColour(Catagory as CatagoryEnum) As Color
		  select case catagory
		  case CatagoryEnum.Logic
		    return &c2F0059
		  case CatagoryEnum.Numbers
		    return &c5CB712
		  case CatagoryEnum.Text
		    return &c008000
		  case CatagoryEnum.Physics
		    return &c4A6CD4
		  case CatagoryEnum.Graphics
		    return &c8A55D7
		  case CatagoryEnum.UI
		    return &c3F96F2
		  case CatagoryEnum.Sound
		    return &cBB427B
		  case CatagoryEnum.Input
		    return &c2CA5E2
		  case CatagoryEnum.Test
		    return &c111111
		  case CatagoryEnum.Misc
		    return &cFFA91A
		  case CatagoryEnum.Custom
		    return &c000000
		  end select
		End Function
	#tag EndMethod


	#tag Enum, Name = CatagoryEnum, Type = Uint32, Flags = &h0
		Logic
		  Numbers
		  Text
		  Physics
		  Graphics
		  UI
		  Sound
		  Input
		  Misc
		  Test
		Custom
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
