#tag Module
Protected Module ResourceContainerMod
	#tag Method, Flags = &h0
		Function TestTabChange(res as Resource, LastOpenResourceTabValue as UInt32) As boolean
		  #pragma unused res
		  //could do with optimization
		  return (MainEditor.OpenResourceTabs.Value <> LastOpenResourceTabValue) 'and res.name = MainEditor.OpenResourceTabs.caption(MainEditor.OpenResourceTabs.Value))
		End Function
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
