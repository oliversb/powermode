#tag Module
Protected Module PWindow
	#tag Method, Flags = &h0
		Function DrawCheckerboard(w as UInt32, h as UInt32) As Picture
		  #pragma BoundsChecking false
		  #pragma StackOverflowChecking false
		  #pragma NilObjectChecking false
		  '#pragma DisableAutoWaitCursor
		  
		  '#pragma BreakOnExceptions false
		  try
		    //create transparency checkerboard
		    const tileSize = 256
		    static tile as Picture
		    
		    const squareSize = 8
		    dim toggle as Boolean = false
		    tile = new Picture(tileSize,tileSize,32)
		    dim tg as graphics = tile.Graphics
		    for i as integer = 0 to tileSize -1 step squareSize
		      #pragma BackgroundTasks false
		      for j as integer = 0 to tileSize - 1 step squareSize
		        if toggle then
		          tg.ForeColor = &cAAAAAA
		        else
		          tg.ForeColor = &cFFFFFF
		        end
		        tg.FillRect j,i,8,8
		        toggle = not toggle
		      next
		      toggle = not toggle
		      #pragma BackgroundTasks true
		    next
		    
		    dim p as new picture(w,h,32)
		    //draw checkerboard
		    for i as integer = 0 to p.height -1 step tileSize
		      #pragma BackgroundTasks false
		      for j as integer = 0 to p.width - 1 step tileSize
		        p.graphics.DrawPicture tile,j,i
		      next
		      #pragma BackgroundTasks true
		    next
		    
		    return p
		  catch OutOfBoundsException
		  end try
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function TurnedInvisible() As boolean
		  //if just turned invisible then reset value and return true
		  if mTurnedInvisible then
		    mTurnedInvisible = false
		    return true
		  else
		    return false
		  end if
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		CanvasHeight As UInt32
	#tag EndProperty

	#tag Property, Flags = &h0
		CanvasWidth As UInt32
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTurnedInvisible As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mVisible As Boolean
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mVisible
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mVisible = value
			  if value = false then mTurnedInvisible = true
			End Set
		#tag EndSetter
		Visible As Boolean
	#tag EndComputedProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
