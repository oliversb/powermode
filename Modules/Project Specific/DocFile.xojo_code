#tag Module
Protected Module DocFile
	#tag Method, Flags = &h0
		Function AppFolder() As FolderItem
		  #if DebugBuild then
		    //don't want to waist time copying files in build step
		    return SpecialFolder.UserHome.Child("Dropbox\PowerMode\")
		  #else
		    return App.ExecutableFile.Parent
		  #endif
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Export(name as string, FileTextData as string, type as FileType, dialog as Boolean = true)
		  dim f as FolderItem
		  if dialog then
		    f = GetSaveFolderItem(type, name)
		    DocFile.ProjectFileItem = f
		  else
		    f = DocFile.ProjectFileItem
		  end if
		  If f <> nil then
		    if f.Name.Right(type.Extensions.Len+1) <> "."+type.Extensions then
		      f.Name = f.Name + "." + type.Extensions
		    end if
		    //set project name
		    DocFile.SetProjectName
		    //delete the folder if it exsists
		    Try
		      Dim t as TextOutputStream = TextOutputStream.Create(f)
		      Try
		        'MsgBox FileTextData //debug saving system's output
		        t.Write(FileTextData)
		        DocFile.ModifiedDoc = false
		      Finally
		        t.Close
		      End Try
		    Catch e as IOException
		      //handle error
		    End Try
		  End if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetProjectName(name as string) As string
		  return name.Left(name.Len - 3)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetResource(type as integer, listIndex as integer) As Resource
		  //return resource when type and list index match is found
		  for each res as Resource in DocFile.Resources
		    if res.typeTab = type and res.ListIndex = listIndex then
		      return res
		    end if
		  next
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetResource(name as string) As Resource
		  //return resource when name match is found
		  for each res as Resource in DocFile.Resources
		    if res.name = name then
		      return res
		    end if
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub NewProject(f as folderitem = nil, addDef as boolean = true)
		  //if we don't choose to cancel or if the document has not  then do the magic
		  if ( (f <> nil) or (DocFile.ModifiedDoc = false) or (MainEditor.saveChangesQuestion <> "Cancel") ) then
		    //reset UniqueIDCount
		    UniqueIDCount = 0
		    
		    //delete all resources for fresh project
		    dim res as resource
		    for i as uint32 = resources.ubound downto 0
		      res = resources(i)
		      res.Delete(false, true)
		    next
		    for i as uint32 = 1 to 4
		      MainEditor.resourceTree(i).DeleteAllRows
		    next
		    
		    if addDef then
		      MainEditor.AddDefaultParentObject
		    end if
		    
		    //reset properties
		    if f <> nil then
		      //set project name and title
		      SetProjectName
		      
		      DocFile.ProjectFolder = f.Parent.Child(DocFile.ProjectName + " files")
		      DocFile.ProjectFolder.CreateAsFolder
		    else
		      DocFile.ProjectName = ""
		      DocFile.ProjectFolder = nil
		      MainEditor.Title = "PowerMode"
		    end if
		    
		    //remove all tabs
		    for i as integer = 0 to MainEditor.OpenResourceTabs.tabCount
		      MainEditor.OpenResourceTabs.removeTab(i, true)
		    next
		    MainEditor.OpenInitialTabs
		    
		    DocFile.ModifiedDoc = false
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub OpenProject(f as folderitem)
		  OpenProject(nil, f)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1000
		Sub OpenProject(closeWindow as window = nil, f as folderitem = nil)
		  //reset UniqueIDCount
		  DocFile.UniqueIDCount = 0
		  
		  DocFile.NewProject(nil, false)
		  
		  if f = nil then
		    Dim dlg as OpenDialog
		    dlg = New OpenDialog
		    #If Not (TargetLinux) then
		      dlg.InitialDirectory = SpecialFolder.Documents
		    #Else //open Home directory on linux
		      dlg.InitialDirectory = SpecialFolder.Home
		    #endif
		    dlg.Title = "Select a PowerMode file"
		    dlg.InitialDirectory = DocFile.ProjectsFolder
		    dlg.Filter = ProjectFile.PowerMode
		    f = dlg.ShowModal()
		  end if
		  
		  If f <> nil then
		    if closeWindow <> nil then
		      closeWindow.Close
		      closeWindow = nil
		    end if
		    
		    If f <> Nil Then
		      If f.Exists Then
		        // Be aware that TextInputStream.Open coud raise an exception
		        Dim t As TextInputStream
		        Try
		          t = TextInputStream.Open(f)
		          //convert text to xml
		          dim x as new XmlDocument(t.ReadAll)
		          for s as uint32 = 0 to x.Child(0).ChildCount - 1
		            'try
		            MainEditor.Import x, x.child(0).child(s)
		            dim res as Resource = DocFile.Resources(s)
		            'catch XmlException
		            'exit //exit loop if error with xml parsing
		            'end try
		          next
		          //set project file
		          DocFile.ProjectFileItem = f
		          //set the project name for the file name
		          DocFile.SetProjectName
		          //set project folder
		          DocFile.ProjectFolder = f.Parent.Child(DocFile.ProjectName + " files")
		          //setup the folder for extra files
		          dim files as FolderItem = DocFile.ProjectFolder
		          for each res as resource in DocFile.resources
		            //perform specific tasks after data is imported
		            select case res
		            case isa ResourceObject
		              dim mRes as ResourceObject = ResourceObject(res)
		              
		              if mRes.IndexParent <> -1 then
		                mRes.Parent = ResourceObject(Resources(mRes.IndexParent))
		              end if
		              if mRes.IndexSprite <> -1 then
		                mRes.Sprite = ResourceSprite(Resources(mRes.IndexSprite))
		              end if
		              
		              //load variable references
		              for each block1 as ContentFrame in mRes.Blocks //loop through all of the blocks (in this object)
		                dim bi as BlockBase = block1.tag
		                
		                bi.VariablePath1 = new ObjectVariablePath(mRes, bi.backupVariablePath1)
		                'ResourceObject( DocFile.Resources(bi.backupVariablePath1Parent) ).Variables(bi.backupVariablePath1)
		                
		                if bi.backupVariablePathArray.Ubound <> -1 then
		                  '//loop through each backed up index
		                  'for each index as integer in bi.backupVariablePathArray
		                  'dim indexOfIndex as uint32 = bi.backupVariablePathArray.IndexOf(index)
		                  '
		                  'if index <> -1 then
		                  'bi.VariablePathArray.append new ObjectVariablePath(bi.backupVariablePathArrayParent(indexOfIndex), bi.backupVariablePathArray(indexOfIndex))
		                  'else
		                  'bi.VariablePathArray.append nil
		                  'end if
		                  'next
		                  
		                  for i as integer = 0 to bi.backupVariablePathArray.Ubound
		                    bi.VariablePathArray.Append(new ObjectVariablePath(mRes, bi.backupVariablePathArray(i)))
		                  next
		                else
		                  bi.VariablePath2 = new ObjectVariablePath(mRes, bi.backupVariablePath2)
		                  bi.VariablePath3 = new ObjectVariablePath(mRes, bi.backupVariablePath3)
		                end if
		              next
		              
		              for each v as ObjectVariable in mRes.Variables //set value of each variable from backup value if it is a resource
		                if v.backupValue <> -1 then
		                  v.Value.ResourceValue = resources(v.backupValue)
		                end if
		              next
		              
		              if files <> nil then
		                dim f1 as FolderItem = files.Child(res.name)
		                if f1 <> nil then
		                  for i as uint32 = 1 to f1.Count
		                    dim curItem as folderitem = f1.Item(i)
		                    dim obj as contentframe = ResourceObject(res).Blocks(val(f1.Item(i).Name.Left(curItem.Name.Len - 4)))
		                    obj.OriginalPicture = Picture.Open(curItem)
		                    RelationalFramesCanvas.SetSize(obj)
		                  next
		                end if
		              end if
		            case isa ResourceSprite
		              if files <> nil then
		                dim f1 as FolderItem = files.Child(res.name)
		                if f1 <> nil then
		                  for i as uint32 = 1 to f1.Count
		                    ResourceSprite(res).Frames.Append new PhotoThumbnail(Picture.Open(f1.Item(i)),"Frame "+str(i-1))
		                  next
		                end if
		              end if
		            case isa ResourceSound
		              if files <> nil then
		                ResourceSound(res).SoundFile = files.Child(res.name).Item(1)
		                if ResourceSound(res).SoundFile <> nil then
		                  ResourceSound(res).Sound = ResourceSound(res).SoundFile.OpenAsSound
		                end if
		              end if
		            case isa ResourceScene
		              //assign background
		              if ResourceScene(res).BackgroundIndex <> -1 then
		                ResourceScene(res).Background = ResourceSprite(DocFile.resources(ResourceScene(res).BackgroundIndex))
		              end if
		              //assign indexes of all canvasobjects
		              for each co as CanvasObject in ResourceScene(res).Scene
		                co.theObject = ResourceObject(docfile.resources((co.backupObjectIndex)))
		              next
		            end select
		          next
		          
		          //no modifications have been made as we have started fresh, opening a project
		          DocFile.ModifiedDoc = false
		        Catch e As IOException
		          t.Close
		          MsgBox("Error accessing file.")
		        Catch XmlException
		          t.Close
		          MsgBox("Expected compatible XML file.")
		        End Try
		      End If
		    End If
		  End if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ReportError(ErrorType as integer, ErrorCode as int32 = - 1)
		  Dim d as New MessageDialog //declare the MessageDialog object
		  d.icon=MessageDialog.GraphicCaution //display warning icon
		  select case ErrorType
		  case 0 //drop picture from web error
		    d.Title = "Error with object drop"
		    d.Explanation = ObjectEditor.HTTPErrorMessage(ErrorCode,true)+"Please ensure that you are dropping a link/URL that directs to a picture."
		  case 1 //error with deleting a file
		    d.Explanation = "There was a problem with deleting a folder while trying to save your project. The error code given is "+str(ErrorCode)+"."
		  case 2 //not writeable
		    d.Explanation = "Close any programs that may be using this folder and then try again."
		  case 3
		    d.Explanation = "Please save your project before attempting to make a build."
		  end select
		  //some items may have the same title and this sets the title accordingly
		  select case ErrorType
		  case 1, 2
		    d.Title = "Error with folder deletion"
		  case 3
		    d.Title = "Error building project"
		  end select
		  
		  call d.ShowModal //display the dialog
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub SaveProject(dialog as Boolean, FileTextData as FileType)
		  for each i as integer in MainEditor.SelectedResource
		    for each res as resource in resources
		      if res.ListIndex = i and res.typeTab = MainEditor.resourceTreeTabs.value then
		        res.makeName(MainEditor.SelectedResourceTree.Cell(i, 0))
		        exit for
		      end if
		    next
		  next
		  
		  dim XmlProject as new XmlDocument
		  dim Resources as XmlNode = XmlProject.AppendChild(XmlProject.CreateElement("resources"))
		  for each res as resource in DocFile.Resources
		    res.WriteToXML(XmlProject,Resources)
		  next
		  //convert xml to string to save in custom format instead of xml format
		  XmlProjectText = XmlProject.ToString
		  
		  if dialog = false then //only consider 'save' as oppose to 'save as'.
		    if DocFile.ProjectFolder = nil then dialog = true
		  end if
		  
		  Export(DocFile.ProjectFileItem.Name, XmlProjectText, FileTextData, dialog)
		  
		  //delete to repopulate folder
		  SetProjectName
		  ProjectFolder = ProjectFileItem.Parent.Child(DocFile.ProjectName +" files")
		  
		  try
		    dim FolderDeleteErrorCode as int32 = DocFile.ProjectFolder.DeleteEntireFolder
		    if FolderDeleteErrorCode = 0 then
		      //create folder to populate
		      ProjectFolder.CreateAsFolder
		      
		      dim picsFolder as FolderItem
		      for each res as resource in DocFile.resources
		        select case res
		        case isa ResourceObject, isa ResourceSprite, isa ResourceSound
		          picsFolder = ProjectFolder.Child(res.name)
		        end select
		        ExportAllImages(picsFolder, res)
		      next
		    else
		      //failed to delete folder
		      //report to user, the error code given
		      DocFile.ReportError(1,FolderDeleteErrorCode)
		      //set back to true because it to delete the project folder
		      DocFile.ModifiedDoc = true
		    end if
		  catch NilObjectException
		    ReportError(2)
		    //if saving fails then we want to give the user the ability to resave the project
		    DocFile.ModifiedDoc = true
		  end try
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetProjectName()
		  DocFile.ProjectName = GetProjectName(DocFile.ProjectFileItem.Name)
		  //set caption for editor
		  MainEditor.Title =  DocFile.ProjectFileItem.Name + " - PowerMode"
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		ChangedCurrentPic As Boolean
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mCurrentPaintImage
			  ChangedCurrentPic = false
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mCurrentPaintImage = value
			  ChangedCurrentPic = true
			End Set
		#tag EndSetter
		CurrentPaintImage As Picture
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		LastAnimationPreviewSpeed As Uint32 = 1
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mCurrentPaintImage As Picture
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mModifiedDoc As Boolean = false
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mModifiedDoc
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mModifiedDoc = value
			  MainEditor.tbSave.Enabled = value
			  
			  if not value then
			    for each SResource as integer in MainEditor.SelectedResource
			      MainEditor.SelectedResourceTree.Selected(SResource) = false
			    next
			  end if
			End Set
		#tag EndSetter
		ModifiedDoc As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mSplashScreen As Picture
	#tag EndProperty

	#tag Property, Flags = &h0
		ProjectFileItem As FolderItem
	#tag EndProperty

	#tag Property, Flags = &h0
		ProjectFolder As FolderItem
	#tag EndProperty

	#tag Property, Flags = &h0
		ProjectName As String
	#tag EndProperty

	#tag Property, Flags = &h0
		ProjectsFolder As FolderItem
	#tag EndProperty

	#tag Property, Flags = &h0
		resources() As resource
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  if mSplashScreen = nil then
			    //the PowerMode logo is the default splashscreen
			    return logo
			  end if
			  
			  return mSplashScreen
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mSplashScreen = value
			End Set
		#tag EndSetter
		SplashScreen As Picture
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		UniqueIDCount As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		VariableScope As uint32
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected XmlProjectText As string
	#tag EndProperty


	#tag Enum, Name = ResourceType, Flags = &h0
		object
		  sprite
		  scene
		sound
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="ChangedCurrentPic"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CurrentPaintImage"
			Group="Behavior"
			Type="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ModifiedDoc"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ProjectName"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="SplashScreen"
			Group="Behavior"
			Type="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="UniqueIDCount"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
