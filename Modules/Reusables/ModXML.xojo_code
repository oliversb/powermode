#tag Module
Protected Module ModXML
	#tag Method, Flags = &h0
		Sub AppendChildCopy(extends parent as XmlNode, other as XmlNode)
		  // parent document for creating new objects
		  dim doc as XmlDocument = parent.OwnerDocument
		  dim neu as XmlNode
		  
		  // create matching type of node in new document
		  if other isa XmlTextNode then
		    neu = doc.CreateTextNode(other.Value)
		  elseif other isa XmlComment then
		    neu = doc.CreateComment(other.Value)
		  else
		    neu = doc.CreateElement(other.Name)
		  end if
		  
		  // now copy all attributes
		  dim u as integer = other.AttributeCount-1
		  for i as integer = 0 to u
		    dim a as XmlAttribute = other.GetAttributeNode(i)
		    neu.SetAttribute(a.Name, a.Value)
		  next
		  
		  // copy all children
		  dim c as XmlNode = other.FirstChild
		  while c <> nil
		    neu.AppendChildCopy c
		    
		    c = c.NextSibling
		  wend
		  
		  // and store new child
		  parent.AppendChild neu
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetNode(extends node as XmlNode, name as string) As XmlNode
		  //gets single node by name
		  return node.Xql("//" + name).Item(0)
		End Function
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
