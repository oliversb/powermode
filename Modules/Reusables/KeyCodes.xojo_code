#tag Module
Protected Module KeyCodes
	#tag Constant, Name = Down, Type = Double, Dynamic = False, Default = \"&h7d", Scope = Public
	#tag EndConstant

	#tag Constant, Name = Enter, Type = Double, Dynamic = False, Default = \"&h4c", Scope = Public
	#tag EndConstant

	#tag Constant, Name = Space, Type = Double, Dynamic = False, Default = \"&h31", Scope = Public
	#tag EndConstant

	#tag Constant, Name = TimesSymbol, Type = Double, Dynamic = False, Default = \"&h43", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
