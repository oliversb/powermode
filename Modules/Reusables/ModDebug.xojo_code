#tag Module
Protected Module ModDebug
	#tag Method, Flags = &h0
		Sub Debug(ParamArray doubles as double)
		  //method use to show the values passed in for debugging
		  dim strings() as string
		  
		  for each i as double in doubles
		    strings.append str(i)
		  next
		  
		  System.DebugLog(Join(strings, " / "))
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Nothing()
		  //use for breakpoints
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		DebugCanvas As Canvas
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
