#tag Module
Protected Module ArrayMod
	#tag Method, Flags = &h21
		Private Function Clone(extends from() as object) As object()
		  dim arr() as object
		  
		  for each item as object in from
		    arr.append item
		  next
		  
		  return arr
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MoveRange(arr() As Object, moveAmount As integer, lowest as uint32, highest as integer = -1)
		  //Thanks goes to Kem Tekinay and Will Shank for helping write this method.
		  //Use this method to rearrange items within an array
		  
		  //if highest not specified then set highest to the same value as lowest
		  if highest = -1 then highest = lowest
		  
		  //index of first selected item, init -1 to signify it's unset
		  dim firstSelIdx As integer = -1
		  
		  //temporary array for selected items
		  dim temp() As Object
		  
		  for i as integer = lowest to highest
		    temp.Append arr(i)  //store it in temp
		    if firstSelIdx = -1 then firstSelIdx = i  //record if this is the first one
		  next
		  
		  for i As integer = highest downto lowest //loop over again (going downwards because removing)
		    arr.Remove(i)  //remove selected items (still stored in temp)
		  next
		  
		  //now arr is devoid of selected items, temp has them and firstSelIdx is set
		  
		  if firstSelIdx = -1 then return //no items to move, skip out
		  
		  //calculate position to start reinserting items
		  dim insertPos As integer = firstSelIdx + moveAmount
		  
		  //fix insertPos to valid index
		  if insertPos < 0 then insertPos = 0
		  if insertPos > arr.Ubound + 1 then insertPos = arr.Ubound + 1
		  
		  for i As integer = 0 to temp.Ubound //now insert all together
		    arr.insert(insertPos + i, temp(i))
		  next
		End Sub
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
