#tag Module
Protected Module ModGraphics
	#tag Method, Flags = &h0
		Function ClonePicture(extends p as picture, hasAlpha as boolean = true) As Picture
		  if hasAlpha then
		    dim newPic as new picture(p.width, p.height)
		    newPic.Graphics.DrawPicture(p, 0, 0)
		    return newPic
		  else
		    dim newPic as new picture(p.width, p.height, 32)
		    newPic.Graphics.DrawPicture(p, 0, 0)
		    return newPic
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DrawObject(extends g as graphics,object2d as Object2D)
		  //this method allows you to draw an object without specifying the X and Y coordinates
		  g.DrawObject(object2d, 0, 0)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DrawPicture(extends g as graphics, image as picture)
		  //this method allows you to draw a picture without specifying the X and Y coordinates
		  g.DrawPicture(image, 0, 0)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function DrawPicture_return(image as PixmapShape, x as integer = 0, y as integer = 0, w1 as integer = -10000, h1 as integer = -10000, sx as integer = 0, sy as integer = 0, w2 as integer = -10000, h2 as integer = -10000) As PixmapShape
		  dim p as new picture(w1, h1)
		  p.Graphics.DrawPicture(image.Image, x, y, w1, h1, sx, sy, w2, h2)
		  dim pxshape as PixmapShape = new PixmapShape(p)
		  pxshape.Rotation = image.Rotation
		  pxshape.x = w1 / 2
		  pxshape.y = h1 / 2
		  return pxshape
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Fill(extends g as graphics)
		  g.FillRect 0, 0, g.width, g.height
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Mirror(p as picture, Vertical as boolean) As picture
		  dim mirrorPic as new picture( p.Width, p.Height, p.Depth )
		  dim x, y as integer
		  dim w as integer = p.width-1
		  dim h as integer = p.height-1
		  
		  if not vertical then
		    for y = 0 to h
		      for x = w downto 0
		        mirrorPic.Graphics.Pixel(x, y) = p.Graphics.Pixel(w-x, y)
		      next
		    next
		  else
		    for x = 0 to w
		      for y = h downto 0
		        mirrorPic.Graphics.Pixel(x, y) = p.Graphics.Pixel(x, h-y)
		      next
		    next
		  end if
		  
		  return mirrorPic
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function MirrorH(extends p as picture) As picture
		  return Mirror(p, false)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function MirrorV(extends p as picture) As picture
		  return Mirror(p, true)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ScalePicture(p as Picture, AvailableWidth as Integer, AvailableHeight as Integer, ApplyAlpha as boolean = false) As Picture
		  // This method takes a picture and returns a new picture that is scaled to fit within the passed boundaries. The aspect ratio is preserved.
		  // The original image is not altered.
		  
		  dim returnPic as picture
		  dim w, h as integer
		  dim ratio as double
		  
		  // Check for nil object
		  if p = nil then return nil
		  
		  // Work out the picture aspect ratio
		  ratio = p.Width / p.Height
		  
		  // Scale depending on aspect ratio
		  select case ratio
		    
		  case 1 ' square image to scale equally
		    
		    if availableWidth = availableHeight then
		      w = availableWidth
		      h = availableHeight
		    elseif availableWidth < availableHeight then
		      w = availableWidth
		      h = availableWidth
		    elseif availableWidth > availableHeight then
		      w = availableHeight
		      h = availableHeight
		    end if
		    
		  case is < 1 ' portrait (taller than wider)
		    
		    if availableWidth <= availableHeight then
		      
		      h = availableWidth
		      
		    else
		      
		      h = availableHeight
		      
		    end if
		    
		    w = h * ratio
		    
		  case is > 1 ' landscape (wider than taller)
		    
		    if availableWidth <= availableHeight then
		      
		      w = availableWidth
		      
		    else
		      
		      w = availableHeight
		      
		    end if
		    
		    h = w / ratio
		    
		  end select
		  
		  if ApplyAlpha then
		    returnPic = new Picture(w, h)
		  else
		    returnPic = new Picture(w, h, 32)
		  end if
		  returnPic.Graphics.DrawPicture( p, 0, 0, returnPic.Width, returnPic.Height, 0, 0, p.Width, p.Height )
		  
		  return returnPic
		  
		End Function
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
