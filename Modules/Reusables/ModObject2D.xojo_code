#tag Module
Protected Module ModObject2D
	#tag Method, Flags = &h0
		Function Clone(extends o as Object2D) As Object2D
		   dim clone as Object2D
		  
		  //clone original object
		  select case o
		  case isa PixmapShape
		    clone = new PixmapShape(PixmapShape(o).Image)
		    clone.x = o.x
		    clone.y = o.y
		    clone.scale = o.scale
		    clone.Rotation = o.Rotation
		  case isa Group2D
		    dim g as Group2D = Group2D(o)
		    clone = new Group2D
		    clone.x = o.x
		    clone.y = o.y
		    clone.scale = o.scale
		    clone.Rotation = o.Rotation
		    dim gclone as Group2D = Group2D(clone)
		    for i as integer = 0 to g.Count - 1
		      gclone.Append(g.Item(i).Clone)
		    next
		  case else
		    raise new UnsupportedFormatException
		  end select
		  
		  return clone
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetLine(extends shape as RectShape, id as integer) As Segment
		  '//get line from rectangle shape
		  'select case id
		  'case 0
		  '//top line
		  'return new Segment(shape.GetPoint(0), shape.GetPoint(1))
		  'case 1
		  '//right line
		  'return new Segment(shape.GetPoint(1), shape.GetPoint(2))
		  'case 2
		  '//bottom line
		  'return new Segment(shape.GetPoint(2), shape.GetPoint(3))
		  'case 3
		  '//left line
		  'return new Segment(shape.GetPoint(3), shape.GetPoint(0))
		  'end select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Point(extends item as Object2D) As Point
		  dim p as new point
		  
		  p.X = item.X
		  p.Y = item.Y
		  
		  return p
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Point(extends item1 as Object2D, assigns thePoint as Point)
		  item1.X = thePoint.X
		  item1.Y = thePoint.Y
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetImage(extends item1 as PixmapShape, image as picture)
		  item1.Constructor(image)
		  item1.X = image.width / 2
		  item1.Y = image.height / 2
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ToRectShape(obj as Object2D, width as integer, height as integer) As RectShape
		  dim item1 as new RectShape
		  
		  item1.x = obj.x + (width / 2)
		  item1.y = obj.y + (height / 2)
		  item1.width = width
		  item1.height = height
		  
		  return item1
		End Function
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
