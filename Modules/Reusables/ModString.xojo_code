#tag Module
Protected Module ModString
	#tag Method, Flags = &h0
		Function Contains(extends source as string, find as string) As boolean
		  return (InStr(source, find) <> 0)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function DeleteIllegalCharacters(extends text as string) As string
		  return text.ReplaceWithNothing("-", "/", "\", ".", "*", "+", "%", "!", " ", "#", "@", "<", ">")
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function EndOfLine(count as integer) As string
		  dim s as string
		  
		  for i as integer = 0 to count - 1
		    s = s + EndOfLine
		  next
		  
		  return s
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ReplaceWithNothing(extends ToReplace as string, ParamArray ReplaceWith as string) As string
		  for each s as string in ReplaceWith
		    ToReplace = ToReplace.ReplaceAll(s, "")
		  next
		  
		  return ToReplace
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ToLowercaseString(b as boolean) As string
		  return Lowercase(str(b))
		End Function
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
