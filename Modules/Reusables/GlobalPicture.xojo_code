#tag Module
Protected Module GlobalPicture
	#tag Method, Flags = &h0
		Function MeasureString(string as string, font as string, size as single, dimension as dimensions) As Double
		  static p as picture = new picture(1, 1, 32)
		  p.graphics.TextFont = font
		  p.graphics.TextUnit = FontUnits.Point
		  p.graphics.TextSize = size
		  
		  select case dimension
		  case Dimensions.Width
		    return p.graphics.StringWidth(string)
		  case Dimensions.Height
		    return p.graphics.StringHeight(string, p.graphics.StringWidth(string))
		  end select
		End Function
	#tag EndMethod


	#tag Enum, Name = Dimensions, Type = Integer, Flags = &h0
		Width
		Height
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
