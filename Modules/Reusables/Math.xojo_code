#tag Module
Protected Module Math
	#tag Method, Flags = &h0
		Sub Add(extends ByRef i as Integer)
		  i = i + 1
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Add(extends ByRef i as Integer, value as integer)
		  i = i + value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function bool(s as string) As boolean
		  return (s = "true")
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetChar(extends mb as MemoryBlock, index as integer) As integer
		  mb = mb.LeftB(index) + mb.RightB(index + 1)
		  return mb.Byte(index) + 1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Increment(extends byref variable as double, increase as double = 1)
		  variable = variable + increase
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Increment(extends variable as integer, increase as double = 1)
		  variable = variable + increase
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Increment(extends byref variable as uint32, increase as double = 1)
		  variable = variable + increase
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function LengthDir(thePoint as Point, dir as double, dist as double) As Point
		  dim pointToReturn as new Point
		  
		  dim x as double = thePoint.X
		  dim y as double = thePoint.Y
		  
		  pointToReturn.X =  x + cos(dir * 3.14 / 180) * dist
		  pointToReturn.Y = y + sin(dir * 3.14 / 180) * dist
		  
		  return pointToReturn
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Max(values() as double) As double
		  dim currentMax as double
		  
		  for each v as double in values
		    if v > currentMax then
		      currentMax = v
		    end if
		  next
		  
		  return currentMax
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function MaxInt(values() as integer) As integer
		  dim castedValues() as double
		  for each v as integer in values
		    castedValues.Append(v)
		  next
		  
		  return Math.Max(castedValues)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function PointDirection(point1 as Point, point2 as Point) As double
		  dim dx, dy, x1, y1, x2, y2 as double
		  x1 = point1.x
		  y1 = point1.y
		  x2 = point2.x
		  y2 = point2.y
		  
		  dy = y2 - y1
		  dx = x2 - x1
		  
		  return ATan(dy / dx) //Radians
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function PointDistance(point1 as Point, point2 as Point) As double
		  dim x1, y1, x2, y2 as double
		  
		  x1 = point1.x
		  y1 = point1.y
		  x2 = point2.x
		  y2 = point2.y
		  
		  return sqrt( (x2 - x1) ^ 2 + (y2 - y1) ^ 2 )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Rad(value as double) As double
		  dim a as new angle(value, true)
		  return a.Radians
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function toColour(Colour as string, pureHex as boolean = false) As Color
		  dim v as variant
		  if pureHex then
		    v = "&c" + Colour
		  else
		    v = Colour
		  end if
		  return v.ColorValue
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function toPureHexString(Colour as Color) As String
		  //convert to string and take last 6 characters
		  return str(Colour).Right(6)
		End Function
	#tag EndMethod


	#tag Constant, Name = PI, Type = Double, Dynamic = False, Default = \"3.14159265358979323846264338327950", Scope = Public
	#tag EndConstant


	#tag Enum, Name = RotateFlipType, Type = Integer, Flags = &h0
		RotateNoneFlipY = 6
	#tag EndEnum

	#tag Enum, Name = Status, Type = Integer, Flags = &h0
		Ok
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
