#tag Class
Protected Class ContainerControlValue
	#tag Method, Flags = &h0
		Sub SetValue(type as string)
		  me.Type = type
		  
		  mContainerControlValue = nil
		  mRectControlValue = nil
		  mWindowValue = nil
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Value() As Object
		  select case Type
		  case "ContainerControl"
		    return ContainerControlValue
		  case "RectControl"
		    return RectControlValue
		  case "Window"
		    return WindowValue
		  case else
		    return nil
		  end select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Window() As Window
		  select case Type
		  case "ContainerControl"
		    return ContainerControlValue.Window
		  case "RectControl"
		    return RectControlValue.Window
		  case "Window"
		    return ContainerControl(WindowValue).Window
		  end select
		End Function
	#tag EndMethod


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mContainerControlValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  SetValue("ContainerControl")
			  mContainerControlValue = value
			End Set
		#tag EndSetter
		ContainerControlValue As ContainerControl
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mContainerControlValue As ContainerControl
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mRectControlValue As RectControl
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mWindowValue As Window
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mRectControlValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  SetValue("RectControl")
			  mRectControlValue = value
			End Set
		#tag EndSetter
		RectControlValue As RectControl
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		Type As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mWindowValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  SetValue("Window")
			  mWindowValue = value
			End Set
		#tag EndSetter
		WindowValue As Window
	#tag EndComputedProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Type"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="WindowValue"
			Group="Behavior"
			Type="Window"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
