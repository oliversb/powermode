#tag Class
Protected Class Segment
	#tag Method, Flags = &h0
		Sub Constructor()
		  p1 = new Point
		  p2 = new Point
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(x1 As integer, y1 As integer, x2 As integer, y2 As integer)
		  p1 = new Point(x1, y1)
		  p2 = new Point(x2, y2)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(point1 as point, point2 as point)
		  p1 = point1
		  p2 = point2
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub draw(g As Graphics, lineWidth As integer, lineColor As Color, dotRadius As double, dotColor As Color)
		  g.ForeColor = dotColor
		  p1.draw(g, dotRadius)
		  p2.draw(g, dotRadius)
		  
		  g.ForeColor = lineColor
		  g.PenWidth = lineWidth
		  g.PenHeight = lineWidth
		  g.DrawLine(p1.x, p1.y, p2.x, p2.y)
		  
		  p1.draw(g, lineWidth * 2)
		  p2.draw(g, lineWidth * 2)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function hitPoint(hitX As integer, hitY As integer, hitRadius As double) As Point
		  
		  if p1.hit(hitX, hitY, hitRadius) then return p1
		  
		  if p2.hit(hitX, hitY, hitRadius) then return p2
		  
		  return nil
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		 Shared Function intersects(seg1 As Segment, seg2 As Segment, intersectionPoint As Point) As boolean
		  
		  dim dx, dy, da, db, B1, Y1, X1, A1 As integer, t, s As double
		  
		  X1 = seg1.p1.x //load common components
		  Y1 = seg1.p1.y
		  A1 = seg2.p1.x
		  B1 = seg2.p1.y
		  
		  dx = seg1.p2.x - X1 //calc differences and check parallel
		  dy = seg1.p2.y - Y1
		  da = seg2.p2.x - A1
		  db = seg2.p2.y - B1
		  If (da * dy - db * dx) = 0 Then return false
		  
		  s = (dx * (B1 - Y1) + dy * (X1 - A1)) / (da * dy - db * dx)  //compute intercept parameters
		  t = (da * (Y1 - B1) + db * (A1 - X1)) / (db * dx - da * dy)
		  
		  if (s >= 0.0 And s <= 1.0 And t >= 0.0 And t <= 1.0) then  //if both hit in [0,1] they intersect
		    
		    if intersectionPoint <> nil then intersectionPoint.set( X1+t*dx, Y1+t*dy ) //compute result point
		    
		    return true
		    
		  else
		    return false
		  end
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		p1 As Point
	#tag EndProperty

	#tag Property, Flags = &h0
		p2 As Point
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
