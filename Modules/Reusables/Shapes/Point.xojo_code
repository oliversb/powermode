#tag Class
Protected Class Point
	#tag Method, Flags = &h0
		Sub Constructor()
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(x As double, y As double)
		  me.X = x
		  me.Y = y
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(p As Point)
		  x = p.x
		  y = p.y
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Draw(g As Graphics, dotRadius As double)
		  dim r2 As double = dotRadius * 2 + 1
		  g.FillOval(x-dotRadius, y-dotRadius, r2, r2)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Hit(hitX As double, hitY As double, hitRadius As double) As boolean
		  return hitRadius > Sqrt( (x-hitX)^2 + (y-hitY)^2 )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Move(dx As double, dy As double)
		  x = x + dx
		  y = y + dy
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Move(dx As double, dy As double) As Point
		  //move point by relative position and return the point
		  dim p as new point(x, y)
		  p.Move(dx, dy)
		  return p
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Operator_Add(p as Point) As Point
		  dim returnP as new point
		  
		  returnP.x = me.x + p.x
		  returnP.y = me.y + p.y
		  
		  return returnP
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Operator_Multiply(p as Point) As Point
		  dim returnP as new point
		  
		  returnP.x = me.x * p.x
		  returnP.y = me.y * p.y
		  
		  return returnP
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Set(x As double, y As double)
		  self.x = x
		  self.y = y
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		X As double
	#tag EndProperty

	#tag Property, Flags = &h0
		Y As double
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="X"
			Group="Behavior"
			Type="double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Y"
			Group="Behavior"
			Type="double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
