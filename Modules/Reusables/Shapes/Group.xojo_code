#tag Class
Protected Class Group
Inherits VectorObject
	#tag Event
		Function Object2DImplementation(returnFromVO as VectorObject, byref obj as Object2D) As Object2D
		  dim returnObj as new Group2D
		  
		  for each item as VectorObject in items
		    returnObj.Append(item.Object2D_GetObject2DFromVO(returnFromVO, obj))
		    debugobjs.append(item.Object2D_GetObject2DFromVO(returnFromVO, obj))
		  next
		  
		  return returnObj
		End Function
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub Append(item as VectorObject)
		  Items.Append(item)
		  item.Parent = me
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		Items() As VectorObject
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Height"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Opacity"
			Group="Behavior"
			Type="double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
