#tag Module
Protected Module ModVectorObject
	#tag Method, Flags = &h0
		Sub DrawObject(extends g as graphics, obj as VectorObject)
		  g.DrawObject obj.Object2D
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ToVectorObject(obj as RectShape) As VectorObject
		  dim vo as new VectorObject
		  
		  vo.Left = obj.X + (obj.width / 2)
		  vo.Top = obj.Y + (obj.Height / 2)
		  vo.Width = obj.Width
		  vo.Height = obj.Height
		  
		  return vo
		End Function
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
