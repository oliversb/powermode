#tag Class
Protected Class VectorObject
	#tag Method, Flags = &h0
		Function AnchorAbsoluteX() As double
		  return AnchorPoint.X * me.Width
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AnchorAbsoluteY() As double
		  return AnchorPoint.Y * me.Height
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function BottomLeft(obj as VectorObject) As Point
		  return TopLeft(obj, obj.Width, 0)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function BottomRight(obj as VectorObject) As Point
		  return TopLeft(obj, obj.Width, obj.Height)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1000
		Sub Constructor()
		  me.Angle = new Angle
		  
		  me.Left = 0
		  me.Top = 0
		  
		  AnchorPoint = new Point(.5, .5)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Contains(x as double, y as double) As boolean
		  dim contains as boolean
		  if me.Visible then
		    dim obj as Object2D
		    select case me
		    case isa Pixmap
		      dim outerGroup as Group = GetOuterGroup
		      dim g2d as Group2D
		      if outerGroup <> nil then
		        //get hold of Object2D for me and store it in 'obj' variable
		        G2D = Group2D(outerGroup.Object2D_GetObject2DFromVO(me, obj))
		      else
		        obj = me.Object2D
		      end if
		      if outerGroup.angle.value > 0 and me isa MouseRect and PixmapShape(obj).Contains(x, y) then
		        //tell me the rotation of the outer Group2D
		        'debug g2d.Rotation
		        nothing
		      end if
		      if obj <> nil then
		        contains = PixmapShape(obj).Contains(x, y)
		      end if
		    case isa Group
		      dim getContains as MyVariant = GetContains(x, y)
		      if getContains <> nil then
		        contains = getContains.BooleanValue
		      else
		        //check through every group item
		        for each item as VectorObject in Group(me).Items
		          if item.Contains(x, y) then
		            contains = true
		            exit for
		          end if
		        next
		      end if
		    else
		      contains = RectShape(Object2D).Contains(x, y)
		    end select
		  end if
		  
		  return contains
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetOuterGroup() As Group
		  dim g as Group = me.Parent
		  
		  while g.Parent <> nil
		    g = g.Parent
		  wend
		  
		  return g
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Intersects(other as VectorObject) As Boolean
		  dim obj as VectorObject = me
		  dim obj2 as VectorObject = other
		  
		  if obj = obj2 then return false
		  
		  Dim p(), p2() As Point
		  
		  if obj.Contains(obj2.Left, obj2.Top) then return true
		  
		  if obj2.Contains(obj.Left, obj.Top) then return true
		  
		  p.Append TopLeft(obj)
		  p.Append TopRight(obj)
		  p.Append BottomRight(obj)
		  p.Append BottomLeft(obj)
		  
		  //check if obj2 contains any of the points in obj
		  for each point as Point in p
		    'point.draw(DebugCanvas.Graphics, 3)
		    if obj2.Contains(point.x, point.y) then
		      return true
		    end if
		  next
		  
		  p2.Append TopLeft(obj2)
		  p2.Append TopRight(obj2)
		  p2.Append BottomRight(obj2)
		  p2.Append BottomLeft(obj2)
		  
		  //check if obj contains any of the points in obj2
		  for each point as Point in p2
		    'point.draw(DebugCanvas.Graphics, 3)
		    if obj.Contains(point.x, point.y) then
		      return true
		    end if
		  next
		  
		  return false
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Object2D() As Object2D
		  //declare so it accepts the desired method
		  dim vo as VectorObject, obj as Object2D, g as Group2D
		  return me.Object2D_GetObject2DFromVO(vo, obj)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Object2DSetup(obj as Object2D)
		  //override default anchor point by adding half the position based on the anchor point, 1 being the full size of the image and 0 covering none of the image
		  obj.X = me.Left + (me.mWidth / 2)
		  obj.Y = me.Top + (me.mHeight / 2)
		  
		  obj.Fill = me.Opacity
		  RotateObject2D(obj, me.Angle.Radians, me.Left + AnchorAbsoluteX, me.Top + AnchorAbsoluteY)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Object2D_GetObject2DFromVO(returnFromVO as VectorObject, ByRef obj as Object2D) As Object2D
		  dim returnValue as Object2D
		  //only construct the Object2D if it is visible
		  if me.Visible then
		    REDIM debugobjs(-1)
		    returnValue = Object2DImplementation(returnFromVO, obj)
		    
		    //if the type is directly of VectorObject then use the rectangular default
		    if returnValue = nil then
		      dim rs as new RectShape
		      
		      rs.Width = me.Width
		      rs.Height = me.Height
		      
		      //return RectShape
		      returnValue = rs
		    end if
		    
		    Object2DSetup(returnValue)
		    if me isa group then
		      if debugobjs.ubound = 1 then
		        returnValue.x  = 0
		        returnValue.y = 0
		        
		        dim containsx, containsy as double
		        dim firstelement as PixmapShape = PixmapShape(debugobjs(0))
		        
		        RotateObject2D(firstelement, firstelement.x, firstelement.y, 0)
		        if PixmapShape(firstelement).Contains(containsx, containsx) then
		          nothing
		        end if
		        RotateObject2D(firstelement, firstelement.x, firstelement.y, -1.5708)
		        if PixmapShape(firstelement).Contains(containsx, containsx) then
		          nothing
		        end if
		      end if
		    end if
		  else
		    //return blank Object2D so if statement does not have to be used to check for a nil object each time
		    returnValue = new Object2D
		  end if
		  
		  //pass the Object2D which is getting tracked
		  if me = returnFromVO then
		    obj = returnValue
		  end if
		  
		  return returnValue
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RotateObject2D(obj As Object2D, absoluteOriginX as double, absoluteOriginY as double, rotateAngle As double)
		  'dim origin as point = new Point(me.Left + AnchorAbsoluteX, me.Top + AnchorAbsoluteY)
		  '
		  'dim sina, cosa, dx, dy As double
		  '
		  'sina = sin(rotateAngle)   //precompute
		  'cosa = cos(rotateAngle)
		  'dx = obj.X - origin.X
		  'dy = obj.Y - origin.Y
		  '
		  'obj.x = origin.x + dx * cosa - dy * sina
		  'obj.y = origin.y + dx * sina + dy * cosa
		  '
		  'obj.Rotation = rotateAngle
		  
		  dim origin as point = new Point(absoluteOriginX, absoluteOriginY)
		  
		  dim sina, cosa, dx, dy As double
		  
		  sina = sin(rotateAngle)
		  cosa = cos(rotateAngle)
		  dx = obj.X - absoluteOriginX
		  dy = obj.Y - absoluteOriginY
		  
		  obj.x = origin.x + dx * cosa - dy * sina
		  obj.y = origin.y + dx * sina + dy * cosa
		  
		  obj.Rotation = rotateAngle
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function TopLeft(obj As VectorObject, addX as double = 0, addY as double = 0) As Point
		  dim ax, ay, aax, aay, radius, angle, x, y as double
		  
		  //set point to anchor around
		  ax = obj.AnchorAbsoluteX
		  ay = obj.AnchorAbsoluteY
		  
		  //calculate the anchor position with added relative position
		  aax = ax - addX
		  aay = ay - addY
		  
		  //calculate length of line from 0, 0 to absolute anchor position with added relative position
		  radius = Sqrt((aax * aax) + (aay * aay))
		  //calculate angle based on were the line just meets the anchor point and add the applied rotation from anchor with added relative position
		  angle = ATan2(aay, aax) + obj.Angle.Radians
		  
		  x = (obj.left + ax) - (radius * cos(angle))
		  y = (obj.top + ay) - (radius * sin(angle))
		  
		  return new Point(x, y)
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function TopRight(obj as VectorObject) As Point
		  return TopLeft(obj, 0, obj.Height)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		 Shared Function ToRectShape(vo as VectorObject) As RectShape
		  dim rs as new RectShape
		  
		  rs.X = vo.Left - (vo.Width / 2)
		  rs.Y = vo.Top - (vo.Height / 2)
		  
		  rs.Width = vo.Width
		  rs.Height = vo.Height
		  
		  return rs
		End Function
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event GetContains(x as integer, y as integer) As MyVariant
	#tag EndHook

	#tag Hook, Flags = &h0
		Event GetHeight() As MyVariant
	#tag EndHook

	#tag Hook, Flags = &h0
		Event GetWidth() As MyVariant
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Object2DImplementation(returnFromVO as VectorObject, byref obj as Object2D) As Object2D
	#tag EndHook


	#tag Property, Flags = &h0
		AnchorPoint As Point
	#tag EndProperty

	#tag Property, Flags = &h0
		Angle As Angle
	#tag EndProperty

	#tag Property, Flags = &h0
		debugobjs() As Object2D
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  dim getHeight as MyVariant = GetHeight
			  
			  if getHeight <> nil then
			    return getHeight.IntValue
			  else
			    return mHeight
			  end if
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mHeight = value
			End Set
		#tag EndSetter
		Height As Integer
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		Left As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mHeight As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mWidth As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		Opacity As double = 100
	#tag EndProperty

	#tag Property, Flags = &h0
		Parent As Group
	#tag EndProperty

	#tag Property, Flags = &h0
		Top As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		Visible As Boolean = true
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  dim getWidth as MyVariant = GetWidth
			  
			  if getWidth <> nil then
			    return getWidth.IntValue
			  else
			    return mWidth
			  end if
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mWidth = value
			End Set
		#tag EndSetter
		Width As Integer
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		Shared withinContainsMethod As boolean
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Height"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Opacity"
			Group="Behavior"
			InitialValue="100"
			Type="double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Group="Behavior"
			InitialValue="true"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
