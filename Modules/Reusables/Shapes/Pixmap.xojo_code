#tag Class
Protected Class Pixmap
Inherits VectorObject
	#tag Event
		Function Object2DImplementation(returnFromVO as VectorObject, byref obj as Object2D) As Object2D
		  #pragma unused returnFromVO
		  #pragma unused obj
		  
		  dim returnObj as new PixmapShape(me.Image)
		  
		  return returnObj
		End Function
	#tag EndEvent


	#tag Method, Flags = &h1000
		Sub Constructor(image as picture)
		  // Calling the overridden superclass constructor.
		  Super.Constructor
		  
		  me.Image = image
		End Sub
	#tag EndMethod


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mImage
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mImage = value
			  me.Width = value.Width
			  me.Height = value.Height
			End Set
		#tag EndSetter
		Image As Picture
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mImage As Picture
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Height"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Image"
			Group="Behavior"
			Type="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Opacity"
			Group="Behavior"
			Type="double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
