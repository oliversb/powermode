#tag Class
Protected Class AKTimer
Inherits Timer
	#tag Event
		Sub Action()
		  dim i as integer
		  dim task as aktask
		  
		  // do the animation
		  for i = 0 to ubound(AKCore.tasks)
		    task = AKCore.tasks(i)
		    if not task.hasstarted then
		      task.start
		    end
		    
		    if microseconds - task.lastframetime >= AKCore.frameperiod then
		      task.perform
		      task.lastframetime = microseconds
		    end
		  next
		  
		  // cleanup
		  for i = ubound(AKCore.tasks) downto 0
		    task = AKCore.tasks(i)
		    if task.hascompleted then
		      // animation is completed, make sure it's final position is set
		      task.perform(true)
		      AKCore.tasks.remove i
		      
		      if task.nexttask <> nil then
		        task.nexttask.run
		      end
		      if task.oncompleteaction <> nil then
		        task.oncompleteaction.invoke(task)
		      end
		    end
		  next
		  
		  if ubound(AKCore.tasks) = -1 then
		    AKCore.stop
		  end
		End Sub
	#tag EndEvent


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			Type="Integer"
			EditorType="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Mode"
			Visible=true
			Group="Behavior"
			InitialValue="2"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Off"
				"1 - Single"
				"2 - Multiple"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Period"
			Visible=true
			Group="Behavior"
			InitialValue="1000"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
