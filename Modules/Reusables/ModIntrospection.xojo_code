#tag Module
Protected Module ModIntrospection
	#tag Method, Flags = &h21
		Private Function GetStringValue(obj as object) As string
		  dim typ as Introspection.TypeInfo
		  dim method, methods() as Introspection.MethodInfo
		  
		  typ = Introspection.GetType(obj)
		  methods = typ.GetMethods
		  
		  for each method in methods
		    if method.Name = "GetStringValue" then
		      return method.Invoke(obj)
		    end if
		  next
		  
		  raise new NilObjectException
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SortClassArrayByValue(extends items() As Object, sortField As String)
		  Dim keys() As String
		  Dim typ As Introspection.TypeInfo
		  Dim prop, props() As Introspection.PropertyInfo
		  Dim val As Variant
		  
		  For each oItem As Object in items
		    typ = Introspection.GetType(oItem)
		    props = typ.GetProperties
		    
		    for each prop in props
		      if prop.Name = sortField then
		        val = prop.Value(oItem)
		        if val isa object = false then
		          //value of property can be used
		          keys.Append str(val)
		        else
		          //value of property is an object that contains the value
		          keys.Append ModIntrospection.GetStringValue(val)
		        end if
		        exit
		      end if
		    next
		  next
		  
		  keys.SortWith(items)
		  
		End Sub
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
