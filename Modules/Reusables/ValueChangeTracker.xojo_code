#tag Class
Protected Class ValueChangeTracker
	#tag Method, Flags = &h0
		Sub Constructor()
		  me.Value.Parent = me
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(value as Angle)
		  me.Value = new MyVariant
		  me.Value.AngleValue = value
		  Constructor()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(value as boolean)
		  me.Value = new MyVariant
		  me.Value.BooleanValue = value
		  Constructor()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(value as double)
		  me.Value = new MyVariant
		  me.Value.DoubleValue = value
		  Constructor()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Copy(assigns toCopy as ValueChangeTracker)
		  Value.Copy = toCopy
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetIntValue(sender as ObjectContainerCanvas) As integer
		  #pragma unused sender
		  
		  return Value.IntValue
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function GetStringValue() As string
		  return Value.StringValue
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		Changed As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		Value As MyVariant
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Changed"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
