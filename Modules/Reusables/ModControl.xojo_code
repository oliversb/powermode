#tag Module
Protected Module ModControl
	#tag Method, Flags = &h0
		Function ProceedControlHierarchy(extends value as RectControl, count as integer) As ContainerControlValue
		  dim current as new ModControl.ContainerControlValue
		  current.RectControlValue = value
		  
		  while count > 0
		    if current.RectControlValue <> nil and current.RectControlValue.Parent isa EmbeddedWindowControl then
		      //cannot use parent anymore so use window property
		      if current.RectControlValue.Window isa ContainerControl then
		        current.ContainerControlValue = ContainerControl(current.Window)
		      else
		        current.WindowValue = current.Window
		      end if
		    else
		      if current.RectControlValue <> nil and current.RectControlValue.Parent <> nil then
		        //RectControl contains the current control
		        current.RectControlValue = current.RectControlValue.Parent
		      elseif current.RectControlValue <> nil and current.Window = current.RectControlValue.TrueWindow then
		        //reached the window
		        current.WindowValue = current.Window
		        count = 0
		      elseif current.Value <> nil and current.Window isa ContainerControl then
		        //window is the ContainerControl containing the control
		        current.ContainerControlValue = ContainerControl(current.Window)
		      else
		        current.WindowValue = current.Window
		      end if
		    end if
		    
		    count = count - 1
		  wend
		  
		  return current
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveContainer(w as window, controlname as string)
		  for i as Integer = w.ControlCount - 1 downto 0
		    system.DebugLog str(w.Control(i).Name)
		    if w.control(i).Name = "_" + controlname + "." + controlname then
		      w.control(i).Close
		    end if
		  next
		End Sub
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
