#tag Module
Protected Module MakeName
	#tag Method, Flags = &h0
		Function CheckForUniqueValue(check as string, against as string) As boolean
		  return (check <> against)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CheckName(ByRef name as string, itemNo as integer, string as string)
		  if itemNo > 0 then
		    name = string+str(itemNo)
		  else
		    name = string
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub FinalizeUniqueName(uniqueName as boolean, name as string)
		  if uniqueName = false then
		    name = name + "1"
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MakeName(ByRef itemNo as integer, ByRef pendingString as string, me as object, curItem as object, curItemName as string, string as string)
		  if me = curItem then
		    return
		  end if
		  
		  MakeNameStringHandler(itemNo, pendingString, curItemName, string)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MakeNameStringHandler(ByRef itemNo as integer, ByRef pendingString as string, curItemName as string, string as string)
		  if pendingString = curItemName then
		    itemNo = itemNo + 1
		    pendingString = string+str(itemNo)
		  end if
		  
		  if itemNo = 0 then
		    pendingString = string
		  end if
		End Sub
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
