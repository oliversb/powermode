/* This script was created by Oliver Scott-Brown for use with the game development software, PowerMode.
This script functions as the engine for PowerMode generated code
If this script is distributed to the public, it must legally be used ONLY with PowerMode compiled web apps

http://www.powermodegames.com
Copyright © 2015 Oliver Scott-Brown (check http://legal.powermodegames.com for legal details)*/


// variables
var game;
var objects = [];
var currentScene = '';
var backgroundImage;
var playingSounds = {};
var loaderState;
var soundQueue;
var sounds = [];
var soundCount = 0;
var currentMusicPlaying;
var consoleHolder;
var preloadBar;
var params = {};
var pieces = [];
var keyDown = false;
var textDisplayPosition = 5;
var lastScrollAmount = 0;
// constants
var MEDIA_PATH = 'media/';
var REPLACE_CONSOLE = false;

//engine init
function engineInit() {
	init();
}

//string functions
function Left(str, n) {
	if (n <= 0)
	    return "";
	else if (n > String(str).length)
	    return str;
	else
	    return String(str).substring(0,n);
}
function Right(str, n) {
    if (n <= 0)
       return "";
    else if (n > String(str).length)
       return str;
    else {
       var iLen = String(str).length;
       return String(str).substring(iLen, iLen - n);
    }
}

//replace console prints
function testText(test, text) {
	return (Left(text, test.length) == test);
}

if (REPLACE_CONSOLE) {
	consoleHolder = console;
	console = {};
	console.log = function(text) {
		if (testText("LOG", text)) {
			consoleHolder.log(Left(text, 3));
		}
	console.warn = function(text) {}
		/*if (testText("%c %c %c Phaser", text)) {
			consoleHolder.log("Phaser");
		}*/
	}
}

// functions to power the engine (excluding classes)
function setupGame(width, height) {
	game = new Phaser.Game(width, height, Phaser.AUTO);
}

function addListener(type, listener, object) {
	switch(type) {
		case 'mousedown':
			object.inputEnabled = true;
			object.events.onInputDown.add(listener, object);
		break;
		case 'mouseheld':
			object.inputEnabled = true;
			object.listeners.push(listener);
			object.updateFunctions.push(function(object, listener) {
				if (game.input.activePointer.isDown && object.input.checkPointerOver(game.input.activePointer)) {
					listener.call(object);
				}
			});
		break;
		case 'mouseup':
			object.inputEnabled = true;
			object.events.onInputUp.add(listener, object);
		break;
		case 'mouseenter':
			object.inputEnabled = true;
			object.events.onInputOver.add(listener, object);
		break;
		case 'mouseleave':
			object.inputEnabled = true;
			object.events.onInputOut.add(listener, object);
		break;
		break;
		case 'globalmousedown':
			game.input.onDown.add(listener, object);
		break;
		case 'globalmouseheld':
			object.listeners.push(listener);
			object.updateFunctions.push(function(object, listener) {
				if (game.input.activePointer.isDown) {
					listener.call(object);
				}
			});
		break;
		case 'globalmouseup':
			game.input.onUp.add(listener, object);
		break;
		case 'globalmousescroll':
			//create function to call listener and keep track of scroll delta
			this.newListener = function (e) {
				lastScrollAmount = Math.max(-0.1, Math.min(0.1, (e.wheelDelta || -e.detail)));
				listener();
			}

			// IE9, Chrome, Safari, Opera
   			game.canvas.addEventListener('mousewheel', this.newListener.bind(object), false);
    		// Firefox
    		game.canvas.addEventListener('DOMMouseScroll', this.newListener.bind(object), false);

    		this.newListener = null;
		break;
		case 'keyboarddown':
			game.input.keyboard.onDownCallback.listeners.push(listener.bind(object));
		break;
		case 'keyboardheld':
			object.listeners.push(listener);
			object.updateFunctions.push(function(object, listener) {
				if (keyDown) {
					listener.call(object);
				}
			});
		break;
		case 'keyboardup':
			game.input.keyboard.onUpCallback.listeners.push(listener.bind(object));
		break;
		/*case 'keyboardgamepaddown':

		break;
		case 'keyboardgamepadheld':

		break;
		case 'keyboardgamepadup':

		break;*/
	}
}

function startFirstState() {
    pieces = window.location.search.slice(1).split('&');
    for (var i=0, l=pieces.length; i<l; i++) {
    var parts = pieces[i].split('=');
    params[parts[0]] = parts[1];
    }

	createjs.FlashPlugin.swfPath = '../src/soundjs/'; // Initialize the base path from this document to the Flash Plugin
    if (params.type == 'flash') {
    	createjs.Sound.registerPlugins([createjs.FlashPlugin]);
    } else if (params.type == 'html5') {
    	createjs.Sound.registerPlugins([createjs.HTMLAudioPlugin]);
    } else {
    	createjs.Sound.registerPlugins([createjs.WebAudioPlugin, createjs.HTMLAudioPlugin, createjs.FlashPlugin]);
    }

	game.state.add('loaderState', loaderState);
	game.state.start('loaderState');
	soundQueue = new createjs.LoadQueue();
	soundQueue.installPlugin(createjs.Sound);
}

function addScene(name, scene) {
	game.state.add(name, scene);
}

function startScene(name) {
	currentScene = name;
	game.state.start(name);
}


function loaderState() {};
loaderState.prototype.preload = function() {
	game.stage.disableVisibilityChange = true;

	game.load.image('splashscreen', 'media/splashscreen.png');
	loadSprites();

	//setup preload
	preloadBar = game.add.sprite(0, 50);
	this.preloadG = game.add.graphics();
	this.preloadG.lineStyle(3, 0xFFFFFF, 1);
	this.preloadG.moveTo(0, 0);
	this.preloadG.lineTo(game.width, 0);
	preloadBar.addChild(this.preloadG);

	preloadBar.scale.x = 0;

	game.stage.backgroundColor = '#0090E3';
}
loaderState.prototype.create = function() {
	game.add.image((game.width / 2) - (game.cache.getImage('splashscreen').width / 2), (game.height / 2) - (game.cache.getImage('splashscreen').height / 2), 'splashscreen');

	//setup keyboard handling
	game.input.keyboard.onDownCallback = function () {
		if (!keyDown) {
			keyDown = true;
			this.listeners = game.input.keyboard.onDownCallback.listeners;
			for (this.loop = 0; this.loop < this.listeners.length; this.loop++) {
					this.listeners[this.loop]();
			}
		}
	}
	game.input.keyboard.onDownCallback.listeners = [];

	game.input.keyboard.onUpCallback = function() {
		keyDown = false;
		this.listeners = game.input.keyboard.onUpCallback.listeners;
		for (this.loop = 0; this.loop < this.listeners.length; this.loop++) {
				this.listeners[this.loop]();
		}
	}
	game.input.keyboard.onUpCallback.listeners = [];

	//clear up
	this.event = null;

	//preload sounds (if any)
	this.preload2();

	if(soundCount == 0) {
		handleLoadComplete();
	} else {
		soundQueue.on('progress', handleProgress);
		soundQueue.on('complete', handleLoadComplete);
	}
}

function handleLoadComplete(event) {
	game.stage.disableVisibilityChange = false;
	preloadBar = null;
	handleLoadComplete2(event);
}

loaderState.prototype.loadUpdate = function() {
	preloadBar.scale.x = game.load.progress * 0.01;
}

function handleProgress(event) {
	preloadBar.scale.x = event.progress * 0.01;
}

function getSound(name) {
	return sounds[name];
}

function preloadSound(name, volume, pan) {
	//game.load.audio(name, MEDIA_PATH + name + '.mp3');
	//this.path = MEDIA_PATH + name + ".";
	soundQueue.loadFile({id: name, src: MEDIA_PATH + name + '.mp3'});
	this.sound = createjs.Sound.createInstance(name);

	this.sound.volume = volume * .001;
	this.sound.pan = pan * .001;

	sounds[name] = this.sound;
	soundCount++;
}

function sceneCreateHandler() {
	game.physics.startSystem(Phaser.Physics.ARCADE);
	for (var i = 0; i < objects.length; i ++) {
		objects[i].theConstructor();
	}
}

function addObject(obj) {
	objects.push(obj);
	game.add.existing(obj);
}

function loadSpriteSheet(name) {
	this.path = MEDIA_PATH + name;
	game.load.atlasJSONArray(name, this.path + '.png', this.path + '.json');
}

function setBackground(name) {
	if (backgroundImage != null) { backgroundImage.destroy(); }
	backgroundImage = game.add.image(0, 0, name);
}

//general
function setScene(obj, value) {
	for(var i = 0;i < objects.length - 1; i++) {
		objects[i].destroy();
		objects.splice(i, 1);
	}
	currentScene = value;
	game.state.start(value);
}

function getScene(obj) {
	return currentScene;
}

function setVisible(obj, value) {
	obj.visible = value;
}

function getVisible(obj, value) {
	return obj.visible;
}

function setOpacity(obj, value) {
	obj.alpha = value / 100;
}

function getOpacity(obj) {
	return obj.alpha * 100;
}

function setPresistant(obj, value) {
	obj.presistant = value;
}

function getPresistant(obj) {
	return obj.presistant;
}

//number
function getPI() {
	return 3.14159265358979323846264338327950;
}

//physics
function setX(obj, value) {
	//set x corresponding to centred anchor (includes minus values)
	obj.x = (obj.scale.x > 0 ? value + obj.body.halfWidth : value - (obj.body.width + obj.body.halfWidth));
	obj.trueX = value;
}
function getX(obj) {
	return obj.trueX;
}

function setY(obj, value) {
	//set y corresponding to centred anchor (includes minus values)
	obj.y = (obj.scale.y > 0 ? value + obj.body.halfHeight : value - (obj.body.height + obj.body.halfHeight));
	obj.trueY = value;
}
function getY(obj) {
	return obj.trueY;
}

function setVelocityX(obj, value) {
	obj.body.velocity.x = value;
}
function getVelocityX(obj) {
	return obj.body.velocity.x;
}

function setVelocityY(obj, value) {
	obj.body.velocity.y = value;
}
function getVelocityY(obj) {
	return obj.body.velocity.y;
}

function setAccelerationX(obj, value) {
	obj.body.acceleration.x = value;
}
function getAccelerationX(obj) {
	return obj.body.acceleration.x;
}

function setAccelerationY(obj, value) {
	obj.body.acceleration.y = value
}
function getAccelerationY(obj) {
	return obj.body.acceleration.y;
}

function setMaxVelocityX(obj, value) {
	obj.body.maxVelocity.x = value;
}
function getMaxVelocityX(obj) {
	return obj.body.maxVelocity.x;
}

function setMaxVelocityY(obj, value) {
	obj.body.maxVelocity.y = value;
}
function getMaxVelocityY(obj) {
	return obj.body.maxVelocity.y;
}

function setGravity(obj, value) {
	obj.body.gravity.y = value;
}
function getGravity(obj) {
	return obj.body.gravity.y;
}

function setMass(obj, value) {
	obj.body.mass = value;
}
function getMass(obj) {
	return obj.body.mass;
}

function setMaxAngularVelocity(obj, value) {
	obj.body.maxAngular = value;
}
function getMaxAngularVelocity(obj) {
	return obj.body.maxAngular;
}

function setRotation(obj, value) {
	obj.rotation = value;
}
function getRotation(obj) {
	return obj.rotation;
}

function resetBody(obj) {
	obj.body.reset();
}

function setCollisionMovement(obj, value) {
	obj.body.move = !immovable;
}
function getCollisionMovement(obj, value) {
	return !obj.body.immovable;
}

//graphics
function setIsPenDown(obj, value) {
	obj.penDown = value;
}
function getIsPenDown(obj) {
	return obj.penDown;
}

function penDown(obj) {
	obj.penDown = true;
}
function penUp(obj) {
	obj.penDown = false;
}

function graphicsSetPoint(obj, x, y) {
	obj.graphicsPointX = x;
	obj.graphicsPointY = y;

	if (obj.penDown) {
		obj.graphics.lineTo(x, y);
	} else {
		obj.graphics.moveTo(x, y);
	}
}

function getDrawPointX(obj) {
	return obj.graphicsPointX;
}

function getDrawPointY(obj) {
	return obj.graphicsPointY;
}

function setLineStyle(obj) {
	obj.graphics.lineStyle(obj.graphicsThickness, obj.graphicsColour, obj.graphicsOpacity);
}

function setDrawColour(obj, value) {
	obj.graphicsColour = value;
	setLineStyle(obj);
}

function getDrawColour(obj) {
	return obj.graphicsColour;
}

function setDrawOpacity(obj, value) {
	obj.graphicsOpacity = value;
	setLineStyle(obj);
}

function getDrawOpacity(obj) {
	return obj.graphicsOpacity;
}

function setDrawThickness(obj, value) {
	obj.graphicsThickness = value;
	setLineStyle(obj);
}

function getDrawThickness(obj) {
	return obj.graphicsThickness;
}

function resetLineStyle(obj) {
	obj.penDown = false;
	obj.graphicsThickness = 1;
	obj.graphicsOpacity = 1;

	obj.graphicsPointY = 0;
	obj.graphicsPointY = 0;

	setLineStyle(obj);
}

function clearGraphics(obj) {
	resetLineStyle(obj);
	obj.graphics.clear();
}

function drawRect(obj, x, y, width, height) {
	obj.graphics.drawRect(x, y, width, height);
}

function drawLine(obj, x1, y1, x2, y2) {
	obj.graphics.moveTo(x1, y1);
	obj.graphics.lineTo(x2, y2);
}

function drawCircle(obj, x, y, radius) {
    obj.graphics.drawCircle(x, y, radius);
}

function drawEllipse(obj, x, y, width, height) {
	obj.graphics.drawEllipse(x, y, width, height);
}

function beginFill(obj, colour, alpha) {
	obj.graphics.beginFill(colour, alpha);
}

function endFill(obj) {
	obj.graphics.endFill();
}

function setSprite(obj, key) {
	obj.loadTexture(key);
}

function getSprite(obj) {
	return obj.key;
}

function setSpriteOpacity(obj, value) {
	obj.alpha = (1 - value) / 100;
}

function getSpriteOpacity(obj) {
	return (1 - obj.alpha) * 100;
}

function setScaleX(obj, value) {
	obj.scale.x = value;
}
function getScaleX(obj, value) {
	return obj.scale.x;
}

function setScaleY(obj, value) {
	obj.scale.y = value;
}
function getScaleY(obj, value) {
	return obj.scale.y;
}

function setWidth(obj, value) {
	obj.width = value;
}
function getWidth(obj) {
	return obj.width;
}

function setHeight(obj, value) {
	obj.height = value;
}
function getHeight(obj) {
	return obj.height;
}

function setAnimationSpeed(obj, value) {
	obj.animations.currentAnim.speed = value;
}

function getAnimationSpeed(obj) {
	return obj.animations.currentAnim.speed;
}

function setAnimationFrame(obj, value) {
	obj.animations.currentFrame = value - 1;
}

function getAnimationFrame(obj) {
	return obj.animations.currentFrame + 1;
}

function getAnimationFrameCount(obj) {
	return obj.animations.frameTotal + 1;
}

function startAnimation(obj, value) {
	obj.animations.currentAnim.play();
}

function pauseAnimation(obj) {
	obj.animations.currentAnim.isPaused = true;
}

function resumeAnimation(obj) {
	obj.animations.currentAnim.isPaused = false;
}

function stopAnimation(obj) {
	obj.animations.currentAnim.stop();
}

function reverseAnimation(obj) {
	obj.animations.add('reverse');
	obj.animations.currentAnim
}

//UI

function confirm(message, yesHandler, noHandler) {
	//document.getElementsByClassName('message')[0].innerHTML = message;
	$('#confirm').modal({
		closeHTML: "<a href='#' title='Close' class='modal-close'>x</a>",
		position: ["20%",],
		overlayId: 'confirm-overlay',
		containerId: 'confirm-container',
		onShow: function (dialog) {
			var modal = this;

			$('.message', dialog.data[0]).append(message);

			// if the user clicks "yes"
			$('.yes', dialog.data[0]).click(function () {
				// call the callback
				if ($.isFunction(callback)) {
					callback.apply();
				}
				// close the dialog
				modal.close(); // or $.modal.close();
			});
		}
	});


	 /*$(function() {
	    $( "#dialog-confirm" ).dialog({
	      resizable: false,
	      height:140,
	      modal: true,
	      buttons: {
	        "Delete all items": function() {
	          $( this ).dialog( "close" );
	        },
	        Cancel: function() {
	          $( this ).dialog( "close" );
	        }
	      }
	    });
	  });*/
}

function showMessage(msg) {
	alert(msg);
}

function askQuestion(msg) {

}

function askYesNoQuestion(msg, yesHandler, noHandler) {
	confirm(msg, yesHandler, noHandler);
}

function askChoiceQuestion(msg, choices) {

}

/*sound
The id for a sound represents an indexed group of sounds
An id of -1 represents all sounds
An id of 0 represents music (normal sounds can be added to this group too though playing music replaces all normal sounds so this is not recommended)
*/

//retrieve variables
function getCurrentMusicPlaying() {
	return currentMusicPlaying;
}

function getCurrentSoundsPlaying() {
	return playingSounds;
}

function getMusicVolume() {
	return currentMusicPlaying.volume;
}
function setMusicVolume(value) {
	currentMusicPlaying.volume = value;
}

function getMusicPan() {
	return currentMusicPlaying.pan;
}
function setMusicPan(value) {
	currentMusicPlaying.pan = value;
}
//

function playMusic(sound, loop) {
	currentMusicPlaying = sound;

	this.loop = 0;

	playingSounds[0] = [sound];
	if(loop) this.loop = -1;
	sound.play({loop: this.loop});
}

function playSound(sound, loop, id) {
	this.loop = 0;

	playingSounds[id].push(sound);
	if(loop) this.loop = -1;
	sound.play();
}

function getSounds(id) {
	//pick from sounds with a certain id or if the id is -1 then get all playing sounds
	this.sounds = [];

	if (id != -1) {
		for(this.snd = 0; this.snd < playingSounds[id].length; this.snd++) {
			this.sounds.push(playingSounds[id][this.snd]);
		}
	} else {
		for(this.snd1 = 0; this.snd1 < playingSounds[id].length; this.snd1++) {
			for(this.snd2 = 0; this.snd2 < this.snd1.length; this.snd2++) {
				this.sounds.push(playingSounds[this.snd1][this.snd2]);
			}
		}
	}

	return this.sounds;
}

function pauseSound(id) {
	this.sounds = getSounds(id);

	for(this.snd = 0; this.snd < playingSounds[id].length; this.snd++) {
		playingSounds[id][this.snd].pause();
	}
}

function resumeSound(id) {
	this.sounds = getSounds(id);

	for(this.snd = 0; this.snd < playingSounds[id].length; this.snd++) {
		playingSounds[id][this.snd].resume();
	}
}

function stopSound(id) {
	this.sounds = getSounds(id);

	for(this.snd = 0; this.snd < playingSounds[id].length; this.snd++) {
		playingSounds[id][this.snd].stop();
	}
}

function setVolume(id, value) {
	this.sounds = getSounds(id);

	for(this.snd = 0; this.snd < playingSounds[id].length; this.snd++) {
		playingSounds[id][this.snd].volume = value / 100;
	}
}
function getVolume(id) {
	//id must be more than -1 (all sounds) because different sounds have different volumes
	return playingSounds[id][0].volume * 100;
}

function setPan(id) {
	this.sounds = getSounds(id);

	for(this.snd = 0; this.snd < playingSounds[id].length; this.snd++) {
		playingSounds[id][this.snd].pan = value / 100;
	}
}
function getPan(id) {
	//id must be more than -1 (all sounds) because different sounds have different pans
	return playingSounds[id][0].pan * 100;
}

//misc
//retrieve variables
function totalObjectCount(obj) {
	return objects.length;
}
//
function spawn(type) {
	this.newObject = new type();
	objects.push(this.newObject);
	game.add.existing(this.newObject);
	this.newObject = null;
}

function spawnAt(type, x, y) {
	this.newObject = new type();
	objects.push(this.newObject);
	game.add.existing(new type(x, y));
	this.newObject = null;
}

function kill(id) {
	objects[id].destroy();
	objects.remove(id);
}

function causeExtinction(type) {
	for(this.loop = 0; this.loop < objects.length; this.loop++) {
		if(objects[this.loop] instanceof type) {
			objects[id].destroy();
			objects.remove(this.loop);
		}
	}
}

//input
function getLastKeyPressed() {
	return game.input.keyboard.lastKey.keyCode;
}

function getLastScrollAmount() {
	return lastScrollAmount;
}

function getMouseX() {
	return game.input.x;
}

function getMouseY() {
	return game.input.y;
}

//test
function powerModeLog(text) {
	console.log("LOG" + text);
}

function displayScore(obj) {
	textDisplayPosition += 5;
	obj.scoreText = game.add.text(5, textDisplayPosition, "Score: " + obj.score);
}

function displayLives(obj) {
	textDisplayPosition += 5;
	obj.livesText = game.add.text(5, textDisplayPosition, "Lives: " + obj.lives);
}

function displayHealth(obj) {
	textDisplayPosition += 5;
	obj.healthText = game.add.text(5, textDisplayPosition, "Health: " + obj.health);
}

/*rgbToHex: function (r, g, b) {
    return "0x" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}*/

function displayHealthbar(obj) {
	if (obj.healthbar == null) {
		obj.healthbar = game.add.graphics(0,0);
		game.group.add(obj.healthbar);

		obj.health = 100;
		this.totalHealth = 100;
		this.lastHealth = 0;
	}

	textDisplayPosition += 5;

	if (obj.lastHealth !== obj.health) {
	    obj.healthbar.clear();
	    obj.healthbarX = (obj.health / obj.totalHealth) * 100;
	    obj.healthbarColour = utils.rgbToHex((x > 50 ? 1-2*(x-50)/100.0 : 1.0) * 255, (x > 50 ? 1.0 : 2*x/100.0) * 255, 0);

	    obj.healthbar.beginFill(colour);
	    obj.healthbar.lineStyle(5, colour, 1);
	    obj.healthbar.moveTo(0,-5);
	    obj.healthbar.lineTo(100 * obj.health / this.totalHealth, -5);
	    obj.healthbar.endFill();
	}

	obj.lastHealth = obj.health;
}

function hideScore(obj) {
	if (obj.scoreText != null) obj.scoreText.destroy();
	obj.scoreText = null;
}

function hideLives(obj) {
	if (obj.livesText != null) obj.livesText.destroy();
	obj.livesText = null;
}

function hideHealth(obj) {
	if (obj.healthText != null) obj.healthText.destroy();
	obj.healthText = null;
}

function hideHealthbar(obj) {
	if (obj.healthbar != null) obj.healthbar.destroy();
	obj.healthbar = null;
}

//classes that are built-in to the PowerMode engine
function PowerModeDefaultObject(x ,y, opacity, rotation, scaleX, scaleY, visible) {
	//declare variables
	this.updateFunctions = [];
	this.listeners = [];
	this.graphics = game.add.graphics(0, 0);

	this.presistant = false;

	resetLineStyle(this);

	Phaser.Sprite.call(this, game, 0, 0, this.spriteKey);

	setOpacity(this, opacity);
	setScaleX(this, scaleX);
	setScaleY(this, scaleY);
	setVisible(this, visible);

	this.animations.add('go');
	game.physics.enable(this, Phaser.Physics.ARCADE);

	setRotation(this, rotation);

	//anchor rotation around centre and set default position
	this.anchor.setTo(.5, .5);

	setX(this, x);
	setY(this, y);
}
PowerModeDefaultObject.prototype = Object.create(Phaser.Sprite.prototype);
PowerModeDefaultObject.prototype.constructor = PowerModeDefaultObject;
PowerModeDefaultObject.prototype.update = function() {
	for(this.loop = 0; this.loop < this.updateFunctions.length; this.loop++) {
		this.updateFunctions[this.loop](this, this.listeners[this.loop]);
	}
}

