#tag Window
Begin EmbeddedControl SpriteEditor
   AcceptFocus     =   False
   AcceptTabs      =   True
   AutoDeactivate  =   True
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   Compatibility   =   ""
   Enabled         =   True
   EraseBackground =   True
   HasBackColor    =   False
   Height          =   523
   HelpTag         =   ""
   InitialParent   =   ""
   Left            =   0
   LockBottom      =   True
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   TabIndex        =   0
   TabPanelIndex   =   0
   TabStop         =   True
   Top             =   0
   Transparent     =   True
   UseFocusRing    =   False
   Visible         =   True
   Width           =   625
   Begin Label lblPreview
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Preview"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   0
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   89
   End
   Begin LVToolBar LVToolBar1
      AcceptFocus     =   False
      AcceptTabs      =   False
      AquaBackground  =   False
      AutoDeactivate  =   True
      Backdrop        =   0
      DoubleBuffer    =   False
      EnableContextualMenu=   True
      Enabled         =   True
      EraseBackground =   True
      FontColourDefault=   False
      Height          =   35
      HelpTag         =   ""
      HideHeight      =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      mToolBarStyleChanged=   True
      RefreshLock     =   False
      Scope           =   0
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      ToolbarStyle    =   0
      Top             =   -40
      Transparent     =   True
      UseFocusRing    =   True
      Visible         =   True
      Width           =   43
   End
   Begin Timer Timer1
      Height          =   32
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   40
      LockedInPosition=   False
      Mode            =   2
      Period          =   1
      Scope           =   0
      TabPanelIndex   =   "0"
      Top             =   40
      Width           =   32
   End
   Begin FGThumbnailCanvas View
      AcceptFocus     =   False
      AcceptTabs      =   False
      AllowMouseWheelScrolling=   True
      AutoDeactivate  =   True
      Backdrop        =   0
      BackgroundColor =   &c67676700
      CellSize        =   128
      CurrentThumbnailUnderMouseIndex=   0
      DoubleBuffer    =   True
      DownKeyCode     =   31
      DragSelectionBorder=   2
      DragSelectionBorderColor=   &cFFFFFF00
      DragSelectionColor=   &cFFFFFF00
      DragSelectionOpacity=   30
      DrawSelectionRectangle=   True
      Enabled         =   True
      EraseBackground =   False
      ForceUpdateOnMouseMove=   True
      GraphicsPicture =   0
      Height          =   523
      HelpTag         =   ""
      Index           =   -2147483648
      InFocus         =   False
      InitialParent   =   ""
      InvertMouseWheelScrollDirection=   False
      Left            =   235
      LeftKeyCode     =   28
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      MaxCellSize     =   256
      MaxColumnPadding=   75
      MaxRowPadding   =   75
      MinCellSize     =   64
      MinColumnPadding=   10
      MinRowPadding   =   10
      RightKeyCode    =   29
      Scope           =   0
      SelectedBorderColor=   &c418CE500
      SelectedBorderThickness=   2
      SelectedIndex   =   0
      Selecting       =   False
      SelectionCount  =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   0
      Transparent     =   True
      UpKeyCode       =   30
      UseFocusRing    =   True
      Visible         =   True
      Width           =   390
   End
   Begin Label lblPreviewAnimationSpeed
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Preview Animation Speed:"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   268
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   139
   End
   Begin UnsignedIntField txtAnimationSpeed
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CheckIfInteger  =   False
      CheckIfPositive =   False
      CheckText       =   "0"
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Default         =   "0"
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InFocus         =   False
      Italic          =   False
      Left            =   143
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ProgrammaticalChange=   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   6
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextIsOk        =   True
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   268
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   54
   End
   Begin Label lblFPS
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   201
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   7
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "FPS"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   269
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   29
   End
   Begin Label lblPreviewExplanation
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   61
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   True
      Left            =   7
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   True
      Scope           =   0
      Selectable      =   False
      TabIndex        =   8
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Please note that this is just for experimentation with animation speed and this does not set the animation speed in-game."
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   302
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   223
   End
   Begin imSplitter imSplitter1
      AcceptFocus     =   False
      AcceptTabs      =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Backdrop        =   0
      DockAfter       =   True
      DockAfterSize   =   40
      DockBefore      =   True
      DockBeforeSize  =   40
      DoubleBuffer    =   False
      DoubleClickAction=   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   522
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      IsDocked        =   False
      IsDockedPosition=   ""
      Left            =   228
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MinAfterArea    =   20
      MinBeforeArea   =   30
      Scope           =   0
      TabIndex        =   9
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   0
      Transparent     =   True
      UseFocusRing    =   True
      Visible         =   True
      Width           =   6
   End
   Begin AKAnimationCanvas Canvas1
      AcceptFocus     =   False
      AcceptTabs      =   False
      AutoDeactivate  =   True
      Backdrop        =   0
      DoubleBuffer    =   False
      Enabled         =   True
      EraseBackground =   True
      Height          =   224
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   32
      Transparent     =   True
      UseFocusRing    =   True
      Visible         =   True
      Width           =   223
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  DefSplitterLeft = imSplitter1.Left
		  
		  txtAnimationSpeed.Text = str(DocFile.LastAnimationPreviewSpeed)
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h1000
		Sub Constructor(r As ResourceSprite)
		  res = r
		  
		  
		  View.Thumbnails = r.frames
		  'r.frames = View.Thumbnails
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub Copy()
		  //if any are selected
		  if View.SelectionCount > 0 then
		    //set the clipboard to the picture within the selected thumbnail
		    dim c as new Clipboard
		    c.Picture = View.Thumbnails(View.SelectedIndex).SourceImage
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub Cut()
		  //copy and delete are methods which both check for selection count
		  Copy
		  Delete
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub Delete()
		  //if anything is selected and the thumbnail viewer is in focus
		  if MainEditor.FocusItem = 1 and View.SelectionCount > 0 then
		    //loop through each selected thumbnail
		    for each i as PhotoThumbnail in View.SelectedThumbnails
		      //clear the selection before deletion
		      View.ClearSelection
		      //delete the selected thumbnails (in loop)
		      View.Clear View.Thumbnails.IndexOf(i)
		    next
		  end if
		  
		  RefreshPreview
		  
		  //enable save
		  docfile.ModifiedDoc = true
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub Duplicate()
		  //if any are selected
		  if View.SelectionCount > 0 then
		    //loop through selected thumbnails
		    for each st as PhotoThumbnail in View.SelectedThumbnails
		      //draw picture from original thumbnail into a new thumbnail (with the mask)
		      dim p as new Picture(st.SourceImage.Width,st.SourceImage.Height,32)
		      p.Graphics.DrawPicture(st.SourceImage,0,0)
		      p.ApplyMask(st.SourceImage.CopyMask)
		      //add the thumbnail with the newly created thumbnail
		      View.AddThumbnail(new PhotoThumbnail(p,FrameName))
		    next
		  end if
		  //refresh the graphics
		  View.Invalidate
		  
		  RefreshPreview
		  
		  //enable save
		  docfile.ModifiedDoc = true
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub EditPicture()
		  //if any are selected
		  if View.SelectedIndex <> -1 then
		    //if there is no sourceimage then create a new one so there are no errors
		    if View.Thumbnails(View.SelectedIndex).SourceImage = nil then
		      View.Thumbnails(View.SelectedIndex).SourceImage = new Picture(1,1)
		    end if
		    //set the width and height of the canvas
		    PWindow.CanvasWidth = View.Thumbnails(View.SelectedIndex).SourceImage.Width
		    PWindow.CanvasHeight = View.Thumbnails(View.SelectedIndex).SourceImage.Height
		    //set the image's instance to keep up with the picture in the selected thumbnail
		    PaintWindow.ImageCanvas1.Image = View.Thumbnails(View.SelectedIndex).SourceImage
		    //show and reference canvas to repaint
		    PaintWindow.Show(View)
		  end if
		  
		  //enable save
		  docfile.ModifiedDoc = true
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function FrameName(optional thumb as PhotoThumbnail) As String
		  dim index as integer
		  if thumb = nil then
		    index = View.Thumbnails.Ubound+1
		  else
		    index = View.Thumbnails.IndexOf(thumb)
		  end if
		  
		  return "Frame "+str(index)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub ImportFrame()
		  dim dlg as new OpenDialog
		  dim f as FolderItem = ImageDialog(dlg)
		  If f <> Nil then
		    //proceed normally
		    #pragma BreakOnExceptions false
		    try
		      for i as integer = 0 to dlg.Count-1
		        View.AddThumbnail(new PhotoThumbnail(Picture.Open(dlg.Item(i)),FrameName))
		      next
		      
		      RefreshPreview
		      
		    catch e as NilObjectException
		    end try
		    View.Invalidate
		    
		    //enable save
		    docfile.ModifiedDoc = true
		  Else
		    //User Cancelled
		  End if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub MoveLeft()
		  if View.SelectedIndex > 0 then
		    MoveThumb(-1)
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub MoveRight()
		  if View.SelectedIndex < View.Thumbnails.Ubound then
		    MoveThumb(1)
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub MoveThumb(amount as integer)
		  for each thumb as PhotoThumbnail in View.SelectedThumbnails
		    thumb.Text = FrameName(thumb)
		  next
		  //reorder thumbnails
		  View.MoveSelected(View.Thumbnails, amount, View)
		  
		  View.SelectRange(View.IndexOfLowestSelectedThumbnail+amount,View.IndexOfHighestSelectedThumbnail+amount)
		  for each thumb as PhotoThumbnail in View.Thumbnails
		    thumb.Text = FrameName(thumb)
		  next
		  
		  //updates the new text
		  View.Invalidate
		  
		  RefreshPreview
		  
		  docfile.ModifiedDoc = true
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub NewFrame()
		  View.AddThumbnail(new PhotoThumbnail(new Picture(1,1),FrameName))
		  SelectLastItem
		  FrameDialog.ShowModal
		  PaintWindow.Show(View)
		  MainEditor.MakeNames
		  
		  'EditPicture //not using because this does not setup a new image
		  View.Invalidate
		  
		  RefreshPreview
		  
		  docfile.ModifiedDoc = true
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub Paste()
		  dim c as new Clipboard
		  if c.PictureAvailable then
		    View.AddThumbnail(new PhotoThumbnail(c.Picture,FrameName))
		    View.Invalidate
		    RefreshPreview
		    docfile.ModifiedDoc = true
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub RefreshPreview()
		  dim frames() as picture
		  for each f as PhotoThumbnail in res.Frames
		    frames.append(ScalePicture(f.SourceImage, Canvas1.width, Canvas1.height))
		  next
		  
		  if PreviewAnimationTask <> nil then
		    AKCore.Tasks.Remove( AKCore.Tasks.IndexOf(PreviewAnimationTask) )
		    PreviewAnimationTask = nil
		    Canvas1.Backdrop = nil
		  end if
		  if frames.ubound <> -1 then
		    Canvas1.Animate(frames, 1 / val(txtAnimationSpeed.text), true)
		    PreviewAnimationTask = AKCore.Tasks(AKCore.Tasks.Ubound)
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub SelectLastItem()
		  //OLD CODE FOR RADTHUMBNAILCANVAS CLASS
		  'View.Selected(View.SelectedThumb) = false
		  'View.Selected(View.Thumbnails.Ubound) = true
		  
		  View.SelectRange(View.Thumbnails.Ubound)
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h1
		Protected check As Boolean
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected DefSplitterLeft As uint32
	#tag EndProperty

	#tag Property, Flags = &h0
		MyAnimation As AKFrameTask
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected PreviewAnimationTask As AKTask
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected res As ResourceSprite
	#tag EndProperty


#tag EndWindowCode

#tag Events LVToolBar1
	#tag Event
		Sub Action(selectedToolBarItem As LVToolBarItem)
		  select case selectedToolBarItem.Caption
		  case "New Frame"
		    NewFrame
		  case "Import Frame"
		    ImportFrame
		  case "Move Left"
		    MoveLeft
		  case "Move Right"
		    MoveRight
		  case "Delete"
		    Delete
		  case "Copy"
		    Copy
		  case "Cut"
		    Cut
		  case "Paste"
		    Paste
		  case "Duplicate"
		    Duplicate
		  end Select
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  me.ToolBarStyle = LVToolBar.STYLE_SMALL_ICONS_LABEL
		  
		  dim tbNew as New LVToolBarItem
		  tbNew.Caption = "New Frame (COMING SOON)"
		  tbNew.HelpTag = "Create an empty frame"
		  tbNew.Image = Icon("New")
		  tbNew.Visible = true
		  me.AddButton(tbNew)
		  
		  dim tbImport as New LVToolBarItem
		  tbImport.Caption = "Import Frame"
		  tbImport.HelpTag = "Create a frame from the imported sprite"
		  tbImport.Image = Icon("Open")
		  tbImport.Visible = true
		  me.AddButton(tbImport)
		  
		  dim tbSeparator1 as New LVStandardToolBarItem
		  me.AddButton(tbSeparator1)
		  
		  dim tbCut as New LVToolBarItem
		  tbCut.Caption = "Cut"
		  tbCut.HelpTag = "Cut"
		  tbCut.Image = Icon("Cut")
		  tbCut.Visible = true
		  me.AddButton(tbCut)
		  
		  dim tbCopy as New LVToolBarItem
		  tbCopy.Caption = "Copy"
		  tbCopy.HelpTag = "Copy"
		  tbCopy.Image = Icon("Copy")
		  tbCopy.Visible = true
		  me.AddButton(tbCopy)
		  
		  dim tbPaste as New LVToolBarItem
		  tbPaste.Caption = "Paste"
		  tbPaste.HelpTag = "Paste"
		  tbPaste.Image = Icon("Paste")
		  tbPaste.Visible = true
		  me.AddButton(tbPaste)
		  
		  dim tbDel as New LVToolBarItem
		  tbDel.Caption = "Delete"
		  tbDel.HelpTag = "Delete selection"
		  tbDel.Image = Icon("Delete")
		  tbDel.Visible = true
		  me.AddButton(tbDel)
		  
		  dim tbDuplicate as New LVToolBarItem
		  tbDuplicate.Caption = "Duplicate"
		  tbDuplicate.HelpTag = "Duplicate selection"
		  tbDuplicate.Image = Icon("Copy")
		  tbDuplicate.Visible = true
		  me.AddButton(tbDuplicate)
		  
		  'dim tbSeparator2 as New LVStandardToolBarItem
		  'me.AddButton(tbSeparator2)
		  
		  'dim tbUndo as New LVToolBarItem
		  'tbUndo.Caption = "Undo"
		  'tbUndo.HelpTag = "Undo last action"
		  'tbUndo.Image =Icon("Undo")
		  'tbUndo.Visible=  true
		  'me.AddButton(tbUndo)
		  '
		  'dim tbRedo as New LVToolBarItem
		  'tbRedo.Caption = "Redo"
		  'tbRedo.HelpTag = "Redo last action"
		  'tbRedo.Image = Icon("Redo")
		  'tbRedo.Visible=  true
		  'me.AddButton(tbRedo)
		  
		  dim tbSeparator3 as New LVStandardToolBarItem
		  me.AddButton(tbSeparator3)
		  
		  dim tbLeft as New LVToolBarItem
		  tbLeft.Caption = "Move Left"
		  tbLeft.HelpTag = "Move selected frames left"
		  tbLeft.Image = Icon("Left")
		  tbLeft.Visible=  true
		  me.AddButton(tbLeft)
		  
		  dim tbRight as New LVToolBarItem
		  tbRight.Caption = "Move Right"
		  tbRight.HelpTag = "Move selected frames right"
		  tbRight.Image = Icon("Right")
		  tbRight.Visible=  true
		  me.AddButton(tbRight)
		  
		  me.Invalidate
		  me.StatusBar = MainEditor.LVStatusBar1
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Timer1
	#tag Event
		Sub Action()
		  'If Keyboard.AsyncKeyDown(8) and check = false then
		  'check = true
		  'Delete
		  'elseif not Keyboard.AsyncKeyDown(8) and check =true then
		  'check = false
		  'end if
		  
		  'if PWindow.Visible = true then
		  ''#pragma BreakOnExceptions false
		  'try
		  'if DocFile.ChangedCurrentPic then
		  'View.Thumbnails(View.SelectedIndex).SourceImage = DocFile.CurrentPaintImage
		  'View.Invalidate
		  'DocFile.CurrentPaintImage = nil
		  'DocFile.ChangedCurrentPic = false
		  'end if
		  'catch OutOfBoundsException
		  'end try
		  'end if
		  
		  if PWindow.TurnedInvisible then
		    call View.CallMouseDown(0, 0)
		    RefreshPreview
		    DocFile.CurrentPaintImage = nil
		    DocFile.ChangedCurrentPic = false
		  end if
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events View
	#tag Event
		Sub DoubleClick(X As Integer, Y As Integer)
		  #pragma Unused X
		  #pragma Unused Y
		  
		  EditPicture
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events txtAnimationSpeed
	#tag Event
		Sub ValidatedTextChange()
		  RefreshPreview
		  DocFile.LastAnimationPreviewSpeed = val(me.text)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events imSplitter1
	#tag Event
		Sub Open()
		  me.AddControl(View,false)
		  
		  me.AddControl(lblFPS, true)
		  me.AddControl(lblPreviewAnimationSpeed, true)
		  me.AddControl(lblPreviewExplanation, true)
		  me.AddControl(lblPreview, true)
		  me.AddControl(Canvas1, true)
		End Sub
	#tag EndEvent
	#tag Event
		Sub SplitterMoved(X As Integer, Y As Integer)
		  #pragma unused y
		  
		  static canvasHeight as uint32 = Canvas1.Height
		  static previewAnimationSpeedLabelTop as uint32 = lblPreviewAnimationSpeed.Top
		  static fpsLabelTop as uint32 = lblFPS.Top
		  static previewExplainationTop as uint32 = lblPreviewExplanation.Top
		  static animationSpeedTop as uint32 = txtAnimationSpeed.Top
		  
		  dim moveAmount as integer = X - DefSplitterLeft
		  
		  Canvas1.Height = moveAmount + canvasHeight
		  lblPreviewAnimationSpeed.Top = moveAmount + previewAnimationSpeedLabelTop
		  lblFPS.Top = moveAmount + fpsLabelTop
		  lblPreviewExplanation.Top = moveAmount + previewExplainationTop
		  txtAnimationSpeed.Top = moveAmount + animationSpeedTop
		  
		  RefreshPreview
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Canvas1
	#tag Event
		Sub Open()
		  RefreshPreview
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="AcceptFocus"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AcceptTabs"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AutoDeactivate"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="EraseBackground"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Appearance"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="InitialParent"
		Group="Position"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ProgrammaticResize"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIndex"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabPanelIndex"
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabStop"
		Visible=true
		Group="Position"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Transparent"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="UseFocusRing"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
