#tag Class
Protected Class HoverSelectItem
	#tag Method, Flags = &h0
		Sub Constructor(canvas as HoverSelectCanvas, text as string)
		  me.Canvas = canvas
		  me.Text = text
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Draw(g as graphics)
		  //draw over background
		  if me = Canvas.CurrentItem then
		    g.ForeColor = &c111111
		    g.DrawRoundRect(0, 0, Canvas.Width, Canvas.Height, 4, 4)
		  end if
		  
		  //draw item's text
		  g.TextFont = "Verdana"
		  g.TextSize = 16
		  g.ForeColor = &cFFFFFF
		  dim yTextPos as integer
		  #pragma unused yTextPos
		  g.DrawString(Text, 5, ((Canvas.Items.IndexOf(me) + 1) * 7) + 5)
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private Canvas As HoverSelectCanvas
	#tag EndProperty

	#tag Property, Flags = &h0
		Text As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Text"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
