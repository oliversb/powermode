#tag Window
Begin EmbeddedControl ObjectEditor Implements ResourceEditorInterface
   AcceptFocus     =   False
   AcceptTabs      =   True
   AutoDeactivate  =   True
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   Compatibility   =   ""
   Enabled         =   True
   EraseBackground =   True
   HasBackColor    =   False
   Height          =   523
   HelpTag         =   ""
   InitialParent   =   ""
   Left            =   0
   LockBottom      =   True
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   TabIndex        =   0
   TabPanelIndex   =   0
   TabStop         =   True
   Top             =   0
   Transparent     =   True
   UseFocusRing    =   False
   Visible         =   True
   Width           =   625
   Begin Listbox Blocks
      AutoDeactivate  =   True
      AutoHideScrollbars=   True
      Bold            =   False
      Border          =   True
      ColumnCount     =   1
      ColumnsResizable=   False
      ColumnWidths    =   ""
      DataField       =   ""
      DataSource      =   ""
      DefaultRowHeight=   -1
      Enabled         =   True
      EnableDrag      =   True
      EnableDragReorder=   False
      GridLinesHorizontal=   0
      GridLinesVertical=   0
      HasHeading      =   False
      HeadingIndex    =   -1
      Height          =   469
      HelpTag         =   ""
      Hierarchical    =   True
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      RequiresSelection=   False
      Scope           =   0
      ScrollbarHorizontal=   False
      ScrollBarVertical=   True
      SelectionType   =   0
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   23
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   155
      _ScrollOffset   =   0
      _ScrollWidth    =   -1
   End
   Begin PopupMenu Catagory
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   0
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   6
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   0
      Underline       =   False
      Visible         =   True
      Width           =   155
   End
   Begin ScrollBar VerticalCodeScroller
      AcceptFocus     =   True
      AutoDeactivate  =   True
      Enabled         =   True
      Height          =   475
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   379
      LineStep        =   110
      LiveScroll      =   True
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      Maximum         =   0
      Minimum         =   0
      PageStep        =   110
      Scope           =   0
      TabIndex        =   9
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   0
      Value           =   0
      Visible         =   True
      Width           =   17
   End
   Begin ScrollBar HorizontalCodeScroller
      AcceptFocus     =   True
      AutoDeactivate  =   True
      Enabled         =   True
      Height          =   17
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   156
      LineStep        =   110
      LiveScroll      =   True
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   False
      Maximum         =   0
      Minimum         =   0
      PageStep        =   110
      Scope           =   0
      TabIndex        =   11
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   475
      Value           =   0
      Visible         =   True
      Width           =   223
   End
   Begin LVToolBar LVToolBar1
      AcceptFocus     =   False
      AcceptTabs      =   False
      AquaBackground  =   False
      AutoDeactivate  =   True
      Backdrop        =   0
      DoubleBuffer    =   False
      EnableContextualMenu=   True
      Enabled         =   True
      EraseBackground =   True
      FontColourDefault=   False
      Height          =   39
      HelpTag         =   ""
      HideHeight      =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      mToolBarStyleChanged=   True
      RefreshLock     =   False
      Scope           =   0
      TabIndex        =   8
      TabPanelIndex   =   0
      TabStop         =   True
      ToolbarStyle    =   0
      Top             =   -51
      Transparent     =   True
      UseFocusRing    =   True
      Visible         =   True
      Width           =   45
   End
   Begin PagePanel PagePanel1
      AutoDeactivate  =   True
      Enabled         =   True
      Height          =   475
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   156
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      PanelCount      =   1
      Panels          =   ""
      Scope           =   0
      TabIndex        =   12
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   0
      Value           =   0
      Visible         =   True
      Width           =   223
      Begin RelationalFrames.RelationalFramesCanvas Code
         AcceptFocus     =   False
         AcceptTabs      =   False
         AutoDeactivate  =   True
         Backdrop        =   0
         CurrentChartPic =   0
         DoubleBuffer    =   False
         Enabled         =   True
         EraseBackground =   True
         Height          =   475
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "PagePanel1"
         Left            =   156
         LockBottom      =   True
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   True
         LockTop         =   True
         Scope           =   0
         TabIndex        =   0
         TabPanelIndex   =   1
         TabStop         =   True
         Top             =   0
         Transparent     =   True
         UseFocusRing    =   True
         Visible         =   True
         Width           =   223
      End
   End
   Begin Canvas propertiesCanvas
      AcceptFocus     =   False
      AcceptTabs      =   False
      AutoDeactivate  =   True
      Backdrop        =   0
      DoubleBuffer    =   False
      Enabled         =   True
      EraseBackground =   True
      Height          =   478
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   398
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      Scope           =   0
      TabIndex        =   13
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   14
      Transparent     =   True
      UseFocusRing    =   True
      Visible         =   True
      Width           =   217
   End
End
#tag EndWindow

#tag WindowCode
	#tag Method, Flags = &h1001
		Protected Sub AddNewObject(x as integer, y as integer, note as string)
		  AddNewObject("Note",x,y,nil,note)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub AddNewObject(p as picture, x as integer, y as integer)
		  AddNewObject("Picture Note",x,y,p)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1001
		Protected Sub AddNewObject(ObjectType as string, x as integer, y as integer, p as picture = nil, note as string = "")
		  dim frame as contentframe = AddNewObject(ObjectType,x,y,p,note,res)
		  if frame <> nil then
		    Code.AddFrame(frame)
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		 Shared Function AddNewObject(ObjectType as string, x as integer, y as integer, p as picture = nil, note as string = "", Res As ResourceObject) As ContentFrame
		  dim tempObj as BlockBase
		  
		  select case ObjectType
		    //UI
		    
		    //Object
		  case "Born"
		    tempObj = new BlockBorn(res)
		  case "Death"
		    tempObj = new BlockDeath
		    //Logic
		  case "If... Then..."
		    tempObj = new BlockIfThen
		  case "Do While"
		    tempObj = new BlockWhile
		  case "Timer"
		    tempObj = new BlockTimer
		  case "If... OR... Then..."
		    tempObj = new BlockOR
		    //Numbers
		  case "Add"
		    tempObj = new BlockAdd
		  case "Subtract"
		    tempObj = new BlockSubtract
		  case "Multiply"
		    tempObj = new BlockMultiply
		  case "Divide"
		    tempObj = new BlockDivide
		  case "Random"
		    tempObj = new BlockRandom
		  case "Remainder"
		    tempObj = new BlockRemainder
		  case "Abs (convert to positive)"
		    tempObj = new BlockAbs
		  case "Round"
		    tempObj = new BlockRound
		  case "Floor (round down)"
		    tempObj = new BlockFloor
		  case "Ceil (round up)"
		    tempObj = new BlockCeil
		  case "Square Root"
		    tempObj = new BlockSquareRoot
		  case "Sin"
		    tempObj = new BlockSin
		  case "Cos"
		    tempObj = new BlockCos
		  case "Tan"
		    tempObj = new BlockTan
		  case "Asin"
		    tempObj = new BlockAsin
		  case "Acos"
		    tempObj = new BlockAcos
		  case "Atan"
		    tempObj = new BlockAtan
		  case "Atan2"
		    tempObj = new BlockAtan2
		    //Text
		  case "Join Text"
		    tempObj = new BlockJoinText
		  case "Lowercase"
		    tempObj = new BlockLowercase
		  case "Uppercase"
		    tempObj = new BlockUppercase
		  case "Replace Text"
		    tempObj = new BlockReplaceText
		  case "Text Length"
		    tempObj = new BlockTextLength
		    //Physics
		  case "Set X"
		    tempObj = new BlockSetX
		  case "Set Y"
		    tempObj = new BlockSetY
		  case "Set Velocity X"
		    tempObj = new BlockSetVelocityX
		  case "Set Velocity Y"
		    tempObj = new BlockSetVelocityY
		  case "Set Acceleration X"
		    tempObj = new BlockSetAccelerationX
		  case "Set Acceleration Y"
		    tempObj = new BlockSetAccelerationY
		  case "Set Max Velocity X"
		    tempObj = new BlockSetMaxVelocityX
		  case "Set Max Velocity Y"
		    tempObj = new BlockSetMaxVelocityY
		  case "Set Gravity"
		    tempObj = new BlockSetGravity
		  case "Set Mass"
		    tempObj = new BlockSetMass
		  case "Set Max Angular Velocity"
		    tempObj = new BlockSetMaxAngularVelocity
		  case "Set Max Angular Velocity"
		    tempObj = new BlockSetMaxAngularVelocity
		  case "Set Rotation"
		    tempObj = new BlockSetRotation
		  case "Reset"
		    tempObj = new BlockReset
		  case "Set Collision Movement?"
		    tempObj = new BlockSetHandleMovement
		    //Graphics
		  case "Pen Down"
		    tempObj = new BlockPenDown
		  case "Pen Up"
		    tempObj = new BlockPenUp
		  case "Set Draw Point"
		    tempObj = new BlockSetPoint
		  case "Set Draw Colour"
		    tempObj = new BlockSetDrawColour
		  case "Set Draw Opacity"
		    tempObj = new BlockSetDrawOpacity
		  case "Set Line Thickness"
		    tempObj = new BlockSetDrawThickness
		  case "Clear"
		    tempObj = new BlockClearGraphics
		  case "Draw Rectangle"
		    tempObj = new BlockDrawRect
		  case "Draw Line"
		    tempObj = new BlockDrawLine
		  case "Draw Circle"
		    tempObj = new BlockDrawCircle
		  case "Draw Ellipse"
		    tempObj = new BlockDrawEllipse
		  case "Begin Fill"
		    tempObj = new BlockBeginFill
		  case "End Fill"
		    tempObj = new BlockEndFill
		  case "Set Sprite"
		    tempObj = new BlockSetSprite
		  case "Set Animation Speed"
		    tempObj = new BlockSetAnimationSpeed
		  case "Set Animation Frame"
		    tempObj = new BlockSetAnimationFrame
		  case "Start Animation"
		    tempObj = new BlockStartAnimation
		  case "Pause Animation"
		    tempObj = new BlockPauseAnimation
		  case "Resume Animation"
		    tempObj = new BlockResumeAnimation
		  case "Stop Animation"
		    tempObj = new BlockStopAnimation
		  case "Reverse Animation"
		    tempObj = new BlockReverseAnimation
		  case "Set Centre Point"
		    tempObj = new BlockSetCentrePoint
		  case "Set X Scale"
		    tempObj = new BlockSetXScale
		  case "Set Y Scale"
		    tempObj = new BlockSetYScale
		  case "Set Width"
		    tempObj = new BlockSetWidth
		  case "Set Height"
		    tempObj = new BlockSetHeight
		  case "Play Music"
		    tempObj = new BlockPlayMusic
		  case "Play Sound"
		    tempObj = new BlockPlaySound
		  case "Pause Sound"
		    tempObj = new BlockPauseSound
		  case "Resume Sound"
		    tempObj = new BlockResumeSound
		  case "Stop Sound"
		    tempObj = new BlockStopSound
		  case "Set Volume"
		    tempObj = new BlockSetVolume
		  case "Set Pan"
		    tempObj = new BlockSetPan
		    //UI
		  case "Show Message"
		    tempObj = new BlockShowMessage
		  case "Ask Question"
		    tempObj = new BlockAskQuestion
		  case "Ask Yes/No Question"
		    tempObj = new BlockAskYesNoQuestion
		  case "Ask Choice Question"
		    tempObj = new BlockAskChoiceQuestion
		    //Input
		  case "Event Mouse Down"
		    tempObj = new BlockEventMouseDown
		  case "Event Mouse Held"
		    tempObj = new BlockEventMouseHeld
		  case "Event Mouse Up"
		    tempObj = new BlockEventMouseUp
		  case "Event Mouse Enter"
		    tempObj = new BlockEventMouseEnter
		  case "Event Mouse Leave"
		    tempObj = new BlockEventMouseLeave
		  case "Event Global Mouse Down"
		    tempObj = new BlockEventGlobalMouseDown
		  case "Event Global Mouse Held"
		    tempObj = new BlockEventGlobalMouseHeld
		  case "Event Global Mouse Up"
		    tempObj = new BlockEventGlobalMouseUp
		  case "Event Global Mouse Scroll"
		    tempObj = new BlockEventGlobalMouseScroll
		  case "Event Keyboard Down"
		    tempObj = new BlockEventKeyboardDown
		  case "Event Keyboard Held"
		    tempObj = new BlockEventKeyboardHeld
		  case "Event Keyboard Up"
		    tempObj = new BlockEventKeyboardUp
		    //Test
		  case "Log"
		    tempObj = new BlockLog
		    //Misc
		  case "Spawn"
		    tempObj = new BlockSpawn
		  case "Spawn At"
		    tempObj = new BlockSpawnAt
		  case "Kill"
		    tempObj = new BlockKill
		  case "Cause Extinction"
		    tempObj = new BlockCauseExtinction
		    //Custom
		  case "Note"
		    tempObj = new BlockNote(note)
		  case "Picture Note"
		    tempObj = new BlockPictureNote(p)
		  case "Execute Script"
		    tempObj = new BlockExecuteScript
		  case "Set Variable"
		    tempObj = new BlockSetVariable
		  else
		    return nil
		  end select
		  
		  // Instantiate and init the new content frame object
		  dim cf as contentFrame
		  cf = new ContentFrame(x,y, tempObj.Width, tempObj.Title, false, tempObj.BGColor,p)
		  cf.tag = tempObj
		  cf.Canvas = res.CodeContainer
		  if tempObj.ObjectItems.Ubound <> -1 then
		    BlockBase(cf.tag).RefreshContents(cf)
		    
		    //automatically connect to Born block
		    select case tempObj
		    case isa BlockTimer
		      tempObj.ConnectToBorn(res)
		    else
		      if tempObj.IsEvent then
		        tempObj.ConnectToBorn(res)
		      end if
		    end select
		    
		  elseif p <> nil then
		    cf.AppendObject new PictureObject(tempObj.OriginalPicture)
		  end if
		  
		  return cf
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CallGotTabFocus()
		  if propertiesContainer isa conObject then
		    conObject(PropertiesContainer).UpdateForms
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1000
		Sub Constructor(r as ResourceObject)
		  Super.Constructor
		  
		  res = r
		  
		  //assign CodeContainer
		  res.CodeContainer = self.Code
		  //load all of the blocks and/or assign frames to resource
		  self.Code.frames = res.Blocks
		  if res.Blocks.Ubound = -1 then
		    //if there are no blocks then add in the defaults
		    AddNewObject("Born", 20, 20)
		    AddNewObject("Death", 240, 165)
		  else
		    //set ResParent if not set
		    for each block as contentframe in res.blocks
		      if block.tag isa BlockBorn then
		        BlockBorn(block.tag).ResParent = r
		      end if
		    next
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		 Shared Function HTTPErrorMessage(HTTPStatusCode as integer, AddNewLines as boolean = false) As string
		  dim s() as string
		  s.append "HTTP Status: "+str(HTTPStatusCode)
		  if HTTPStatusCode = 200 then
		    s.append " (This should be OK, in context to the HTTP request)"
		  end if
		  if AddNewLines then
		    s.append EndOfLine+EndOfLine
		  end if
		  return Join(s)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub PlatformRefresh()
		  // Carbon requires us to do a Refresh(), because it won't get
		  // around to redrawing it until after the drag operation has
		  // finished. However, on other platforms, we can just call
		  // Invalidate(), which is much more efficient.
		  #If TargetCarbon
		    Code.Refresh
		  #Else
		    Code.Invalidate
		  #EndIf
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub PopulateBlocks()
		  dim theCatagory as CatagoryEnum
		  
		  Blocks.DeleteAllRows
		  
		  select case Catagory.text
		  case "Logic"
		    theCatagory = CatagoryEnum.Logic
		  case "Numbers"
		    theCatagory = CatagoryEnum.Numbers
		  case "Text"
		    theCatagory = CatagoryEnum.Text
		  case "Physics"
		    theCatagory = CatagoryEnum.Physics
		  case "Graphics"
		    theCatagory = CatagoryEnum.Graphics
		  case "UI"
		    theCatagory = CatagoryEnum.UI
		  case "Sound"
		    theCatagory=  CatagoryEnum.Sound
		  case "Input"
		    theCatagory = CatagoryEnum.Input
		  case "Test"
		    theCatagory = CatagoryEnum.Test
		  case "Misc"
		    theCatagory = CatagoryEnum.Misc
		  case "Custom"
		    theCatagory = CatagoryEnum.Custom
		  end select
		  
		  BlockColour = GetBlockColour(theCatagory)
		  ProgrammingBlocks.AddBlockRows(theCatagory, Blocks)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RefreshCatagoryList()
		  dim lastItemText as string
		  
		  lastItemText = Catagory.Text
		  
		  Catagory.DeleteAllRows
		  
		  dim NotBoneless as boolean = (not res.Boneless)
		  
		  Catagory.AddRow "Logic"
		  'Catagory.AddRow "Events"
		  Catagory.AddRow "Numbers"
		  Catagory.AddRow "Text"
		  if NotBoneless then
		    Catagory.AddRow "Physics"
		    Catagory.AddRow "Graphics"
		    Catagory.AddRow "UI (COMING SOON)"
		    Catagory.AddRow "Sound"
		    Catagory.AddRow "Input"
		  end if
		  Catagory.AddRow "Test"
		  Catagory.AddRow "Misc"
		  Catagory.AddRow "Custom"
		  
		  //if this is the first time determining the ListIndex just go with item 0
		  if lastItemText = "" then
		    Catagory.ListIndex = 0
		    return
		  end if
		  //find item corrosponding to the last item
		  for i as uint32 = 0 to Catagory.ListCount - 1
		    if Catagory.List(i) = lastItemText then
		      Catagory.ListIndex = i
		      exit for
		    end if
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SetCodeCanvasHeight(sender as PopoverSize, height as double)
		  #pragma unused sender
		  Code.Height = height
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SetCodeCanvasWidth(sender as PopoverSize, width as double)
		  #pragma unused sender
		  Code.Width = width
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SetScrollbarMax()
		  HorizontalCodeScroller.Maximum = Code.Width
		  VerticalCodeScroller.Maximum = Code.Height
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ShowObjectProperties(theObject as BlockBase = nil)
		  if Code <> nil then
		    if ShowObjectPropertiesOkay then
		      if propertiesContainer <> nil then
		        propertiesContainer.Close
		      end if
		      
		      select case theObject
		      case isa BlockPenDown, isa BlockPenUp, _
		        isa BlockClearGraphics, isa BlockEndFill, _
		        isa BlockEventMouseDown, isa BlockEventMouseHeld, isa BlockEventMouseUp, isa BlockEventGlobalMouseDown, isa BlockEventGlobalMouseHeld, isa BlockEventGlobalMouseUp, _
		        isa BlockEventMouseLeave, isa BlockEventMouseEnter, isa BlockEventGlobalMouseScroll, _
		        isa BlockEventKeyboardDown, isa BlockEventKeyboardHeld, isa BlockEventKeyboardUp, _
		        isa BlockStartAnimation, isa BlockPauseAnimation, isa BlockResumeAnimation, isa BlockStopAnimation, isa BlockReverseAnimation, _
		        isa BlockDisplayScore, isa BlockDisplayLives, isa BlockDisplayHealth, isa BlockDisplayHealthbar, isa BlockHideScore, isa BlockHideLives, isa BlockHideHealth, isa BlockHideHealthbar
		        //no container
		        return
		        
		        //Default blocks
		      case isa BlockObject, nil
		        propertiesContainer = new conObject(res)
		        //Logic
		      case isa BlockIfThen
		        propertiesContainer = new conIfThen
		      case isa BlockWhile
		        propertiesContainer = new conWhile
		      case isa BlockTimer
		        propertiesContainer = new conTimer
		      case isa BlockOR
		        propertiesContainer = new conOR
		        //Numbers
		      case isa BlockAdd
		        propertiesContainer = new conAdd
		      case isa BlockSubtract
		        propertiesContainer = new conSubtract
		      case isa BlockMultiply
		        propertiesContainer = new conMultiply
		      case isa BlockDivide
		        propertiesContainer = new conDivide
		      case isa BlockRandom
		        propertiesContainer = new conRandom
		      case isa BlockRemainder
		        propertiesContainer = new conRemainder
		      case isa BlockRound
		        propertiesContainer = new conRound
		      case isa BlockAbs
		        propertiesContainer = new conAbs
		      case isa BlockFloor
		        propertiesContainer = new conFloor
		      case isa BlockCeil
		        propertiesContainer = new conCeil
		      case isa BlockSquareRoot
		        propertiesContainer = new conSquareRoot
		      case isa BlockSin
		        propertiesContainer = new conSin
		      case isa BlockTan
		        propertiesContainer = new conTan
		      case isa BlockCos
		        propertiesContainer = new conCos
		      case isa BlockAsin
		        propertiesContainer = new conAsin
		      case isa BlockAtan, isa BlockAtan2
		        propertiesContainer = new conAtan
		      case isa BlockAcos
		        propertiesContainer = new conAcos
		        //Text
		      case isa BlockJoinText
		        propertiesContainer = new conJoinText
		      case isa BlockLowercase
		        propertiesContainer = new conLowercase
		      case isa BlockUppercase
		        propertiesContainer = new conUppercase
		      case isa BlockReplaceText
		        propertiesContainer = new conReplaceText
		      case isa BlockTextLength
		        propertiesContainer = new conTextLength
		        //Physics
		      case isa BlockSetX, isa BlockSetY, _
		        isa BlockSetVelocityX, isa BlockSetVelocityY, _
		        isa BlockSetAccelerationX, isa BlockSetAccelerationY, _
		        isa BlockSetMaxVelocityX, isa BlockSetMaxVelocityY, _
		        isa BlockSetGravity, _
		        isa BlockSetMass, _
		        isa BlockSetMaxAngularVelocity, _
		        isa BlockSetRotation, _
		        isa BlockReset, _
		        isa BlockSetHandleMovement
		        propertiesContainer = new conSetMovementBasic
		        //Graphics
		      case isa BlockSetPoint
		        propertiesContainer = new conSetPoint
		      case isa BlockSetDrawColour
		        propertiesContainer = new conSetDrawColour
		      case isa BlockSetDrawOpacity
		        propertiesContainer = new conSetDrawOpacity
		      case isa BlockSetDrawThickness
		        propertiesContainer = new conSetDrawThickness
		      case isa BlockDrawRect
		        propertiesContainer = new conDrawRect
		      case isa BlockDrawLine
		        propertiesContainer = new conDrawLine
		      case isa BlockDrawCircle
		        propertiesContainer = new conDrawCircle
		      case isa BlockDrawEllipse
		        //ellipse defined the same as a rectangle
		        propertiesContainer = new conDrawRect
		      case isa BlockBeginFill
		        propertiesContainer = new conBeginFill
		      case isa BlockSetSprite
		        propertiesContainer = new conSetSprite
		      case isa BlockSetAnimationSpeed, isa BlockSetAnimationFrame, isa BlockSetXScale, isa BlockSetYScale, isa BlockSetWidth, isa BlockSetHeight
		        propertiesContainer = new conSetAnimationSpeed
		      case isa BlockSetCentrePoint
		        propertiesContainer = new conSetPoint
		        //UI
		      case isa BlockShowMessage
		        propertiesContainer = new conShowMessage
		      case isa BlockAskQuestion
		        propertiesContainer = new conAskQuestion
		      case isa BlockAskYesNoQuestion
		        propertiesContainer = new conAskYesNoQuestion
		      case isa BlockAskChoiceQuestion
		        PropertiesContainer = new conAskChoiceQuestion
		        //Sound
		      case isa BlockPlayMusic
		        propertiesContainer = new conPlayMusic
		      case isa BlockPlaySound
		        PropertiesContainer = new conPlaySound
		      case isa BlockPauseSound, isa BlockResumeSound, isa BlockStopSound
		        propertiesContainer = new conGroupID
		      case isa BlockSetVolume
		        propertiesContainer = new conSetVolume
		      case isa BlockSetPan
		        propertiesContainer = new conSetVolume
		        //Test
		      case isa BlockLog
		        propertiesContainer = new conLog
		        //Misc
		      case isa BlockSpawn
		        propertiesContainer = new conSpawn
		      case isa BlockSpawnAt
		        propertiesContainer = new conSpawnAt
		      case isa BlockKill
		        propertiesContainer = new conKill
		      case isa BlockCauseExtinction
		        propertiesContainer = new conCauseExtinction
		        //Custom
		      case isa BlockNote
		        propertiesContainer = new conNote
		      case isa BlockExecuteScript
		        propertiesContainer = new conExecuteScript
		      case isa BlockPictureNote
		        propertiesContainer = new conPictureNote
		      case isa BlockSetVariable
		        propertiesContainer = new conSetVariable
		      end select
		      
		      if not propertiesContainer isa BlockObject then
		        PropertiesContainer.Constructor(res, Code.selectedFrame, self)
		        propertiesContainer.EmbedWithin(propertiesCanvas,0,0, propertiesCanvas.width, propertiesCanvas.height)
		        propertiesContainer.Invalidate
		      end if
		    end if
		  end if
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		BlockColour As Color
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected CurX As UInt32
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected CurY As UInt32
	#tag EndProperty

	#tag Property, Flags = &h21
		Private DoneStartAction As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected HTTPStatusCode As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		LastOpenResourceTabValue As Uint32
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mHorizontalScrollBarLast As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mVerticalScrollBarLast As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private MyScroll As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private propertiesContainer As PropertiesContainer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected res As ResourceObject
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected ShowObjectPropertiesOkay As Boolean = true
	#tag EndProperty


#tag EndWindowCode

#tag Events Blocks
	#tag Event
		Function CellTextPaint(g As Graphics, row As Integer, column As Integer, x as Integer, y as Integer) As Boolean
		  #pragma Unused column
		  #pragma Unused x
		  #pragma Unused y
		  
		  'Black Border
		  g.ForeColor = RGB(0,0,0)
		  g.FillRect(0,2,g.Width - 7, g.Height - 4)
		  
		  'Hilight stripe
		  g.ForeColor = rgb(BlockColour.Red+20,BlockColour.Green+20,BlockColour.Blue+20)
		  g.FillRect(1,3,g.Width - 9, 1)
		  
		  'Main fill
		  g.ForeColor = BlockColour
		  g.FillRect(1, 4, g.Width - 9, g.Height - 7)
		  
		  'g.Gradient(1,3,10,26)', BlockColour, rgb(BlockColour.Red-20,BlockColour.Green-20,BlockColour.Blue-20)
		  
		  // Draw the text
		  g.TextSize = 10
		  g.ForeColor = rgb(0,0,0)
		  if TargetLinux = true then
		    g.DrawString(me.List(row), 3, g.Height - 7)
		  ElseIf TargetWin32 = true then
		    g.DrawString(me.List(row), 3, g.Height - 5)
		  elseif TargetMacOS = true then
		    g.DrawString(me.List(row), 3, g.Height - 5)
		  end if
		  
		  g.ForeColor = rgb(255,255,255)
		  if TargetLinux = true then
		    g.DrawString(me.List(row), 3, g.Height - 6)
		  ElseIf TargetWin32 = true then
		    g.DrawString(me.List(row), 3, g.Height - 4)
		  elseif TargetMacOS = true then
		    g.DrawString(me.List(row), 3, g.Height - 4)
		  end if
		  
		  Return true
		End Function
	#tag EndEvent
	#tag Event
		Function DragRow(drag As DragItem, row As Integer) As Boolean
		  Drag.PrivateRawData("text") = Me.List(Row)
		  
		  Return true
		End Function
	#tag EndEvent
	#tag Event
		Function ConstructContextualMenu(base as MenuItem, x as Integer, y as Integer) As Boolean
		  #pragma unused x
		  #pragma unused y
		  
		  dim NewChild As MenuItem
		  
		  if me.ListIndex <> -1 then
		    NewChild = new MenuItem
		    NewChild.Text = "Help for " + me.List(me.ListIndex)
		    NewChild.Tag = "Help"
		    NewChild.Icon = Icon("Help")
		    base.append NewChild
		  end if
		End Function
	#tag EndEvent
	#tag Event
		Function ContextualMenuAction(hitItem as MenuItem) As Boolean
		  if hitItem.tag = "Help" then
		    //'Help for ' counts
		    WinManual.LoadHelp(hitItem.Text.Mid(10))
		  end if
		End Function
	#tag EndEvent
	#tag Event
		Sub ExpandRow(row As Integer)
		  dim cell as string = me.cell(row, 0)
		  
		  select case Catagory.Text
		  case "Graphics"
		    select case cell
		    case "Sprite/Animation"
		      blocks.AddRow "Set Sprite"
		      blocks.AddRow "Set Animation Speed"
		      blocks.AddRow "Set Animation Frame"
		      blocks.AddRow "Start Animation"
		      blocks.AddRow "Pause Animation"
		      blocks.AddRow "Resume Animation"
		      blocks.AddRow "Stop Animation"
		      blocks.AddRow "Reverse Animation"
		      blocks.AddRow "Set Centre Point"
		      blocks.AddRow "Set X Scale"
		      blocks.AddRow "Set Y Scale"
		      blocks.AddRow "Set Width"
		      blocks.AddRow "Set Height"
		    end select
		  case "Input"
		    select case cell
		    case "Events"
		      blocks.AddRow "Event Mouse Down"
		      blocks.AddRow "Event Mouse Held"
		      blocks.AddRow "Event Mouse Up"
		      blocks.AddRow "Event Mouse Enter"
		      blocks.AddRow "Event Mouse Leave"
		      blocks.AddRow "Event Global Mouse Down"
		      blocks.AddRow "Event Global Mouse Held"
		      blocks.AddRow "Event Global Mouse Up"
		      blocks.AddRow "Event Global Mouse Scroll"
		      blocks.AddRow "Event Keyboard Down"
		      blocks.AddRow "Event Keyboard Held"
		      blocks.AddRow "Event Keyboard Up"
		      'blocks.AddRow "Event Gamepad Down"
		      'blocks.AddRow "Event Gamepad Held"
		      'blocks.AddRow "Event Gamepad Up"
		    end select
		  end select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Catagory
	#tag Event
		Sub Change()
		  PopulateBlocks
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  RefreshCatagoryList
		  
		  PopulateBlocks
		End Sub
	#tag EndEvent
	#tag Event
		Sub GotFocus()
		  RefreshCatagoryList
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events VerticalCodeScroller
	#tag Event
		Sub ValueChanged()
		  // Calculate the delta that the scrollbar was
		  // moved and scroll the canvas accordingly.
		  
		  Dim delta As Integer
		  
		  delta = mVerticalScrollBarLast - Me.Value
		  
		  mYScroll = mYScroll + delta
		  Code.Top = Code.Top + delta
		  
		  mVerticalScrollBarLast = Me.Value
		  
		  Code.Invalidate
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events HorizontalCodeScroller
	#tag Event
		Sub ValueChanged()
		  // Calculate the delta that the scrollbar was
		  // moved and scroll the canvas accordingly.
		  
		  Dim delta As Integer
		  
		  delta = mHorizontalScrollBarLast - Me.Value
		  
		  mYScroll = mYScroll + delta
		  Code.Left = Code.Left + delta
		  
		  mHorizontalScrollBarLast = Me.Value
		  
		  Code.Invalidate
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events LVToolBar1
	#tag Event
		Sub Open()
		  me.ToolBarStyle = LVToolBar.STYLE_SMALL_ICONS_LABEL
		  dim sep as LVStandardToolBarItem
		  #pragma unused sep
		  dim btn as LVToolBarItem
		  
		  btn = new LVToolBarItem
		  btn.Caption = "Variable Manager"
		  btn.HelpTag = "Opens variable management system"
		  btn.Image = nil
		  btn.Visible = true
		  me.AddButton(btn)
		  
		  btn = new LVToolBarItem
		  btn.Caption = "Set Code Canvas Size"
		  btn.HelpTag = "Resize code canvas"
		  btn.Image = nil
		  btn.Visible = true
		  me.AddButton(btn)
		End Sub
	#tag EndEvent
	#tag Event
		Sub Action(selectedToolBarItem As LVToolBarItem)
		  select case selectedToolBarItem.Caption
		  case "Variable Manager"
		    propertiesContainer.Close
		    propertiesContainer = new VariableManager
		    propertiesContainer.res = self.res
		    propertiesContainer.EmbedWithin(propertiesCanvas, 0, 0, propertiesCanvas.width, propertiesCanvas.height)
		    //deselect all so that when you select the currently selected block then the item can change from the variable manager
		    Code.DeselectAll
		  case "Set Code Canvas Size"
		    dim MyPopoverCodeCanvasSize as new PopoverSize
		    //set default fields
		    MyPopoverCodeCanvasSize.txtWidth.Text = Code.Width.ToText
		    MyPopoverCodeCanvasSize.txtHeight.Text = Code.Height.ToText
		    
		    //set code canvas size
		    AddHandler MyPopoverCodeCanvasSize.SetWidth, AddressOf SetCodeCanvasWidth
		    AddHandler MyPopoverCodeCanvasSize.SetHeight, AddressOf SetCodeCanvasHeight
		    
		    MyPopoverCodeCanvasSize.displayPopoverAt(System.MouseX, System.MouseY,-1, self)
		  end select
		End Sub
	#tag EndEvent
	#tag Event
		Sub ToolBarChanged()
		  if DoneStartAction = false then
		    DoneStartAction = true
		  else
		    //show properties for the object by default
		    ShowObjectProperties
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Code
	#tag Event
		Sub DropObject(obj As DragItem, action As Integer)
		  #pragma Unused action
		  
		  'if TargetLinux then
		  CurX = MouseX - me.window.left - me.left
		  CurY = MouseY - me.window.top - me.top
		  'end if
		  
		  'if frmProject.me.frames.Ubound > 9 then
		  'if modPrefs.isRegGood = false then
		  'modPrefs.ShowUnregisteredDialog
		  'Return
		  'end if
		  'end if
		  If Obj.PictureAvailable then
		    AddNewObject(obj.picture,curx,cury)
		  elseif Obj.FolderItemAvailable then
		    AddNewObject(Picture.Open(obj.FolderItem),curx,cury)
		  elseif obj.PrivateRawData("text") <> "" then
		    AddNewObject(obj.PrivateRawData("text"), curx, cury)
		  elseif obj.Text.Left(7) = "http://" or obj.Text.Left(8) = "https://" then
		    Dim socket As New HTTPSocket
		    Dim data As String = socket.Get(obj.text, 5) ' using synchronous GET
		    HTTPStatusCode= socket.HTTPStatusCode
		    select case HTTPStatusCode
		    case 200 ' OK
		      ' Picture.FromData creates a new picture from raw binary data
		      if data <> "" then
		        Dim p As Picture = Picture.FromData(data)
		        if p <> nil then
		          AddNewObject(p,curx,cury)
		        else
		          ReportError 0,HTTPStatusCode
		        end if
		      else
		        ReportError 0,HTTPStatusCode
		      end if
		    else
		      ReportError 0,HTTPStatusCode
		    end select
		    
		  else
		    AddNewObject(curx,cury,obj.text)
		  end if
		  
		  me.SelectFrame(me.Frames(me.Frames.Ubound)) //select last frame
		  me.Refresh
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseMove(X As Integer, Y As Integer)
		  curx = x
		  cury = y
		End Sub
	#tag EndEvent
	#tag Event
		Function ShouldAllowLink(fromObject as ContentObject, toObject as ContentObject) As Boolean
		  #pragma unused fromObject
		  #pragma unused toObject
		  
		  if fromObject = nil then
		    Return false
		  end if
		  
		  if toObject = nil then
		    Return false
		  end if
		  
		  if fromObject.ShowOutput = false then
		    Return false
		  end if
		  
		  if toObject.ShowInput = false then
		    Return false
		  end if
		  
		  //Allow All Links
		  DocFile.ModifiedDoc = true
		  Return true
		End Function
	#tag EndEvent
	#tag Event
		Sub Open()
		  me.AcceptRawDataDrop("text")
		  me.AcceptPictureDrop
		  'MainEditor.AcceptFileDrop("image/jpeg")
		  'MainEditor.AcceptFileDrop("image/png")
		  'MainEditor.AcceptFileDrop("image/gif")
		  me.AcceptTextDrop
		  
		  SetScrollbarMax
		End Sub
	#tag EndEvent
	#tag Event
		Sub FrameSelected(frame as contentFrame)
		  if frame <> nil then
		    ShowObjectProperties(BlockBase(frame.tag))
		  else
		    if propertiesContainer <> nil then
		      ShowObjectProperties
		    end if
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Function ContextualMenuAction(hitItem as MenuItem) As Boolean
		  if me.selectedFrame <> nil then
		    Select Case hititem.text
		    case "Remove Block"
		      me.DeleteFrame(me.selectedFrame)
		      me.Invalidate
		    case "Remove Blocks"
		      ShowObjectPropertiesOkay = false
		      dim frame as contentframe
		      for f as uint32 = me.frames.ubound downto 0
		        frame = me.frames(f)
		        if (frame.Selected or frame = me.selectedFrame) and (not frame.tag isa BlockObject) then
		          me.DeleteFrame(frame)
		        end if
		      next
		      ShowObjectPropertiesOkay = true
		      me.Invalidate
		      //show object properties for default
		      ShowObjectProperties
		    case else
		      if hitItem.Text.InStr("Remove Link") > 0 then // Remove some links
		        dim theCO as integer
		        dim theOutput as integer
		        theCO = val(NthField(hitItem.Tag, "*", 1))
		        theOutput = val(NthField(hitItem.Tag, "*", 2))
		        me.selectedFrame.RemoveLink(theCO, theOutput)
		        me.Refresh
		        DocFile.ModifiedDoc = true
		      end if
		    end select
		  end if
		End Function
	#tag EndEvent
	#tag Event
		Function ConstructContextualMenu(base as MenuItem, x as Integer, y as Integer) As Boolean
		  #pragma Unused X
		  #pragma Unused Y
		  
		  if me.selectedFrame <> nil then
		    dim i as integer
		    dim j as integer
		    
		    if me.selectedFrame.tag isa BlockObject then
		    else
		      dim item as menuitem
		      
		      item = new MenuItem("Remove Block")
		      item.Icon = Icon("Delete")
		      base.append(item)
		      
		      item = new MenuItem("Remove Blocks")
		      item.Icon = Icon("Delete")
		      base.append(item)
		      
		      base.append(New MenuItem(MenuItem.TextSeparator))
		    end if
		    
		    'dim tester as integer
		    'for tester = 0 to me.selectedFrame.contents.Ubound
		    'base.append(New MenuItem(TextObject(me.selectedFrame.contents(tester)).text))
		    'next
		    
		    if me.selectedFrame.contents.Ubound > -1 then
		      for i = 0 to me.selectedFrame.contents.Ubound
		        if me.selectedFrame.contents(i).outputs.Ubound > -1 then
		          for j = 0 to me.selectedFrame.contents(i).outputs.Ubound
		            base.append(New MenuItem("Remove Link : " + _
		            TextObject(me.selectedFrame.contents(i)).text + " -> " + _
		            TextObject(me.selectedFrame.contents(i).outputs(j)).text))
		            base.Item(base.Count - 1).Tag  = str(i) + "*" + str(j)
		          next
		        end if
		      next
		    end if
		    
		    
		    Return True  //display the contextual menu
		    
		  end if
		End Function
	#tag EndEvent
	#tag Event
		Function MouseWheel(X As Integer, Y As Integer, deltaX as Integer, deltaY as Integer) As Boolean
		  #pragma unused x
		  #pragma unused y
		  
		  HorizontalCodeScroller.Value = HorizontalCodeScroller.Value + deltaX
		  VerticalCodeScroller.Value = VerticalCodeScroller.Value + deltaY
		End Function
	#tag EndEvent
	#tag Event
		Sub MouseUp()
		  dim boundry as NESW
		  for each f as RelationalFrames.ContentFrame in me.frames
		    if f.TouchingBounds(boundry) then
		      select case boundry
		       case NESW.East
		        //increase scrollbar size horizontally
		        me.Width = f.Right + f.width
		      case NESW.SouthEast
		        //increase scrollbar size horizontally and vertically
		        me.Width = f.Right + f.width
		        me.Height = f.Bottom + f.height
		      case NESW.South
		        //increase scrollbar size vertically
		        me.Height = f.Bottom + f.height
		      end select
		      exit for
		    end if
		  next
		  
		  SetScrollbarMax
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="AcceptFocus"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AcceptTabs"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AutoDeactivate"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BlockColour"
		Group="Behavior"
		InitialValue="&c000000"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="EraseBackground"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Appearance"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="InitialParent"
		Group="Position"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ProgrammaticResize"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIndex"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabPanelIndex"
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabStop"
		Visible=true
		Group="Position"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Transparent"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="UseFocusRing"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
