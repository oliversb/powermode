#tag Window
Begin Window PaintWindow
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   True
   Frame           =   1
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   550
   ImplicitInstance=   True
   LiveResize      =   True
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   True
   MaxWidth        =   32000
   MenuBar         =   1501559397
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   2
   Resizeable      =   True
   Title           =   "Frame Editor"
   Visible         =   True
   Width           =   830
   Begin Timer Timer1
      Height          =   "32"
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   0
      LockedInPosition=   False
      Mode            =   2
      Period          =   1
      Scope           =   "0"
      TabPanelIndex   =   "0"
      Top             =   0
      Width           =   "32"
   End
   Begin Rectangle ColourBox
      AutoDeactivate  =   True
      BorderWidth     =   1
      BottomRightColor=   &c66666600
      Enabled         =   True
      FillColor       =   &c00000000
      Height          =   32
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      Scope           =   "2"
      TabIndex        =   8
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   191
      TopLeftColor    =   &c66666600
      Visible         =   True
      Width           =   32
   End
   Begin BevelButton ToolBtn
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   2
      Caption         =   ""
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   24
      HelpTag         =   ""
      Icon            =   1389322546
      IconAlign       =   1
      IconDX          =   0
      IconDY          =   0
      Index           =   4
      InitialParent   =   ""
      Italic          =   False
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      MenuValue       =   0
      Scope           =   "0"
      TabIndex        =   14
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   163
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   32
   End
   Begin BevelButton ToolBtn
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   2
      Caption         =   ""
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   24
      HelpTag         =   ""
      Icon            =   849345544
      IconAlign       =   1
      IconDX          =   0
      IconDY          =   0
      Index           =   3
      InitialParent   =   ""
      Italic          =   False
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      MenuValue       =   0
      Scope           =   "0"
      TabIndex        =   12
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   117
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   32
   End
   Begin BevelButton ToolBtn
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   2
      Caption         =   ""
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   23
      HelpTag         =   ""
      Icon            =   624789712
      IconAlign       =   1
      IconDX          =   0
      IconDY          =   0
      Index           =   0
      InitialParent   =   ""
      Italic          =   False
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      MenuValue       =   0
      Scope           =   "2"
      TabIndex        =   9
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   48
      Underline       =   False
      Value           =   True
      Visible         =   True
      Width           =   32
   End
   Begin BevelButton ToolBtn
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   2
      Caption         =   ""
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   24
      HelpTag         =   ""
      Icon            =   1793156207
      IconAlign       =   1
      IconDX          =   0
      IconDY          =   0
      Index           =   1
      InitialParent   =   ""
      Italic          =   False
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      MenuValue       =   0
      Scope           =   "0"
      TabIndex        =   10
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   71
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   32
   End
   Begin CanvasScrollbar CanvasScrollbar2
      AcceptFocus     =   True
      AutoDeactivate  =   True
      Enabled         =   True
      Height          =   16
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   52
      LineStep        =   1
      LiveScroll      =   True
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   False
      Maximum         =   100
      Minimum         =   0
      PageStep        =   20
      Scope           =   "0"
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   534
      Value           =   0
      Visible         =   True
      Width           =   752
   End
   Begin CanvasScrollbar CanvasScrollbar1
      AcceptFocus     =   True
      AutoDeactivate  =   True
      Enabled         =   True
      Height          =   490
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   805
      LineStep        =   1
      LiveScroll      =   True
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      Maximum         =   100
      Minimum         =   0
      PageStep        =   20
      Scope           =   "0"
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   40
      Value           =   0
      Visible         =   True
      Width           =   16
   End
   Begin BevelButton ToolBtn
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   2
      Caption         =   ""
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   24
      HelpTag         =   ""
      Icon            =   1418533084
      IconAlign       =   1
      IconDX          =   0
      IconDY          =   0
      Index           =   2
      InitialParent   =   ""
      Italic          =   False
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      MenuValue       =   0
      Scope           =   "0"
      TabIndex        =   11
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   94
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   32
   End
   Begin UnsignedIntField TextField1
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CheckIfInteger  =   False
      CheckIfPositive =   False
      CheckText       =   "0"
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Default         =   "0"
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InFocus         =   False
      Italic          =   False
      Left            =   0
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ProgrammaticalChange=   False
      ReadOnly        =   False
      Scope           =   "0"
      TabIndex        =   15
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "1"
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextIsOk        =   True
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   271
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   47
   End
   Begin Canvas SizePreview
      AcceptFocus     =   False
      AcceptTabs      =   False
      AutoDeactivate  =   True
      Backdrop        =   0
      DoubleBuffer    =   True
      Enabled         =   True
      EraseBackground =   False
      Height          =   40
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   16
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   226
      Transparent     =   True
      UseFocusRing    =   True
      Visible         =   True
      Width           =   47
   End
   Begin BevelButton ToolBtn
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   2
      Caption         =   "T"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   24
      HelpTag         =   ""
      Icon            =   0
      IconAlign       =   1
      IconDX          =   0
      IconDY          =   0
      Index           =   5
      InitialParent   =   ""
      Italic          =   False
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      MenuValue       =   0
      Scope           =   "0"
      TabIndex        =   13
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   140
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   32
   End
   Begin UnsignedIntField TextField11
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CheckIfInteger  =   False
      CheckIfPositive =   False
      CheckText       =   "0"
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Default         =   "0"
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InFocus         =   False
      Italic          =   False
      Left            =   105
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ProgrammaticalChange=   False
      ReadOnly        =   False
      Scope           =   "0"
      TabIndex        =   17
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "12"
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextIsOk        =   True
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   6
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   37
   End
   Begin Label Label1
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   52
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   18
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Font Size:"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   7
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   50
   End
   Begin Label Label2
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   154
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   19
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Font:"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   6
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   30
   End
   Begin ComboBox PopupMenu1
      AutoComplete    =   False
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      Index           =   -2147483648
      InitialValue    =   "Arial"
      Italic          =   False
      Left            =   187
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   20
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   5
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   92
   End
   Begin BevelButton BtnBold
      AcceptFocus     =   True
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   1
      Caption         =   "B"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   ""
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   291
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   "0"
      TabIndex        =   22
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   6
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   33
   End
   Begin BevelButton BtnItalic
      AcceptFocus     =   True
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   1
      Caption         =   "I"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   ""
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   324
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   "0"
      TabIndex        =   23
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   6
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   33
   End
   Begin BevelButton BtnUnderline
      AcceptFocus     =   True
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   1
      Caption         =   "U"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   ""
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   357
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   "0"
      TabIndex        =   24
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   6
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   33
   End
   Begin PushButton PushButton1
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Apply Effect"
      Default         =   False
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   402
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   25
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   6
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin ImageCanvas ImageCanvas1
      AcceptFocus     =   False
      AcceptTabs      =   False
      AutoDeactivate  =   True
      Backdrop        =   0
      ContentHeight   =   0
      ContentWidth    =   0
      DoubleBuffer    =   True
      Enabled         =   True
      EraseBackground =   False
      Height          =   490
      HelpTag         =   ""
      Image           =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   52
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Scope           =   "0"
      ScrollX         =   0
      ScrollY         =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   40
      Transparent     =   True
      UseFocusRing    =   True
      Visible         =   True
      Width           =   749
      Zoom            =   0.0
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Close()
		  PWindow.Visible = false
		End Sub
	#tag EndEvent

	#tag Event
		Function KeyDown(Key As String) As Boolean
		  if GraphicObject <> nil then
		    GraphicTextObject(GraphicObject).HandleKeyControls(key)
		    ImageCanvas1.Invalidate
		  end if
		End Function
	#tag EndEvent

	#tag Event
		Function MouseDown(X As Integer, Y As Integer) As Boolean
		  #pragma unused x
		  #pragma unused y
		  
		  return true
		End Function
	#tag EndEvent

	#tag Event
		Sub MouseDrag(X As Integer, Y As Integer)
		  #pragma unused x
		  #pragma unused y
		  
		  RightClick = IsContextualClick
		End Sub
	#tag EndEvent

	#tag Event
		Sub MouseUp(X As Integer, Y As Integer)
		  #pragma unused x
		  #pragma unused y
		  
		  RightClick = IsContextualClick //instead of just setting to false, I want to make sure that it is the contextual
		  'click that is up
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  //attach scrollbars to canvas
		  ImageCanvas1.VScroll = CanvasScrollbar1
		  ImageCanvas1.HScroll = CanvasScrollBar2
		  
		  PWindow.Visible = true
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Sub DrawLine(g as graphics, x1 as integer, y1 as integer, x2 as integer, y2 as integer, col as Color, pensize as Integer = 1)
		  dim w as integer = x2 - x1
		  dim h as integer = y2 - y1
		  
		  g.ForeColor = col
		  
		  if h = 0 and w = 0 then
		    
		    DrawRect g, x1, y1, pensize
		    return
		    
		  elseif abs(w) > abs(h) then
		    
		    if w < 0 then
		      w = -w
		      h = -h
		      x1 = x2
		      y1 = y2
		    end
		    
		    dim vstep as double = h/w
		    for i as integer = 0 to w
		      dim x as integer = x1 + i
		      dim y as integer = y1 + round(i*vstep)
		      DrawRect g, x, y, pensize
		    next
		    
		  else
		    
		    if h < 0 then
		      w = -w
		      h = -h
		      x1 = x2
		      y1 = y2
		    end
		    
		    dim hstep as double = w/h
		    for i as integer = 0 to h
		      dim x as integer = x1 + round(i*hstep)
		      dim y as integer = y1 + i
		      DrawRect g, x, y, pensize
		    next
		    
		  end
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub DrawRect(g as graphics, x as Integer, y as Integer, size as Integer)
		  g.FillRect x - size/2, y - size/2, size, size
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Fill(x as integer, y as integer, col as Color)
		  //if in bounds
		  if x > 0 and x < ImageCanvas1.Image.Width _
		    and y > 0 and y < ImageCanvas1.Image.Height then
		    
		    //if pixel is partially or fully opaque
		    if ImageCanvas1.Image.Mask.RGBSurface.Pixel(x, y) <> &cFFFFFF  then
		      ImageCanvas1.Image.RGBSurface.FloodFill(x, y, col)
		    else
		      dim original as picture
		      dim w, h as uint32
		      original = ImageCanvas1.Image
		      w = original.width
		      h = original.height
		      
		      dim resultPic, ffPic as picture
		      resultPic = new picture(w, h, 32)
		      ffPic = new picture(w, h, 32)
		      
		      dim ffMask as picture
		      ffMask = original.Mask
		      'resultPic.ApplyMask(ffMask)
		      
		      //draw the unmasked original picture on the result picture
		      ffPic.Graphics.DrawPicture(ffMask)
		      
		      //build masked floodfill picture
		      ffPic.Graphics.DrawPicture(ffMask)
		      //and on the mask of the floodfill picture
		      ffPic.Mask.Graphics.DrawPicture(ffMask)
		      
		      //floodfill on this newly built floodfill picture
		      ffPic.RGBSurface.FloodFill(x, y, col)
		      
		      //invert mask of floodfilled picture
		      ImagePlayEffectsLibrary.Invert(ffPic)
		      
		      //draw floodfilled picture on result picture
		      resultPic.Graphics.DrawPicture(ffPic)
		      
		      ImageCanvas1.Image = resultPic
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SetCursor()
		  dim x as integer = ImageCanvas1.MouseX
		  dim y as integer = ImageCanvas1.MouseY
		  #pragma unused x
		  #pragma unused y
		  
		  dim PendingMouseCursor as MouseCursor
		  
		  if ImageCanvas1.InCanvas(ImageCanvas1.MouseX,ImageCanvas1.MouseY) then
		    
		    select case private_tool
		    case Tool.Pencil
		      PendingMouseCursor = PencilCursor
		    case Tool.Eraser
		      PendingMouseCursor = EraserCursor
		    case Tool.Bucket
		      PendingMouseCursor = BucketCursor
		    case Tool.Pipette
		      PendingMouseCursor = PipetteCursor
		    case Tool.Text
		      static c as new mousecursor(TextCursor, 6, 6)
		      PendingMouseCursor = c
		    case Tool.Magnify
		      if AltMode then
		        PendingMouseCursor = System.Cursors.MagnifySmaller
		      else
		        PendingMouseCursor = System.Cursors.MagnifyLarger
		      end
		    end
		    
		  else
		    
		    PendingMouseCursor = System.Cursors.StandardPointer
		    
		  end
		  
		  if MouseCursor <> PendingMouseCursor then
		    MouseCursor = PendingMouseCursor
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetTool(tool as Tool)
		  private_tool = tool
		  SetCursor
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Show(c as FGThumbnailCanvas)
		  theCanvas = c
		  
		  if ImageCanvas1.Image = nil then
		    ImageCanvas1.Image = new picture(PWindow.CanvasWidth, PWindow.CanvasHeight, 32)
		    ImageCanvas1.Image.ApplyMask(ImageCanvas1.Image.ClonePicture)
		    theCanvas.SelectedThumbnail.SourceImage = ImageCanvas1.Image
		  end if
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private AltMode As boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		Bold As Boolean
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return ColourBox.FillColor
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  ColourBox.FillColor = value
			End Set
		#tag EndSetter
		Colour As color
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		Font As string
	#tag EndProperty

	#tag Property, Flags = &h0
		FontSize As Uint32 = 12
	#tag EndProperty

	#tag Property, Flags = &h0
		GraphicObject As GraphicObject
	#tag EndProperty

	#tag Property, Flags = &h0
		h As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		Italic As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSize As Uint32 = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		PreviewNormalZoomLevel As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private private_tool As Tool
	#tag EndProperty

	#tag Property, Flags = &h21
		Private RightClick As Boolean
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  if PreviewNormalZoomLevel then
			    return mSize
			  else
			    return mSize * ImageCanvas1.Zoom
			  end if
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mSize = value
			End Set
		#tag EndSetter
		Size As Uint32
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private theCanvas As FGThumbnailCanvas
	#tag EndProperty

	#tag Property, Flags = &h0
		Underline As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		w As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private x As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private y As Integer
	#tag EndProperty


#tag EndWindowCode

#tag Events Timer1
	#tag Event
		Sub Action()
		  AltMode = Keyboard.AsyncAltKey or RightClick
		  RightClick = false
		  SetCursor
		  
		  '#pragma BreakOnExceptions false
		  try
		    if ImageCanvas1.Image.Width <> PWindow.CanvasWidth or _
		      ImageCanvas1.Image.Height <> PWindow.CanvasHeight then
		      
		      dim p as new picture(PWindow.CanvasWidth,PWindow.CanvasHeight)
		      
		      'if not(p.graphics is nil) then
		      '//add transparent mask to part of image
		      ''p.Mask.Graphics.ForeColor = &cFFFFFF
		      ''p.Mask.Graphics.FillRect 0,0,PWindow.CanvasWidth,PWindow.CanvasHeight
		      'else
		      p.Graphics.DrawPicture(ImageCanvas1.Image,0,0) //draw picture over resized picture
		      'end if
		      ImageCanvas1.Image = p
		    end if
		  catch e as OutOfBoundsException
		  end try
		  
		  DocFile.CurrentPaintImage = ImageCanvas1.Image
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ColourBox
	#tag Event
		Function MouseDown(X As Integer, Y As Integer) As Boolean
		  #pragma unused x
		  #pragma unused y
		  
		  dim b as Boolean
		  dim c as Color
		  b = SelectColor(c,"Select a Colour")
		  Colour = c
		  SizePreview.Invalidate
		End Function
	#tag EndEvent
#tag EndEvents
#tag Events ToolBtn
	#tag Event
		Sub Action()
		  //toggle buttons
		  for i as integer = 0 to ControlCount - 1
		    if Control(i).Name = "ToolBtn" and Control(i) <> me then
		      BevelButton(control(i)).value = false
		    end
		  next
		  
		  //set tool
		  if me.Value then
		    PaintWindow.SetTool Tool(index)
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events TextField1
	#tag Event
		Sub TextChange()
		  Size = val(me.text)
		  SizePreview.Invalidate
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events SizePreview
	#tag Event
		Sub Paint(g As Graphics, areas() As REALbasic.Rect)
		  #pragma unused areas
		  
		  g.ForeColor = &cFFFFFF
		  g.Fill
		  g.ForeColor = Colour
		  g.FillRect((g.width / 2) - (size / 2), (g.height / 2) - (size / 2), size, size)
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  Me.Invalidate
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  PreviewNormalZoomLevel = true
		  me.Invalidate
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  PreviewNormalZoomLevel = false
		  me.Invalidate
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events TextField11
	#tag Event
		Sub TextChange()
		  TextSize = val(me.text)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events PopupMenu1
	#tag Event
		Sub Open()
		  For i As Integer = 0 To FontCount-1
		    me.AddRow(Font(i))
		  Next
		End Sub
	#tag EndEvent
	#tag Event
		Sub Change()
		  Font = me.text
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BtnBold
	#tag Event
		Sub Action()
		  Bold = me.value
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BtnItalic
	#tag Event
		Sub Action()
		  Italic = me.value
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BtnUnderline
	#tag Event
		Sub Action()
		  Underline = me.value
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events PushButton1
	#tag Event
		Sub Action()
		  dim item as new menuitem
		  item.append new menuitem("Grayscale")
		  item.append new menuitem("Invert")
		  item.append new menuitem("Brightness")
		  item.append new menuitem(MenuItem.TextSeparator)
		  item.append new menuitem("Delete Alpha Mask")
		  'item.append new menuitem("Smooth Edges")
		  item.append new menuitem("Opacity")
		  item.append new menuitem("Import Alpha Mask")
		  item.append new menuitem("Blur")
		  item.append new menuitem("Sharpen")
		  'item.append new menuitem(MenuItem.TextSeparator)
		  'item.append new menuitem("Shadow")
		  'item.append new menuitem("Bottonize")
		  'item.append new menuitem("Gradient Fill")
		  //show menu
		  dim selectedEffect as menuitem = item.PopUp
		  
		  if selectedEffect <> nil then
		    dim p as picture = ImageCanvas1.Image
		    
		    select case selectedEffect.Text
		    case "Grayscale"
		      ImageCanvas1.Image = ImagePlayEffectsLibrary.Greyscale(p)
		    case "Invert"
		      ImagePlayEffectsLibrary.Invert(p)
		      ImageCanvas1.Image = p
		    case "Brightness", "Opacity", "Blur", "Sharpen"
		      NumericValueWindow.ShowModal(selectedEffect.Text, ImageCanvas1.Image)
		    case "Delete Alpha Mask"
		      ImageCanvas1.Image = p.ClonePicture(false)
		    case "Import Alpha Mask"
		      dim dlg as new OpenDialog
		      dim f as FolderItem = ImageDialog(dlg, false)
		      dlg.title = "Select picture with alpha mask"
		      If f <> nil then
		        #pragma BreakOnExceptions false
		        try
		          ImageCanvas1.Image.ApplyMask(Picture.Open(f))
		          ImageCanvas1.Invalidate
		        catch UnsupportedOperationException
		          MsgBox "The imported image does not support the alpha mask of the canvas image."
		        end try
		      end if
		      'case "Shadow"
		      'ShadowOptionsWindow.ShowModal(selectedEffect.Text, p)
		      'case "Bottonize"
		      'ButtonizeOptionsWindow.ShowModal(selectedEffect.Text, p)
		      'case "Gradient Fill"
		      'GradientFillOptionsWindow.ShowModal(selectedEffect.Text, p)
		    end select
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ImageCanvas1
	#tag Event
		Function MouseDown(X As Integer, Y As Integer, ContentX As Double, ContentY As Double) As Boolean
		  self.x = ContentX
		  self.y = ContentY
		  
		  select case private_tool
		  case Tool.Pencil, Tool.Eraser
		    return true
		  case Tool.Bucket
		    Fill ContentX, ContentY, Colour
		  case Tool.Pipette
		    if me.InContentRegion(X, Y) then
		      Colour = me.Image.Graphics.Pixel(ContentX, ContentY)
		    end if
		  case Tool.Text
		    GraphicObject = new GraphicTextObject(me)
		    me.Invalidate
		  case Tool.Magnify
		    if AltMode then
		      me.Zoom x, y, 0.5
		    else
		      me.Zoom x, y, 2
		    end if
		    SizePreview.Invalidate
		  end select
		End Function
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  SetCursor
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  App.MouseCursor = nil
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDrag(X As Integer, Y As Integer, ContentX As Double, ContentY As Double)
		  #pragma unused x
		  #pragma unused y
		  
		  select case private_tool
		  case Tool.Pencil
		    DrawLine Me.Image.Graphics, self.x, self.y, ContentX, ContentY, Colour, mSize
		    DrawLine Me.Image.Mask.Graphics, self.x, self.y, ContentX, ContentY, &c000000, mSize
		    me.Invalidate
		  case Tool.Eraser
		    DrawLine Me.Image.Graphics, self.x, self.y, ContentX, ContentY, &cFFFFFF, mSize
		    DrawLine Me.Image.Mask.Graphics, self.x, self.y, ContentX, ContentY, &cFFFFFF, mSize
		    me.Invalidate
		  end
		  
		  self.x = ContentX
		  self.y = ContentY
		End Sub
	#tag EndEvent
	#tag Event
		Sub UnclippedPaint(g as graphics, ContentX as integer, ContentY as integer)
		  if GraphicObject <> nil then
		    GraphicObjectInterface(GraphicObject).Draw(g, ContentX, ContentY)
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Bold"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Colour"
		Group="Behavior"
		InitialValue="&c000000"
		Type="color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Font"
		Group="Behavior"
		Type="string"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
			"11 - Modeless Dialog"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="h"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="400"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Italic"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="PreviewNormalZoomLevel"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Underline"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="w"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="600"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
