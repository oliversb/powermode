#tag Class
Protected Class GraphicTextObject
Inherits GraphicObject
Implements GraphicObjectInterface
	#tag Method, Flags = &h1000
		Sub Constructor(c as ImageCanvas)
		  // Calling the overridden superclass constructor.
		  Super.Constructor
		  
		  Canvas = c
		  
		  me.x = Canvas.Window.MouseX
		  me.y = Canvas.Window.MouseY
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Draw(g as graphics, ContentX as integer, ContentY as integer)
		  me.left = ContentX
		  me.top = ContentY
		  
		  dim text as string
		  
		  if me.text = "" then
		    text = "Type to print"
		  else
		    text = me.text
		  end if
		  
		  SetupGraphics(g)
		  
		  me.width = MeasureString(text, PaintWindow.font, PaintWindow.fontsize, Dimensions.Width) + 5
		  me.height = g.TextAscent
		  
		  if not me.Intersects(new REALbasic.Rect(0, 0, Canvas.Image.Width, Canvas.Image.Height)) then
		    //if the colour is mostly red then use yellow other it is more appropriate to use red
		    if Max(g.ForeColor.Red, g.ForeColor.Green, g.ForeColor.Blue) = g.ForeColor.Red then
		      g.ForeColor = &cFFFF00
		    else
		      g.ForeColor = &cFF0000
		    end if
		  end if
		  
		  g.DrawRect(me.X, me.Y - g.TextAscent, me.width, me.height)
		  g.DrawString(text, me.X, me.Y)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub HandleKeyControls(key as string)
		  select case Asc(key)
		  case 8 //backspace
		    me.text = me.text.left(me.text.len - 1)
		  case 127 //del
		    PaintWindow.GraphicObject = nil
		  case 28 //left
		    
		  case 29 //right
		    
		  case 27 //esc
		    SetupGraphics(PaintWindow.ImageCanvas1.Image.Graphics)
		    PaintWindow.ImageCanvas1.Image.Graphics.DrawString(text, me.left, me.top)
		    //draw information to mask
		    SetupGraphics(PaintWindow.ImageCanvas1.Image.Mask.Graphics, false)
		    PaintWindow.ImageCanvas1.Image.Mask.Graphics.DrawString(text, me.left, me.top)
		    
		    PaintWindow.GraphicObject = nil
		  else
		    me.text = me.text + key
		  end select
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SetupGraphics(g as graphics, setupColour as boolean = true)
		  g.TextFont = PaintWindow.Font
		  g.TextUnit = FontUnits.Point
		  g.TextSize = PaintWindow.FontSize
		  if setupColour then
		    g.ForeColor = PaintWindow.Colour
		  end if
		  g.Bold = PaintWindow.Bold
		  g.Italic = PaintWindow.Italic
		  g.Underline = PaintWindow.Underline
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		Canvas As ImageCanvas
	#tag EndProperty

	#tag Property, Flags = &h0
		Text As String
	#tag EndProperty

	#tag Property, Flags = &h0
		X As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		Y As Integer
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Bottom"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Right"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Text"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="X"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Y"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
