#tag Module
Protected Module Dialog
	#tag Method, Flags = &h0
		Function ImageDialog(dlg as OpenDialog, multiselect as boolean = true) As FolderItem
		  if multiselect then
		    dlg.MultiSelect = true
		    dlg.Title="Select one or more image files"
		  else
		    dlg.MultiSelect = false
		    dlg.Title = "Select an image"
		  end if
		  dlg.InitialDirectory = SpecialFolder.Pictures
		  dlg.Filter = ImageFile.All
		  return dlg.ShowModal()
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ImageDialogIMAGE(dlg as OpenDialog, multiselect as boolean = true) As Picture
		  dim f as folderitem = ImageDialog(dlg, multiselect)
		  
		  if f <> nil then
		    return Picture.Open(f)
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function YesNoDialog(message as string, explanation as string) As YesNoDialogEnum
		  Dim d as New MessageDialog //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  d.icon = MessageDialog.GraphicCaution //display warning icon
		  d.ActionButton.Caption = "Yes"
		  d.AlternateActionButton.Visible = True //show the "No" button
		  d.AlternateActionButton.Caption = "No"
		  
		  d.Message = message
		  d.Explanation = explanation
		  
		  b = d.ShowModal
		  
		  select case b
		  case d.ActionButton
		    return YesNoDialogEnum.Yes
		  case d.AlternateActionButton
		    return YesNoDialogEnum.No
		  else
		    return YesNoDialogEnum.Cancel
		  end select
		End Function
	#tag EndMethod


	#tag Enum, Name = YesNoDialogEnum, Type = Integer, Flags = &h0
		Yes
		  No
		Cancel
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
