#tag Window
Begin Window conSelectSceneInstance Implements InstanceListMod.InstanceListInterface
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   1
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   458
   ImplicitInstance=   True
   LiveResize      =   True
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   False
   MaxWidth        =   32000
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   400
   MinimizeButton  =   False
   MinWidth        =   600
   Placement       =   2
   Resizeable      =   True
   Title           =   "Select An Instance"
   Visible         =   True
   Width           =   600
   Begin ObjectContainerCanvas SceneCanvas1
      AcceptFocus     =   False
      AcceptTabs      =   False
      AllowManipulations=   False
      AutoDeactivate  =   True
      Backdrop        =   0
      Background      =   0
      DoubleBuffer    =   True
      Enabled         =   True
      EraseBackground =   True
      Height          =   400
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   0
      Transparent     =   True
      UseFocusRing    =   True
      Visible         =   True
      Width           =   437
      WithinDraggingArea=   False
   End
   Begin Listbox listCurrentInstances
      AutoDeactivate  =   True
      AutoHideScrollbars=   True
      Bold            =   False
      Border          =   True
      ColumnCount     =   1
      ColumnsResizable=   False
      ColumnWidths    =   ""
      DataField       =   ""
      DataSource      =   ""
      DefaultRowHeight=   -1
      Enabled         =   True
      EnableDrag      =   False
      EnableDragReorder=   False
      GridLinesHorizontal=   0
      GridLinesVertical=   0
      HasHeading      =   False
      HeadingIndex    =   -1
      Height          =   400
      HelpTag         =   ""
      Hierarchical    =   False
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   440
      LockBottom      =   True
      LockedInPosition=   True
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      RequiresSelection=   False
      Scope           =   "0"
      ScrollbarHorizontal=   False
      ScrollBarVertical=   True
      SelectionType   =   1
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   0
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   160
      _ScrollOffset   =   0
      _ScrollWidth    =   -1
   End
   Begin OKCancelButton OKCancelButton1
      AcceptFocus     =   False
      AcceptTabs      =   False
      ActionButtonText=   "OK"
      AutoDeactivate  =   True
      BackColor       =   &cFFFFFF00
      Backdrop        =   0
      CancelButtonText=   "Cancel"
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   54
      HelpTag         =   ""
      Index           =   "-2147483648"
      InitialParent   =   ""
      Left            =   388
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   False
      Scope           =   "0"
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   404
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   212
   End
End
#tag EndWindow

#tag WindowCode
	#tag Method, Flags = &h0
		Function Canvas() As Canvas
		  // Part of the InstanceListMod.InstanceListInterface interface.
		  return SceneCanvas1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function OriginalScene() As CanvasObject()
		  return mOriginalScene
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Res() As ResourceScene
		  // Part of the InstanceListMod.InstanceListInterface interface.
		  return mRes
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Res(assigns r as ResourceScene)
		  // Part of the InstanceListMod.InstanceListInterface interface.
		  mRes = r
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ShowModal(scene as ResourceScene)
		  res = scene
		  SceneCanvas1.res = res
		  SceneCanvas1.CanvasObjects = res.Scene
		  SceneCanvas1.DeselectAll
		  SceneCanvas1.Invalidate
		  
		  me.width = scene.SceneWidth + listCurrentInstances.Width
		  me.height = scene.SceneHeight
		  
		  if Screen(0).AvailableWidth < me.Width or Screen(0).AvailableHeight < me.Height then
		    me.Maximize
		  end if
		  
		  PopulateCurrentInstances(listCurrentInstances, res)
		  
		  me.ShowModal
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Visibles() As Dictionary
		  // Part of the InstanceListMod.InstanceListInterface interface.
		  return mVisibles
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Visibles(assigns v as Dictionary)
		  // Part of the InstanceListMod.InstanceListInterface interface.
		  mVisibles = v
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private mOriginalScene() As CanvasObject
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mRes As ResourceScene
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mVisibles As Dictionary
	#tag EndProperty

	#tag Property, Flags = &h0
		ProgrammaticChange As Boolean
	#tag EndProperty


#tag EndWindowCode

#tag Events SceneCanvas1
	#tag Event
		Sub ObjectSelected(co As CanvasObject, selecting As Boolean)
		  #pragma unused selecting
		  
		  ProgrammaticChange = true
		  //select instance in list
		  for i as uint32 = 0 to listCurrentInstances.ListCount - 1
		    if listCurrentInstances.RowTag(i) = co then
		      listCurrentInstances.Selected(i) = true
		      exit for
		    end if
		  next
		  ProgrammaticChange = false
		End Sub
	#tag EndEvent
	#tag Event
		Sub DeselectedAll()
		  listCurrentInstances.DeselectAll
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  SceneCanvas1.DeselectAll
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events listCurrentInstances
	#tag Event
		Sub Change()
		  if ProgrammaticChange = false then
		    SceneCanvas1.DeselectAll(false)
		    
		    for i as uint32 = 0 to me.ListCount - 1
		      if me.Selected(i) = true then
		        SceneCanvas1.SelectedObjects.Append( me.RowTag(i) )
		        SceneCanvas1.SelectedObjects(SceneCanvas1.SelectedObjects.Ubound).Selected = true
		      end if
		    next
		    conUpdateSceneInstances.InstancesToUpdate = SceneCanvas1.SelectedObjects
		    SceneCanvas1.Invalidate
		  end if
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events OKCancelButton1
	#tag Event
		Sub Action()
		  Self.Close
		End Sub
	#tag EndEvent
	#tag Event
		Sub CancelAction()
		  conUpdateSceneInstances.InstancesToUpdate = nil
		  self.Close
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
			"11 - Modeless Dialog"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="400"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="ProgrammaticChange"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="600"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
