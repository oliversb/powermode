#tag Window
Begin Window conUpdateSceneInstances
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   1
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   282
   ImplicitInstance=   True
   LiveResize      =   True
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   False
   MaxWidth        =   32000
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   False
   MinWidth        =   64
   Placement       =   2
   Resizeable      =   False
   Title           =   "Update Scene Instances"
   Visible         =   True
   Width           =   364
   Begin Label Label1
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   43
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "How would you like to update the scene instances?"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   14
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   278
   End
   Begin GroupBox GroupBox1
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "Update What?"
      Enabled         =   True
      Height          =   85
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   46
      Underline       =   False
      Visible         =   True
      Width           =   324
      Begin RadioButton radioUpdateAllProperties
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Update All Properties"
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   0
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   34
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   0
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   62
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   131
      End
      Begin RadioButton radioUpdateAllProperties
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Update Changes Since Tab Open"
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   1
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   34
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   1
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   82
         Underline       =   False
         Value           =   True
         Visible         =   True
         Width           =   193
      End
      Begin RadioButton radioUpdateAllProperties
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Select Property To Update"
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   2
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   34
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   2
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   104
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   298
      End
   End
   Begin GroupBox GroupBox2
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "Update Where?"
      Enabled         =   True
      Height          =   85
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   143
      Underline       =   False
      Visible         =   True
      Width           =   324
      Begin RadioButton radioUpdateAllScenes
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Update All Scenes"
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   0
         InitialParent   =   "GroupBox2"
         Italic          =   False
         Left            =   40
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   0
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   159
         Underline       =   False
         Value           =   True
         Visible         =   True
         Width           =   114
      End
      Begin RadioButton radioUpdateAllScenes
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Update Scene"
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   1
         InitialParent   =   "GroupBox2"
         Italic          =   False
         Left            =   40
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   1
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   179
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   97
      End
      Begin RadioButton radioUpdateAllScenes
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Select Instances From Scene To Update"
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   2
         InitialParent   =   "GroupBox2"
         Italic          =   False
         Left            =   40
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   2
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   199
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   241
      End
   End
   Begin PushButton btnUpdate
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Update"
      Default         =   True
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   133
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   False
      Scope           =   0
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   240
      Underline       =   False
      Visible         =   True
      Width           =   101
   End
   Begin PushButton btnCancel
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   True
      Caption         =   "Cancel"
      Default         =   False
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   246
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   False
      Scope           =   0
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   240
      Underline       =   False
      Visible         =   True
      Width           =   98
   End
End
#tag EndWindow

#tag WindowCode
	#tag Method, Flags = &h21
		Private Function DefCanvasObject() As CanvasObject
		  return ThisObject.DefaultCanvasObject
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub OpenSelectSceneInstance()
		  conSelectSceneInstance.ShowModal(SceneToUpdate)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ShowModal(obj as ResourceObject)
		  ThisObject = obj
		  me.ShowModal
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Update(co as CanvasObject)
		  select case UpdateWhatRadio
		  case 0
		    co.Copy(DefCanvasObject)
		  case 1
		    co.CopyChanges(DefCanvasObject)
		  case 2
		    co.UpdateProperty(DefCanvasObject, PropertyToUpdate)
		  end select
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		InstancesToUpdate() As CanvasObject
	#tag EndProperty

	#tag Property, Flags = &h21
		Private PropertyToUpdate As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private SceneToUpdate As ResourceScene
	#tag EndProperty

	#tag Property, Flags = &h0
		ThisObject As ResourceObject
	#tag EndProperty

	#tag Property, Flags = &h0
		UpdateWhatRadio As Integer = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		UpdateWhereRadio As Integer
	#tag EndProperty


#tag EndWindowCode

#tag Events radioUpdateAllProperties
	#tag Event
		Sub Action()
		  UpdateWhatRadio = index
		  
		  select case index
		  case 2 //update single property
		    dim menu, item, result as menuitem
		    menu = new menuitem
		    
		    item = new PropertyMenuItem("Scale X")
		    menu.append item
		    
		    item = new PropertyMenuItem("Scale Y")
		    menu.append item
		    
		    item = new PropertyMenuItem("Visible")
		    menu.append item
		    
		    item = new PropertyMenuItem("Opacity")
		    menu.append item
		    
		    item = new PropertyMenuItem("Layer")
		    menu.append item
		    
		    item = new PropertyMenuItem("Rotation")
		    menu.append item
		    
		    result = menu.PopUp
		    if result <> nil then
		      PropertyToUpdate = result.tag
		      radioUpdateAllProperties(2).Caption = "Select Property To Update (" +result.text + ")" 
		    end if
		  end select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events radioUpdateAllScenes
	#tag Event
		Sub Action()
		  UpdateWhereRadio = index
		  
		  select case index
		  case 1, 2
		    dim menu, item, result as menuitem
		    menu = new menuitem
		    
		    //list scenes and tag them
		    for each s as Resource in DocFile.Resources
		      if s isa ResourceScene then
		        item = new menuitem(s.name)
		        item.tag = s
		        menu.append item
		      end if
		    next
		    
		    result = menu.PopUp
		    
		    if result <> nil then
		      SceneToUpdate = ResourceScene(result.tag)
		      select case index
		      case 2
		        OpenSelectSceneInstance
		      end select
		    end if
		  end select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnUpdate
	#tag Event
		Sub Action()
		  //perform actions to update
		  select case UpdateWhereRadio
		  case 0
		    //update every instance of every scene
		    for each co as CanvasObject in ThisObject.AllInstances
		      Update(co)
		    next
		  case 1
		    //update every instance in a particular scene
		    if SceneToUpdate <> nil then
		      for each co as CanvasObject in ThisObject.AllInstances(SceneToUpdate)
		        Update(co)
		      next
		    else
		      MsgBox "You have not selected a scene to update."
		      return
		    end if
		  case 2
		    //update particular instances of a particular scene
		    if InstancesToUpdate <> nil and InstancesToUpdate.Ubound <> -1 then
		      for each co as CanvasObject in InstancesToUpdate
		        Update(co)
		      next
		    else
		      MsgBox "You have not selected any instances."
		      return
		    end if
		  end select
		  
		  self.close
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnCancel
	#tag Event
		Sub Action()
		  self.Close
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
			"11 - Modeless Dialog"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="400"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="UpdateWhatRadio"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="UpdateWhereRadio"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="600"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
