#tag Class
Protected Class MinimalArrayMainContainer
Inherits PropertiesContainer
	#tag Method, Flags = &h0
		Sub AfterClosedOrContainers(sender as ControlCloseTimer)
		  #pragma unused sender
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AppendedOrContainers() As MinimalArrayContainer()
		  //miss out the first item
		  dim list() as MinimalArrayContainer
		  for i as integer = 1 to OrContainers.Ubound
		    list.Append(OrContainers(i))
		  next
		  
		  return list
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AppendedOrContainers(index as integer) As MinimalArrayContainer
		  return OrContainers(index + 1)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AppendedOrContainersRemove(index as integer)
		  OrContainers.Remove(index + 1)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AppendedOrContainersUbound() As integer
		  return OrContainers.Ubound - 1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AppendVariable(c as MinimalArrayContainer)
		  //tell the textfield which object to link to
		  c.txtThis.res = iRes
		  
		  //increase the current index
		  VariablePathArrayIndex = VariablePathArrayIndex + 1
		  
		  //add list item based on how the specific MinimalArrayContainers handle it
		  if not me isa conListValue then
		    VariablePathArrayAppend
		  else
		    conListValue(me).ListAppend new ObjectVariablePath
		  end if
		  
		  //add OrContainer
		  me.OrContainers.append MinimalArrayContainer(c)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1000
		Sub CallOpen()
		  // Calling the overridden superclass constructor.
		  Super.Constructor
		  
		  //call Open event of textfield
		  'if ValueContainer isa MinimalArrayMainContainer then
		  if CalledOpen = false then
		    conORIf1.txtThis.CallOpen
		    CalledOpen = true
		  end if
		  'end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function conOrIf1() As MinimalArrayContainer
		  select case me
		  case isa conOR
		    return conOR(me).conORIf1
		  case isa conListValue
		    return conListValue(me).conORIf1
		  case isa EditVariable
		    return MinimalArrayContainer(EditVariable(me).ValueContainer)
		  end select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1000
		Sub Constructor()
		  // Calling the overridden superclass constructor.
		  Super.Constructor
		  
		  ControlCloseTimer = new ControlCloseTimer
		  AddHandler ControlCloseTimer.AfterClose, AddressOf AfterClosedOrContainers
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ContainerCanvas() As Canvas
		  select case me
		  case isa conOR
		    return conOR(me).ContainerCanvas
		  case isa conListValue
		    return conListValue(me).ContainerCanvas
		  end select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function IndexOfFirstEmptyContainer() As integer
		  //index of first empty container
		  dim firstIndex as integer = -1
		  
		  //find where to start removing containers
		  dim cc() as MinimalArrayContainer = AppendedOrContainers
		  
		  for i as integer = 0 to cc.Ubound
		    dim c1 as MinimalArrayContainer = cc(i)
		    if c1.txtThis.Text = "" then
		      firstIndex = i + 1
		      exit for
		    end if
		  next
		  
		  return firstIndex
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function iRes() As ResourceObject
		  select case me
		  case isa conOR
		    return conOR(me).res
		  else
		    select case me.window
		    case isa VariableContainer
		      return VariableContainer(me.window).res
		    else
		      //if all fails to get hold of the resource then inform error
		      raise new UnsupportedOperationException
		    end select
		  end select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Label2() As Label
		  select case me
		  case isa ConOR
		    return conOR(me).Label2
		  case isa conListValue
		    '//convert groupbox to label
		    'dim l as new label
		    'l.text = conListValue(me).groupIfThis1.Caption
		    'return l
		  end select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function MoveHeight() As integer
		  return (OrContainers.Ubound + 1)  * conORIf1.Height
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveUneccasseryContainers()
		  dim firstIndex as integer = IndexOfFirstEmptyContainer
		  
		  if firstIndex <> -1 then
		    for index as integer = AppendedOrContainersUbound downto firstIndex
		      ControlCloseTimer.ContainerControls.Append(AppendedOrContainers(index))
		      AppendedOrContainersRemove(index)
		    next
		    ControlCloseTimer.CloseAll
		    SetScrollbar
		  else
		    //call event assiocated with closing the ControlCloseTimer
		    ControlCloseTimer.CloseAll
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ScrollBar1() As ScrollBar
		  select case me
		  case isa ConOR
		    return conOR(me).ScrollBar1
		  case isa conListValue
		    return conListValue(me).ScrollBar1
		  end select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetScrollbar()
		  ScrollBar1.Maximum = MoveHeight - ScrollBar1.Height
		  ScrollBar1.Value = ScrollBar1.Maximum
		  RaiseEvent ChangedAppendedOrContainersUbound(OrContainers.Ubound)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TextFieldChange(f as TextField)
		  //if there is text in box and this is the last box
		  if f.Text <> "" then
		    dim c as MinimalArrayContainer
		    select case me
		    case isa conListValue
		      c = new conListItem
		    case isa conOR
		      c = new conORIf
		    end select
		    //only add new item if this is the last item before the first empty container
		    dim firstIndex as integer = IndexOfFirstEmptyContainer
		    if firstIndex = AppendedOrContainersUbound or firstIndex = -1 then
		      c.EmbedWithin(ContainerCanvas, 0, MoveHeight  - ScrollBar1.Value)
		      SetScrollbar
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function VariablePathArray(index as integer) As ObjectVariablePath
		  if not self isa conListValue then
		    return BlockBase(CodeFrame.tag).VariablePathArray(index)
		  else
		    return conListValue(self).List(index)
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function VariablePathArray(index as integer, assigns variable as ObjectVariablePath) As ObjectVariablePath
		  if not self isa conListValue then
		    BlockBase(CodeFrame.tag).VariablePathArray(index) = variable
		  else
		    conListValue(me).List(index) = variable
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function VariablePathArrayIndex() As Int32
		  return mVariablePathArrayIndex
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub VariablePathArrayIndex(assigns value as integer)
		  mVariablePathArrayIndex = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function VariablePathArrayUbound() As integer
		  if not self isa conListValue then
		    return BlockBase(CodeFrame.tag).VariablePathArray.Ubound
		  else
		    return conListValue(me).List.Ubound
		  end if
		End Function
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event ChangedAppendedOrContainersUbound(index as integer)
	#tag EndHook


	#tag Property, Flags = &h0
		CalledOpen As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		ControlCloseTimer As ControlCloseTimer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mmVariablePathArrayIndex As Integer = -1
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mmVariablePathArrayIndex
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mmVariablePathArrayIndex = value
			End Set
		#tag EndSetter
		mVariablePathArrayIndex As Integer
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		OrContainers() As MinimalArrayContainer
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="AcceptFocus"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AcceptTabs"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AutoDeactivate"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="BackColor"
			Visible=true
			Group="Appearance"
			InitialValue="&hFFFFFF"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Backdrop"
			Visible=true
			Group="Appearance"
			Type="Picture"
			EditorType="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CalledOpen"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="EraseBackground"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HasBackColor"
			Visible=true
			Group="Appearance"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Position"
			InitialValue="300"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Appearance"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="InitialParent"
			Group="Position"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="mVariablePathArrayIndex"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabIndex"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabPanelIndex"
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabStop"
			Visible=true
			Group="Position"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Transparent"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="UseFocusRing"
			Visible=true
			Group="Appearance"
			InitialValue="False"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Position"
			InitialValue="300"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
