#tag Interface
Protected Interface VariablePathArrayInterface
Implements ResObjectInterface
	#tag Method, Flags = &h0
		Function VariablePathArray(index as integer) As ObjectVariablePath
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub VariablePathArray(index as integer, assigns variable as ObjectVariablePath)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function VariablePathArrayIndex() As Int32
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub VariablePathArrayIndex(assigns value as integer)
		  
		End Sub
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Interface
#tag EndInterface
