#tag Class
Protected Class PropertiesContainer
Inherits ContainerControl
Implements VariablePathArrayInterface, ResObjectInterface
	#tag Method, Flags = &h0
		Sub Constructor(r as resource, optional selectedFrame as ContentFrame, optional cont as ObjectEditor)
		  res = ResourceObject(r)
		  CodeFrame = selectedFrame
		  
		  Container = cont
		  
		  //CHECK VARIABLE FILTERS NOTES
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function iRes() As ResourceObject
		  return res
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function PathGetRes(sender as ObjectVariablePath) As ResourceObject
		  #pragma unused sender
		  return res
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function VariablePathArray(index as integer) As ObjectVariablePath
		  if not self isa conListValue then
		    return BlockBase(CodeFrame.tag).VariablePathArray(index)
		  else
		    return conListValue(me).List(index)
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub VariablePathArray(index as integer, assigns variable as ObjectVariablePath)
		  if not self isa conListValue then
		    BlockBase(CodeFrame.tag).VariablePathArray(index) = variable
		  else
		    conListValue(me).List(index) = variable
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub VariablePathArrayAppend()
		  dim path as ObjectVariablePath = new ObjectVariablePath
		  BlockBase(CodeFrame.tag).VariablePathArray.Append path
		  AddHandler path.GetRes, AddressOf PathGetRes
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function VariablePathArrayIndex() As integer
		  return mVariablePathArrayIndex
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub VariablePathArrayIndex(assigns value as integer)
		  mVariablePathArrayIndex = value
		  
		  
		End Sub
	#tag EndMethod


	#tag Note, Name = Variable filters
		-1 = any
		0 = text
		1 = number
		
		IfThen ✔
		-1
		While ✔
		-1
		Timer ✔
		1
		
		Add/Subtract/Multiply/Divide/Random ✔
		1
		
		Join/Lowercase/Uppercase/Replace ✔
		0
		
		Length
		(set = 1) ✔ (to length = 0) ✔
	#tag EndNote


	#tag Property, Flags = &h0
		CodeFrame As ContentFrame
	#tag EndProperty

	#tag Property, Flags = &h0
		Container As ObjectEditor
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mVariablePathArrayIndex As integer
	#tag EndProperty

	#tag Property, Flags = &h0
		res As ResourceObject
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="AcceptFocus"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AcceptTabs"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AutoDeactivate"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="BackColor"
			Visible=true
			Group="Appearance"
			InitialValue="&hFFFFFF"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Backdrop"
			Visible=true
			Group="Appearance"
			Type="Picture"
			EditorType="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="EraseBackground"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HasBackColor"
			Visible=true
			Group="Appearance"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Position"
			InitialValue="300"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Appearance"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="InitialParent"
			Group="Position"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabIndex"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabPanelIndex"
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabStop"
			Visible=true
			Group="Position"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Transparent"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="UseFocusRing"
			Visible=true
			Group="Appearance"
			InitialValue="False"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Position"
			InitialValue="300"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
