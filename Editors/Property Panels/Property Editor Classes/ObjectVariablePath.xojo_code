#tag Class
Protected Class ObjectVariablePath
	#tag Method, Flags = &h0
		Sub Append(var as ObjectVariable)
		  History.Append(var)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor()
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(res as ResourceObject, path as string)
		  #pragma unused res
		  #pragma unused path
		  Constructor()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Current() As ObjectVariable
		  if History.Ubound <> -1 then
		    return History(History.Ubound)
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetHTML5CodeName() As string
		  dim code as string = "this"
		  
		  for each v as ObjectVariable in me.History
		    code = code + "." + v.GetCodeName
		  next
		  
		  return code
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function HasPath() As boolean
		  return (me.History.Ubound <> -1 or me.SelfReference = true)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Proceed(name as string) As boolean
		  if GetRes.Variable(name) = nil and name <> "*THIS" then
		    //cannot proceed because the variable was not found
		    return false
		  end if
		  
		  if History.Ubound = -1 then
		    //make something to proceed from
		    History.Append(GetRes.Variable(name))
		  else
		    //proceed from last
		    History.Append(Current.res.Variable(name))
		  end if
		  
		  //okay to proceed
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function StringValue() As String
		  dim s() as string
		  dim v as ObjectVariable
		  
		  for i as integer = 0 to me.History.Ubound
		    v = me.History(i)
		    if v = nil then
		      //remove anything that cannot be used
		      me.History.Remove(i)
		    else
		      s.Append(v.Name)
		    end if
		  next
		  
		  return Join(s, " > ")
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Variable() As ObjectVariable
		  return me.Current
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Variable(assigns value as ObjectVariable)
		  redim me.History(-1)
		  me.Append(value)
		  
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event GetRes() As ResourceObject
	#tag EndHook


	#tag Property, Flags = &h0
		History() As ObjectVariable
	#tag EndProperty

	#tag Property, Flags = &h0
		SelfReference As boolean
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="SelfReference"
			Group="Behavior"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
