#tag Class
Protected Class VariableTextfield
Inherits FocusTextfield
	#tag Event
		Function ConstructContextualMenu(base as MenuItem, x as Integer, y as Integer) As Boolean
		  #pragma unused x
		  #pragma unused y
		  
		  varScope = VariableScope //global if global segment is selected
		  
		  redim me.VariablePath.History(-1)
		  ConstructBase(base, res, varScope)
		  
		  return true
		End Function
	#tag EndEvent

	#tag Event
		Function ContextualMenuAction(hitItem as MenuItem) As Boolean
		  dim cHitItem as ObjectVariableMenuItem = ObjectVariableMenuItem(hitItem)
		  ''VariablePath.Append(cHitItem.Variable)
		  
		  'if varScope = 1 then
		  me.VariablePath.Append(cHitItem.Variable)
		  'end if
		  if cHitItem.Text = "*THIS" then
		    me.Text = cHitItem.Text
		  else
		    me.Text = GetTextFromHitItem(cHitItem)
		  end if
		  
		  //further branch in if there is a tag and the item is not itself
		  if varScope = 1 and cHitItem.Variable <> nil and cHitItem.Variable.OfType("Object") then
		    dim item as ObjectVariableMenuItem = NewObjectVariableMenuItem
		    VariablePath.Append(cHitItem.Variable)
		    ConstructGlobalBase(item)
		    
		    //variable to hold clicked item
		    dim item2 as ObjectVariableMenuItem
		    //variable to determine wether we should branch in
		    dim gotVariableToBranchIn as boolean = true
		    
		    //loop until we have nothing to branch into
		    while gotVariableToBranchIn
		      item = NewObjectVariableMenuItem
		      //create the menu
		      ConstructGlobalBase(item)
		      //have the menu popup
		      item2 = item.Open
		      //if any item is chosen
		      if item2 <> nil then
		        //add selected variable to path
		        VariablePath.Append(item2.Variable)
		        //there is a variable to branch into if the item clicked has a variable attatched and of the object type
		        gotVariableToBranchIn = ((me.VariablePath.Current <> nil) and (me.VariablePath.Current.OfType("Object")))
		        //add variable to text
		        dim appendedText as string = GetTextFromHitItem(item2)
		        if appendedText <> "" then
		          me.Text = me.Text + " > " + appendedText
		        end if
		      else
		        //if the user didn't select anything then there is nothing to branch into
		        gotVariableToBranchIn = false
		        //remove text
		        me.Text = ""
		      end if
		    wend
		  end if
		  
		  DocFile.ModifiedDoc = true
		End Function
	#tag EndEvent

	#tag Event
		Sub LostFocus()
		  LostFocusMethod
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  //call open if not first item
		  if me.window isa MinimalArrayContainer then
		    select case me.parent.parent.window
		    case isa conListValue
		      if conListValue(me.parent.parent.window).OrContainers.IndexOf(MinimalArrayContainer(me.window)) <> 0 then
		        CallOpen
		        return
		      end if
		    case isa conOR
		      if conOR(me.parent.parent.window).OrContainers.IndexOf(MinimalArrayContainer(me.window)) <> 0 then
		        CallOpen
		        return
		      end if
		    end select
		  end if
		  
		  select case me.window
		  case isa conListItem, isa conORIf
		    if MinimalArrayMainContainer(me.parent.parent.window).CalledOpen then
		      CallOpen
		    end if
		  else
		    CallOpen
		  end select
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub TextChange()
		  //seperate code so return won't stop the TextChange event from being executed
		  if ProgrammaticTextChange = false then
		    TextChangeMethod
		    TextChange()
		  end if
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Function BlockInfo() As BlockBase
		  return BlockBase(me.CodeFrame.tag)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CallOpen()
		  //see if propertiescontainer is within a container of the propertiescontainer
		  select case self.window
		  case isa conListItem
		    castedWin = VariablePathArrayInterface(me.parent.parent.window)
		  case isa PropertiesContainer
		    win = PropertiesContainer(me.window)
		  else
		    win = PropertiesContainer(me.parent.parent.window)
		  end select
		  if castedWin = nil then
		    castedWin = VariablePathArrayInterface(win)
		  end if
		  
		  res = castedWin.iRes
		  
		  dim mainContainer as MinimalArrayMainContainer
		  dim innerContainer as MinimalArrayContainer
		  
		  if me.ProceedControlHierarchy(3).ContainerControlValue isa MinimalArrayMainContainer then
		    mainContainer = MinimalArrayMainContainer(me.ProceedControlHierarchy(3).ContainerControlValue)
		  end if
		  
		  //check if this is an 'or' container other than the first one
		  if mainContainer isa conOR then
		    innerContainer = MinimalArrayContainer(me.ProceedControlHierarchy(2).ContainerControlValue)
		    //include this 'or' container in the list
		    mainContainer.AppendVariable(innerContainer)
		  else
		    nothing
		  end if
		  
		  if win <> nil then
		    CodeFrame = win.CodeFrame
		  end if
		  
		  select case me
		  case isa txtVar1, isa txtVar2, isa txtVar3, isa txtVar4, isa txtVarArray
		    if VariablePath = nil then
		      VariablePath = new ObjectVariablePath
		      AddHandler VariablePath.GetRes, AddressOf GetRes
		    end if
		  end select
		  
		  if VariablePath <> nil then
		    me.Text = me.VariablePath.StringValue
		  end if
		  
		  Open()
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function CheckType(var as ObjectVariable) As boolean
		  //check for valid type and use search query
		  dim ofType as boolean
		  
		  if me.Filter = -1 then
		    ofType = true
		  else
		    ofType = var.OfType(me.Filter)
		    
		    //further check if it is of the type but make sure that the scope is global
		    if ofType = false and varScope = 1 then
		      ofType = var.OfType("Object")
		    end if
		  end if
		  
		  if ofType then
		    if VariablePath.History.Ubound > 0 then
		      return true
		    else
		      return (me.Text = "" or var.Name.Contains(me.Text))
		    end if
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ConstructBase(base as MenuItem, res as ResourceObject, varScope as integer)
		  #pragma unused varScope
		  
		  dim base2 as MenuItem
		  //list all variables contained within that object
		  dim allVars() as ObjectVariable =  ResourceObject(res).AllVariables
		  
		  dim item as ObjectVariableMenuItem
		  //add item to choose the current variable rather than branching in further
		  item = NewObjectVariableMenuItem("*THIS")
		  base.append(item)
		  base2 = base
		  
		  //populate all variables of any type
		  'if Filter = -1 then
		  for i as uint32 = 0 to ResourceObject(res).AllVariables.Ubound
		    if CheckType(allVars(i)) then
		      item = NewObjectVariableMenuItem(allVars(i))
		      base2.Append(item)
		    end if
		  next
		  'else
		  '//convert number to the type that is being filtered
		  'dim f as string = ObjectVariable.FilterToString(me.filter)
		  
		  'dim populated as boolean = false
		  '//populate all variables of this particular type
		  'for i as uint32 = 0 to allVars.Ubound
		  'if allVars(i).Type = f and (allVars(i).Name.Contains(me.text) or me.text = "") then
		  'item = new MenuItem(allVars(i).name, base2.Text)
		  'if allVars(i).OfType("Object") then
		  'item.tag = res.name
		  'end if
		  'base2.Append(item)
		  'populated = true
		  'end if
		  '
		  'if populated = false then
		  'base2.Enabled = false
		  'end if
		  'next
		  'end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ConstructGlobalBase(base as ObjectVariableMenuItem)
		  //get last selected asset
		  dim getRes as Resource = VariablePath.Current.res
		  
		  if getRes <> nil then
		    dim currentRes as ResourceObject = ResourceObject(getRes)
		    dim item as ObjectVariableMenuItem
		    
		    item = NewObjectVariableMenuItem("*THIS")
		    base.append(item)
		    
		    //populate base with variables from this object
		    for each v as ObjectVariable in currentRes.AllVariables
		      if CheckType(v) then
		        item = NewObjectVariableMenuItem(v)
		        //add item to menu
		        base.append(item)
		      end if
		    next
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetRes(sender as ObjectVariablePath) As ResourceObject
		  #pragma unused sender
		  return res
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function GetTextFromHitItem(hitItem as ObjectVariableMenuItem) As string
		  //if this is the first item then we are fine with "*THIS"
		  if me.VariablePath.History.Ubound = 0 or hitItem.Text <> "*THIS" then
		    return hitItem.Text
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub LostFocusMethod()
		  TextChangeMethod(true)
		  
		  if me.Variable <> nil then
		    ProgrammaticTextChange = true
		    'me.Text = VariableTextfieldInterface(me).Variable.GetName(false, me.Path)
		    ProgrammaticTextChange = false
		  end if
		  
		  'if me.varScope = 0 then
		  'for each var as ObjectVariable in res.AllVariables
		  'if me.text = var.name then
		  'VariableTextfieldInterface(me).Variable = var
		  'exit for
		  'end if
		  'next
		  'else
		  'for each res1 as resource in DocFile.resources
		  'if res1 isa ResourceObject then
		  'for each var as ObjectVariable in ResourceObject(res1).AllVariables
		  'if me.text = res1.name + " > " + var.name then
		  'VariableTextfieldInterface(me).Variable = var
		  'exit for
		  'end if
		  'next
		  'end if
		  'next
		  'end if
		  
		  ModifiedDoc = true
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function NewObjectVariableMenuItem(var as ObjectVariable = nil) As ObjectVariableMenuItem
		  return new ObjectVariableMenuItem(VariablePath, var, "")
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function NewObjectVariableMenuItem(text as string) As ObjectVariableMenuItem
		  return new ObjectVariableMenuItem(VariablePath, nil, text)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub TextChangeMethod(LostFocus as boolean = false)
		  ProgrammaticTextChange = true
		  
		  if me.text = "" then
		    me.Variable = nil
		  else
		    dim gotVar as boolean
		    for each v as ObjectVariable in res.AllVariables
		      if me.text = v.name then
		        me.Variable = v
		        exit for
		        gotVar = true
		      end if
		    next
		    
		    if gotVar = false then
		      redim me.VariablePath.History(-1)
		      for each v as string in me.Text.ReplaceWithNothing(" ").Split(">")  //find each separator and replace spaces with nothing
		        //add to path and check if it is okay to proceed
		        if me.VariablePath.Proceed(v) = false then
		          //if it is not okay to proceed then inform the user
		          if LostFocus then //this will only apply to the changing of text in LostFocus event
		            me.text = ""
		            exit for
		          end if
		        end if
		      next
		    end if
		  end if
		  
		  ProgrammaticTextChange = false
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Variable() As ObjectVariable
		  if me.VariablePath <> nil then
		    return me.VariablePath.Variable
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Variable(assigns value as ObjectVariable)
		  me.VariablePath.Variable = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function VariablePath() As ObjectVariablePath
		  //override
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub VariablePath(assigns value as ObjectVariablePath)
		  #pragma unused value
		  //override
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event TextChange()
	#tag EndHook


	#tag Note, Name = VarSelectorPlan
		LOCAL {
		 LIST ALL VARS (NOT SUBLISTS)
		}
		
		GLOBAL {
		 LIST ALL VARS {
		  LIST ALL VARS FROM THAT TYPE OF OBJECT { GLOBAL }
		 }
		}
		
	#tag EndNote


	#tag Property, Flags = &h1
		Protected castedWin As VariablePathArrayInterface
	#tag EndProperty

	#tag Property, Flags = &h0
		CodeFrame As ContentFrame
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected currentSuggestionWindow As suggestionWindow
	#tag EndProperty

	#tag Property, Flags = &h0
		Filter As Integer = -1
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mPath As ObjectVariablePath
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mProgrammaticTextChange As boolean
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mProgrammaticTextChange
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mProgrammaticTextChange = value
			End Set
		#tag EndSetter
		ProgrammaticTextChange As boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		res As ResourceObject
	#tag EndProperty

	#tag Property, Flags = &h0
		varScope As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		win As PropertiesContainer
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="AcceptTabs"
			Visible=true
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Alignment"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Default"
				"1 - Left"
				"2 - Center"
				"3 - Right"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="AutoDeactivate"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AutomaticallyCheckSpelling"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="BackColor"
			Visible=true
			Group="Appearance"
			InitialValue="&hFFFFFF"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Bold"
			Visible=true
			Group="Font"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Border"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CueText"
			Visible=true
			Group="Initial State"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DataField"
			Visible=true
			Group="Database Binding"
			Type="String"
			EditorType="DataField"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DataSource"
			Visible=true
			Group="Database Binding"
			Type="String"
			EditorType="DataSource"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Filter"
			Group="Behavior"
			InitialValue="-1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="focus"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Format"
			Visible=true
			Group="Appearance"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Position"
			InitialValue="22"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Appearance"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
			EditorType="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Italic"
			Visible=true
			Group="Font"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LimitText"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Mask"
			Visible=true
			Group="Behavior"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Password"
			Visible=true
			Group="Appearance"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ProgrammaticTextChange"
			Group="Behavior"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ReadOnly"
			Visible=true
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabIndex"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabPanelIndex"
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabStop"
			Visible=true
			Group="Position"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Text"
			Group="Initial State"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TextColor"
			Visible=true
			Group="Appearance"
			InitialValue="&h000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TextFont"
			Visible=true
			Group="Font"
			InitialValue="System"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TextSize"
			Visible=true
			Group="Font"
			InitialValue="0"
			Type="Single"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TextUnit"
			Visible=true
			Group="Font"
			InitialValue="0"
			Type="FontUnits"
			EditorType="Enum"
			#tag EnumValues
				"0 - Default"
				"1 - Pixel"
				"2 - Point"
				"3 - Inch"
				"4 - Millimeter"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Underline"
			Visible=true
			Group="Font"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="UseFocusRing"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="varScope"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Position"
			InitialValue="80"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
