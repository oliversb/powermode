#tag Class
Protected Class PhysicsCanvas
Inherits Canvas
	#tag Event
		Sub Close()
		  t = nil
		End Sub
	#tag EndEvent

	#tag Event
		Sub Paint(g As Graphics, areas() As REALbasic.Rect)
		  #pragma unused areas
		  
		  //if width changed or LastWidth not been set
		  if me.width <> LastWidth or LastWidth = 0 then
		    //set y (to centre sprite)
		    dim w as uint32
		    if Sprite <> nil and Sprite.Frames.Ubound <> -1 then
		      w = Sprite.Frames(0).SourceImage.Width
		    else
		      w = Icon("Sprite").Width
		    end if
		    X = (me.width / 2) - (w / 2)
		  end if
		  LastWidth = me.width
		  
		  //white background
		  g.ForeColor = &cFFFFFF
		  g.FillRect(0, 0, g.width, g.height)
		  //draw sprite
		  if Sprite <> nil and Sprite.Frames.Ubound <> -1 then
		    g.DrawPicture(Sprite.Frames(0).SourceImage, X, Y)
		  else
		    g.DrawPicture(iconSprite, X, Y)
		  end if
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h1000
		Sub Constructor()
		  // Calling the overridden superclass constructor.
		  Super.Constructor
		  
		  t = new Timer
		  t.Period = 12
		  t.Mode = Timer.ModeMultiple
		  AddHandler t.Action, AddressOf Tick
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Tick(sender as timer)
		  #pragma unused sender
		  '
		  'dim VariablePath as ObjectVariablePath
		  '
		  'VariablePath = BlockBase(PropertiesContainer(me.window).CodeFrame.tag).VariablePath1
		  '
		  'if VariablePath <> nil then
		  '//reset Y if out of bounds
		  'if VariablePath.Value.DoubleValue > 0 then //positive speed or no speed
		  'if Y >= me.height then
		  'Y = 0
		  'else
		  ''Y = Y + VariablePath.Value
		  'Y = AKEasing.linearTween(1, Y, VariablePath.Value.DoubleValue, 1)
		  'end if
		  'elseif VariablePath.Value.DoubleValue < 0 then //negative speed
		  'if Y <= 0 then
		  'Y = me.height
		  'else
		  ''Y = Y + VariablePath.Value
		  'Y = AKEasing.linearTween(1, Y, VariablePath.Value.DoubleValue, 1)
		  'end if
		  'end if
		  'else //value is 0
		  'return
		  'end if
		  '
		  'me.Invalidate
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		LastWidth As Uint32
	#tag EndProperty

	#tag Property, Flags = &h0
		Sprite As ResourceSprite
	#tag EndProperty

	#tag Property, Flags = &h0
		t As Timer
	#tag EndProperty

	#tag Property, Flags = &h0
		X As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		Y As Integer = 0
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="AcceptFocus"
			Visible=true
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AcceptTabs"
			Visible=true
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AutoDeactivate"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Backdrop"
			Visible=true
			Group="Appearance"
			Type="Picture"
			EditorType="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DoubleBuffer"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="EraseBackground"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Position"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Appearance"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			Type="Integer"
			EditorType="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="InitialParent"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabIndex"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabPanelIndex"
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabStop"
			Visible=true
			Group="Position"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Transparent"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="UseFocusRing"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Position"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="X"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Y"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
