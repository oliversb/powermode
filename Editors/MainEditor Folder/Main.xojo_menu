#tag Menu
Begin Menu Main
   Begin MenuItem FileMenu
      SpecialMenu = 0
      Text = "&File"
      Index = -2147483648
      AutoEnable = True
      Visible = True
      Begin MenuItem FileNew
         SpecialMenu = 0
         Text = "New &Project (From Project Manager)"
         Index = -2147483648
         ShortcutKey = "N"
         Shortcut = "Cmd+N"
         MenuModifier = True
         Icon = 1273460735
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem UntitledSeparator3
         SpecialMenu = 0
         Text = "-"
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem FileRegularNew
         SpecialMenu = 0
         Text = "&New"
         Index = -2147483648
         ShortcutKey = "N"
         Shortcut = "Cmd+Shift+N"
         MenuModifier = True
         AltMenuModifier = True
         Icon = 1273460735
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem FileOpen
         SpecialMenu = 0
         Text = "&Open"
         Index = -2147483648
         ShortcutKey = "O"
         Shortcut = "Cmd+O"
         MenuModifier = True
         Icon = 332953599
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem UntitledSeparator2
         SpecialMenu = 0
         Text = "-"
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem FileSave
         SpecialMenu = 0
         Text = "&Save"
         Index = -2147483648
         ShortcutKey = "S"
         Shortcut = "Cmd+S"
         MenuModifier = True
         Icon = 322347007
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem FileSaveAs
         SpecialMenu = 0
         Text = "Save &As..."
         Index = -2147483648
         Icon = 158859263
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem UntitledSeparator
         SpecialMenu = 0
         Text = "-"
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin QuitMenuItem FileQuit
         SpecialMenu = 0
         Text = "#App.kFileQuit"
         Index = -2147483648
         ShortcutKey = "#App.kFileQuitShortcut"
         Shortcut = "Cmd+#App.kFileQuitShortcut"
         MenuModifier = True
         AutoEnable = True
         Visible = True
      End
   End
   Begin MenuItem EditMenu
      SpecialMenu = 0
      Text = "&Edit"
      Index = -2147483648
      AutoEnable = True
      Visible = True
      Begin MenuItem EditUndo
         SpecialMenu = 0
         Text = "&Undo"
         Index = -2147483648
         ShortcutKey = "Z"
         Shortcut = "Cmd+Z"
         MenuModifier = True
         Icon = 325218303
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem EditRedo
         SpecialMenu = 0
         Text = "&Redo"
         Index = -2147483648
         ShortcutKey = "Y"
         Shortcut = "Cmd+Y"
         MenuModifier = True
         Icon = 1679067135
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem EditSeparator1
         SpecialMenu = 0
         Text = "-"
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem EditCut
         SpecialMenu = 0
         Text = "Cu&t"
         Index = -2147483648
         ShortcutKey = "X"
         Shortcut = "Cmd+X"
         MenuModifier = True
         Icon = 303738879
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem EditCopy
         SpecialMenu = 0
         Text = "&Copy"
         Index = -2147483648
         ShortcutKey = "C"
         Shortcut = "Cmd+C"
         MenuModifier = True
         Icon = 651296767
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem EditPaste
         SpecialMenu = 0
         Text = "&Paste"
         Index = -2147483648
         ShortcutKey = "V"
         Shortcut = "Cmd+V"
         MenuModifier = True
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem EditDelete
         SpecialMenu = 0
         Text = "&Delete"
         Index = -2147483648
         ShortcutKey = "Del"
         Shortcut = "Del"
         Icon = 1024180223
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem EditDuplicate
         SpecialMenu = 0
         Text = "Dupl&icate"
         Index = -2147483648
         ShortcutKey = "D"
         Shortcut = "Cmd+D"
         MenuModifier = True
         AutoEnable = True
         Visible = True
      End
   End
   Begin MenuItem ResourcesMenu
      SpecialMenu = 0
      Text = "&Resources"
      Index = -2147483648
      AutoEnable = True
      Visible = True
      Begin MenuItem ResourcesNewObject
         SpecialMenu = 0
         Text = "New &Object"
         Index = -2147483648
         ShortcutKey = "O"
         Shortcut = "Cmd+Shift+O"
         MenuModifier = True
         AltMenuModifier = True
         Icon = 1888638975
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem ResourcesNewSprite
         SpecialMenu = 0
         Text = "New &Sprite"
         Index = -2147483648
         ShortcutKey = "S"
         Shortcut = "Cmd+Shift+S"
         MenuModifier = True
         AltMenuModifier = True
         Icon = 538847231
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem ResourcesNewScene
         SpecialMenu = 0
         Text = "New S&cene"
         Index = -2147483648
         ShortcutKey = "R"
         Shortcut = "Cmd+Shift+R"
         MenuModifier = True
         AltMenuModifier = True
         Icon = 1997875199
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem ResourcesNewSound
         SpecialMenu = 0
         Text = "New S&ound"
         Index = -2147483648
         ShortcutKey = "A"
         Shortcut = "Cmd+Shift+A"
         MenuModifier = True
         AltMenuModifier = True
         Icon = 1846331391
         AutoEnable = True
         Visible = True
      End
   End
   Begin MenuItem ProjectMenu
      SpecialMenu = 0
      Text = "&Project"
      Index = -2147483648
      AutoEnable = True
      Visible = True
      Begin MenuItem MenuRun
         SpecialMenu = 0
         Text = "&Run"
         Index = -2147483648
         ShortcutKey = "R"
         Shortcut = "Cmd+R"
         MenuModifier = True
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem BuildItem
         SpecialMenu = 0
         Text = "&Build"
         Index = -2147483648
         ShortcutKey = "B"
         Shortcut = "Cmd+B"
         MenuModifier = True
         AutoEnable = True
         Visible = True
      End
   End
   Begin MenuItem PluginsMenu
      SpecialMenu = 0
      Text = "P&lugins"
      Index = -2147483648
      AutoEnable = True
      Visible = True
   End
   Begin MenuItem HelpMenu
      SpecialMenu = 0
      Text = "&Help"
      Index = -2147483648
      AutoEnable = True
      Visible = True
      Begin MenuItem AboutContents
         SpecialMenu = 0
         Text = "&Contents"
         Index = -2147483648
         ShortcutKey = "F2"
         Shortcut = "Cmd+F2"
         MenuModifier = True
         Icon = 540198911
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem UntitledSeparator0
         SpecialMenu = 0
         Text = "-"
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem AboutCheckForUpdates
         SpecialMenu = 0
         Text = "Check For &Updates"
         Index = -2147483648
         ShortcutKey = "U"
         Shortcut = "Cmd+U"
         MenuModifier = True
         Icon = 1169387519
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem AboutForums
         SpecialMenu = 0
         Text = "&Forums"
         Index = -2147483648
         Icon = 1882671103
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem AboutRegisterProduct
         SpecialMenu = 0
         Text = "&Register Product"
         Index = -2147483648
         Icon = 1650862079
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem UntitledSeparator1
         SpecialMenu = 0
         Text = "-"
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem AboutAbout
         SpecialMenu = 0
         Text = "&About"
         Index = -2147483648
         Icon = 1900564479
         AutoEnable = True
         Visible = True
      End
   End
End
#tag EndMenu
