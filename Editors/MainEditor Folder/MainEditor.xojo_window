#tag Window
Begin Window MainEditor
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   0
   FullScreen      =   False
   FullScreenButton=   True
   HasBackColor    =   False
   Height          =   538
   ImplicitInstance=   True
   LiveResize      =   True
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   True
   MaxWidth        =   32000
   MenuBar         =   2135144447
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   True
   Title           =   "PowerMode"
   Visible         =   True
   Width           =   974
   Begin PagePanel resourceEditor
      AutoDeactivate  =   True
      Enabled         =   True
      Height          =   491
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   240
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      PanelCount      =   1
      Panels          =   ""
      Scope           =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   27
      Value           =   0
      Visible         =   True
      Width           =   733
   End
   Begin LVStatusBar LVStatusBar1
      AcceptFocus     =   False
      AcceptTabs      =   False
      AutoDeactivate  =   True
      Backdrop        =   0
      DoubleBuffer    =   False
      Enabled         =   True
      EraseBackground =   True
      Height          =   32
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   80
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      Scope           =   0
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   -63
      Transparent     =   True
      UseFocusRing    =   True
      Visible         =   True
      Width           =   40
   End
   Begin LVToolBar LVToolBar1
      AcceptFocus     =   False
      AcceptTabs      =   False
      AquaBackground  =   False
      AutoDeactivate  =   True
      Backdrop        =   0
      DoubleBuffer    =   False
      EnableContextualMenu=   True
      Enabled         =   True
      EraseBackground =   True
      FontColourDefault=   False
      Height          =   32
      HelpTag         =   ""
      HideHeight      =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   39
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      mToolBarStyleChanged=   True
      RefreshLock     =   False
      Scope           =   0
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      ToolbarStyle    =   0
      Top             =   -63
      Transparent     =   True
      UseFocusRing    =   True
      Visible         =   True
      Width           =   38
   End
   Begin CustomTabPanelTabs OpenResourceTabs
      AcceptFocus     =   False
      AcceptTabs      =   False
      AutoDeactivate  =   True
      Backdrop        =   0
      DoubleBuffer    =   False
      Enabled         =   True
      EnableTabReordering=   False
      EraseBackground =   True
      Facing          =   0
      Height          =   25
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   240
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Scope           =   0
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   0
      Transparent     =   True
      UseFocusRing    =   True
      value           =   0
      Visible         =   True
      Width           =   733
   End
   Begin PagePanel resourceTreePanel
      AutoDeactivate  =   True
      Enabled         =   True
      Height          =   491
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      PanelCount      =   0
      Panels          =   ""
      Scope           =   0
      TabIndex        =   6
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   27
      Value           =   0
      Visible         =   True
      Width           =   228
   End
   Begin CustomTabPanelTabs resourceTreeTabs
      AcceptFocus     =   False
      AcceptTabs      =   False
      AutoDeactivate  =   True
      Backdrop        =   0
      DoubleBuffer    =   False
      Enabled         =   True
      EnableTabReordering=   False
      EraseBackground =   True
      Facing          =   0
      Height          =   25
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   7
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   0
      Transparent     =   True
      UseFocusRing    =   True
      value           =   0
      Visible         =   True
      Width           =   232
   End
   Begin imSplitter imSplitter1
      AcceptFocus     =   False
      AcceptTabs      =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Backdrop        =   0
      DockAfter       =   True
      DockAfterSize   =   40
      DockBefore      =   True
      DockBeforeSize  =   40
      DoubleBuffer    =   True
      DoubleClickAction=   1
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   491
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      IsDocked        =   False
      IsDockedPosition=   ""
      Left            =   229
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MinAfterArea    =   20
      MinBeforeArea   =   30
      Scope           =   0
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   27
      Transparent     =   True
      UseFocusRing    =   True
      Visible         =   True
      Width           =   10
   End
   Begin Listbox resourceTree
      AutoDeactivate  =   True
      AutoHideScrollbars=   True
      Bold            =   False
      Border          =   True
      ColumnCount     =   1
      ColumnsResizable=   False
      ColumnWidths    =   ""
      DataField       =   ""
      DataSource      =   ""
      DefaultRowHeight=   -1
      Enabled         =   True
      EnableDrag      =   True
      EnableDragReorder=   True
      GridLinesHorizontal=   0
      GridLinesVertical=   0
      HasHeading      =   False
      HeadingIndex    =   -1
      Height          =   32
      HelpTag         =   ""
      Hierarchical    =   True
      Index           =   0
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      RequiresSelection=   False
      Scope           =   0
      ScrollbarHorizontal=   True
      ScrollBarVertical=   True
      SelectionType   =   1
      TabIndex        =   7
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   -63
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   False
      Width           =   36
      _ScrollOffset   =   0
      _ScrollWidth    =   -1
   End
   Begin Timer timerStartAction
      Height          =   32
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   0
      LockedInPosition=   False
      Mode            =   2
      Period          =   1
      Scope           =   0
      TabPanelIndex   =   "0"
      Top             =   0
      Width           =   32
   End
   Begin Timer Repeater
      Height          =   32
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   20
      LockedInPosition=   False
      Mode            =   2
      Period          =   1
      Scope           =   0
      TabPanelIndex   =   "0"
      Top             =   20
      Width           =   32
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Function CancelClose(appQuitting as Boolean) As Boolean
		  #pragma unused appQuitting
		  
		  if DocFile.ModifiedDoc then
		    select case saveChangesQuestion
		    case "Save", "Don't Save"
		      return false
		    case "Cancel"
		      return true
		    end select
		  end if
		End Function
	#tag EndEvent

	#tag Event
		Sub EnableMenuItems()
		  FileSave.Enabled = DocFile.ModifiedDoc
		  DisableItemAccordingly(EditPaste,1,SelectedResource)
		  'EditUndo.Enabled = CanUndo
		  'EditRedo.Enabled = CanRedo
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  //build the resourceTrees
		  for loopIndex as integer = 0 to 3
		    dim i as new resourceTree
		    i.Parent = resourceTreePanel
		    i.PanelIndex = loopIndex
		    i.Width = resourceTreePanel.Width
		    i.Height = resourceTreePanel.Height
		    i.visible = true
		  next
		  
		  theNumKeys = new NumKeys
		  
		  'BuildProject.RequesterIndex = new MyRequesterIndex
		  
		End Sub
	#tag EndEvent


	#tag MenuHandler
		Function AboutAbout() As Boolean Handles AboutAbout.Action
			WinAbout.ShowModal
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function AboutCheckForUpdates() As Boolean Handles AboutCheckForUpdates.Action
			
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function AboutContents() As Boolean Handles AboutContents.Action
			
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function AboutForums() As Boolean Handles AboutForums.Action
			ShowURL("http://forum.powermodegames.com")
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function AboutRegisterProduct() As Boolean Handles AboutRegisterProduct.Action
			
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function BuildItem() As Boolean Handles BuildItem.Action
			call BuildProjectHTML5
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function EditCopy() As Boolean Handles EditCopy.Action
			Copy
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function EditCut() As Boolean Handles EditCut.Action
			Cut
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function EditDelete() As Boolean Handles EditDelete.Action
			DeleteResource
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function EditDuplicate() As Boolean Handles EditDuplicate.Action
			
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function EditPaste() As Boolean Handles EditPaste.Action
			Paste
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function EditRedo() As Boolean Handles EditRedo.Action
			Redo
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function EditUndo() As Boolean Handles EditUndo.Action
			Undo
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FileNew() As Boolean Handles FileNew.Action
			NewProjectFromManager
			Return True
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FileOpen() As Boolean Handles FileOpen.Action
			call OpenProject
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FileQuit() As Boolean Handles FileQuit.Action
			
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FileRegularNew() As Boolean Handles FileRegularNew.Action
			NewProject
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FileSave() As Boolean Handles FileSave.Action
			call SaveProject
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FileSaveAs() As Boolean Handles FileSaveAs.Action
			call saveProjectAs
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function MenuRun() As Boolean Handles MenuRun.Action
			RunProjectHTML5
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function PluginsMenu() As Boolean Handles PluginsMenu.Action
			PluginDev.Show
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function ResourcesNewObject() As Boolean Handles ResourcesNewObject.Action
			call NewObject
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function ResourcesNewScene() As Boolean Handles ResourcesNewScene.Action
			call NewScene
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function ResourcesNewSound() As Boolean Handles ResourcesNewSound.Action
			call NewSound
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function ResourcesNewSprite() As Boolean Handles ResourcesNewSprite.Action
			call NewSprite
			Return True
			
		End Function
	#tag EndMenuHandler


	#tag Method, Flags = &h0
		Sub AddDefaultParentObject()
		  //create object without setting it up for editing or opening it in the tabpanel
		  NewObject(false, false, new ResourceObjectDefaultParent)
		  //rename the resource without performing any checks (as we know it is a valid name already)
		  DocFile.Resources(DocFile.Resources.Ubound).name = ResourceObject.DEFAULT_PARENT_NAME
		  DocFile.Resources(DocFile.Resources.Ubound).MakeName ResourceObject.DEFAULT_PARENT_NAME, false
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub ConstructResMenu(base As MenuItem)
		  dim SResource() as integer  = SelectedResource
		  dim NewChild As MenuItem
		  dim isDefaultParent as boolean = false
		  for each res as uint32 in SResource
		    if resources(res) isa ResourceObjectDefaultParent and resourceTreeTabs.Value = uint32(ResourceType.object) then
		      isDefaultParent = true
		      exit for
		    end if
		  next
		  
		  NewChild = new MenuItem
		  NewChild.Text = "Import"
		  NewChild.Icon = Icon("Open")
		  base.append NewChild
		  
		  if isDefaultParent = false then
		    NewChild = new MenuItem
		    DisableItemAccordingly(NewChild,0,SResource)
		    NewChild.Text = "Export"
		    NewChild.Icon = nil
		    base.append NewChild
		    
		    base.append(New MenuItem(MenuItem.TextSeparator))
		    
		    NewChild = new MenuItem
		    DisableItemAccordingly(NewChild,0,SResource)
		    NewChild.Text = "Cut"
		    #if TargetMacOS
		      NewChild.KeyboardShortcut = "Cmd-X"
		    #else
		      NewChild.KeyboardShortcut = "Ctrl-X"
		    #endif
		    NewChild.Icon = Icon("Cut")
		    base.append NewChild
		    
		    NewChild = new MenuItem
		    DisableItemAccordingly(NewChild,0,SResource)
		    NewChild.Text = "Copy"
		    #if TargetMacOS
		      NewChild.KeyboardShortcut = "Cmd-C"
		    #else
		      NewChild.KeyboardShortcut = "Ctrl-C"
		    #endif
		    NewChild.Icon = Icon("Copy")
		    base.append NewChild
		  end if
		  
		  NewChild = new MenuItem
		  DisableItemAccordingly(NewChild,1,SResource)
		  NewChild.Text = "Paste"
		  #if TargetMacOS
		    NewChild.KeyboardShortcut = "Cmd-V"
		  #else
		    NewChild.KeyboardShortcut = "Ctrl-V"
		  #endif
		  NewChild.Icon = Icon("Paste")
		  base.append newChild
		  
		  if isDefaultParent = false then
		    NewChild = new MenuItem
		    DisableItemAccordingly(NewChild,0,SResource)
		    NewChild.Text = "Delete"
		    NewChild.KeyboardShortcut = "Del"
		    NewChild.Icon = Icon("Delete")
		    base.append NewChild
		    
		    NewChild = new MenuItem
		    DisableItemAccordingly(NewChild,0,SResource)
		    NewChild.Text = "Duplicate"
		    #if TargetMacOS
		      NewChild.KeyboardShortcut = "Cmd-D"
		    #else
		      NewChild.KeyboardShortcut = "Ctrl-D"
		    #endif
		    NewChild.Icon = Icon("Copy")
		    base.append NewChild
		    
		    base.append(New MenuItem(MenuItem.TextSeparator))
		    
		    NewChild = new MenuItem
		    DisableItemAccordingly(NewChild,0,SResource)
		    NewChild.Text = "Rename"
		    #if TargetMacOS
		      NewChild.KeyboardShortcut = "Cmd-R"
		    #else
		      NewChild.KeyboardShortcut = "Ctrl-R"
		    #endif
		    NewChild.Icon = nil
		    base.append NewChild
		  end if
		  if isDefaultParent then
		    base.append(New MenuItem(MenuItem.TextSeparator))
		    
		    NewChild = new MenuItem
		    NewChild.Text = "The object " + ResourceObject.DEFAULT_PARENT_NAME + " is not to be copied or deleted in any way."
		    NewChild.Enabled = false
		    base.append NewChild
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub Copy()
		  CopyResource
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub CopyResource()
		  dim c as new Clipboard
		  dim selectedRes() as integer = SelectedResource(true)
		  for each res as resource in DocFile.resources
		    if res.ListIndex = selectedRes(0) and res.typeTab = resourceTreeTabs.Value then
		      c.SetText(res.WriteXMLString)
		      exit for
		    end if
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub Cut()
		  if FocusItem = 0 then
		    CutResource
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub CutResource()
		  DeleteResource(true)
		  CopyResource
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub DeleteResource(ready as Boolean = false)
		  if FocusItem = 0 then
		    dim SResource() as integer
		    
		    dim ResourceTypeName as string
		    select case resourceTreeTabs.value
		    case 0
		      ResourceTypeName = "object"
		    case 1
		      ResourceTypeName = "sprite"
		    case 2
		      ResourceTypeName = "scene"
		    case 3
		      ResourceTypeName = "sound"
		    end select
		    
		    if ready = false then
		      'see if user wants to delete selected objects
		      Dim d as New MessageDialog //declare the MessageDialog object
		      Dim b as MessageDialogButton //for handling the result
		      d.icon = MessageDialog.GraphicCaution //display warning icon
		      d.ActionButton.Caption = "Yes"
		      d.AlternateActionButton.Visible = True //show the "No" button
		      d.AlternateActionButton.Caption = "No"
		      'get resource to delete
		      if SelectedResourceTree.ListCount <> 0 then
		        SResource = SelectedResource
		        dim shownDialog as Boolean = false
		        for s as uint32 = SResource.Ubound downto 0 //loops backwards for the correct indexes
		          dim i as uint32 = SResource(s)
		          if shownDialog = false then
		            if SResource.Ubound = 0 then
		              if SelectedResourceTree.RowTag(i) isa resource then
		                d.Message = "Are you sure you want to delete the  " + resourceTypeName + ", """ + SelectedResourceTree.Cell(i,0)+"""?"
		                d.Explanation = "This selected "+resourceTypeName+", will be deleted from your project if you choose 'Yes'."
		              else
		                d.Message = "Are you sure you want to delete the  folder, """ + SelectedResourceTree.Cell(i,0) + """?"
		                d.Explanation = "This selected "+resourceTypeName+", will be deleted from your project if you choose 'Yes'."
		              end if
		            else
		              d.Message = "Are you sure you want to delete your selection of "+ResourceTypeName+"s and/or folders?"
		              d.Explanation = "This selection "+resourceTypeName+"s and/or folders, will be deleted from your project if you choose 'Yes'."
		              shownDialog = true
		            end if
		            b = d.ShowModal //display the dialog
		          end if
		        next
		        Select Case b //determine which button was pressed.
		        Case d.ActionButton
		          'for each res as resource in DocFile.resources
		          'if res.ListIndex = i and res.typeTab = resourceTreeTabs.Value then
		          'res.Delete
		          'exit for
		          'end if
		          'next
		          resource.ConfirmedDeletion(SResource)
		        End Select
		      end if
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1001
		Protected Sub DisableItemAccordingly(Item as MenuItem, disableType as integer, SResource() as integer)
		  'default to being enabled
		  'Item.Enabled = true
		  
		  if disableType = 0 then 'disable if nothing selected
		    item.Enabled = (SResource.Ubound <> -1)
		  elseif disableType = 1 then 'disable if clipboard can be used
		    Clipboard = new Clipboard
		    try
		      #pragma BreakOnExceptions Off
		      dim x as new XmlDocument(Clipboard.Text)
		      // check first node of document:
		      Dim n As XmlNode = x.FirstChild
		      item.Enabled = (n.Name = "resource")
		    catch e as XmlException
		      item.Enabled = false
		    end try
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub DuplicateResource()
		  for each i as integer in SelectedResource
		    for each res as resource in DocFile.resources
		      if res.ListIndex = i and res.typeTab = resourceTreeTabs.Value then
		        //write and import xml to directly duplicate the resource
		        dim x as new XmlDocument
		        res.WriteXml(x)
		        Import(x)
		        exit for
		      end if
		    next
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub ExportResource()
		  for each i as integer in SelectedResource
		    for each res as resource in DocFile.resources
		      if res.ListIndex = i and res.typeTab = MainEditor.resourceTreeTabs.value then
		        res.Export
		        exit for
		      end if
		    next
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetEmbeddedEditorByTabValue(value as integer) As EmbeddedControl
		  dim tab as CustomTab = OpenResourceTabs.tabs(value)
		  
		  //find editor that is assigned to that tab
		  for each c as EmbeddedControl in me.EmbeddedEditors
		    if c.Tab = tab then
		      return c
		    end if
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetResByEmbeddedEditor(editor as EmbeddedControl) As Resource
		  for each r as resource in DocFile.Resources
		    if r.EmbeddedEditor = editor then
		      return r
		    end if
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Import(x as XmlDocument, node as XmlNode = nil)
		  '#pragma BreakOnExceptions false
		  try
		    DocFile.ModifiedDoc = true
		    
		    
		    Dim resourceNode As XmlNode
		    if node = nil then
		      //first child should directly be the resource
		      resourceNode = x.FirstChild
		    else
		      //given node should directly be the resource
		      resourceNode = node
		    end if
		    
		    // and now walk over all sub nodes:
		    dim name as string =resourceNode.GetAttribute("Name")
		    dim typeTab as integer = val(resourceNode.GetAttribute("TypeID"))
		    
		    dim customResource as Resource
		    
		    if name = ResourceObjectDefaultParent.DEFAULT_PARENT_NAME then
		      customResource = new ResourceObjectDefaultParent
		    end if
		    
		    resource.Institute(name, ResourceType(typeTab), (node <> nil), x, resourceNode, true, true, customResource, false)
		  catch e as XmlException
		  end try
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		 Shared Function LastSelectedResource() As integer
		  dim SResource() as integer = MainEditor.SelectedResource
		  if SResource.Ubound <> -1 then
		    return SResource(SResource.Ubound)
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MakeNames()
		  for each res as resource in DocFile.resources
		    if res.typeTab = MainEditor.OpenResourceTabs.value and MainEditor.LastSelectedResource = res.ListIndex then
		      res.MakeName(MainEditor.SelectedResourceTree.Cell(MainEditor.LastSelectedResource,0))
		      exit for res
		    end if
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub NewFolder(editCell as boolean = true)
		  const name = "Folder"
		  dim list as listbox = SelectedResourceTree
		  dim selectedItems() as integer = SelectedResource
		  if selectedItems.Ubound <> -1 then
		    //insert folder after selected item
		    list.InsertFolder(selectedItems(selectedItems.Ubound) + 1, name)
		  else
		    list.AddFolder(name)
		  end if
		  
		  if editCell then
		    list.EditCell(list.LastIndex, 0)
		  end if
		  
		  for each res as resource in resources
		    if res.ListIndex >= list.LastIndex then
		      res.ListIndex = res.ListIndex + 1
		    end if
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub NewObject(editCell as boolean = true, openTab as  boolean = true, customResource as resource = nil)
		  resource.Institute("Object",ResourceType.object, false, nil, nil, editCell, openTab, customResource, true)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub NewProjectFromManager()
		  ProjectChooser.Show
		  SetupNewProject.Show
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub NewScene(editCell as boolean = true)
		  resource.Institute("Scene",ResourceType.scene,false, nil, nil, editCell)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub NewSound(editCell as boolean = true)
		  resource.Institute("Sound",ResourceType.sound, false, nil, nil, editCell)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub NewSprite(editCell as boolean = true)
		  resource.Institute("Sprite",ResourceType.sprite, false, nil, nil, editCell)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub OpenInitialTabs()
		  if OpenResourceTabs.tabCount = 0 then
		    dim cont as new conPreferences
		    cont.EmbedWithinPanel(MainEditor.resourceEditor, 0) //embed preferences
		    OpenResourceTabs.Append "App Preferences", nil, false
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		 Shared Sub OpenResTab(tag as resource, selectInTree as boolean = false)
		  if tag.OpenTabValue = -1 then
		    tag.MakeOpenTab
		  else
		    MainEditor.OpenResourceTabs.value = tag.OpenTabValue
		  end if
		  
		  if selectInTree then
		    MainEditor.resourceTreeTabs.value = tag.typeTab
		    MainEditor.SelectedResourceTree.DeselectAll
		    MainEditor.SelectedResourceTree.Selected(tag.ListIndex) = true
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub Paste()
		  PasteResource
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1001
		Protected Sub PasteResource()
		  try
		    Clipboard = new Clipboard
		    Import(new XmlDocument(Clipboard.text))
		  catch XmlException
		  end try
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrepImport()
		  Dim dlg as OpenDialog
		  Dim f as FolderItem
		  dlg = New OpenDialog
		  dlg.MultiSelect = true
		  #If Not (TargetLinux) then
		    dlg.InitialDirectory = SpecialFolder.Documents
		  #Else //open Home directory on linux
		    dlg.InitialDirectory = SpecialFolder.Home
		  #endif
		  dlg.Title = "Select one or more XML files"
		  dlg.Filter = ProjectFile.Project //mif file type defined in FileTypes1 set
		  f = dlg.ShowModal()
		  If f <> Nil then
		    //proceed normally
		    dim xml as new XmlDocument
		    for i as integer = 0 to dlg.count-1
		      xml.LoadXml(dlg.Item(i))
		      Import(xml)
		    next
		  Else
		    //User Cancelled
		  End if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub Redo()
		  'DocFile.ModifiedDoc = true
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveTab(tabIndex as integer)
		  for each res as resource in DocFile.resources //if the resource matches the tab when the tab is removed then delete specify that there is no resource tab open
		    if res.OpenTabValue = tabIndex then
		      res.OpenTabValue = -1
		    end if
		    if res.OpenTabValue>tabIndex then
		      res.OpenTabValue = res.OpenTabValue -1
		    end if
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RenameResource()
		  dim selectedRes(0) as integer = SelectedResource
		  SelectedResourceTree.EditCell(selectedRes(0),0)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub ResMenuAction(hitItem as MenuItem)
		  select case hitItem.Text
		  case "Export"
		    ExportResource
		  case "Copy"
		    CopyResource
		  case "Paste"
		    PasteResource
		  case "Cut"
		    CutResource
		  case "Import"
		    PrepImport
		  case "Delete"
		    DeleteResource
		  case "Duplicate"
		    DuplicateResource
		  case "Rename"
		    RenameResource
		  end select
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1000
		Function saveChangesQuestion() As string
		  Dim d as New MessageDialog //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  d.icon = MessageDialog.GraphicCaution //display warning icon
		  d.ActionButton.Caption = "Save"
		  d.CancelButton.Visible = True //show the Cancel button
		  d.AlternateActionButton.Visible = True //show the "Don't Save" button
		  d.AlternateActionButton.Caption = "Don't Save"
		  d.Message = "Do you want to save changes to this document before closing?"
		  d.Explanation = "If you don't save, your changes will be lost. "
		  
		  b = d.ShowModal //display the dialog
		  Select Case b //determine which button was pressed.
		  Case d.ActionButton
		    //user pressed Save
		    SaveProject
		    if DocFile.ProjectFileItem = nil then
		      return "Save"
		    else
		      return "Don't Save"
		    end if
		  Case d.AlternateActionButton
		    //user pressed Don't Save
		    return "Don't Save"
		  Case d.CancelButton
		    //user pressed Cancel
		    return "Cancel"
		  End select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SaveProject(dialog as boolean = false)
		  DocFile.SaveProject dialog, ProjectFile.PowerMode
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SaveProjectAs()
		  DocFile.SaveProject true,ProjectFile.PowerMode
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1000
		Function SelectedResource(checkOnce as Boolean = false) As integer()
		  if SelectedResourceTree <> nil then
		    dim value() as integer
		    for i as integer = 0 to SelectedResourceTree.ListCount - 1
		      if SelectedResourceTree.Selected(i) then
		        value.append i
		        if checkOnce = true then exit for i //only execites this once if specified to do so
		      end if
		    next
		    return value
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function SelectedResourceTree() As Listbox
		  return resourceTree(resourceTreeTabs.value +1)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub SetCaption()
		  //set caption for editor
		  MainEditor.Title = DocFile.ProjectFileItem.Name + " - PowerMode"
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub Undo()
		  'DocFile.ModifiedDoc = true
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private Clipboard As Clipboard
	#tag EndProperty

	#tag Property, Flags = &h0
		EmbeddedEditors() As EmbeddedControl
	#tag EndProperty

	#tag Property, Flags = &h0
		FocusItem As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		JustPressedControlKey As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mPressedControlKey As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private name As string
	#tag EndProperty

	#tag Property, Flags = &h0
		objectToDelete As Integer
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mPressedControlKey
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if (mPressedControlKey = false) and (theNumKeys.AnyKeyDown = false) and (value = true) then
			    JustPressedControlKey = true
			  end if
			  mPressedControlKey = value
			End Set
		#tag EndSetter
		PressedControlKey As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private ResourceTreeOldListIndex As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		tbSave As LVToolBarItem
	#tag EndProperty

	#tag Property, Flags = &h21
		Private theNumKeys As NumKeys
	#tag EndProperty


#tag EndWindowCode

#tag Events resourceEditor
	#tag Event
		Sub MouseExit()
		  FocusItem = 0
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  FocusItem = 1
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events LVToolBar1
	#tag Event
		Sub Action(selectedToolBarItem As LVToolBarItem)
		  select case selectedToolBarItem.Caption
		  case "New (Project Manager)"
		    NewProjectFromManager
		  case "New"
		    NewProject
		  case "Open"
		    OpenProject
		  case "Save"
		    SaveProject
		  case "Save As"
		    saveProjectAs
		  case "Undo"
		    Undo
		  case "Redo"
		    Redo
		  case "New Object"
		    NewObject
		  case "New Sprite"
		    NewSprite
		  case "New Scene"
		    NewScene
		  case "New Sound"
		    NewSound
		  case "New Folder"
		    NewFolder
		  case "Copy"
		    Copy
		  case "Paste"
		    Paste
		  case "Cut"
		    Cut
		  case "Duplicate"
		    DuplicateResource
		  case "Delete"
		    DeleteResource(false)
		  case "Run"
		    RunProjectHTML5
		  case "Build"
		    call BuildProjectHTML5
		  end Select
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  dim tb as LVToolBarItem
		  dim tbSeparator as LVStandardToolBarItem
		  
		  me.StatusBar = LVStatusBar1
		  
		  tb = New LVToolBarItem
		  tb.Caption = "New (Project Manager)"
		  tb.HelpTag = "Create a new project from the project manager"
		  tb.Image = Icon("New")
		  tb.Visible = true
		  me.AddButton(tb)
		  
		  tbSeparator = New LVStandardToolBarItem
		  me.AddButton(tbSeparator)
		  
		  tb = New LVToolBarItem
		  tb.Caption = "New"
		  tb.HelpTag = "Create a new project"
		  tb.Image = Icon("New")
		  tb.Visible = true
		  me.AddButton(tb)
		  
		  tb = New LVToolBarItem
		  tb.Caption = "Open"
		  tb.HelpTag = "Open an existing project"
		  tb.Image = Icon("Open")
		  tb.Visible = true
		  me.AddButton(tb)
		  
		  tbSeparator = New LVStandardToolBarItem
		  me.AddButton(tbSeparator)
		  
		  tb = new LVToolBarItem
		  tb.Caption = "Save"
		  tb.HelpTag = "Save project"
		  tb.Image = Icon("Save")
		  tb.Visible = true
		  me.AddButton(tb)
		  tb.Enabled = false //redraws button so you have to set this variable after executing the AddButton method
		  tbSave = tb
		  
		  tb = new LVToolBarItem
		  tb.Caption = "Save As"
		  tb.HelpTag = "Save project as"
		  tb.Image = Icon("SaveAs")
		  tb.Visible = true
		  me.AddButton(tb)
		  
		  tbSeparator = New LVStandardToolBarItem
		  me.AddButton(tbSeparator)
		  
		  tb = New LVToolBarItem
		  tb.Caption = "Cut"
		  tb.HelpTag = "Cut"
		  tb.Image = Icon("Cut")
		  tb.Visible = true
		  me.AddButton(tb)
		  
		  tb = New LVToolBarItem
		  tb.Caption = "Copy"
		  tb.HelpTag = "Copy"
		  tb.Image = Icon("Copy")
		  tb.Visible = true
		  me.AddButton(tb)
		  
		  tb = new LVToolBarItem
		  tb.Caption = "Paste"
		  tb.HelpTag = "Paste"
		  tb.Image = Icon("Paste")
		  tb.Visible = true
		  me.AddButton(tb)
		  
		  tb = New LVToolBarItem
		  tb.Caption = "Delete"
		  tb.HelpTag = "Delete selection"
		  tb.Image = Icon("Delete")
		  tb.Visible = true
		  me.AddButton(tb)
		  
		  tb = New LVToolBarItem
		  tb.Caption = "Duplicate"
		  tb.HelpTag = "Duplicate selection"
		  tb.Image = Icon("Copy")
		  tb.Visible = true
		  me.AddButton(tb)
		  
		  tbSeparator = New LVStandardToolBarItem
		  me.AddButton(tbSeparator)
		  
		  tb = New LVToolBarItem
		  tb.Caption = "Undo (COMING SOON)"
		  tb.HelpTag = "Undo last action"
		  tb.Image =Icon("Undo")
		  tb.Visible = true
		  me.AddButton(tb)
		  
		  tb = New LVToolBarItem
		  tb.Caption = "Redo (COMING SOON)"
		  tb.HelpTag = "Redo last action"
		  tb.Image = Icon("Redo")
		  tb.Visible = true
		  Me.AddButton(tb)
		  
		  tbSeparator = New LVStandardToolBarItem
		  me.AddButton(tbSeparator)
		  
		  tb = new LVToolBarItem
		  tb.Caption = "New Object"
		  tb.HelpTag = "Create a new object"
		  tb.Image =Icon("Object")
		  tb.Visible = true
		  Me.AddButton(tb)
		  
		  tb = new LVToolBarItem
		  tb.Caption = "New Sprite"
		  tb.HelpTag = "Create a new sprite"
		  tb.Image = Icon("Sprite")
		  tb.Visible = true
		  Me.AddButton(tb)
		  
		  tb = New LVToolBarItem
		  tb.Caption = "New Scene"
		  tb.HelpTag = "Create a new scene"
		  tb.Image = Icon("Scene")
		  tb.Visible = true
		  Me.AddButton(tb)
		  
		  tb = New LVToolBarItem
		  tb.Caption = "New Sound"
		  tb.HelpTag = "Create a new sound"
		  tb.Image = Icon("Sound")
		  tb.Visible = true
		  Me.AddButton(tb)
		  
		  tb = New LVToolBarItem
		  tb.Caption = "New Folder (COMING SOON)"
		  tb.HelpTag = "Create a folder to organise assets such as objects, sprites, scenes and sounds"
		  tb.Image = Icon("NewFolder")
		  tb.Visible = true
		  Me.AddButton(tb)
		  
		  tbSeparator = New LVStandardToolBarItem
		  me.AddButton(tbSeparator)
		  
		  tb = New LVToolBarItem
		  tb.Caption = "Run"
		  tb.HelpTag = "Run project from localhost"
		  tb.Image = Icon("Play")
		  tb.Visible = true
		  me.AddButton(tb)
		  
		  tb = New LVToolBarItem
		  tb.Caption = "Build"
		  tb.HelpTag = "Build project"
		  tb.Image = Icon("Build")
		  tb.Visible = true
		  Me.AddButton(tb)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events OpenResourceTabs
	#tag Event
		Sub Open()
		  me.EnableTabReordering = true
		  me.attachPanel resourceEditor
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub TabMoved(oldTabVal as integer,newTabVal as integer)
		  for each res as resource in DocFile.resources
		    if  res.OpenTabValue = oldTabVal then
		      res.BackupOpenTabValue = newTabVal
		    end if
		    
		    if newTabVal<oldTabVal and res.OpenTabValue<oldTabVal and res.OpenTabValue >= newTabVal then
		      res.BackupOpenTabValue = res.OpenTabValue +1
		    end if
		    
		    if newTabVal>oldTabVal and res.OpenTabValue>oldTabVal and res.OpenTabValue<= newTabVal then
		      res.BackupOpenTabValue = res.OpenTabValue -1
		    end if
		  next
		  
		  
		  for each res as resource in DocFile.resources
		    if res.BackupOpenTabValue <> -1 then
		      res.OpenTabValue = res.BackupOpenTabValue
		    end if
		  next
		  
		  EmbeddedEditors.Insert(newTabVal, EmbeddedEditors(oldTabVal))
		  EmbeddedEditors.Remove(oldTabVal)
		End Sub
	#tag EndEvent
	#tag Event
		Function ConstructContextualMenu(base as MenuItem, x as Integer, y as Integer) As Boolean
		  #pragma Unused x
		  #pragma Unused y
		  
		  for each res as resource in resources
		    if res.OpenTabValue = me.value then
		      resourceTreeTabs.value = res.typeTab
		      //refocus so listbox is not in edit mode
		      self.SetFocus
		      me.SetFocus
		      //loop through each selected resource and deselect them all
		      for each i as uint32 in SelectedResource
		        res.UsedResourceTree.Selected(i) = false
		      next
		      //select the resource's designated cell
		      res.UsedResourceTree.Selected(res.ListIndex) = true
		      exit for
		    end if
		  next
		  
		  if OpenResourceTabs.caption(OpenResourceTabs.Value) <> "App Preferences" then
		    ConstructResMenu(base)
		  end if
		End Function
	#tag EndEvent
	#tag Event
		Function ContextualMenuAction(hitItem as MenuItem) As Boolean
		  ResMenuAction(hitItem)
		  return true
		End Function
	#tag EndEvent
	#tag Event
		Sub TabChanged(value as integer)
		  if GetEmbeddedEditorByTabValue(value)isa ResourceEditorInterface then
		    ResourceEditorInterface(GetEmbeddedEditorByTabValue(value)).CallGotTabFocus
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub BeforeTabRemove(tabIndex as integer, massRemove as boolean)
		  if massRemove = false then
		    dim indexOfEditor as integer = EmbeddedEditors.IndexOf(GetEmbeddedEditorByTabValue(tabIndex))
		    GetResByEmbeddedEditor(EmbeddedEditors(indexOfEditor)).EmbeddedEditor = nil
		    EmbeddedEditors.Remove(indexOfEditor)
		    RemoveTab(tabIndex)
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events resourceTreePanel
	#tag Event
		Function ConstructContextualMenu(base as MenuItem, x as Integer, y as Integer) As Boolean
		  #pragma Unused x
		  #pragma Unused y
		  
		  ConstructResMenu(base)
		End Function
	#tag EndEvent
	#tag Event
		Function ContextualMenuAction(hitItem as MenuItem) As Boolean
		  ResMenuAction(hitItem)
		  return true
		End Function
	#tag EndEvent
#tag EndEvents
#tag Events resourceTreeTabs
	#tag Event
		Sub Open()
		  me.attachPanel(resourceTreePanel)
		  
		  me.Append("",  Icon("Object"), false)
		  me.Append("", Icon("Sprite"), false)
		  me.Append("", Icon("Scene"), false)
		  me.Append("", Icon("Sound"), false)
		  
		  //select first tab
		  me.value = 0
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events resourceTree
	#tag Event
		Sub DoubleClick()
		  #pragma unused index
		  
		  dim res() as integer = SelectedResource(true)
		  if not me.RowTag(res(0)) isa ResourceObjectDefaultParent then
		    Me.EditCell(res(0),0)
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub CellTextChange(row as Integer, column as Integer)
		  me.Cell(row,column) = me.Cell(row,column).ReplaceAll(" ","")
		  for each res as resource in DocFile.resources
		    if res.ListIndex = row and res.typeTab = index-1 then
		      OpenResourceTabs.caption(res.OpenTabValue) = resourceTree(index).Cell(row,0)
		      exit for
		    end if
		  next
		End Sub
	#tag EndEvent
	#tag Event
		Function CellClick(row as Integer, column as Integer, x as Integer, y as Integer) As Boolean
		  #pragma unused index
		  #pragma Unused column
		  #pragma Unused x
		  #pragma Unused y
		  
		  if not IsContextualClick then
		    OpenResTab(me.RowTag(row))
		  end if
		End Function
	#tag EndEvent
	#tag Event
		Sub CellAction(row As Integer, column As Integer)
		  #pragma unused index
		  #pragma unused column
		  
		  DocFile.ModifiedDoc = true
		  
		  dim editor as EmbeddedControl = GetEmbeddedEditorByTabValue(OpenResourceTabs.value)
		  
		  for i as integer = resources.Ubound downto 0
		    dim res as resource = resources(i)
		    if res.ListIndex = row and res.UsedResourceTree = me then
		      res.MakeName me.Cell(row, 0)
		      
		      //refresh info in object editor
		      if editor isa ResourceEditorInterface then
		        ResourceEditorInterface(editor).CallGotTabFocus
		      end if
		      
		      exit for
		    end if
		  next
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events timerStartAction
	#tag Event
		Sub Action()
		  me.Enabled = false
		  self.maximize
		  
		  //ensure positions are correct after maximize
		  for loopIndex as integer = 1 to 4
		    dim i as Listbox = resourceTree(loopIndex)
		    i.Left = resourceTreePanel.Left
		    i.Top = resourceTreePanel.Top
		  next
		  
		  imSplitter1.AddControl(resourceTreePanel,true)
		  imSplitter1.AddControl(resourceEditor,false)
		  
		  OpenInitialTabs
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Repeater
	#tag Event
		Sub Action()
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FocusItem"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
			"11 - Modeless Dialog"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="400"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="JustPressedControlKey"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="objectToDelete"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="PressedControlKey"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="600"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
