#tag Class
Protected Class NumKeys
	#tag Method, Flags = &h0
		Function AnyKeyDown() As boolean
		  return (CurrentKey <> -1)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CurrentKey() As integer
		  //assign id for each numkey
		  static numKeys() as integer
		  if numKeys.Ubound = -1 then
		    numKeys = Array(Int32(&h1D), &h12, &h13, &h14, &h15, &h17, &h16, &h1A, &h1C, &h19)
		  end if
		  
		  //loop through number keys
		  dim k, i as integer
		  for i = 0 to numKeys.Ubound
		    //get numKey id
		    k = numKeys(i)
		    if Keyboard.AsyncKeyDown(k) then
		      //return the key based on which number was pressed
		      return i
		    end if
		  next
		  
		  return -1
		End Function
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
