#tag Module
Protected Module InstanceListMod
	#tag Method, Flags = &h0
		Sub PopulateCurrentInstances(lbCurrent as listbox, res as ResourceScene)
		  lbCurrent.DeleteAllRows
		  
		  dim s as string
		  
		  //populate current instances
		  for each i as CanvasObject in res.Scene
		    s = "#" + str(res.Scene.IndexOf(i)) + " - " + i.theObject.name
		    if i.Visible.Value.BooleanValue then
		      s = s + " (" + str(i.Left) + ", " + str(i.Top) + ")"
		    else
		      s = s + " (unplaced/invisible)"
		    end if
		    lbCurrent.AddRow s
		    //tag instance to new row
		    lbCurrent.RowTag(lbCurrent.ListCount - 1) = i
		  next
		End Sub
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
