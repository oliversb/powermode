#tag Window
Begin Window WinInstanceList Implements InstanceListMod.InstanceListInterface
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   1
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   276
   ImplicitInstance=   True
   LiveResize      =   True
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   False
   MaxWidth        =   32000
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   False
   MinWidth        =   64
   Placement       =   2
   Resizeable      =   False
   Title           =   "Instance List"
   Visible         =   True
   Width           =   551
   Begin ListBox lbAvailable
      AutoDeactivate  =   True
      AutoHideScrollbars=   True
      Bold            =   False
      Border          =   True
      ColumnCount     =   1
      ColumnsResizable=   False
      ColumnWidths    =   ""
      DataField       =   ""
      DataSource      =   ""
      DefaultRowHeight=   -1
      Enabled         =   True
      EnableDrag      =   False
      EnableDragReorder=   False
      GridLinesHorizontal=   0
      GridLinesVertical=   0
      HasHeading      =   False
      HeadingIndex    =   -1
      Height          =   190
      HelpTag         =   ""
      Hierarchical    =   False
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      RequiresSelection=   False
      Scope           =   0
      ScrollbarHorizontal=   False
      ScrollBarVertical=   True
      SelectionType   =   0
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   34
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   200
      _ScrollOffset   =   0
      _ScrollWidth    =   -1
   End
   Begin ListBox lbCurrent
      AutoDeactivate  =   True
      AutoHideScrollbars=   True
      Bold            =   False
      Border          =   True
      ColumnCount     =   1
      ColumnsResizable=   False
      ColumnWidths    =   ""
      DataField       =   ""
      DataSource      =   ""
      DefaultRowHeight=   -1
      Enabled         =   True
      EnableDrag      =   True
      EnableDragReorder=   False
      GridLinesHorizontal=   0
      GridLinesVertical=   0
      HasHeading      =   False
      HeadingIndex    =   -1
      Height          =   190
      HelpTag         =   ""
      Hierarchical    =   False
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   331
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      RequiresSelection=   False
      Scope           =   0
      ScrollbarHorizontal=   False
      ScrollBarVertical=   True
      SelectionType   =   0
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   34
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   200
      _ScrollOffset   =   0
      _ScrollWidth    =   -1
   End
   Begin PushButton pbAdd
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "#TEXT_ADD"
      Default         =   True
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   230
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      Scope           =   0
      TabIndex        =   7
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   57
      Underline       =   False
      Visible         =   True
      Width           =   90
   End
   Begin PushButton pbRemove
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "#TEXT_REMOVE"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   230
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      Scope           =   0
      TabIndex        =   8
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   89
      Underline       =   False
      Visible         =   True
      Width           =   90
   End
   Begin PushButton pbMoveUp
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "#TEXT_MOVEUP"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   230
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      Scope           =   0
      TabIndex        =   9
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   146
      Underline       =   False
      Visible         =   True
      Width           =   90
   End
   Begin PushButton pbMoveDown
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "#TEXT_MOVEDOWN"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   230
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      Scope           =   0
      TabIndex        =   10
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   178
      Underline       =   False
      Visible         =   True
      Width           =   90
   End
   Begin Label StaticText2
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   11
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "#TEXT_AVAILABLE_OBJECTS"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   14
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   200
   End
   Begin Label StaticText3
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   331
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   12
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "#TEXT_CURRENT_INSTANCES"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   14
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   199
   End
   Begin OkCancelButton OKCancelButton1
      AcceptFocus     =   False
      AcceptTabs      =   True
      ActionButtonText=   "OK"
      AutoDeactivate  =   True
      BackColor       =   &cFFFFFF00
      Backdrop        =   0
      CancelButtonText=   "Cancel"
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   46
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   326
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Scope           =   0
      TabIndex        =   13
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   230
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   205
   End
End
#tag EndWindow

#tag WindowCode
	#tag Method, Flags = &h21
		Private Sub Add()
		   if lbAvailable.ListIndex <> -1 then
		    lbCurrent.AddRow "#" + str(lbCurrent.ListCount) + " - "  + lbAvailable.Cell(lbAvailable.ListIndex, 0) + " (unplaced/invisible)"
		    
		    dim co as new CanvasObject
		    lbCurrent.RowTag(lbCurrent.LastIndex) = co
		    co.theObject = lbAvailable.RowTag(lbAvailable.ListIndex)
		    co.Visible.Value.BooleanValue = false
		    res.Scene.Append co
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub Move(by as integer)
		  if selectedItem <> -1 then
		    dim lastIndex as uint32
		    
		    if lbCurrentHasFocus then
		      lastIndex = lbCurrent.ListIndex
		      ArrayMod.MoveRange(res.Scene, by, selectedItem)
		      PopulateCurrentInstances
		      lbCurrent.ListIndex = lastIndex + by
		    else
		      lastIndex = lbAvailable.ListIndex
		      ArrayMod.MoveRange(DocFile.Resources, by, selectedItem)
		      PopulateObjects
		      lbAvailable.ListIndex = lastIndex + by
		    end if
		    DrawingCanvas.Invalidate
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function OriginalScene() As CanvasObject()
		  return mOriginalScene
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub PopulateCurrentInstances()
		  InstanceListMod.PopulateCurrentInstances(lbCurrent, res)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub PopulateObjects()
		  lbAvailable.DeleteAllRows
		  
		  //populate objects
		  for each i as Resource in DocFile.resources
		    if i isa ResourceObject and (not i isa ResourceObjectDefaultParent) then
		      i = ResourceObject(i)
		      
		      //add with name of object
		      lbAvailable.AddRow i.name
		      //tag object to new row
		      lbAvailable.RowTag(lbAvailable.ListCount - 1) = i
		    end if
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub Remove()
		  dim selectedRow as integer = lbCurrent.ListIndex
		  
		  if selectedRow <> - 1 then
		    lbCurrent.RemoveRow(selectedRow)
		    res.Scene.Remove(selectedRow)
		    PopulateCurrentInstances
		    DrawingCanvas.Invalidate
		    //use previous ListIndex for new one
		    lbCurrent.ListIndex = selectedRow
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Res() As ResourceScene
		  // Part of the InstanceListMod.InstanceListInterface interface.
		  return mRes
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Res(assigns r as ResourceScene)
		  mRes = r
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function selectedItem() As integer
		  //choose listbox to take ListIndex from
		  if lbCurrentHasFocus then return lbCurrent.ListIndex else return lbAvailable.ListIndex
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Show(r as ResourceScene, c as ObjectContainerCanvas)
		  me.show
		  
		  me.res = r
		  me.DrawingCanvas = c
		  
		  PopulateCurrentInstances
		  PopulateObjects
		  
		  for each co as CanvasObject in OriginalScene
		    co.Visible = Visibles.Value(co)
		    Res.Scene.Append co
		  next
		  redim Res.Scene(-1)
		  DrawingCanvas.Invalidate
		  
		  DocFile.ModifiedDoc = true
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Visibles() As Dictionary
		  // Part of the InstanceListMod.InstanceListInterface interface.
		  return mVisibles
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Visibles(assigns v as dictionary)
		  mVisibles = v
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private DrawingCanvas As ObjectContainerCanvas
	#tag EndProperty

	#tag Property, Flags = &h21
		Private lbCurrentHasFocus As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h21
		#tag Note
			This property is used to store a backup of the OriginalScene in case the user does not want to keep the changes made
		#tag EndNote
		Private mOriginalScene() As CanvasObject
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mRes As ResourceScene
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mVisibles As Dictionary
	#tag EndProperty


	#tag Constant, Name = TEXT_ADD, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Add"
		#Tag Instance, Platform = Any, Language = ja, Definition  = \"\xE8\xBF\xBD\xE5\x8A\xA0"
	#tag EndConstant

	#tag Constant, Name = TEXT_AVAILABLE_OBJECTS, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Available objects:"
	#tag EndConstant

	#tag Constant, Name = TEXT_CANCEL, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Cancel"
		#Tag Instance, Platform = Any, Language = ja, Definition  = \"\xE3\x82\xAD\xE3\x83\xA3\xE3\x83\xB3\xE3\x82\xBB\xE3\x83\xAB"
	#tag EndConstant

	#tag Constant, Name = TEXT_CURRENT_INSTANCES, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Current instances (in layer order):"
	#tag EndConstant

	#tag Constant, Name = TEXT_MOVEDOWN, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Move Down"
		#Tag Instance, Platform = Any, Language = ja, Definition  = \"\xE4\xB8\x8B\xE3\x81\xB8"
	#tag EndConstant

	#tag Constant, Name = TEXT_MOVEUP, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Move Up"
		#Tag Instance, Platform = Any, Language = ja, Definition  = \"\xE4\xB8\x8A\xE3\x81\xB8"
	#tag EndConstant

	#tag Constant, Name = TEXT_OK, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"OK"
		#Tag Instance, Platform = Any, Language = ja, Definition  = \"OK"
	#tag EndConstant

	#tag Constant, Name = TEXT_REMOVE, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Remove"
		#Tag Instance, Platform = Any, Language = ja, Definition  = \"\xE5\x8F\x96\xE9\x99\xA4"
	#tag EndConstant

	#tag Constant, Name = TEXT_RESET_DEFAULTS, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Reset Defaults"
		#Tag Instance, Platform = Any, Language = ja, Definition  = \"\xE3\x83\x87\xE3\x83\x95\xE3\x82\xA9\xE3\x83\xAB\xE3\x83\x88\xE8\xA8\xAD\xE5\xAE\x9A\xE3\x81\xB8"
	#tag EndConstant

	#tag Constant, Name = TEXT_VIEW_AS, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"View As:"
		#Tag Instance, Platform = Any, Language = ja, Definition  = \"\xE8\xA1\xA8\xE7\xA4\xBA\xEF\xBC\x9A"
	#tag EndConstant


#tag EndWindowCode

#tag Events lbAvailable
	#tag Event
		Sub DoubleClick()
		  Add()
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  #If TargetWin32
		    Me.DefaultRowHeight = 20
		  #Endif
		End Sub
	#tag EndEvent
	#tag Event
		Function CellClick(row as Integer, column as Integer, x as Integer, y as Integer) As Boolean
		  #pragma unused row
		  #pragma unused column
		  #pragma unused x
		  #pragma unused y
		  
		  lbCurrentHasFocus = false
		End Function
	#tag EndEvent
	#tag Event
		Function ConstructContextualMenu(base as MenuItem, x as Integer, y as Integer) As Boolean
		  #pragma unused x
		  #pragma unused y
		  
		  dim item as new menuitem
		  
		  item = new menuitem("Open In Editor")
		  base.append item
		End Function
	#tag EndEvent
	#tag Event
		Function ContextualMenuAction(hitItem as MenuItem) As Boolean
		  select case hititem.text
		  case "Open In Editor"
		    MainEditor.OpenResTab(ResourceObject(hititem.tag))
		  end select
		End Function
	#tag EndEvent
#tag EndEvents
#tag Events lbCurrent
	#tag Event
		Sub Open()
		  #If TargetWin32
		    Me.DefaultRowHeight = 20
		  #Endif
		End Sub
	#tag EndEvent
	#tag Event
		Function CellClick(row as Integer, column as Integer, x as Integer, y as Integer) As Boolean
		  #pragma unused row
		  #pragma unused column
		  #pragma unused x
		  #pragma unused y
		  
		  lbCurrentHasFocus = true
		End Function
	#tag EndEvent
	#tag Event
		Function ConstructContextualMenu(base as MenuItem, x as Integer, y as Integer) As Boolean
		  #pragma unused x
		  #pragma unused y
		  
		  if lbCurrent.ListIndex <> -1 then
		    dim item as new menuitem
		    
		    item = new menuitem("Visible")
		    item.Checked = CanvasObject(lbCurrent.RowTag(lbCurrent.ListIndex)).Visible.Value.BooleanValue
		    base.append item
		  end if
		End Function
	#tag EndEvent
	#tag Event
		Function ContextualMenuAction(hitItem as MenuItem) As Boolean
		  select case hititem.text
		  case "Visible"
		    CanvasObject(lbCurrent.RowTag(lbCurrent.ListIndex)).Visible.Value.BooleanValue = (not CanvasObject(lbCurrent.RowTag(lbCurrent.ListIndex)).Visible.Value.BooleanValue)
		    DrawingCanvas.Invalidate
		    PopulateCurrentInstances
		  end select
		End Function
	#tag EndEvent
#tag EndEvents
#tag Events pbAdd
	#tag Event
		Sub Action()
		  Add()
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbRemove
	#tag Event
		Sub Action()
		  Remove()
		  PopulateCurrentInstances
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbMoveUp
	#tag Event
		Sub Action()
		  Move(-1)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbMoveDown
	#tag Event
		Sub Action()
		  Move(1)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events OKCancelButton1
	#tag Event
		Sub Action()
		  Self.Close
		End Sub
	#tag EndEvent
	#tag Event
		Sub CancelAction()
		  //clone array to restore values
		  Visibles = new Dictionary
		  for each co as CanvasObject in res.Scene
		    OriginalScene.Append co
		    Visibles.Value(co) = co.Visible
		  next
		  
		  self.Close
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
			"11 - Modeless Dialog"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="400"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="600"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
