#tag Window
Begin ContainerControl scenePropertiesContainer Implements ResObjectInterface
   AcceptFocus     =   False
   AcceptTabs      =   True
   AutoDeactivate  =   True
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   Compatibility   =   ""
   Enabled         =   True
   EraseBackground =   True
   HasBackColor    =   False
   Height          =   729
   HelpTag         =   ""
   InitialParent   =   ""
   Left            =   0
   LockBottom      =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   TabIndex        =   0
   TabPanelIndex   =   0
   TabStop         =   True
   Top             =   0
   Transparent     =   True
   UseFocusRing    =   False
   Visible         =   True
   Width           =   227
   Begin Timer Timer1
      Height          =   32
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   140
      LockedInPosition=   False
      Mode            =   2
      Period          =   1
      Scope           =   0
      TabPanelIndex   =   "0"
      Top             =   140
      Width           =   32
   End
   Begin PagePanel PagePanel1
      AutoDeactivate  =   True
      Enabled         =   True
      Height          =   702
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      PanelCount      =   2
      Panels          =   ""
      Scope           =   0
      TabIndex        =   19
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   27
      Value           =   0
      Visible         =   True
      Width           =   227
      Begin PushButton pushInstanceList
         AutoDeactivate  =   True
         Bold            =   False
         ButtonStyle     =   "0"
         Cancel          =   False
         Caption         =   "&Instance List"
         Default         =   False
         Enabled         =   True
         Height          =   25
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "PagePanel1"
         Italic          =   False
         Left            =   43
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   3
         TabPanelIndex   =   2
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   176
         Underline       =   False
         Visible         =   True
         Width           =   109
      End
      Begin FocusTextField txtName
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         focus           =   False
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "PagePanel1"
         Italic          =   False
         Left            =   59
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   8
         TabPanelIndex   =   2
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   47
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   148
      End
      Begin Label Label11
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "PagePanel1"
         Italic          =   False
         Left            =   8
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   9
         TabPanelIndex   =   2
         TabStop         =   True
         Text            =   "Name:"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   48
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   47
      End
      Begin GroupBox GroupBox2
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Background"
         Enabled         =   True
         Height          =   119
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "PagePanel1"
         Italic          =   False
         Left            =   8
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   14
         TabPanelIndex   =   2
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   213
         Underline       =   False
         Visible         =   True
         Width           =   200
         Begin Label Label13
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox2"
            Italic          =   False
            Left            =   16
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   0
            TabPanelIndex   =   2
            TabStop         =   True
            Text            =   "Image:"
            TextAlign       =   0
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   0.0
            TextUnit        =   0
            Top             =   269
            Transparent     =   False
            Underline       =   False
            Visible         =   True
            Width           =   39
         End
         Begin txtSprite txtBackground
            AcceptsNoValue  =   True
            AcceptTabs      =   False
            Alignment       =   0
            AutoDeactivate  =   True
            AutomaticallyCheckSpelling=   False
            BackColor       =   &cFFFFFF00
            Bold            =   False
            Border          =   True
            CueText         =   ""
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            focus           =   False
            Format          =   ""
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox2"
            Italic          =   False
            Left            =   67
            LimitText       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Mask            =   ""
            Password        =   False
            ReadOnly        =   False
            ResourceTextCheck=   True
            Scope           =   0
            TabIndex        =   1
            TabPanelIndex   =   2
            TabStop         =   True
            Text            =   ""
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   0.0
            TextUnit        =   0
            Top             =   269
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   True
            Width           =   134
         End
         Begin Label Label12
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox2"
            Italic          =   False
            Left            =   16
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   3
            TabPanelIndex   =   2
            TabStop         =   True
            Text            =   "Colour:"
            TextAlign       =   0
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   237
            Transparent     =   False
            Underline       =   False
            Visible         =   True
            Width           =   45
         End
         Begin PushButton btnNewBackground
            AutoDeactivate  =   True
            Bold            =   False
            ButtonStyle     =   "0"
            Cancel          =   False
            Caption         =   "New Image"
            Default         =   False
            Enabled         =   True
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox2"
            Italic          =   False
            Left            =   28
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   0
            TabIndex        =   4
            TabPanelIndex   =   2
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   301
            Underline       =   False
            Visible         =   True
            Width           =   80
         End
         Begin PushButton BtnNoBackground
            AutoDeactivate  =   True
            Bold            =   False
            ButtonStyle     =   "0"
            Cancel          =   False
            Caption         =   "No Image"
            Default         =   False
            Enabled         =   True
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox2"
            Italic          =   False
            Left            =   108
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   0
            TabIndex        =   5
            TabPanelIndex   =   2
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   301
            Underline       =   False
            Visible         =   True
            Width           =   80
         End
         Begin ColorPicker BgPicker
            AcceptFocus     =   False
            AcceptTabs      =   False
            AutoDeactivate  =   True
            Backdrop        =   0
            DoubleBuffer    =   False
            Enabled         =   True
            EraseBackground =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox2"
            Left            =   67
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   0
            SelectedColor   =   &c00000000
            TabIndex        =   6
            TabPanelIndex   =   2
            TabStop         =   True
            Top             =   237
            Transparent     =   True
            UseFocusRing    =   True
            Visible         =   True
            Width           =   134
         End
      End
      Begin GroupBox GroupBox3
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Dimensions"
         Enabled         =   True
         Height          =   83
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "PagePanel1"
         Italic          =   False
         Left            =   8
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   15
         TabPanelIndex   =   2
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   81
         Underline       =   False
         Visible         =   True
         Width           =   211
         Begin Label Label10
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox3"
            Italic          =   False
            Left            =   14
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   0
            TabPanelIndex   =   2
            TabStop         =   True
            Text            =   "Height:"
            TextAlign       =   0
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   136
            Transparent     =   False
            Underline       =   False
            Visible         =   True
            Width           =   46
         End
         Begin Label Label9
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox3"
            Italic          =   False
            Left            =   14
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   1
            TabPanelIndex   =   2
            TabStop         =   True
            Text            =   "Width:"
            TextAlign       =   0
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   103
            Transparent     =   False
            Underline       =   False
            Visible         =   True
            Width           =   46
         End
         Begin NumberField txtWidth
            AcceptTabs      =   False
            Alignment       =   0
            AutoDeactivate  =   True
            AutomaticallyCheckSpelling=   False
            BackColor       =   &cFFFFFF00
            Bold            =   False
            Border          =   True
            CheckIfInteger  =   True
            CheckIfPositive =   True
            CheckText       =   "1"
            CueText         =   ""
            DataField       =   ""
            DataSource      =   ""
            Default         =   "0"
            Enabled         =   True
            Format          =   ""
            Height          =   21
            HelpTag         =   ""
            Index           =   -2147483648
            InFocus         =   False
            InitialParent   =   "GroupBox3"
            Italic          =   False
            Left            =   65
            LimitText       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            Mask            =   ""
            Password        =   False
            ProgrammaticalChange=   False
            ReadOnly        =   False
            Scope           =   0
            TabIndex        =   2
            TabPanelIndex   =   2
            TabStop         =   True
            Text            =   "640"
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextIsOk        =   True
            TextSize        =   0.0
            TextUnit        =   0
            Top             =   103
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   True
            Width           =   148
         End
         Begin NumberField txtHeight
            AcceptTabs      =   False
            Alignment       =   0
            AutoDeactivate  =   True
            AutomaticallyCheckSpelling=   False
            BackColor       =   &cFFFFFF00
            Bold            =   False
            Border          =   True
            CheckIfInteger  =   True
            CheckIfPositive =   True
            CheckText       =   "1"
            CueText         =   ""
            DataField       =   ""
            DataSource      =   ""
            Default         =   "0"
            Enabled         =   True
            Format          =   ""
            Height          =   21
            HelpTag         =   ""
            Index           =   -2147483648
            InFocus         =   False
            InitialParent   =   "GroupBox3"
            Italic          =   False
            Left            =   65
            LimitText       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            Mask            =   ""
            Password        =   False
            ProgrammaticalChange=   False
            ReadOnly        =   False
            Scope           =   0
            TabIndex        =   3
            TabPanelIndex   =   2
            TabStop         =   True
            Text            =   "480"
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextIsOk        =   True
            TextSize        =   0.0
            TextUnit        =   0
            Top             =   135
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   True
            Width           =   148
         End
      End
      Begin GroupBox GroupBox4
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Position"
         Enabled         =   True
         Height          =   46
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "PagePanel1"
         Italic          =   False
         Left            =   20
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   9
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   35
         Underline       =   False
         Visible         =   True
         Width           =   180
         Begin Label Label1
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox4"
            Italic          =   False
            Left            =   54
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   0
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   "X:"
            TextAlign       =   0
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   0.0
            TextUnit        =   0
            Top             =   53
            Transparent     =   False
            Underline       =   False
            Visible         =   True
            Width           =   13
         End
         Begin NumberField txtX
            AcceptTabs      =   False
            Alignment       =   0
            AutoDeactivate  =   True
            AutomaticallyCheckSpelling=   False
            BackColor       =   &cFFFFFF00
            Bold            =   False
            Border          =   True
            CheckIfInteger  =   False
            CheckIfPositive =   False
            CheckText       =   "0"
            CueText         =   ""
            DataField       =   ""
            DataSource      =   ""
            Default         =   "0"
            Enabled         =   True
            Format          =   ""
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InFocus         =   False
            InitialParent   =   "GroupBox4"
            Italic          =   False
            Left            =   65
            LimitText       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Mask            =   ""
            Password        =   False
            ProgrammaticalChange=   False
            ReadOnly        =   False
            Scope           =   0
            TabIndex        =   1
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   ""
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextIsOk        =   True
            TextSize        =   0.0
            TextUnit        =   0
            Top             =   52
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   True
            Width           =   42
         End
         Begin Label Label2
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox4"
            Italic          =   False
            Left            =   133
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   2
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   "Y:"
            TextAlign       =   0
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   0.0
            TextUnit        =   0
            Top             =   52
            Transparent     =   False
            Underline       =   False
            Visible         =   True
            Width           =   13
         End
         Begin NumberField txtY
            AcceptTabs      =   False
            Alignment       =   0
            AutoDeactivate  =   True
            AutomaticallyCheckSpelling=   False
            BackColor       =   &cFFFFFF00
            Bold            =   False
            Border          =   True
            CheckIfInteger  =   False
            CheckIfPositive =   False
            CheckText       =   "0"
            CueText         =   ""
            DataField       =   ""
            DataSource      =   ""
            Default         =   "0"
            Enabled         =   True
            Format          =   ""
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InFocus         =   False
            InitialParent   =   "GroupBox4"
            Italic          =   False
            Left            =   144
            LimitText       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Mask            =   ""
            Password        =   False
            ProgrammaticalChange=   False
            ReadOnly        =   False
            Scope           =   0
            TabIndex        =   3
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   ""
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextIsOk        =   True
            TextSize        =   0.0
            TextUnit        =   0
            Top             =   51
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   True
            Width           =   42
         End
         Begin UpDownArrows UpDownY
            AcceptFocus     =   False
            AutoDeactivate  =   True
            Enabled         =   True
            Height          =   23
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox4"
            Left            =   187
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   0
            TabIndex        =   4
            TabPanelIndex   =   1
            TabStop         =   True
            Top             =   51
            Visible         =   True
            Width           =   13
         End
         Begin UpDownArrows UpDownX
            AcceptFocus     =   False
            AutoDeactivate  =   True
            Enabled         =   True
            Height          =   23
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox4"
            Left            =   108
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   0
            TabIndex        =   5
            TabPanelIndex   =   1
            TabStop         =   True
            Top             =   51
            Visible         =   True
            Width           =   13
         End
      End
      Begin conReusableObjectProperties conReusableObjectProperties1
         AcceptFocus     =   False
         AcceptTabs      =   True
         AutoDeactivate  =   True
         BackColor       =   &cFFFFFF00
         Backdrop        =   0
         Enabled         =   True
         EraseBackground =   True
         HasBackColor    =   False
         Height          =   195
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "PagePanel1"
         Left            =   20
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         ProgrammaticChange=   False
         Scope           =   0
         TabIndex        =   8
         TabPanelIndex   =   1
         TabStop         =   True
         Top             =   87
         Transparent     =   True
         UseFocusRing    =   False
         Visible         =   True
         Width           =   183
      End
      Begin Label Label7
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   17
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "PagePanel1"
         Italic          =   True
         Left            =   7
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   True
         Scope           =   0
         Selectable      =   False
         TabIndex        =   10
         TabPanelIndex   =   1
         TabStop         =   True
         Text            =   "Left Click - Add/Move Object\r\n+<Shift>+<Ctrl> - Add Line Of Multiple\r\n+<Alt> - No Snap\r\n+<Home> - Add Overlapped\r\n+<Space> - Scroll/Drag Scene View\r\n\r\nRight Click / Left Click+<Control> - Contextual Menu\r\n+<Shift> - Delete All Underneath\r\n+<Home> - Delete\r\n\r\nMiddle Mouse Scroll - Zoom\r\n\r\n<Ctrl>+C - Copy Selection\r\n<Ctrl>+V - Paste Selection\r\n"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   478
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   214
      End
      Begin Canvas Canvas1
         AcceptFocus     =   False
         AcceptTabs      =   False
         AutoDeactivate  =   True
         Backdrop        =   0
         DoubleBuffer    =   True
         Enabled         =   True
         EraseBackground =   True
         Height          =   131
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "PagePanel1"
         Left            =   7
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   True
         LockTop         =   True
         Scope           =   0
         TabIndex        =   11
         TabPanelIndex   =   1
         TabStop         =   True
         Top             =   286
         Transparent     =   True
         UseFocusRing    =   True
         Visible         =   True
         Width           =   214
      End
      Begin txtObjectRegularSelector txtObject1
         AcceptsNoValue  =   True
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         focus           =   False
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "PagePanel1"
         Italic          =   False
         Left            =   7
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         ResourceTextCheck=   True
         Scope           =   0
         TabIndex        =   12
         TabPanelIndex   =   1
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   421
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   214
      End
   End
   Begin TabPanel TabPanel1
      AutoDeactivate  =   True
      Bold            =   False
      Enabled         =   True
      Height          =   26
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Panels          =   ""
      Scope           =   0
      SmallTabs       =   False
      TabDefinition   =   "Objects\rSettings"
      TabIndex        =   22
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   0
      Underline       =   False
      Value           =   0
      Visible         =   True
      Width           =   227
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  //removes conflict with Open event definition
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub CallOpen()
		  //loop until we have a resourceobject
		  for each res as Resource in DocFile.Resources
		    if res isa ResourceObject and (not res isa ResourceObjectDefaultParent) then
		      CurrentObject = ResourceObject(res)
		      RefreshCurrentObject
		      //we have got the first item so we need to exit the loop
		      exit for
		    end if
		  next
		  
		  Open()
		  
		  ProgrammaticTextChange = true
		  //if scene width and height are not set
		  if thisRes.SceneWidth = 0 then
		    //get default width and height from the text added in at design time
		    thisRes.SceneWidth = val(txtWidth.Text)
		    thisRes.SceneHeight = val(txtHeight.Text)
		  else
		    txtWidth.Text = str(thisRes.SceneWidth)
		    txtHeight.Text = str(thisRes.SceneHeight)
		  end if
		  
		  BgPicker.SelectedColor = thisRes.Colour
		  
		  if thisRes.Background <> nil then
		    txtBackground.Text = thisRes.Background.name
		  end if
		  ProgrammaticTextChange = false
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub EmbedWithin(containingControl As RectControl, editor as SceneEditor)
		  self.EmbedWithin(containingControl)
		  self.tag = editor
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function iRes() As ResourceObject
		  return me.CurrentObject
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ObjectSelected(co as CanvasObject)
		  ProgrammaticTextChange = true
		  
		  conReusableObjectProperties1.ObjectSelected(co)
		  
		  txtX.Text = str(co.Left)
		  txtY.Text = str(co.Top)
		  
		  SelectedCanvasObject = co
		  
		  ProgrammaticTextChange = false
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RefreshCurrentObject(setText as boolean = true)
		  //check if sprite of currently selected object has any contents
		  if CurrentObject <> nil then
		    //blank out picture in case there is no sprite to draw
		    CurrentPicture = nil
		    if CurrentObject.Sprite <> nil then
		      if CurrentObject.Sprite.Frames.Ubound <> -1 then
		        //draw the preview of the currently selected object
		        CurrentPicture = scaleToFit(CurrentObject.Sprite.Frames(0).SourceImage, Canvas1.width, Canvas1.height,1)
		      end if
		    end if
		    if setText then
		      txtObject1.Text = CurrentObject.name
		    end if
		    Canvas1.Invalidate(false)
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ValueChanged(s as NumberField = nil) As boolean
		  if ProgrammaticTextChange = false then
		    if SelectedCanvasObject <> nil then
		      SceneEditor(self.tag).RefreshDrawingCanvas
		      return true
		    else
		      if s <> nil then
		        s.text = ""
		      end if
		    end if
		  end if
		End Function
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook


	#tag Property, Flags = &h0
		CurrentObject As ResourceObject
	#tag EndProperty

	#tag Property, Flags = &h0
		CurrentPicture As Picture
	#tag EndProperty

	#tag Property, Flags = &h0
		ProgrammaticTextChange As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		SelectedCanvasObject As CanvasObject
	#tag EndProperty

	#tag Property, Flags = &h0
		tag As SceneEditor
	#tag EndProperty

	#tag Property, Flags = &h0
		thisRes As ResourceScene
	#tag EndProperty


#tag EndWindowCode

#tag Events Timer1
	#tag Event
		Sub Action()
		  //this will be more efficient because values are set, only when they are being displayed
		  if PagePanel1 = nil then return
		  
		  select case PagePanel1.value
		  case 1
		    if not txtName.focus then
		      txtName.text = thisRes.name
		    end if
		  end select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events PagePanel1
	#tag Event
		Sub Open()
		  //set the defaults to the text added in at design time
		  //it is more efficient to use the text directly (before it has changed) instead of converting it back to a string from the SceneWidth and SceneHeight variables
		  txtWidth.Default = txtWidth.Text
		  txtHeight.Default = txtHeight.Text
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pushInstanceList
	#tag Event
		Sub Action()
		  WinInstanceList.Show(SceneEditor(self.tag).res, SceneEditor(self.tag).DrawingCanvas)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events txtName
	#tag Event
		Sub LostFocus()
		  thisRes.MakeName me.text
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events txtBackground
	#tag Event
		Function GetResValue() As resource
		  return thisRes.Background
		End Function
	#tag EndEvent
	#tag Event
		Sub SetResValue(value as Resource)
		  if value <> nil then
		    thisRes.Background = ResourceSprite(value)
		  else
		    thisRes.Background = nil
		  end if
		  SceneEditor(self.tag).DrawingCanvas.Invalidate
		  DocFile.ModifiedDoc = true
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnNewBackground
	#tag Event
		Sub Action()
		  MainEditor.NewSprite
		  Dim mres As ResourceSprite = ResourceSprite(resources(resources.UBound))
		  mres.MakeName mres.UsedResourceTree.Cell(mres.ListIndex,0)
		  txtBackground.Text = mres.name
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BtnNoBackground
	#tag Event
		Sub Action()
		  txtBackground.Text = ""
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BgPicker
	#tag Event
		Sub ColorChanged()
		  thisRes.Colour = me.SelectedColor
		  SceneEditor(self.tag).DrawingCanvas.Invalidate
		  
		  DocFile.ModifiedDoc = true
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events txtWidth
	#tag Event
		Sub TextChange()
		  if ProgrammaticTextChange = false then
		    thisRes.SceneWidth = val(me.text)
		    SceneEditor(self.tag).res.RedrawGrid
		    SceneEditor(self.tag).RefreshDrawingCanvas
		    docfile.ModifiedDoc = true
		    'self.tag.close
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events txtHeight
	#tag Event
		Sub TextChange()
		  if ProgrammaticTextChange = false then
		    thisRes.SceneHeight = val(me.text)
		    SceneEditor(self.tag).res.RedrawGrid
		    SceneEditor(self.tag).RefreshDrawingCanvas
		    docfile.ModifiedDoc = true
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events txtX
	#tag Event
		Sub TextChange()
		  if not ProgrammaticTextChange then
		    if SelectedCanvasObject <> nil then
		      SelectedCanvasObject.Left = val(me.text)
		      SceneEditor(self.tag).RefreshDrawingCanvas
		    else
		      me.text = ""
		    end if
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events txtY
	#tag Event
		Sub TextChange()
		  if not ProgrammaticTextChange then
		    if SelectedCanvasObject <> nil then
		      SelectedCanvasObject.Top = val(me.text)
		      SceneEditor(self.tag).RefreshDrawingCanvas
		    else
		      me.text = ""
		    end if
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events UpDownY
	#tag Event
		Sub Up()
		  txtY.Text = str(val(txtY.Text)+1)
		End Sub
	#tag EndEvent
	#tag Event
		Sub Down()
		  txtY.Text = str(val(txtY.Text)-1)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events UpDownX
	#tag Event
		Sub Up()
		  txtX.Text = str(val(txtX.Text)+1)
		End Sub
	#tag EndEvent
	#tag Event
		Sub Down()
		  txtX.Text = str(val(txtX.Text)-1)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events conReusableObjectProperties1
	#tag Event
		Sub OpacityChanged(s as NumberField)
		  if ValueChanged(s) then
		    SelectedCanvasObject.SetOpacity val(s.text)
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub RotationChanged(s as NumberField)
		  if ValueChanged(s) then
		    SelectedCanvasObject.SetRotation val(s.text)
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub VisibleChanged(s as CheckBox)
		  if ValueChanged then
		    SelectedCanvasObject.Visible.Value.BooleanValue = s.value
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub LayerChanged(s as NumberField)
		  if ValueChanged(s) then
		    SelectedCanvasObject.Layer.Value.IntValue = val(s.text)
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub ScaleYChanged(s as NumberField)
		  if ValueChanged(s) then
		    SelectedCanvasObject.SetScaleY(val(s.text))
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub ScaleXChanged(s as NumberField)
		  if ValueChanged(s) then
		    SelectedCanvasObject.SetScaleX(val(s.text))
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Canvas1
	#tag Event
		Sub Paint(g As Graphics, areas() As REALbasic.Rect)
		  #pragma unused areas
		  
		  //draw the checkerboard
		  g.DrawPicture PWindow.DrawCheckerboard(g.width, g.height), 0, 0
		  
		  //draw the contents
		  g.DrawPicture CurrentPicture, 0, 0
		End Sub
	#tag EndEvent
	#tag Event
		Function ConstructContextualMenu(base as MenuItem, x as Integer, y as Integer) As Boolean
		  #pragma unused x
		  #pragma unused y
		  
		  for each res as resource in DocFile.resources
		    if res isa ResourceObject and (not res isa ResourceObjectDefaultParent) then
		      res.MakeName
		      dim item as new menuitem(res.name)
		      item.tag = res
		      base.append item
		    end if
		  next
		End Function
	#tag EndEvent
	#tag Event
		Function ContextualMenuAction(hitItem as MenuItem) As Boolean
		  CurrentObject = hitItem.tag
		  
		  RefreshCurrentObject(true)
		End Function
	#tag EndEvent
#tag EndEvents
#tag Events TabPanel1
	#tag Event
		Sub Change()
		  PagePanel1.Value = me.Value
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="AcceptFocus"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AcceptTabs"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AutoDeactivate"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CurrentPicture"
		Group="Behavior"
		Type="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="EraseBackground"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Appearance"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="InitialParent"
		Group="Position"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ProgrammaticTextChange"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIndex"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabPanelIndex"
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabStop"
		Visible=true
		Group="Position"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Transparent"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="UseFocusRing"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
