#tag Window
Begin ContainerControl conReusableObjectProperties
   AcceptFocus     =   False
   AcceptTabs      =   True
   AutoDeactivate  =   True
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   Compatibility   =   ""
   Enabled         =   True
   EraseBackground =   True
   HasBackColor    =   False
   Height          =   230
   HelpTag         =   ""
   InitialParent   =   ""
   Left            =   0
   LockBottom      =   False
   LockLeft        =   False
   LockRight       =   False
   LockTop         =   False
   TabIndex        =   0
   TabPanelIndex   =   0
   TabStop         =   True
   Top             =   0
   Transparent     =   True
   UseFocusRing    =   False
   Visible         =   True
   Width           =   183
   Begin GroupBox groupScale
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "Scale"
      Enabled         =   True
      Height          =   46
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   0
      Underline       =   False
      Visible         =   True
      Width           =   180
      Begin Label Label3
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "groupScale"
         Italic          =   False
         Left            =   34
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   0
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "X:"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   17
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   13
      End
      Begin NumberField txtScaleX
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CheckIfInteger  =   False
         CheckIfPositive =   False
         CheckText       =   "0"
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Default         =   "0"
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InFocus         =   False
         InitialParent   =   "groupScale"
         Italic          =   False
         Left            =   45
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ProgrammaticalChange=   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   1
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextIsOk        =   True
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   17
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   42
      End
      Begin Label Label4
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "groupScale"
         Italic          =   False
         Left            =   113
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   2
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "Y:"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   17
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   13
      End
      Begin NumberField txtScaleY
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CheckIfInteger  =   False
         CheckIfPositive =   False
         CheckText       =   "0"
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Default         =   "0"
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InFocus         =   False
         InitialParent   =   "groupScale"
         Italic          =   False
         Left            =   124
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ProgrammaticalChange=   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   3
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextIsOk        =   True
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   16
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   42
      End
      Begin UpDownArrows UpDownScaleX
         AcceptFocus     =   False
         AutoDeactivate  =   True
         Enabled         =   True
         Height          =   23
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "groupScale"
         Left            =   88
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   4
         TabPanelIndex   =   0
         TabStop         =   True
         Top             =   16
         Visible         =   True
         Width           =   13
      End
      Begin UpDownArrows UpDownScaleY
         AcceptFocus     =   False
         AutoDeactivate  =   True
         Enabled         =   True
         Height          =   23
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "groupScale"
         Left            =   167
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   5
         TabPanelIndex   =   0
         TabStop         =   True
         Top             =   16
         Visible         =   True
         Width           =   13
      End
   End
   Begin Label Label5
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   32
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Opacity:"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   93
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   43
   End
   Begin Label Label6
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   141
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "%"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   93
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   10
   End
   Begin PercentField txtOpacity
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CheckIfInteger  =   False
      CheckIfPositive =   False
      CheckText       =   "0"
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Default         =   "0"
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InFocus         =   False
      Italic          =   False
      Left            =   87
      Limit           =   100
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ProgrammaticalChange=   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "0"
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextIsOk        =   True
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   92
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   42
   End
   Begin UpDownArrows UpDownOpacity
      AcceptFocus     =   False
      AutoDeactivate  =   True
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   129
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   92
      Visible         =   True
      Width           =   13
   End
   Begin CheckBox checkVisible
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "&Visible"
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   64
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      State           =   0
      TabIndex        =   6
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   58
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   53
   End
   Begin Label Label7
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   32
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   7
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Layer:"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   126
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   30
   End
   Begin UnsignedIntField txtLayer
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CheckIfInteger  =   False
      CheckIfPositive =   False
      CheckText       =   "0"
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Default         =   "0"
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InFocus         =   False
      Italic          =   False
      Left            =   68
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ProgrammaticalChange=   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   8
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextIsOk        =   True
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   126
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   73
   End
   Begin UpDownArrows UpDownArrows1
      AcceptFocus     =   False
      AutoDeactivate  =   True
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   141
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   9
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   125
      Visible         =   True
      Width           =   13
   End
   Begin CheckBox checkPresistant
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "P&ersistent"
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      State           =   0
      TabIndex        =   10
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   -32
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   70
   End
   Begin Label lblRotation
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   9
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   11
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Rotation:"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   161
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   53
   End
   Begin UnsignedIntField txtRotation
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CheckIfInteger  =   False
      CheckIfPositive =   False
      CheckText       =   "0"
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Default         =   "0"
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InFocus         =   False
      Italic          =   False
      Left            =   64
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ProgrammaticalChange=   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   12
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextIsOk        =   True
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   160
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   53
   End
   Begin RotationCanvas RotationCanvas1
      AcceptFocus     =   False
      AcceptTabs      =   False
      AutoDeactivate  =   True
      Backdrop        =   0
      DoubleBuffer    =   False
      DragRotation    =   0.0
      Enabled         =   True
      EraseBackground =   True
      Height          =   34
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   129
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   13
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   151
      Transparent     =   True
      UseFocusRing    =   True
      Visible         =   True
      Width           =   34
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  //set the default for the scales to 1
		  txtScaleX.Default = "1"
		  txtScaleY.Default = "1"
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub ObjectSelected(co as CanvasObject)
		  ProgrammaticChange = true
		  
		  checkPresistant.value = co.Presistant.Value.BooleanValue
		  checkVisible.value = co.Visible.Value.BooleanValue
		  
		  txtLayer.Text = co.Layer.Value.StringValue
		  
		  txtScaleX.Text = co.ScaleX.Value.StringValue
		  txtScaleY.Text = co.ScaleY.Value.StringValue
		  
		  txtOpacity.Text = co.Opacity.Value.StringValue
		  
		  RotationCanvas1.DragRotation = co.Rotation.Value.AngleValue.Value
		  txtRotation.ProgrammaticalChange = true
		  txtRotation.Text = str(co.Rotation.Value.AngleValue.Value)
		  txtRotation.ProgrammaticalChange = false
		  
		  ProgrammaticChange = false
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event LayerChanged(s as NumberField)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event OpacityChanged(s as NumberField)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event PresistantChanged(s as CheckBox)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event RotationChanged(s as NumberField)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ScaleXChanged(s as NumberField)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ScaleYChanged(s as NumberField)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event VisibleChanged(s as CheckBox)
	#tag EndHook


	#tag Property, Flags = &h0
		ProgrammaticChange As Boolean
	#tag EndProperty


#tag EndWindowCode

#tag Events txtScaleX
	#tag Event
		Sub ValidatedTextChange()
		  if ProgrammaticChange = false then
		    ScaleXChanged(me)
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events txtScaleY
	#tag Event
		Sub ValidatedTextChange()
		  if ProgrammaticChange = false then
		    ScaleYChanged(me)
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events UpDownScaleX
	#tag Event
		Sub Up()
		  txtScaleX.Text = str(val(txtScaleX.Text)+1)
		End Sub
	#tag EndEvent
	#tag Event
		Sub Down()
		  txtScaleX.Text = str(val(txtScaleX.Text)-1)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events UpDownScaleY
	#tag Event
		Sub Up()
		  txtScaleY.Text = str(val(txtScaleY.Text)+1)
		End Sub
	#tag EndEvent
	#tag Event
		Sub Down()
		  txtScaleY.Text = str(val(txtScaleY.Text)-1)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events txtOpacity
	#tag Event
		Sub ValidatedTextChange()
		  if ProgrammaticChange = false then
		    OpacityChanged(me)
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events UpDownOpacity
	#tag Event
		Sub Up()
		  dim curVal as uint32 = val(txtOpacity.Text)
		  if curVal < 100 then
		    txtOpacity.Text = str(curVal+1)
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub Down()
		  dim curVal as uint32 = val(txtOpacity.Text)
		  if curVal > 0 then
		    txtOpacity.Text = str(curVal-1)
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events checkVisible
	#tag Event
		Sub Action()
		  if programmaticchange = false then
		    VisibleChanged(me)
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events txtLayer
	#tag Event
		Sub ValidatedTextChange()
		  if programmaticchange = false then
		    LayerChanged(me)
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events UpDownArrows1
	#tag Event
		Sub Down()
		  if val(txtLayer.text) > 0 then
		    txtLayer.Text = str(val(txtLayer.text)-1)
		  end if
		  txtLayer.SelectAll
		End Sub
	#tag EndEvent
	#tag Event
		Sub Up()
		  txtLayer.Text = str(val(txtLayer.text)+1)
		  txtLayer.SelectAll
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events checkPresistant
	#tag Event
		Sub Action()
		  if programmaticchange = false then
		    PresistantChanged(me)
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events txtRotation
	#tag Event
		Sub ValidatedTextChange()
		  RotationCanvas1.Rotation.Value = val(me.text)
		  
		  RotationCanvas1.DragRotation = RotationCanvas1.Rotation.Value
		  RotationCanvas1.Invalidate
		  
		  RotationChanged(me)
		End Sub
	#tag EndEvent
	#tag Event
		Sub TextChange()
		  me.TextIsOk = (val(me.text) <= 360) 
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events RotationCanvas1
	#tag Event
		Sub RotationDragged()
		  txtRotation.text = str(me.Rotation.Value)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="AcceptFocus"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AcceptTabs"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AutoDeactivate"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="EraseBackground"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Appearance"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="InitialParent"
		Group="Position"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ProgrammaticChange"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIndex"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabPanelIndex"
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabStop"
		Visible=true
		Group="Position"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Transparent"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="UseFocusRing"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
