#tag Window
Begin EmbeddedControl SceneEditor Implements ResourceEditorInterface
   AcceptFocus     =   False
   AcceptTabs      =   True
   AutoDeactivate  =   True
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   Compatibility   =   ""
   Enabled         =   True
   EraseBackground =   True
   HasBackColor    =   False
   Height          =   637
   HelpTag         =   ""
   InitialParent   =   ""
   Left            =   0
   LockBottom      =   True
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   TabIndex        =   0
   TabPanelIndex   =   0
   TabStop         =   True
   Top             =   0
   Transparent     =   True
   UseFocusRing    =   False
   Visible         =   True
   Width           =   871
   Begin LVToolBar LVToolBar1
      AcceptFocus     =   False
      AcceptTabs      =   False
      AquaBackground  =   False
      AutoDeactivate  =   True
      Backdrop        =   0
      DoubleBuffer    =   False
      EnableContextualMenu=   True
      Enabled         =   True
      EraseBackground =   True
      FontColourDefault=   False
      Height          =   35
      HelpTag         =   ""
      HideHeight      =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      mToolBarStyleChanged=   True
      RefreshLock     =   False
      Scope           =   0
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      ToolbarStyle    =   0
      Top             =   -39
      Transparent     =   True
      UseFocusRing    =   True
      Visible         =   True
      Width           =   43
   End
   Begin scenePropertiesScrollable sceneProperties1
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFFFF00
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   637
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   644
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      Scope           =   0
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   0
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   227
   End
   Begin ObjectContainerCanvas DrawingCanvas
      AcceptFocus     =   False
      AcceptTabs      =   False
      AllowManipulations=   True
      AutoDeactivate  =   True
      Backdrop        =   0
      Background      =   0
      DoubleBuffer    =   True
      Enabled         =   True
      EraseBackground =   False
      Height          =   637
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Scope           =   0
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   0
      Transparent     =   True
      UseFocusRing    =   True
      Visible         =   True
      Width           =   632
      WithinDraggingArea=   False
   End
   Begin imSplitter imSplitter1
      AcceptFocus     =   False
      AcceptTabs      =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Backdrop        =   0
      DockAfter       =   True
      DockAfterSize   =   40
      DockBefore      =   True
      DockBeforeSize  =   40
      DoubleBuffer    =   False
      DoubleClickAction=   2
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   637
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      IsDocked        =   False
      IsDockedPosition=   ""
      Left            =   633
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      MinAfterArea    =   20
      MinBeforeArea   =   30
      Scope           =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   0
      Transparent     =   True
      UseFocusRing    =   True
      Visible         =   True
      Width           =   10
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Paint(g As Graphics, areas() As REALbasic.Rect)
		  #pragma unused g
		  #pragma unused areas
		  
		  dim assignedImage as boolean = false
		  for each co as CanvasObject in DrawingCanvas.CanvasObjects
		    //check if there is an object selected for adding
		    if sceneProperties1.scenePropertiesContainer1.CurrentObject <> nil then
		      //check if there is a sprite assigned to the currentobject
		      if sceneProperties1.scenePropertiesContainer1.CurrentObject.Sprite <> nil then
		        //check if there is anything contained within the sprite
		        if sceneProperties1.scenePropertiesContainer1.CurrentObject.Sprite.Frames.Ubound <> -1 then
		          //re-reference the image of the canvasobjects
		          assignedImage = true
		          co.Rescale
		        end if
		      end if
		    end if
		  next
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub CallGotTabFocus()
		  DrawingCanvas.SetFocus
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1000
		Sub Constructor(r as ResourceScene)
		  res = r
		  for each co as CanvasObject in r.Scene
		    co.Selected = false
		  next
		  DrawingCanvas.CanvasObjects = r.Scene
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub RebuildObjectMenu(type as string, menu as menuitem)
		  //repopulate clear instances menu
		  for item as uint32 = menu.Count - 1 downto 0
		    menu.Remove(item)
		  next
		  if res.Scene.Ubound <> -1 then
		    menu.Append(new menuitem(type + " All"))
		    menu.Append(new menuitem(type + " Selected"))
		    menu.Append(new menuitem(MenuItem.TextSeparator))
		    //list all of the objects
		    dim item as menuitem
		    for each co as CanvasObject in res.Scene
		      //checking if the object has already been listed
		      if not co.theObject.InObjectList then
		        co.theObject.InObjectList = true
		        item = new menuitem
		        item.tag = co.theObject
		        item.text = co.theObject.name
		        menu.Append(item)
		      end if
		    next
		    //resetting the InObjectList variable for each CanvasObject
		    for each co as CanvasObject in res.Scene
		      co.theObject.InObjectList = false
		    next
		  else
		    dim item as new menuitem
		    item.text = "Nothing to " + Lowercase(type)
		    item.enabled = false
		    menu.Append(item)
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub RebuildObjectMenus()
		  RebuildObjectMenu("Clear", tbClearMenu)
		  RebuildObjectMenu("Lock", tbLockMenu)
		  RebuildObjectMenu("Unlock", tbUnlockMenu)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RefreshDrawingCanvas()
		  DrawingCanvas.Invalidate
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TheConstructor()
		  dim r as ResourceScene = res
		  
		  sceneProperties1.TheConstructor
		  
		  for each co as CanvasObject in r.Scene
		    DrawingCanvas.AddObject(co)
		  next
		  
		  r.Scene = DrawingCanvas.CanvasObjects
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h1
		Protected ContextualClickOnObject As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		res As ResourceScene
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected SelectedObjects() As CanvasObject
	#tag EndProperty

	#tag Property, Flags = &h0
		tbClearMenu As MenuItem
	#tag EndProperty

	#tag Property, Flags = &h0
		tbLockMenu As MenuItem
	#tag EndProperty

	#tag Property, Flags = &h0
		tbUnlockMenu As MenuItem
	#tag EndProperty


#tag EndWindowCode

#tag Events LVToolBar1
	#tag Event
		Sub Action(selectedToolBarItem As LVToolBarItem)
		  select case selectedToolBarItem.Caption
		  case "Snap"
		    popoverSnap.displayPopoverAt(System.MouseX, System.MouseY,-1, self)
		  case "Zoom"
		    popoverZoom.displayPopoverAt(System.MouseX, System.MouseY,-1, self)
		  case "Zoom In"
		    res.Zoom = res.Zoom + 10
		    DrawingCanvas.Invalidate
		  case "Zoom Out"
		    res.Zoom = res.Zoom - 10
		    DrawingCanvas.Invalidate
		  case "Instance List"
		    WinInstanceList.Show(res, DrawingCanvas)
		  end Select
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  me.ToolBarStyle = LVToolBar.STYLE_SMALL_ICONS_LABEL
		  
		  dim tbClear as New LVToolBarItem
		  tbClear.Caption = "Clear Instances (COMING SOON)"
		  tbClear.HelpTag = "Create instances place in this room (of a specific object or any)"
		  tbClear.Image = Icon("New")
		  tbClearMenu = new MenuItem
		  tbClearMenu.tag = "CLEAR"
		  tbClear.BaseMenu = tbClearMenu
		  tbClear.Visible = true
		  me.AddButton(tbClear)
		  
		  dim tbSeparator1 as New LVStandardToolBarItem
		  me.AddButton(tbSeparator1)
		  
		  dim tbLock as New LVToolBarItem
		  tbLock.Caption = "Lock Instances (COMING SOON)"
		  tbLock.HelpTag = "Lock instances from transformations on canvas (of a specific object or any)"
		  tbLock.Image = Icon("Lock")
		  tbLockMenu = new MenuItem
		  tbLockMenu.tag = "LOCK"
		  tbLock.BaseMenu = tbLockMenu
		  tbLock.Visible = true
		  me.AddButton(tbLock)
		  
		  dim tbUnlock as New LVToolBarItem
		  tbUnlock.Caption = "Unlock Instances (COMING SOON)"
		  tbUnlock.HelpTag = "Unlock instances from transformations on canvas (of a specific object or any)"
		  tbUnlock.Image = Icon("Unlock")
		  tbUnlockMenu = new Menuitem
		  tbunlockMenu.tag = "UNLOCK"
		  tbUnlock.BaseMenu = tbUnlockMenu
		  tbUnlock.Visible = true
		  me.AddButton(tbUnlock)
		  
		  dim tbSeparator2 as New LVStandardToolBarItem
		  me.AddButton(tbSeparator2)
		  
		  dim tbSnap as New LVToolBarItem
		  tbSnap.Caption = "Snap (COMING SOON)"
		  tbSnap.HelpTag = "Snap options"
		  tbSnap.Image =Icon("Snap")
		  tbSnap.Visible = true
		  me.AddButton(tbSnap)
		  
		  dim tbSeparator3 as New LVStandardToolBarItem
		  me.AddButton(tbSeparator3)
		  
		  dim tbZoomOut as New LVToolBarItem
		  tbZoomOut.Caption = "Zoom Out (COMING SOON)"
		  tbZoomOut.HelpTag = "Zoom out of the canvas"
		  tbZoomOut.Image = Icon("ZoomOut")
		  tbZoomOut.Visible = true
		  me.AddButton(tbZoomOut)
		  
		  dim tbZoomIn as New LVToolBarItem
		  tbZoomIn.Caption = "Zoom In (COMING SOON)"
		  tbZoomIn.HelpTag = "Zoom into the canvas"
		  tbZoomIn.Image =Icon("ZoomIn")
		  tbZoomIn.Visible=  true
		  me.AddButton(tbZoomIn)
		  
		  dim tbZoom as New LVToolBarItem
		  tbZoom.Caption = "Zoom (COMING SOON)"
		  tbZoom.HelpTag = "Specify zoom level"
		  tbZoom.Image = Icon("Zoom")
		  tbZoom.Visible=  true
		  me.AddButton(tbZoom)
		  
		  dim tbSeparator4 as New LVStandardToolBarItem
		  me.AddButton(tbSeparator4)
		  
		  'dim tbKeys as New LVToolBarItem
		  'tbKeys.Caption = "Shortcut Keys"
		  'tbKeys.HelpTag = "Display the shortcut keys for scenes"
		  'tbKeys.Image = nil
		  'tbKeys.Visible=  true
		  'me.AddButton(tbKeys)
		  
		  dim tbOrder as New LVToolBarItem
		  tbOrder.Caption = "Instance List (COMING SOON)"
		  tbOrder.HelpTag = "Manage instances in the scene with the ability to reorder, add, remove and edit instances"
		  tbOrder.Image = Icon("InstanceList")
		  tbOrder.Visible=  true
		  me.AddButton(tbOrder)
		  
		  //prepare warning, there is no instances to perform actions on
		  RebuildObjectMenus
		  
		  me.Invalidate
		  me.StatusBar = MainEditor.LVStatusBar1
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub ActionMenu(selectedMenuItem As MenuItem)
		  'select case selectedMenuItem.tag
		  'case "CLEAR"
		  'for co as uint32 = res.Scene.Ubound downto 0
		  '//find match for tagged ResourceObject
		  'if res.Scene(co).theObject = selectedMenuItem.tag then
		  'res.Scene.Remove(co)
		  'end if
		  'next
		  'DrawingCanvas.Invalidate
		  'end select
		  
		  select case selectedMenuItem.text
		  case "Clear All"
		    for co as uint32 = res.Scene.Ubound downto 0
		      res.Scene.Remove(co)
		    next
		    RebuildObjectMenus
		    DrawingCanvas.Invalidate
		  case "Lock All"
		    for each co as CanvasObject in res.Scene
		      co.Locked = true
		    next
		    DrawingCanvas.Invalidate
		  case "Unlock All"
		    for each co as CanvasObject in res.Scene
		      co.Locked = false
		    next
		    DrawingCanvas.Invalidate
		  case "Clear Selected"
		    dim co as canvasobject
		    for i as uint32 = res.scene.ubound downto 0
		      co = res.scene(i)
		      if co.selected then
		        res.Scene.Remove(i)
		      end if
		    next
		    RebuildObjectMenus
		    DrawingCanvas.Invalidate
		  case "Lock Selected"
		    for co as uint32 = 0 to SelectedObjects.Ubound
		      SelectedObjects(co).Locked = true
		    next
		    DrawingCanvas.Invalidate
		  end select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events sceneProperties1
	#tag Event
		Sub Moved()
		  imSplitter1.Left = self.width - sceneProperties1.width - imSplitter1.width
		End Sub
	#tag EndEvent
	#tag Event
		Sub CallOpen()
		  me.scenePropertiesContainer1.thisRes = res
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events DrawingCanvas
	#tag Event
		Sub ObjectSelected(co As CanvasObject, selecting As Boolean)
		  if selecting = false then
		    MainEditor.LVStatusBar1.Text = "Selected: " + co.Caption + " (" + Str(co.Left) + ", " + Str(co.Top) + ")"
		    sceneProperties1.scenePropertiesContainer1.ObjectSelected(co)
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub ObjectMoved(co As CanvasObject)
		  MainEditor.LVStatusBar1.Text = "Moved: " + co.Caption + " (" + Str(co.Left) + ", " + Str(co.Top) + ")"
		  sceneProperties1.scenePropertiesContainer1.ObjectSelected(co)
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseUpOnNothing(x as integer, y as integer)
		  if not IsContextualClick and sceneProperties1.scenePropertiesContainer1.CurrentObject <> nil then
		    me.AddObject(sceneProperties1.scenePropertiesContainer1.CurrentObject.name,x,y,sceneProperties1.scenePropertiesContainer1.CurrentObject)
		    
		    RebuildObjectMenus
		  else
		    ContextualClickOnObject = false
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Function ConstructContextualMenu(base as MenuItem, x as Integer, y as Integer) As Boolean
		  #pragma Unused x
		  #pragma Unused y
		  
		  if ContextualClickOnObject then
		    dim item as MenuItem
		    
		    item = new MenuItem
		    item.Icon = Icon("New")
		    item.Text = "Clear Selected"
		    base.append item
		    
		    base.append new MenuItem(MenuItem.TextSeparator)
		    
		    item = new MenuItem
		    item.Icon = Icon("Lock")
		    item.Text = "Lock Selected"
		    base.append item
		    
		    item = new MenuItem
		    item.Icon = Icon("Unlock")
		    item.Text = "Unlock Selected"
		    base.append item
		    
		    item = new MenuItem
		    //base icon on the colour of the zeroth instance selected
		    item.Icon = me.SelectedObjects(0).theObject.LockIcon
		    item.Text = "Lock Icon Colour (COMING SOON)"
		    base.append item
		    
		    base.append new MenuItem(MenuItem.TextSeparator)
		    
		    item = new MenuItem
		    item.Icon = Icon("Draw")
		    item.Text = "Edit Object"
		    base.append item
		    
		    item = new MenuItem
		    item.Icon = nil
		    item.Text = "Select Object (To Add)"
		    base.append item
		  end if
		End Function
	#tag EndEvent
	#tag Event
		Sub ContextualClickOnObject()
		  ContextualClickOnObject = true
		End Sub
	#tag EndEvent
	#tag Event
		Function ContextualMenuAction(hitItem as MenuItem) As Boolean
		  select case hitItem.text
		  case "Clear Selected"
		    for each co as canvasobject in res.Scene
		      if co.Selected then
		        res.Scene.Remove(res.Scene.IndexOf(co))
		      end if
		    next
		    redim SelectedObjects(-1)
		    me.invalidate
		    RebuildObjectMenus
		  case "Lock Selected"
		    for each co as CanvasObject in SelectedObjects
		      co.Locked = true
		    next
		  case "Unlock Selected"
		    for each co as CanvasObject in SelectedObjects
		      co.Locked = false
		    next
		  case "Lock Icon Colour"
		    dim c as color, b as boolean
		    
		    for each co as CanvasObject in SelectedObjects
		      if co.Locked then
		        c = SelectedObjects(0).theObject.LockColour
		        return true
		      end if
		    next
		    
		    b = SelectColor(c, "Select Icon Colour")
		    
		    if b then
		      dim p as picture = new picture(iconLock.width, Icon("Lock").height)
		      p.graphics.DrawPicture(iconLock, 0, 0)
		      ImagePlayEffectsLibrary.RGB(p, ImagePlayEffectsLibrary.kRGBStyleNormal, c.Red, c.Green, c.Blue)
		      
		      for each co as CanvasObject in SelectedObjects
		        co.theObject.LockColour = c
		        co.theObject.LockIcon = p
		      next
		      
		      me.Invalidate
		    end if
		  end select
		End Function
	#tag EndEvent
	#tag Event
		Sub Open()
		  me.res = self.res
		  
		  DebugCanvas = me
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events imSplitter1
	#tag Event
		Sub Open()
		  me.AddControl(sceneProperties1, false)
		  me.AddControl(DrawingCanvas, true)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="AcceptFocus"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AcceptTabs"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AutoDeactivate"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="EraseBackground"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Appearance"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="InitialParent"
		Group="Position"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ProgrammaticResize"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIndex"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabPanelIndex"
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabStop"
		Visible=true
		Group="Position"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Transparent"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="UseFocusRing"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
