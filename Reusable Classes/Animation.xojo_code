#tag Class
Protected Class Animation
	#tag Method, Flags = &h0
		Sub Constructor()
		  AnimationTimer = new timer
		  AnimationTimer.Mode = Timer.ModeMultiple
		  AnimationTimer.Period = Speed
		  
		  AddHandler AnimationTimer.Action, AddressOf ProceedFrame
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CurrentFrameImage() As Picture
		  if CurrentFrame <> -1 then
		    return Frames(CurrentFrame)
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Frames(index as integer) As picture
		  dim p() as picture = GetFrames
		  if p <> nil then
		    return p(index)
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ProceedFrame(sender as timer)
		  #pragma unused sender
		  CurrentFrame = CurrentFrame + 1
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event GetFrames() As picture()
	#tag EndHook


	#tag Property, Flags = &h0
		AnimationTimer As Timer
	#tag EndProperty

	#tag Property, Flags = &h0
		CurrentFrame As Integer = -1
	#tag EndProperty

	#tag Property, Flags = &h0
		Speed As Double
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="CurrentFrame"
			Group="Behavior"
			InitialValue="-1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Speed"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
