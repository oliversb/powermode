#tag Class
Protected Class PluginContext
	#tag Method, Flags = &h0
		Sub AddAsset(type as string, name as string)
		  select case type
		  case "Object"
		    MainEditor.NewObject(false)
		  case "Sprite"
		    MainEditor.NewSprite(false)
		  case "Scene"
		    MainEditor.NewScene(false)
		  case "Sound"
		    MainEditor.NewSound(false)
		  case "Folder"
		    MainEditor.NewFolder(false)
		  else
		    PrintError("Invalid asset type - """ + type + """")
		    return
		  end select
		  
		  if name <> "" then
		    dim asset as object = MainEditor.SelectedResourceTree.RowTag(MainEditor.SelectedResourceTree.LastIndex)
		    
		    if asset <> nil then
		      dim res as resource = resource(asset)
		      //object, sprite, scene or sound
		      res.MakeName(name)
		    else
		      //folder
		      MainEditor.SelectedResourceTree.Cell(MainEditor.SelectedResourceTree.LastIndex, 0) = name
		    end if
		  else
		    PrintError("Invalid asset name - """ + name + """")
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ApplyTheme()
		  MainEditor.SaveProject
		  MainEditor.Close
		  OpenProject(DocFile.ProjectFileItem)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AskYesNo(question as string) As Boolean
		  dim msg as new MessageDialog
		  msg.Title = "PowerMode Plugin"
		  msg.Explanation = question
		  msg.ActionButton.Caption = "Yes"
		  msg.AlternateActionButton.Visible = true
		  msg.AlternateActionButton.Caption = "No"
		  msg.Icon = msg.GraphicCaution
		  
		  dim msgbutton as MessageDialogButton = msg.ShowModal
		  //return true if clicked yes, return false if clicked no (there is no closebox so the user has to choose)
		  return msgbutton = msg.ActionButton
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AssetCount() As integer
		  return DocFile.resources.Ubound + 1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Beep()
		  REALbasic.Beep
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CloseTab(index as integer)
		  MainEditor.OpenResourceTabs.removeTab(index)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CloseTab(name as string)
		  for each res as resource in DocFile.resources
		    if res.name = name then
		      MainEditor.OpenResourceTabs.removeTab(res.OpenTabValue)
		      exit for
		    end if
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CloseWindow(window as string)
		  select case window
		  case "Main"
		    MainEditor.Close
		  case "Plugin"
		    PluginDev.Close
		  case "PowerScript"
		    CodeEditor.Close
		  case "Command"
		    PopoverQuickCommand.Close
		  else
		    PrintError("Unidentified window")
		  end select
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CopyAsset()
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CurrentAssetName() As string
		  return CurrentAsset.name
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CurrentAssetType() As string
		  select case CurrentAsset
		  case nil
		    PrintError("No selected asset to determine the type of.")
		  case isa ResourceObject
		    return "Object"
		  case isa ResourceSprite
		    return "Sprite"
		  case isa ResourceScene
		    return "Scene"
		  case isa ResourceSound
		    return "Sound"
		  end select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DeleteAsset(index as integer)
		  dim resArray(0) as integer
		  resArray(0) = index
		  resource.ConfirmedDeletion(resArray)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DeleteAsset(name as string)
		  for each res as resource in resources
		    if res.name = name then
		      dim resArray(0) as integer
		      resArray(0) = resources.IndexOf(res)
		      res.ConfirmedDeletion(resArray)
		      exit for
		    end if
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DeleteBlock(index as integer)
		  if CurrentAsset isa ResourceObject then
		    //remove the block
		    ResourceObject(CurrentAsset).Blocks.Remove(index + 2)
		  else
		    PrintError("No object selected to delete block from.")
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ExecutePlugin(name as string)
		  'for each pluginName as string in PluginDev.GetPluginNames
		  PluginDev.RunSourceByPluginName(name)
		  'next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetAsset_Open(index as integer) As boolean
		  return (DocFile.resources(index).OpenTabValue <> -1)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetBlock_Selected(index as integer) As boolean
		  if CurrentAsset isa ResourceObject then
		    return ResourceObject(CurrentAsset).Blocks(index).Selected
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MsgBox(text as string)
		  REALbasic.MsgBox(text)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub NewProject(name as string)
		  SetupNewProject.NewProject(name)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub PrintError(error as string)
		  PluginDev.AddMessage(true, error)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SaveProject()
		  MainEditor.SaveProject
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SaveProjectAs()
		  MainEditor.SaveProjectAs
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SaveProjectAs(directory as string)
		  DocFile.ProjectFileItem = GetFolderItem(directory)
		  DocFile.ModifiedDoc = true
		  MainEditor.SaveProject
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SelectAsset(index as integer)
		  PluginMod.SelectAsset(DocFile.resources(index))
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SelectAsset(name as string)
		  for each res as resource in DocFile.resources
		    if res.name = name then
		      PluginMod.SelectAsset(res)
		      return
		    end if
		  next
		  
		  PrintError("No asset of this name.")
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetBlockValue_(blockIndex as integer, blockItemIndex as integer, objectIndex as integer, variableIndex as integer)
		  #pragma unused blockIndex
		  #pragma unused blockItemIndex 
		  #pragma unused objectIndex 
		  #pragma unused variableIndex 
		  'if CurrentAsset isa ResourceObject then
		  'dim block as BlockBase = ResourceObject(CurrentAsset).Blocks(blockIndex).tag
		  'dim var as ObjectVariable = ResourceObject(DocFile.Resources(objectIndex)).Variables(variableIndex)
		  ' select case blockItemIndex
		  'case 1
		  'block.VariablePath1 = var
		  'case 0
		  'block.VariablePath2 = var
		  'case 2
		  'block.VariablePath3 = var
		  'case 3
		  'block.VariablePath4 = var
		  'end select
		  'else
		  'PrintError("No object selected to set a block's value for.")
		  'end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetBlock_Selected(index as integer, selected as boolean)
		  if CurrentAsset isa ResourceObject then
		    ResourceObject(CurrentAsset).Blocks(index).Selected = selected
		    ResourceObject(CurrentAsset).CodeContainer.Invalidate
		  else
		    PrintError("No object selected to select a block from.")
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ShowWindow(window as string)
		  select case window
		  case "Main"
		    MainEditor.Show
		  case "Plugin"
		    PluginDev.Show
		  case "PowerScript"
		    CodeEditor.Show
		  case "Command"
		    PopoverQuickCommand.displayPopoverAt(0, 0, nil)
		  else
		    PrintError("Unidentified window")
		  end select
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Speak(text as string)
		  REALbasic.Speak(text)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function TabCount() As integer
		  return MainEditor.OpenResourceTabs.tabCount
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ThemeSetHoverStyle(style as string)
		  LVToolBarItem.HoverStyle = style
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ThemeSetIconFile(id as string, path as string)
		  Icon(id) = Picture.Open(GetFolderItem(path))
		  if Icon(id) = nil then
		    PrintError("No image file retrieved from """ + path + """.")
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ThemeSetToolbarBackColour(colour as color)
		  LVToolBar.BackColor = Colour
		  LVToolBar.BackColourDefault = false
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ThemeSetToolbarFontColour(colour as color)
		  LVToolBarItem.FontColour = Colour
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ThemeSetToolbarHoverColour(colour as color)
		  LVToolBarItem.HoverColour = Colour
		End Sub
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
