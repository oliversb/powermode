#tag Window
Begin Window PluginDev
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   0
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   602
   ImplicitInstance=   True
   LiveResize      =   True
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   True
   MaxWidth        =   32000
   MenuBar         =   51245055
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   2
   Resizeable      =   True
   Title           =   "PowerMode Plugin Manager"
   Visible         =   True
   Width           =   756
   Begin Listbox listPlugins
      AutoDeactivate  =   True
      AutoHideScrollbars=   True
      Bold            =   False
      Border          =   True
      ColumnCount     =   1
      ColumnsResizable=   False
      ColumnWidths    =   ""
      DataField       =   ""
      DataSource      =   ""
      DefaultRowHeight=   -1
      Enabled         =   True
      EnableDrag      =   False
      EnableDragReorder=   False
      GridLinesHorizontal=   0
      GridLinesVertical=   0
      HasHeading      =   False
      HeadingIndex    =   -1
      Height          =   416
      HelpTag         =   ""
      Hierarchical    =   False
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      RequiresSelection=   False
      Scope           =   0
      ScrollbarHorizontal=   False
      ScrollBarVertical=   True
      SelectionType   =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   24
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   184
      _ScrollOffset   =   0
      _ScrollWidth    =   -1
   End
   Begin Label Label1
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   40
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Plugin List"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   0
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   71
   End
   Begin PushButton btnRefresh
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Refresh List"
      Default         =   False
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      Scope           =   0
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   440
      Underline       =   False
      Visible         =   True
      Width           =   89
   End
   Begin LVToolBar LVToolbar1
      AcceptFocus     =   False
      AcceptTabs      =   False
      AquaBackground  =   False
      AutoDeactivate  =   True
      Backdrop        =   0
      DoubleBuffer    =   True
      EnableContextualMenu=   True
      Enabled         =   True
      EraseBackground =   False
      FontColourDefault=   False
      Height          =   60
      HelpTag         =   ""
      HideHeight      =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      mToolBarStyleChanged=   True
      RefreshLock     =   False
      Scope           =   0
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      ToolbarStyle    =   0
      Top             =   -95
      Transparent     =   True
      UseFocusRing    =   True
      Visible         =   True
      Width           =   68
   End
   Begin Listbox ErrorListbox
      AutoDeactivate  =   True
      AutoHideScrollbars=   True
      Bold            =   False
      Border          =   True
      ColumnCount     =   1
      ColumnsResizable=   False
      ColumnWidths    =   ""
      DataField       =   ""
      DataSource      =   ""
      DefaultRowHeight=   -1
      Enabled         =   True
      EnableDrag      =   False
      EnableDragReorder=   False
      GridLinesHorizontal=   0
      GridLinesVertical=   0
      HasHeading      =   False
      HeadingIndex    =   -1
      Height          =   140
      HelpTag         =   ""
      Hierarchical    =   False
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   False
      RequiresSelection=   False
      Scope           =   0
      ScrollbarHorizontal=   False
      ScrollBarVertical=   True
      SelectionType   =   0
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   462
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   756
      _ScrollOffset   =   0
      _ScrollWidth    =   -1
   End
   Begin CustomScrollableEditField Edit
      AcceptFocus     =   True
      AcceptTabs      =   True
      AutoCloseBrackets=   False
      AutocompleteAppliesStandardCase=   True
      AutoDeactivate  =   True
      AutoIndentNewLines=   True
      BackColor       =   &cFFFFFF00
      Backdrop        =   0
      Border          =   True
      BorderColor     =   &c88888800
      CaretColor      =   &c00000000
      CaretPos        =   0
      ClearHighlightedRangesOnTextChange=   False
      DirtyLinesColor =   &cFF999900
      DisplayDirtyLines=   True
      DisplayInvisibleCharacters=   False
      DisplayLineNumbers=   True
      DisplayRightMarginMarker=   False
      EnableAutocomplete=   False
      Enabled         =   True
      EnableLineFoldings=   False
      EraseBackground =   False
      GutterBackgroundColor=   &cEEEEEE00
      GutterSeparationLineColor=   &c88888800
      GutterWidth     =   0
      HasBackColor    =   False
      Height          =   438
      HelpTag         =   ""
      HighlightMatchingBrackets=   True
      HighlightMatchingBracketsMode=   0
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   216
      LeftMarginOffset=   4
      LineNumbersColor=   &c88888800
      LineNumbersTextFont=   "System"
      LineNumbersTextSize=   9
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      MaxVisibleLines =   0
      RighScrollMargin=   0
      RightMarginAtPixel=   0
      Scope           =   0
      ScrollPosition  =   0
      ScrollPositionX =   0
      SelLength       =   0
      SelStart        =   0
      SelText         =   ""
      Stats           =   ""
      TabIndex        =   6
      TabPanelIndex   =   0
      TabStop         =   True
      TabWidth        =   4
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "smallSystem"
      TextHeight      =   0
      TextLength      =   0
      TextSelectionColor=   &c00000000
      TextSize        =   0
      ThickInsertionPoint=   True
      Top             =   24
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   540
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  self.Maximize
		  
		  PluginDev.RefreshList
		  SelectPlugin(0)
		End Sub
	#tag EndEvent


	#tag MenuHandler
		Function EditClear() As Boolean Handles EditClear.Action
			DeletePlugin
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function EditCopy() As Boolean Handles EditCopy.Action
			Edit.Copy
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function EditCut() As Boolean Handles EditCut.Action
			Edit.Copy
			Edit.ClearHighlightedCharacterRanges
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function EditPaste() As Boolean Handles EditPaste.Action
			Edit.Paste
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function EditRedo() As Boolean Handles EditRedo.Action
			Edit.Redo
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function EditSelectAll() As Boolean Handles EditSelectAll.Action
			Edit.SelectAll
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function EditUndo() As Boolean Handles EditUndo.Action
			Edit.Undo
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function ExtrasFoldAllLines() As Boolean Handles ExtrasFoldAllLines.Action
			Edit.FoldAllLines
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function ExtrasSelectNextPlaceholder() As Boolean Handles ExtrasSelectNextPlaceholder.Action
			Edit.contentField.SelectNextPlaceholder
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function ExtrasUnfoldAllLines() As Boolean Handles ExtrasUnfoldAllLines.Action
			Edit.UnfoldAllLines
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FileDeletePlugin() As Boolean Handles FileDeletePlugin.Action
			DeletePlugin
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FileNew() As Boolean Handles FileNew.Action
			NewPlugin
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FileOpenFile() As Boolean Handles FileOpenFile.Action
			OpenPluginFolder
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FileSave() As Boolean Handles FileSave.Action
			SavePlugin
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function OptionsMenu() As Boolean Handles OptionsMenu.Action
			PluginOptions.ShowOptions(OptionsCatagoryEnum.Plugins)
			Return True
			
		End Function
	#tag EndMenuHandler


	#tag Method, Flags = &h0
		Sub AddMessage(isError as boolean, messageText as string, optional location as XojoScriptLocation, optional extraInfo as Dictionary)
		  Dim message As New CompilerMessage
		  message.IsError = isError
		  message.Location = location
		  message.ExtraInfo = extraInfo
		  
		  if location <> nil then
		    Dim locationText As String = "[" + Str( Location.line ) + ":" + Str(location.Column) + "-" + Str( location.EndLine ) + ":" +  Str(location.EndColumn ) + "]"
		    If extraInfo.Count > 0 Then
		      ErrorListbox.AddFolder( locationText + " " + messageText )
		    Else
		      ErrorListbox.AddRow( locationText + " " + messageText )
		    End If
		    ErrorListbox.RowTag( ErrorListbox.LastIndex ) = message
		  else
		    ErrorListbox.AddRow( messageText)
		  end if
		  
		  Dim icon As New Picture( 16, 16, 32 )
		  If isError Then
		    if location <> nil then
		      icon.Graphics.ForeColor = &cFF0000
		    else
		      //runtime warning
		      icon.Graphics.ForeColor = &cFFCC00
		    end if
		  Else
		    icon.Graphics.ForeColor = &cFF7F00
		  End If
		  icon.Graphics.FillRoundRect( 0, 0, icon.Width, icon.Height, 4, 4 )
		  
		  ErrorListbox.RowPicture( ErrorListbox.LastIndex ) = icon
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		 Shared Sub AssignPluginsFolder()
		  if PluginsFolder = nil then
		    PluginsFolder = GetFolderItem("").Child("Plugins")
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub DeletePlugin()
		  dim index as integer = listPlugins.ListIndex
		  
		  if index <> -1 then
		    Dim d as New MessageDialog
		    Dim b as MessageDialogButton
		    d.icon = MessageDialog.GraphicCaution
		    d.ActionButton.Caption = "Yes"
		    d.CancelButton.Visible = True
		    d.CancelButton.Caption = "No"
		    d.Message = "Are you sure would you like to permenantly delete the '" + listPlugins.Cell(index, 0) + "' plugin?"
		    d.Explanation = "If you don't save, your changes will be lost. "
		    
		    b = d.ShowModal
		    Select Case b
		    Case d.ActionButton
		      //yes
		      listPlugins.RemoveRow(index)
		    End select
		    
		    //delete plugin through file system
		    PluginFolderItem.Delete
		    if PluginFolderItem.LastErrorCode > 0 then
		      MsgBox "Error deleting file. Error code is " + str(PluginFolderItem.LastErrorCode) + "."
		    end if
		  else
		    MsgBox "Please select a plugin to delete first."
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		 Shared Function GetPluginFolderItem(name as string) As FolderItem
		  return PluginDev.PluginsFolder.Child(name + ".pmplugin")
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		 Shared Function GetPluginNames(instance as boolean = false) As string()
		  dim pluginName as string
		  //return value of this
		  dim pluginList() as string
		  
		  if instance then
		    PluginDev.listPlugins.DeleteAllRows
		  end if
		  
		  AssignPluginsFolder
		  dim f2 as folderitem
		  
		  //.pmplugin counts 9
		  const extcount = 9
		  
		  if PluginsFolder <> nil then
		    for i as uint32 = 1 to PluginsFolder.Count
		      f2 = PluginsFolder.Item(i)
		      if f2.Name.Right(extcount) = ".pmplugin" then
		        pluginName = f2.Name.Left(f2.Name.Len - extcount)
		        
		        if instance then
		          PluginDev.listPlugins.AddRow(pluginName)
		          PluginDev.listPlugins.RowTag(PluginDev.listPlugins.LastIndex) = f2
		          //select plugin depending on wether it is the currently open plugin
		          PluginDev.listPlugins.Selected(i) = (f2 = PluginFolderItem)
		        else
		          pluginList.Append(pluginName)
		        end if
		      end if
		    next
		  end if
		  
		  return pluginList
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub NewPlugin()
		  if CurrentPluginSaved then
		    PluginFolderItem = nil
		  else
		    CurrentPluginSaved = true
		    Dim d as New MessageDialog                  //declare the MessageDialog object
		    Dim b as MessageDialogButton                //for handling the result
		    d.icon = MessageDialog.GraphicCaution         //display warning icon
		    d.ActionButton.Caption = "Save"
		    d.CancelButton.Visible = True                 //show the Cancel button
		    d.AlternateActionButton.Visible = True        //show the "Don't Save" button
		    d.AlternateActionButton.Caption = "Don't Save"
		    d.Message = "Do you want to save changes to this document before closing?"
		    d.Explanation = "If you don't save, your changes will be lost. "
		    select case b
		    case d.ActionButton //Save
		      WinNamePlugin.ShowModal
		    end select
		  end if
		  
		  Edit.Text = ""
		  
		  WinNamePlugin.ShowModal
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub OpenPluginFolder()
		  PluginsFolder.Launch
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RefreshList()
		  call GetPluginNames(true)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		 Shared Sub RunSource(f as folderitem)
		  RunSource(f.ReadAll, false)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		 Shared Sub RunSource(source as string = "", instance as boolean)
		  if instance then
		    source = PluginDev.Edit.Text
		    instance = true
		  end if
		  
		  dim test as string = "@ClearLog Off"
		  dim newText as string
		  //do not clear log if script tells us not to
		  if instance = false then
		    newText = source
		  else
		    if source.Left(test.len) <> test and instance = true then
		      PluginDev.ErrorListbox.DeleteAllRows
		      newText = source
		    else
		      //remove indicator (for compilation)
		      newText = source.Right(source.Len - source.Len)
		    end if
		  end if
		  
		  //remove unused script runner instances
		  dim script as ScriptRunnerMod.ScriptRunnerClass
		  dim i as integer
		  
		  for i = ScriptRunners.Ubound downto 0
		    script = ScriptRunners(i)
		    if script.State = XojoScript.States.Complete then
		      ScriptRunners.Remove(i)
		    end if
		  next
		  
		  //setup last script
		  if ScriptRunner = nil or ScriptRunner.State = XojoScript.States.Running then
		    ScriptRunnerMod.Setup
		  end if
		  
		  ScriptRunner.Context = new PluginContext
		  ScriptRunner.Source = newText
		  
		  if ScriptRunner.Precompile( XojoScript.OptimizationLevels.High ) then
		    ScriptRunner.Run
		  else
		    Beep
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		 Shared Sub RunSourceByPluginName(pluginName as string)
		  RunSource(GetPluginFolderItem(pluginName))
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SavePlugin()
		  if PluginFolderItem = nil then
		    WinNamePlugin.ShowModal
		  else
		    if PluginDev.PluginsFolder <> nil then
		      try
		        dim t as TextOutputStream = TextOutputStream.Create(PluginFolderItem)
		        t.Write(PluginDev.Edit.Text)
		        t.Close
		        PluginDev.RefreshList
		      catch err as IOException
		        MsgBox "Error Code: "+Str(err.errorNumber)
		      catch err as NilObjectException
		        MsgBox "Error Code: "+Str(err.errorNumber)
		      end try
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SelectPlugin(row as integer)
		  dim t as TextInputStream
		  try
		    PluginFolderItem = listPlugins.RowTag(row)
		    t = TextInputStream.Open(FolderItem(PluginFolderItem))
		    t.Encoding = Encodings.UTF16
		    Edit.Text = t.ReadAll
		  catch IOException
		    Edit.Text = "//There was an error accessing this file."
		  end try
		  
		  if t <> nil then
		    t.close
		  end if
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private CurrentPluginSaved As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private MyAutocomplete As PaTrie
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected MyAutocompleteOptions As AutocompleteOptions
	#tag EndProperty

	#tag Property, Flags = &h0
		Shared PluginFolderItem As FolderItem
	#tag EndProperty

	#tag Property, Flags = &h0
		Shared PluginsFolder As FolderItem
	#tag EndProperty


	#tag Constant, Name = XojoDefinition, Type = String, Dynamic = False, Default = \"<\?xml version\x3D\"1.0\" encoding\x3D\"UTF-8\"\?>\r<highlightDefinition version\x3D\"1.3\">\r\t<name>REALbasic</name>\r\r\t<blockStartMarker indent\x3D\"1\">^\\s*(\?:(if|elseif)\\b.*\\sthen\\b\\s*((\'|//)|(\?!.+\?))|if\\s(\?!.*\\sthen\\b)|^\\s*(public|protected|private)\?\\s*(shared)\?\\s*(\?&lt;!end)\\s*(sub|function|property|class|module)|(do|for|while|select|try|catch|else|Get|Set|#if|#elseif|#else)\\b)</blockStartMarker>\r\t<blockEndMarker>^\\s*(\?:end|wend|next|loop|else|catch|elseif|#else|#elseif|#endif)\\b</blockEndMarker>\r\r\t<!-- these prevent subs and functions inside an interface from being indented: -->\r\t<blockStartMarker indent\x3D\"1\" newstate\x3D\"inside_interface\">^\\s*(protected|private)\?\\s*interface\\b</blockStartMarker>\r\t<blockEndMarker condition\x3D\"inside_interface\" newstate\x3D\"\">^\\s*end\\b</blockEndMarker>\r\r\t<lineContinuationMarker indent\x3D\"2\">^(|.*\\W)_\\s*(|//.*|\'.*)$</lineContinuationMarker>\r\r\t<symbols> <!-- these are used to identify declared symbols that are useful for natigation inside larger source file -->\r\t\t<symbol type\x3D\"Method\">\r\t\t\t<entryRegEx>^\\s*(\?:public|protected|private)\?\\s*(shared)\?\\s*(\?&lt;!end)\\s*(\?:sub|function).+\?\\([^\\)]*\\)\\s*(\?:as\\s*\\w+\\s*)\?</entryRegEx>\r\t\t</symbol>\r\t\t<symbol type\x3D\"Property\">\r\t\t\t<entryRegEx>^\\s*(\?:public|protected|private)\?\\s*(shared)\?\\s*(\?&lt;!end)\\s*(property).+</entryRegEx>\r\t\t</symbol>\r\t\t<symbol type\x3D\"Class\">\r\t\t\t<entryRegEx>^\\s*(\?:protected|private)\?\\s*class\\s+\\w+\\s*</entryRegEx>\r\t\t</symbol>\r\t\t<symbol type\x3D\"Module\">\r\t\t\t<entryRegEx>^\\s*(\?:protected|private)\?\\s*module\\s+\\w+\\s*</entryRegEx>\r\t\t</symbol>\t\t\r\t\t<symbol type\x3D\"Interface\">\r\t\t\t<entryRegEx>^\\s*(\?:protected|private)\?\\s*interface\\s+\\w+\\s*</entryRegEx>\r\t\t</symbol>\t\t\r\t</symbols>\r\t\r\t<contexts defaultColor\x3D\"#000000\" caseSensitive\x3D\"no\">\t\t\r        <highlightContext name\x3D\"Doubles\" highlightColor\x3D\"#006532\">\r            <entryRegEx>(\?&lt;\x3D[^\\w\\d]|^)(([0-9]+\\.[0-9]*)|([0-9]{11\x2C}))(\?\x3D[^\\w\\d]|$)</entryRegEx>\r        </highlightContext>    \t\r        \r        <highlightContext name\x3D\"Integers\" highlightColor\x3D\"#326598\">\r            <entryRegEx>(\?&lt;\x3D[^\\w\\d]|^)([0-9]{1\x2C10})(\?\x3D[^\\w\\d]|$)</entryRegEx>\r        </highlightContext>\r\r\t\t<highlightContext name\x3D\"PreProcessor\" highlightColor\x3D\"#0000FF\">\r            <entryRegEx>(#\\w+)</entryRegEx>\r        </highlightContext>        \r\t\t\r\t\t<highlightContext name\x3D\"Comment\" highlightColor\x3D\"#7F0000\" italic\x3D\"true\">\r\t\t\t<startRegEx>\'</startRegEx>\r\t\t\t<endRegEx>[\\n\\r]</endRegEx>\r\t\t</highlightContext>\r\t\t\r\t\t<highlightContext name\x3D\"C-Comment\" highlightColor\x3D\"#7F0000\" italic\x3D\"true\">\r\t\t\t<startRegEx>//</startRegEx>\r\t\t\t<endRegEx>[\\n\\r]</endRegEx>\r\t\t</highlightContext>\r\t\t\r\t\t<highlightContext name\x3D\"REM-Comment\" highlightColor\x3D\"#7F0000\" italic\x3D\"true\">\r\t\t\t<startRegEx>^\\s*rem\\s</startRegEx>\r\t\t\t<endRegEx>[\\n\\r]</endRegEx>\r\t\t</highlightContext>\r\t\t\r\t\t<highlightContext name\x3D\"String\" highlightColor\x3D\"#6500FE\">\r\t\t\t<startRegEx>\"</startRegEx>\r\t\t\t<endRegEx>[^\"\\n\\r]*[\"\\n\\r]</endRegEx>\r\t\t</highlightContext>\r\r        <highlightContext name\x3D\"BasicTypes\" highlightColor\x3D\"#0000FF\" bold\x3D\"true\">\r            <keywords>\r\t\t\t\t<string>Boolean</string>\r\t\t\t\t<string>Color</string>\r\t\t\t\t<string>Double</string>\r\t\t\t\t<string>Integer</string>\r\t\t\t\t<string>Int8</string>\r\t\t\t\t<string>UInt8</string>\r\t\t\t\t<string>Int16</string>\r\t\t\t\t<string>UInt16</string>\r\t\t\t\t<string>Int32</string>\r\t\t\t\t<string>UInt32</string>\r\t\t\t\t<string>Int64</string>\r\t\t\t\t<string>UInt64</string>\r\t\t\t\t<string>Single</string>\r\t\t\t\t<string>String</string>\r\t\t\t\t<string>CFString</string>\r\t\t\t\t<string>Ptr</string>\r            </keywords>\r        </highlightContext>\r\r        <highlightContext name\x3D\"Keywords\" highlightColor\x3D\"#0000FF\" bold\x3D\"true\">\r            <keywords>\r\t\t\t\t<string>#Bad</string>\r\t\t\t\t<string>#Else</string>\r\t\t\t\t<string>#ElseIf</string>\r\t\t\t\t<string>#EndIf</string>\r\t\t\t\t<string>#If</string>\r\t\t\t\t<string>#Pragma</string>\r\t\t\t\t<string>#Tag</string>\r\t\t\t\t<string>AddressOf</string>\r\t\t\t\t<string>And</string>\r\t\t\t\t<string>Array</string>\r\t\t\t\t<string>As</string>\r\t\t\t\t<string>Assigns</string>\r\t\t\t\t<string>ByRef</string>\r\t\t\t\t<string>ByVal</string>\r\t\t\t\t<string>Call</string>\r\t\t\t\t<string>Case</string>\r\t\t\t\t<string>Catch</string>\r\t\t\t\t<string>Class</string>\r\t\t\t\t<string>Const</string>\r\t\t\t\t<string>Continue</string>\r\t\t\t\t<string>Declare</string>\r\t\t\t\t<string>Delegate</string>\r\t\t\t\t<string>Dim</string>\r\t\t\t\t<string>Do</string>\r\t\t\t\t<string>DownTo</string>\r\t\t\t\t<string>Each</string>\r\t\t\t\t<string>Else</string>\r\t\t\t\t<string>ElseIf</string>\r\t\t\t\t<string>End</string>\r\t\t\t\t<string>Enum</string>\r\t\t\t\t<string>Event</string>\r\t\t\t\t<string>Exception</string>\r\t\t\t\t<string>Exit</string>\r\t\t\t\t<string>Extends</string>\r\t\t\t\t<string>False</string>\r\t\t\t\t<string>Finally</string>\r\t\t\t\t<string>For</string>\r\t\t\t\t<string>Function</string>\r\t\t\t\t<string>Get</string>\r\t\t\t\t<string>Global</string>\r\t\t\t\t<string>GoTo</string>\r\t\t\t\t<string>Handles</string>\r\t\t\t\t<string>If</string>\r\t\t\t\t<string>Implements</string>\r\t\t\t\t<string>In</string>\r\t\t\t\t<string>Inherits</string>\r\t\t\t\t<string>Inline68K</string>\r\t\t\t\t<string>Interface</string>\r\t\t\t\t<string>Is</string>\r\t\t\t\t<string>IsA</string>\r\t\t\t\t<string>Lib</string>\r\t\t\t\t<string>Loop</string>\r\t\t\t\t<string>Me</string>\r\t\t\t\t<string>Mod</string>\r\t\t\t\t<string>Module</string>\r\t\t\t\t<string>Namespace</string>\r\t\t\t\t<string>New</string>\r\t\t\t\t<string>Next</string>\r\t\t\t\t<string>Nil</string>\r\t\t\t\t<string>Not</string>\r\t\t\t\t<string>Object</string>\r\t\t\t\t<string>Of</string>\r\t\t\t\t<string>Optional</string>\r\t\t\t\t<string>Or</string>\r\t\t\t\t<string>ParamArray</string>\r\t\t\t\t<string>Private</string>\r\t\t\t\t<string>Property</string>\r\t\t\t\t<string>Protected</string>\r\t\t\t\t<string>Public</string>\r\t\t\t\t<string>Raise</string>\r\t\t\t\t<string>RaiseEvent</string>\r\t\t\t\t<string>Redim</string>\r\t\t\t\t<string>Rem</string>\r\t\t\t\t<string>Return</string>\r\t\t\t\t<string>Select</string>\r\t\t\t\t<string>Self</string>\r\t\t\t\t<string>Set</string>\r\t\t\t\t<string>Shared</string>\r\t\t\t\t<string>Soft</string>\r\t\t\t\t<string>Static</string>\r\t\t\t\t<string>Step</string>\r\t\t\t\t<string>Structure</string>\r\t\t\t\t<string>Sub</string>\r\t\t\t\t<string>Super</string>\r\t\t\t\t<string>Then</string>\r\t\t\t\t<string>To</string>\r\t\t\t\t<string>True</string>\r\t\t\t\t<string>Try</string>\r\t\t\t\t<string>Until</string>\r\t\t\t\t<string>Wend</string>\r\t\t\t\t<string>While</string>\r\t\t\t\t<string>With</string>\r            </keywords>\r        </highlightContext>\r\t</contexts>\r</highlightDefinition>", Scope = Public
	#tag EndConstant


#tag EndWindowCode

#tag Events listPlugins
	#tag Event
		Function CellClick(row as Integer, column as Integer, x as Integer, y as Integer) As Boolean
		  #pragma unused column
		  #pragma unused x
		  #pragma unused y
		  
		  SelectPlugin(row)
		  RefreshList
		End Function
	#tag EndEvent
#tag EndEvents
#tag Events btnRefresh
	#tag Event
		Sub Action()
		  RefreshList
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events LVToolbar1
	#tag Event
		Sub Open()
		  me.ToolBarStyle = LVToolBar.STYLE_BIG_ICONS_LABEL
		  dim sep as LVStandardToolBarItem
		  dim btn as LVToolBarItem
		  
		  btn = new LVToolBarItem
		  btn.Caption = "Run"
		  btn.HelpTag = "Run the source code."
		  btn.Image = Icon("Play")
		  btn.Visible = true
		  me.AddButton(btn)
		  
		  sep = new LVStandardToolBarItem
		  me.AddButton(sep)
		  
		  btn = new LVToolBarItem
		  btn.Caption = "New Plugin"
		  btn.HelpTag = "Start a plugin from scratch."
		  btn.Image = Icon("New")
		  btn.Visible = true
		  me.AddButton(btn)
		  
		  btn = new LVToolBarItem
		  btn.Caption = "Save Plugin"
		  btn.HelpTag = "Save open plugin."
		  btn.Image = Icon("Save")
		  btn.Visible = true
		  me.AddButton(btn)
		  
		  btn = new LVToolBarItem
		  btn.Caption = "Delete Plugin"
		  btn.HelpTag = "Delete selected plugin."
		  btn.Image = Icon("Delete")
		  btn.Visible = true
		  me.AddButton(btn)
		  
		  sep = new LVStandardToolBarItem
		  me.AddButton(sep)
		  
		  btn = new LVToolBarItem
		  btn.Caption = "Plugin Folder"
		  btn.HelpTag = "Opens the folder where plugins are stored."
		  btn.Image = Icon("Open")
		  btn.Visible = true
		  me.AddButton(btn)
		  
		  //workaround for bug in toolbar
		  self.width = self.width + 1
		  self.width = self.width -1
		  'me.Invalidate
		End Sub
	#tag EndEvent
	#tag Event
		Sub Action(selectedToolBarItem As LVToolBarItem)
		  select case selectedToolBarItem.Caption
		  case "Run"
		    RunSource(true)
		  case "New Plugin"
		    NewPlugin
		  case "Save Plugin"
		    SavePlugin
		  case "Delete Plugin"
		    DeletePlugin
		  case "Plugin Folder"
		    OpenPluginFolder
		  end select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ErrorListbox
	#tag Event
		Function CellBackgroundPaint(g As Graphics, row As Integer, column As Integer) As Boolean
		  #pragma unused row
		  #pragma unused column
		  
		  //alternating row colours
		  if (row mod 2) = 0 then
		    g.foreColor = rgb(237,243,254)
		    g.fillrect 0,0,g.Width,g.height
		  end if
		End Function
	#tag EndEvent
	#tag Event
		Function CellClick(row as Integer, column as Integer, x as Integer, y as Integer) As Boolean
		  #pragma unused column
		  #pragma unused x
		  #pragma unused y
		  
		  if me.RowTag(row) <> nil then
		    //get location of error from tag of selected row
		    dim location as XojoScriptLocation = CompilerMessage(me.RowTag(row)).Location
		    //select the source of the error
		    Edit.contentField.changeSelection(location.Character, location.EndCharacter - location.Character)
		  end if
		End Function
	#tag EndEvent
#tag EndEvents
#tag Events Edit
	#tag Event
		Sub Open()
		  me.ClearDirtyLines
		  
		  dim def as new HighlightDefinition
		  call def.LoadFromXml(XojoDefinition)
		  me.SyntaxDefinition = def
		  
		  //autocomplete
		  me.EnableAutocomplete = true
		  MyAutocompleteOptions = new AutocompleteOptions
		  
		  //setup autocomplete keys
		  MyAutocomplete = new PaTrie
		  Dim p as new PluginContext
		  Dim methods() As Introspection.MethodInfo = Introspection.GetType(p).GetMethods
		  For i As Integer = 0 To methods.Ubound
		    call MyAutocomplete.addKey (methods(i).Name)
		  Next
		  
		  
		  me.contentField.SetFocus
		End Sub
	#tag EndEvent
	#tag Event
		Sub TextChanged()
		  CurrentPluginSaved = false
		End Sub
	#tag EndEvent
	#tag Event
		Function AutocompleteOptionsForPrefix(prefix as string) As AutocompleteOptions
		  dim commonPrefix as string
		  MyAutocompleteOptions.Prefix = prefix
		  MyAutocompleteOptions.Options = MyAutocomplete.wordsForPrefix(prefix, commonPrefix)
		  MyAutocompleteOptions.LongestCommonPrefix = commonPrefix
		  
		  return  MyAutocompleteOptions
		End Function
	#tag EndEvent
	#tag Event
		Function ShouldTriggerAutocomplete(Key as string, hasAutocompleteOptions as boolean) As boolean
		  'Return Keyboard.AsyncKeyDown(53) //to use ESC (xcode, mail, coda...)
		  Return key = chr(9) and hasAutocompleteOptions and not Keyboard.OptionKey//to use tab, if there are options
		  'Return Keyboard.AsyncControlKey and Keyboard.AsyncKeyDown(49) //to use ctrl-space as in visual studio
		  'Return false //no autocomplete, ever
		End Function
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
			"11 - Modeless Dialog"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="400"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="600"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
