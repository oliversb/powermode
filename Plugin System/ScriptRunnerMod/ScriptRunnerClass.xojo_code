#tag Class
Protected Class ScriptRunnerClass
Inherits XojoScript
	#tag Event
		Function CompilerError(location As XojoScriptLocation, error As XojoScript.Errors, errorInfo As Dictionary) As Boolean
		  PluginDev.AddMessage( True, ErrorCodeToString( error ), location, errorInfo )
		End Function
	#tag EndEvent

	#tag Event
		Sub CompilerWarning(location As XojoScriptLocation, warning As XojoScript.Warnings, warningInfo As Dictionary)
		  #pragma unused location
		  #pragma unused warning
		  #pragma unused warningInfo
		  
		  'AddMessage( False, WarningCodeToString( warning ), location, warningInfo )
		  
		End Sub
	#tag EndEvent

	#tag Event
		Function Input(prompt As String) As String
		  #pragma unused prompt
		  
		  'Dim inputWin As New InputWindow()
		  'Return inputWin.Present( Self, prompt )
		End Function
	#tag EndEvent

	#tag Event
		Sub Print(msg As String)
		  PluginDev.ErrorListbox.AddRow(msg)
		  PluginDev.Hide
		End Sub
	#tag EndEvent

	#tag Event
		Sub RuntimeError(error As RuntimeException)
		  Dim dialog As New MessageDialog
		  dialog.Title = "Unhandled Exception"
		  dialog.Message = "Unhandled Exception"
		  dialog.Explanation = Introspection.GetType( error ).Name + " was not caught."
		  If error.Message <> "" Then
		    dialog.Explanation = dialog.Explanation + " " + error.Message
		  End If
		  
		  Call dialog.ShowModal
		End Sub
	#tag EndEvent


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Source"
			Visible=true
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
