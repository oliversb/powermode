#tag Module
Protected Module CodeOptions
	#tag Property, Flags = &h0
		AutoCloseBrackets As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		AutoIndentNewLines As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		Border As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		DisplayDirtyLines As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		DisplayInvisibleCharacters As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		DisplayLineNumbers As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		DisplayRightMarginMarker As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		EnableLineFoldings As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		HighlightMatchingBrackets As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		HighlightMatchingBracketsMode As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		IndentVisually As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		KeepEntireTextIndented As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		LeftMarginOffset As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		LineNumbersTextSize As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		RightMarginAtPixel As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		SyntaxDefinition As HighlightDefinition
	#tag EndProperty

	#tag Property, Flags = &h0
		TabWidth As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		TextFont As String
	#tag EndProperty

	#tag Property, Flags = &h0
		TextSize As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		ThickInsertionPoint As Boolean
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="AutoCloseBrackets"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AutoIndentNewLines"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Border"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DisplayDirtyLines"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DisplayInvisibleCharacters"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DisplayLineNumbers"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DisplayRightMarginMarker"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="EnableLineFoldings"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HighlightMatchingBrackets"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HighlightMatchingBracketsMode"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IndentVisually"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="KeepEntireTextIndented"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LeftMarginOffset"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LineNumbersTextSize"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RightMarginAtPixel"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabWidth"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TextFont"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TextSize"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ThickInsertionPoint"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
