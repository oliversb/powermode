#tag Window
Begin Window CodeEditor
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   True
   Frame           =   0
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   606
   ImplicitInstance=   True
   LiveResize      =   True
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   True
   MaxWidth        =   32000
   MenuBar         =   1061007404
   MenuBarVisible  =   True
   MinHeight       =   560
   MinimizeButton  =   True
   MinWidth        =   500
   Placement       =   0
   Resizeable      =   True
   Title           =   "PowerScript"
   Visible         =   True
   Width           =   847
   Begin ScrollBar verticalSB
      AcceptFocus     =   True
      AutoDeactivate  =   True
      Enabled         =   True
      Height          =   586
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   831
      LineStep        =   1
      LiveScroll      =   True
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      Maximum         =   0
      Minimum         =   0
      PageStep        =   20
      Scope           =   "0"
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   0
      Value           =   0
      Visible         =   True
      Width           =   16
   End
   Begin ScrollBar horizontalSB
      AcceptFocus     =   True
      AutoDeactivate  =   True
      Enabled         =   True
      Height          =   16
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   0
      LineStep        =   1
      LiveScroll      =   True
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   False
      Maximum         =   0
      Minimum         =   0
      PageStep        =   20
      Scope           =   "0"
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   590
      Value           =   0
      Visible         =   True
      Width           =   827
   End
   Begin CustomEditField TestField
      AcceptFocus     =   True
      AcceptTabs      =   True
      AutoCloseBrackets=   False
      AutocompleteAppliesStandardCase=   True
      AutoDeactivate  =   True
      AutoIndentNewLines=   True
      BackColor       =   &cFFFFFF00
      Backdrop        =   0
      Border          =   True
      BorderColor     =   &c88888800
      BracketHighlightColor=   &cFFFF0000
      CaretColor      =   &c00000000
      CaretLine       =   0
      CaretPos        =   0
      ClearHighlightedRangesOnTextChange=   False
      DirtyLinesColor =   &cFF999900
      disableReset    =   False
      DisplayDirtyLines=   True
      DisplayInvisibleCharacters=   False
      DisplayLineNumbers=   True
      DisplayRightMarginMarker=   False
      DoubleBuffer    =   False
      EnableAutocomplete=   False
      Enabled         =   True
      EnableLineFoldings=   False
      enableLineFoldingSetting=   False
      EraseBackground =   False
      GraphicsPicture =   0
      GutterBackgroundColor=   &cEEEEEE00
      GutterSeparationLineColor=   &c88888800
      GutterWidth     =   0
      Height          =   474
      HelpTag         =   ""
      HighlightBlocksOnMouseOverGutter=   True
      HighlightMatchingBrackets=   True
      HighlightMatchingBracketsMode=   0
      ignoreRepaint   =   False
      IndentPixels    =   16
      IndentVisually  =   True
      Index           =   -2147483648
      InitialParent   =   ""
      KeepEntireTextIndented=   True
      Left            =   0
      leftMarginOffset=   4
      LineNumbersColor=   &c88888800
      LineNumbersTextFont=   "System"
      LineNumbersTextSize=   9
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      MaxVisibleLines =   0
      ReadOnly        =   False
      RightMarginAtPixel=   0
      RightScrollMargin=   150
      Scope           =   "0"
      ScrollPosition  =   0
      ScrollPositionX =   0
      selLength       =   0
      selStart        =   0
      SelText         =   ""
      Stats           =   ""
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      TabWidth        =   4
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "smallSystem"
      TextHeight      =   0
      TextLength      =   0
      TextSelectionColor=   &c00000000
      TextSize        =   0
      ThickInsertionPoint=   True
      Top             =   0
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   827
   End
   Begin Listbox listErrors
      AutoDeactivate  =   True
      AutoHideScrollbars=   True
      Bold            =   False
      Border          =   True
      ColumnCount     =   1
      ColumnsResizable=   False
      ColumnWidths    =   ""
      DataField       =   ""
      DataSource      =   ""
      DefaultRowHeight=   -1
      Enabled         =   True
      EnableDrag      =   False
      EnableDragReorder=   False
      GridLinesHorizontal=   0
      GridLinesVertical=   0
      HasHeading      =   False
      HeadingIndex    =   -1
      Height          =   111
      HelpTag         =   ""
      Hierarchical    =   False
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   False
      RequiresSelection=   False
      Scope           =   "0"
      ScrollbarHorizontal=   False
      ScrollBarVertical=   True
      SelectionType   =   0
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   475
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   827
      _ScrollOffset   =   0
      _ScrollWidth    =   -1
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub EnableMenuItems()
		  //enable/disable undo/redo menus.
		  'EditUndo.Enabled = TestField.CanUndo
		  'EditRedo.Enabled = TestField.CanRedo
		End Sub
	#tag EndEvent

	#tag Event
		Function MouseDown(X As Integer, Y As Integer) As Boolean
		  #pragma unused x
		  #pragma unused y
		  
		  // this is just a cheap trick to cause a redraw for debugging
		  testField.Redraw
		  
		  #if DebugBuild
		    'Debugging.DebugLog Debugging.AccumulationResult
		    'Debugging.AccumulationClear
		  #endif
		End Function
	#tag EndEvent

	#tag Event
		Sub Open()
		  // Note: If you handle large text, e.g. more than a few 100 lines, and if you also want to use
		  // indentation, then the syntax definition should be set to nil before setting the new text,
		  //
		  
		  TestField.ReindentText // cleans up indentations, removing any leading blanks from the lines
		  TestField.ResetUndo // needed so that the Reindentation doesn't become an undoable action
		  
		  me.maximize
		End Sub
	#tag EndEvent


	#tag MenuHandler
		Function EditRedo() As Boolean Handles EditRedo.Action
			TestField.Redo
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function EditUndo() As Boolean Handles EditUndo.Action
			TestField.Undo
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function ExtrasFoldAllLines() As Boolean Handles ExtrasFoldAllLines.Action
			TestField.FoldAllLines
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function ExtrasFoldCurrentBlock() As Boolean Handles ExtrasFoldCurrentBlock.Action
			TestField.FoldBlockAtCaretPos
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function ExtrasSelectNextPlaceholder() As Boolean Handles ExtrasSelectNextPlaceholder.Action
			TestField.SelectNextPlaceholder
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function ExtrasUnfoldAllLines() As Boolean Handles ExtrasUnfoldAllLines.Action
			TestField.UnfoldAllLines
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FileClose() As Boolean Handles FileClose.Action
			self.Close
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FilePageSetup() As Boolean Handles FilePageSetup.Action
			dim ps as new PrinterSetup
			if PrinterSetupString <> "" then ps.SetupString = PrinterSetupString
			
			if ps.PageSetupDialog then
			PrinterSetupString = ps.SetupString
			TestField.RightMarginAtPixel = ps.Width
			end if
			
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FilePrint() As Boolean Handles FilePrint.Action
			print
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FileSave() As Boolean Handles FileSave.Action
			dim file as FolderItem
			file = GetSaveFolderItem("Text", "MyFile.txt")
			if file = nil then Return true
			
			if TestField.Save(file, "Text", encodings.UTF8) then
			SetWindowModified(TestField.IsDirty)
			Return true
			end if
			Return False
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FindFind() As Boolean Handles FindFind.Action
			FindWindow.Show
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FindFindNext() As Boolean Handles FindFindNext.Action
			call FindWindow.findNext
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FindReplace() As Boolean Handles FindReplace.Action
			FindWindow.replace
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FindReplaceFindNext() As Boolean Handles FindReplaceFindNext.Action
			FindWindow.replaceAndFind
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function OptionsMenu() As Boolean Handles OptionsMenu.Action
			WinCodeOptions.ShowModal
			
			Return True
			
		End Function
	#tag EndMenuHandler


	#tag Method, Flags = &h0
		 Shared Function GuessEncoding(s As String) As TextEncoding
		  // Guess what text encoding the text in the given string is in.
		  // This ignores the encoding set on the string, and guesses
		  // one of the following:
		  //
		  //   * UTF-32
		  //   * UTF-16
		  //   * UTF-8
		  //   * Encodings.SystemDefault
		  //
		  // Written by Joe Strout, slightly modernized by Thomas Tempelmann
		  
		  #pragma DisableBackgroundTasks
		  #pragma DisableBoundsChecking
		  
		  static isBigEndian, endianChecked As Boolean
		  if not endianChecked then
		    Dim temp As String = Encodings.UTF16.Chr( &hFEFF )
		    isBigEndian = (AscB( MidB( temp, 1, 1 ) ) = &hFE)
		    endianChecked = true
		  end if
		  
		  // check for a BOM
		  Dim b0 As Integer = AscB( s.MidB( 1, 1 ) )
		  Dim b1 As Integer = AscB( s.MidB( 2, 1 ) )
		  Dim b2 As Integer = AscB( s.MidB( 3, 1 ) )
		  Dim b3 As Integer = AscB( s.MidB( 4, 1 ) )
		  if b0=0 and b1=0 and b2=&hFE and b3=&hFF then
		    // UTF-32, big-endian
		    if isBigEndian then
		      #if RBVersion >= 2012.02
		        return Encodings.UTF32
		      #else
		        return Encodings.UCS4
		      #endif
		    else
		      return Encodings.UTF32BE
		    end if
		  elseif b0=&hFF and b1=&hFE and b2=0 and b3=0 and s.LenB >= 4 then
		    // UTF-32, little-endian
		    if isBigEndian then
		      return Encodings.UTF32LE
		    else
		      #if RBVersion >= 2012.02
		        return Encodings.UTF32
		      #else
		        return Encodings.UCS4
		      #endif
		    end if
		  elseif b0=&hFE and b1=&hFF then
		    // UTF-16, big-endian
		    if isBigEndian then
		      return Encodings.UTF16
		    else
		      return Encodings.UTF16BE
		    end if
		  elseif b0=&hFF and b1=&hFE then
		    // UTF-16, little-endian
		    if isBigEndian then
		      return Encodings.UTF16LE
		    else
		      return Encodings.UTF16
		    end if
		  elseif b0=&hEF and b1=&hBB and b1=&hBF then
		    // UTF-8 (ah, a sensible encoding where endianness doesn't matter!)
		    return Encodings.UTF8
		  end if
		  
		  // no BOM; see if it's entirely ASCII.
		  Dim m As MemoryBlock = s
		  Dim i, maxi As Integer = s.LenB - 1
		  for i = 0 to maxi
		    if m.Byte(i) > 127 then exit
		  next
		  if i > maxi then return Encodings.ASCII
		  
		  // Not ASCII; check for a high incidence of nulls every other byte,
		  // which suggests UTF-16 (at least in Roman text).
		  Dim nulls(1) As Integer  // null count in even (0) and odd (1) bytes
		  for i = 0 to maxi
		    if m.Byte(i) = 0 then
		      nulls(i mod 2) = nulls(i mod 2) + 1
		    end if
		  next
		  if nulls(0) > nulls(1)*2 and nulls(0) > maxi\2 then
		    // UTF-16, big-endian
		    if isBigEndian then
		      return Encodings.UTF16
		    else
		      return Encodings.UTF16BE
		    end if
		  elseif nulls(1) > nulls(0)*2 and nulls(1) > maxi\2 then
		    // UTF-16, little-endian
		    if isBigEndian then
		      return Encodings.UTF16LE
		    else
		      return Encodings.UTF16
		    end if
		  end if
		  
		  // it's not ASCII; check for illegal UTF-8 characters.
		  // See Table 3.1B, "Legal UTF-8 Byte Sequences",
		  // at <http://unicode.org/versions/corrigendum1.html>
		  Dim b As Byte
		  for i = 0 to maxi
		    select case m.Byte(i)
		    case &h00 to &h7F
		      // single-byte character; just continue
		    case &hC2 to &hDF
		      // one additional byte
		      if i+1 > maxi then exit for
		      b = m.Byte(i+1)
		      if b < &h80 or b > &hBF then exit for
		      i = i+1
		    case &hE0
		      // two additional bytes
		      if i+2 > maxi then exit for
		      b = m.Byte(i+1)
		      if b < &hA0 or b > &hBF then exit for
		      b = m.Byte(i+2)
		      if b < &h80 or b > &hBF then exit for
		      i = i+2
		    case &hE1 to &hEF
		      // two additional bytes
		      if i+2 > maxi then exit for
		      b = m.Byte(i+1)
		      if b < &h80 or b > &hBF then exit for
		      b = m.Byte(i+2)
		      if b < &h80 or b > &hBF then exit for
		      i = i+2
		    case &hF0
		      // three additional bytes
		      if i+3 > maxi then exit for
		      b = m.Byte(i+1)
		      if b < &h90 or b > &hBF then exit for
		      b = m.Byte(i+2)
		      if b < &h80 or b > &hBF then exit for
		      b = m.Byte(i+3)
		      if b < &h80 or b > &hBF then exit for
		      i = i+3
		    case &hF1 to &hF3
		      // three additional bytes
		      if i+3 > maxi then exit for
		      b = m.Byte(i+1)
		      if b < &h80 or b > &hBF then exit for
		      b = m.Byte(i+2)
		      if b < &h80 or b > &hBF then exit for
		      b = m.Byte(i+3)
		      if b < &h80 or b > &hBF then exit for
		      i = i+3
		    case &hF4
		      // three additional bytes
		      if i+3 > maxi then exit for
		      b = m.Byte(i+1)
		      if b < &h80 or b > &h8F then exit for
		      b = m.Byte(i+2)
		      if b < &h80 or b > &hBF then exit for
		      b = m.Byte(i+3)
		      if b < &h80 or b > &hBF then exit for
		      i = i+3
		    else
		      exit for
		    end select
		  next i
		  if i > maxi then return Encodings.UTF8  // no illegal UTF-8 sequences, so that's probably what it is
		  
		  // If not valid UTF-8, then let's just guess the system default.
		  return Encodings.SystemDefault
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub print()
		  dim ps as new PrinterSetup
		  
		  //if the user changed the settings using page setup, set those.
		  if PrinterSetupString <> "" then _
		  ps.SetupString = PrinterSetupString
		  
		  //get graphics context
		  dim g as Graphics = OpenPrinterDialog(ps)
		  if g = nil then Return
		  
		  //now, get printer object from editfield.
		  dim fieldPrinter as CustomEditFieldPrinter = TestField.CustomEditFieldPrinter(g)
		  
		  //range to print.
		  dim maxLine as Integer = TestField.LineCount - 1
		  dim range as DataRange = new DataRange(0, maxLine)
		  
		  //change these if you want to play with different block sizes...
		  dim left, top, maxw, maxh as Integer
		  left = ps.Left
		  top = ps.Top
		  maxw = ps.Width
		  maxh = ps.Height
		  
		  dim wrap as Boolean = true //wrap text?
		  
		  //print...
		  //drawBlock returns the last printed line, so you can recalculate the next lines to print.
		  g.ForeColor = &ccccccc
		  g.DrawRect left, top, maxw, maxh
		  dim lastPrintedLine as Integer = fieldPrinter.DrawBlock(left, top, maxw, maxh, range, wrap, TestField.DisplayLineNumbers)
		  while lastPrintedLine < maxLine
		    //recalculate range...
		    range.offset = lastPrintedLine + 1
		    range.length = maxLine - range.offset
		    
		    //get next page
		    g.NextPage
		    
		    //and print next block.
		    g.ForeColor = &ccccccc
		    g.DrawRect left, top, maxw, maxh
		    lastPrintedLine = fieldPrinter.DrawBlock(left, top, maxw, maxh, range, wrap, TestField.DisplayLineNumbers)
		  wend
		  
		  g.NextPage
		  g.DrawString "Now, with different block settings....", 10, g.TextHeight
		  
		  //do it again with different settings.
		  //try these
		  left = ps.Width / 2 - 200
		  top = ps.Height / 2 - 200
		  maxw = 400
		  maxh = 400
		  
		  range = new DataRange(0, maxLine) //reset range
		  
		  g.ForeColor = &ccccccc
		  g.DrawRect left, top, maxw, maxh
		  lastPrintedLine = fieldPrinter.DrawBlock(left, top, maxw, maxh, range, wrap, TestField.DisplayLineNumbers)
		  while lastPrintedLine < maxLine
		    //recalculate range...
		    range.offset = lastPrintedLine + 1
		    range.length = maxLine - range.offset
		    
		    //get next page
		    g.NextPage
		    
		    //and print next block.
		    g.ForeColor = &ccccccc
		    g.DrawRect left, top, maxw, maxh
		    lastPrintedLine = fieldPrinter.DrawBlock(left, top, maxw, maxh, range, wrap, TestField.DisplayLineNumbers)
		  wend
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub reloadBookmarks()
		  'bookmarks.DeleteAllRows
		  
		  bookmarkIndexes = TestField.BookmarkList
		  for i as Integer = 0 to UBound(bookmarkIndexes)
		    'bookmarks.AddRow "Lineindex: "+str(bookmarkIndexes(i))
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SetWindowModified(b as boolean)
		  #pragma unused b
		  
		  // Added 11/15/2001 by Jarvis Badgley
		  // Modified 2/3/2002 by Kevin Ballard
		  
		  dim w as Window=self
		  Dim osErr As Integer
		  dim v as variant
		  #pragma unused w
		  #pragma unused osErr
		  #pragma unused v
		  
		  #if TargetMacOS then
		    #if TargetCarbon or TargetCocoa then
		      Declare Function SetWindowModified Lib "Carbon" (window As WindowPtr, modified As Integer) As Integer
		    #else
		      Declare Function SetWindowModified Lib "WindowsLib" (window As WindowPtr, modified As Integer) As Integer
		    #endif // TargetCarbon
		    
		    v=b
		    osErr=SetWindowModified(w,v)
		  #endif // TargetMacOS
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Show(cf As ContentFrame)
		  CodeEditor.Show
		  Block = BlockExecuteScript(cf.tag)
		  
		  CodeEditor.TestField.Text = Block.CustomMessage
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub toggleBookmark()
		  if TestField.LineHasBookmark(TestField.CaretLine) then
		    TestField.ClearBookmark(TestField.CaretLine)
		  else
		    TestField.AddBookmark(TestField.CaretLine)
		  end if
		  
		  reloadBookmarks
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h1
		Protected Block As BlockExecuteScript
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected BookmarkButtonIndex As Integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected bookmarkIndexes() As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private javaSyntaxIdx As Integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected lastLine As Integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected MyAutocomplete As patrie
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected PrinterSetupString As string
	#tag EndProperty

	#tag Property, Flags = &h21
		Private realbasicSyntaxIdx As Integer
	#tag EndProperty


	#tag Constant, Name = PowerScriptDefinition, Type = String, Dynamic = False, Default = \"<\?xml version\x3D\"1.0\" encoding\x3D\"UTF-8\"\?>\r<highlightDefinition version\x3D\"1.3\">\r\t<name>REALbasic</name>\r\r\t<blockStartMarker indent\x3D\"1\">^\\s*(\?:(if|elseif)\\b.*\\sthen\\b\\s*((\'|//)|(\?!.+\?))|if\\s(\?!.*\\sthen\\b)|^\\s*(public|protected|private)\?\\s*(shared)\?\\s*(\?&lt;!end)\\s*(sub|function|property|class|module)|(do|for|while|select|try|catch|else|Get|Set|#if|#elseif|#else)\\b)</blockStartMarker>\r\t<blockEndMarker>^\\s*(\?:end|wend|next|loop|else|catch|elseif|#else|#elseif|#endif)\\b</blockEndMarker>\r\r\t<!-- these prevent subs and functions inside an interface from being indented: -->\r\t<blockStartMarker indent\x3D\"1\" newstate\x3D\"inside_interface\">^\\s*(protected|private)\?\\s*interface\\b</blockStartMarker>\r\t<blockEndMarker condition\x3D\"inside_interface\" newstate\x3D\"\">^\\s*end\\b</blockEndMarker>\r\r\t<lineContinuationMarker indent\x3D\"2\">^(|.*\\W)_\\s*(|//.*|\'.*)$</lineContinuationMarker>\r\r\t<symbols> <!-- these are used to identify declared symbols that are useful for natigation inside larger source file -->\r\t\t<symbol type\x3D\"Method\">\r\t\t\t<entryRegEx>^\\s*(\?:public|protected|private)\?\\s*(shared)\?\\s*(\?&lt;!end)\\s*(\?:sub|function).+\?\\([^\\)]*\\)\\s*(\?:as\\s*\\w+\\s*)\?</entryRegEx>\r\t\t</symbol>\r\t\t<symbol type\x3D\"Property\">\r\t\t\t<entryRegEx>^\\s*(\?:public|protected|private)\?\\s*(shared)\?\\s*(\?&lt;!end)\\s*(property).+</entryRegEx>\r\t\t</symbol>\r\t\t<symbol type\x3D\"Class\">\r\t\t\t<entryRegEx>^\\s*(\?:protected|private)\?\\s*class\\s+\\w+\\s*</entryRegEx>\r\t\t</symbol>\r\t\t<symbol type\x3D\"Module\">\r\t\t\t<entryRegEx>^\\s*(\?:protected|private)\?\\s*module\\s+\\w+\\s*</entryRegEx>\r\t\t</symbol>\t\t\r\t\t<symbol type\x3D\"Interface\">\r\t\t\t<entryRegEx>^\\s*(\?:protected|private)\?\\s*interface\\s+\\w+\\s*</entryRegEx>\r\t\t</symbol>\t\t\r\t</symbols>\r\t\r\t<contexts defaultColor\x3D\"#000000\" caseSensitive\x3D\"no\">\t\t\r        <highlightContext name\x3D\"Doubles\" highlightColor\x3D\"#006532\">\r            <entryRegEx>(\?&lt;\x3D[^\\w\\d]|^)(([0-9]+\\.[0-9]*)|([0-9]{11\x2C}))(\?\x3D[^\\w\\d]|$)</entryRegEx>\r        </highlightContext>    \t\r        \r        <highlightContext name\x3D\"Integers\" highlightColor\x3D\"#326598\">\r            <entryRegEx>(\?&lt;\x3D[^\\w\\d]|^)([0-9]{1\x2C10})(\?\x3D[^\\w\\d]|$)</entryRegEx>\r        </highlightContext>\r\r\t\t<highlightContext name\x3D\"PreProcessor\" highlightColor\x3D\"#0000FF\">\r            <entryRegEx>(#\\w+)</entryRegEx>\r        </highlightContext>        \r\t\t\r\t\t<highlightContext name\x3D\"Comment\" highlightColor\x3D\"#7F0000\" italic\x3D\"true\">\r\t\t\t<startRegEx>\'</startRegEx>\r\t\t\t<endRegEx>[\\n\\r]</endRegEx>\r\t\t</highlightContext>\r\t\t\r\t\t<highlightContext name\x3D\"C-Comment\" highlightColor\x3D\"#7F0000\" italic\x3D\"true\">\r\t\t\t<startRegEx>//</startRegEx>\r\t\t\t<endRegEx>[\\n\\r]</endRegEx>\r\t\t</highlightContext>\r\t\t\r\t\t<highlightContext name\x3D\"REM-Comment\" highlightColor\x3D\"#7F0000\" italic\x3D\"true\">\r\t\t\t<startRegEx>^\\s*rem\\s</startRegEx>\r\t\t\t<endRegEx>[\\n\\r]</endRegEx>\r\t\t</highlightContext>\r\t\t\r\t\t<highlightContext name\x3D\"String\" highlightColor\x3D\"#6500FE\">\r\t\t\t<startRegEx>\"</startRegEx>\r\t\t\t<endRegEx>[^\"\\n\\r]*[\"\\n\\r]</endRegEx>\r\t\t</highlightContext>\r\r        <highlightContext name\x3D\"BasicTypes\" highlightColor\x3D\"#0000FF\" bold\x3D\"true\">\r            <keywords>\r\t\t\t\t<string>Boolean</string>\r\t\t\t\t<string>Color</string>\r\t\t\t\t<string>Double</string>\r\t\t\t\t<string>Integer</string>\r\t\t\t\t<string>Int8</string>\r\t\t\t\t<string>UInt8</string>\r\t\t\t\t<string>Int16</string>\r\t\t\t\t<string>UInt16</string>\r\t\t\t\t<string>Int32</string>\r\t\t\t\t<string>UInt32</string>\r\t\t\t\t<string>Int64</string>\r\t\t\t\t<string>UInt64</string>\r\t\t\t\t<string>Single</string>\r\t\t\t\t<string>String</string>\r\t\t\t\t<string>CFString</string>\r\t\t\t\t<string>Ptr</string>\r            </keywords>\r        </highlightContext>\r\r        <highlightContext name\x3D\"Keywords\" highlightColor\x3D\"#0000FF\" bold\x3D\"true\">\r            <keywords>\r\t\t\t\t<string>#Bad</string>\r\t\t\t\t<string>#Else</string>\r\t\t\t\t<string>#ElseIf</string>\r\t\t\t\t<string>#EndIf</string>\r\t\t\t\t<string>#If</string>\r\t\t\t\t<string>#Pragma</string>\r\t\t\t\t<string>#Tag</string>\r\t\t\t\t<string>AddressOf</string>\r\t\t\t\t<string>And</string>\r\t\t\t\t<string>Array</string>\r\t\t\t\t<string>As</string>\r\t\t\t\t<string>Assigns</string>\r\t\t\t\t<string>ByRef</string>\r\t\t\t\t<string>ByVal</string>\r\t\t\t\t<string>Call</string>\r\t\t\t\t<string>Case</string>\r\t\t\t\t<string>Catch</string>\r\t\t\t\t<string>Class</string>\r\t\t\t\t<string>Const</string>\r\t\t\t\t<string>Continue</string>\r\t\t\t\t<string>Declare</string>\r\t\t\t\t<string>Delegate</string>\r\t\t\t\t<string>Dim</string>\r\t\t\t\t<string>Do</string>\r\t\t\t\t<string>DownTo</string>\r\t\t\t\t<string>Each</string>\r\t\t\t\t<string>Else</string>\r\t\t\t\t<string>ElseIf</string>\r\t\t\t\t<string>End</string>\r\t\t\t\t<string>Enum</string>\r\t\t\t\t<string>Event</string>\r\t\t\t\t<string>Exception</string>\r\t\t\t\t<string>Exit</string>\r\t\t\t\t<string>Extends</string>\r\t\t\t\t<string>False</string>\r\t\t\t\t<string>Finally</string>\r\t\t\t\t<string>For</string>\r\t\t\t\t<string>Function</string>\r\t\t\t\t<string>Get</string>\r\t\t\t\t<string>Global</string>\r\t\t\t\t<string>GoTo</string>\r\t\t\t\t<string>Handles</string>\r\t\t\t\t<string>If</string>\r\t\t\t\t<string>Implements</string>\r\t\t\t\t<string>In</string>\r\t\t\t\t<string>Inherits</string>\r\t\t\t\t<string>Inline68K</string>\r\t\t\t\t<string>Interface</string>\r\t\t\t\t<string>Is</string>\r\t\t\t\t<string>IsA</string>\r\t\t\t\t<string>Lib</string>\r\t\t\t\t<string>Loop</string>\r\t\t\t\t<string>Me</string>\r\t\t\t\t<string>Mod</string>\r\t\t\t\t<string>Module</string>\r\t\t\t\t<string>Namespace</string>\r\t\t\t\t<string>New</string>\r\t\t\t\t<string>Next</string>\r\t\t\t\t<string>Nil</string>\r\t\t\t\t<string>Not</string>\r\t\t\t\t<string>Object</string>\r\t\t\t\t<string>Of</string>\r\t\t\t\t<string>Optional</string>\r\t\t\t\t<string>Or</string>\r\t\t\t\t<string>ParamArray</string>\r\t\t\t\t<string>Private</string>\r\t\t\t\t<string>Property</string>\r\t\t\t\t<string>Protected</string>\r\t\t\t\t<string>Public</string>\r\t\t\t\t<string>Raise</string>\r\t\t\t\t<string>RaiseEvent</string>\r\t\t\t\t<string>Redim</string>\r\t\t\t\t<string>Rem</string>\r\t\t\t\t<string>Return</string>\r\t\t\t\t<string>Select</string>\r\t\t\t\t<string>Self</string>\r\t\t\t\t<string>Set</string>\r\t\t\t\t<string>Shared</string>\r\t\t\t\t<string>Soft</string>\r\t\t\t\t<string>Static</string>\r\t\t\t\t<string>Step</string>\r\t\t\t\t<string>Structure</string>\r\t\t\t\t<string>Sub</string>\r\t\t\t\t<string>Super</string>\r\t\t\t\t<string>Then</string>\r\t\t\t\t<string>To</string>\r\t\t\t\t<string>True</string>\r\t\t\t\t<string>Try</string>\r\t\t\t\t<string>Until</string>\r\t\t\t\t<string>Wend</string>\r\t\t\t\t<string>While</string>\r\t\t\t\t<string>With</string>\r            </keywords>\r        </highlightContext>\r\t</contexts>\r</highlightDefinition>", Scope = Public
	#tag EndConstant


#tag EndWindowCode

#tag Events verticalSB
	#tag Event
		Sub ValueChanged()
		  testField.ScrollPosition = me.Value
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events horizontalSB
	#tag Event
		Sub ValueChanged()
		  testField.ScrollPositionx = me.Value
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events TestField
	#tag Event
		Sub Open()
		  TestField.ClearDirtyLines
		  
		  dim def as new HighlightDefinition
		  call def.LoadFromXml(PowerScriptDefinition)
		  me.SyntaxDefinition = def
		  
		  //set the scrollbars
		  me.setScrollbars(horizontalSB, verticalSB)
		  
		  //add a set of dummy autocomplete words.
		  'dim tmp() as String = autocompleteWords.Split
		  'dim word as String
		  '
		  'MyAutocomplete = new PaTrie
		  'dim Words as new Dictionary
		  '
		  'dim trimmed as String
		  'for each word in tmp
		  'trimmed = word.Trim
		  'if words.HasKey(trimmed) then
		  'Continue
		  'end if
		  'call MyAutocomplete.addKey(trimmed)
		  ''autocompleteWordsList.AddRow(trimmed)
		  'words.Value(trimmed) = true
		  'next
		End Sub
	#tag EndEvent
	#tag Event
		Function AutocompleteOptionsForPrefix(prefix as string) As AutocompleteOptions
		  //you can replace this with your own Autocomplete engine...
		  dim options as new AutocompleteOptions
		  dim commonPrefix as String
		  
		  //prefix is the word that triggered these Autocomplete options
		  options.Prefix = prefix
		  
		  //options is the array that holds the different Autocomplete options for this word (prefix)
		  options.Options = MyAutocomplete.wordsForPrefix(prefix, commonPrefix)
		  
		  //the longest common prefix of the different options
		  options.LongestCommonPrefix = commonPrefix
		  
		  Return options
		End Function
	#tag EndEvent
	#tag Event
		Function ShouldTriggerAutocomplete(Key as string, hasAutocompleteOptions as boolean) As boolean
		  'Return Keyboard.AsyncKeyDown(53) //to use ESC (xcode, mail, coda...)
		  Return key = chr(9) and hasAutocompleteOptions and not Keyboard.OptionKey//to use tab, if there are options
		  'Return Keyboard.AsyncControlKey and Keyboard.AsyncKeyDown(49) //to use ctrl-space as in visual studio
		  'Return false 'no autocomplete, ever
		End Function
	#tag EndEvent
	#tag Event
		Sub SelChanged(line as integer, column as integer, length as integer)
		  dim tmp as String = str(line)+":"+str(column)
		  if length > 0 then tmp = tmp + " ("+str(length)+")"
		  
		  'fieldInfo.TextSelectionInfo = tmp
		  
		  if lastLine <> line and TestField.SymbolCount > 0 then
		    'fieldInfo.currentSymbol = TestField.CaretSymbol
		  end if
		  lastLine = line
		End Sub
	#tag EndEvent
	#tag Event
		Sub HighlightingComplete()
		  'if TestField.SymbolCount = 0 then fieldInfo.currentSymbol = nil
		End Sub
	#tag EndEvent
	#tag Event
		Sub TextChanged()
		  SetWindowModified(me.IsDirty)
		  
		  Block.ParseCode(me.text)
		  
		  //list errors
		  listErrors.DeleteAllRows
		  for each s as string in Block.ErrorList
		    listErrors.AddRow(s)
		  next
		End Sub
	#tag EndEvent
	#tag Event
		Function UseBackgroundColorForLine(lineIndex as integer, byref lineBackgroundColor as color) As boolean
		  //Return false //remove this to have lines with alternate colors as background.
		  
		  
		  //highlight the currentLine.
		  if lineIndex = me.CaretLine then
		    lineBackgroundColor = &cFFFEE4 //light yellow
		    Return true
		  end if
		  
		  Return true //remove this to go back to alternating blue-white lines
		  
		  //alternate backgrounds...
		  if lineIndex mod 2 <> 0 then Return False
		  
		  lineBackgroundColor = RGB(237,243,255) //faint blue
		  Return true
		End Function
	#tag EndEvent
	#tag Event
		Sub PaintBelowLine(lineIndex as integer, g as graphics, x as integer, y as integer, w as integer, h as integer)
		  if lineIndex <> me.CaretLine then Return
		  
		  g.ForeColor = &CCCCCCC
		  g.DrawLine x, y + h - 1, x + w, y + h - 1
		  g.DrawLine x, y , x + w, y
		End Sub
	#tag EndEvent
	#tag Event
		Sub GutterClicked(onLine as integer, x as integer, y as integer)
		  #pragma unused onLine
		  #pragma unused x
		  #pragma unused y
		  if x < 10 then _
		  toggleBookmark
		End Sub
	#tag EndEvent
	#tag Event
		Sub PlaceholderSelected(placeholderLabel as String, lineIndex as integer, line as textLine, placeholder as textPlaceholder, doubleClick as Boolean)
		  #pragma unused placeholderLabel
		  #pragma unused lineIndex
		  #pragma unused line
		  #pragma unused placeholder
		  #pragma unused doubleClick
		  //I guess you could use placeholders as buttons somehow here...
		  'if doubleClick then
		  'me.SelText = placeholderLabel
		  'me.SelStart = line.offset + placeholder.offset
		  'me.SelLength = placeholderLabel.len
		  'end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events listErrors
	#tag Event
		Sub Change()
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
			"11 - Modeless Dialog"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="400"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="600"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
