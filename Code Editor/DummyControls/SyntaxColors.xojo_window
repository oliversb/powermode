#tag Window
Begin Window SyntaxColors
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   8
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   300
   ImplicitInstance=   True
   LiveResize      =   True
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   False
   MaxWidth        =   32000
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   False
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   False
   Title           =   "Untitled"
   Visible         =   True
   Width           =   278
   Begin Listbox definitionColors
      AutoDeactivate  =   True
      AutoHideScrollbars=   True
      Bold            =   False
      Border          =   True
      ColumnCount     =   6
      ColumnsResizable=   False
      ColumnWidths    =   "*,40,40,24,24,24"
      DataField       =   ""
      DataSource      =   ""
      DefaultRowHeight=   24
      Enabled         =   True
      EnableDrag      =   False
      EnableDragReorder=   False
      GridLinesHorizontal=   0
      GridLinesVertical=   0
      HasHeading      =   True
      HeadingIndex    =   -1
      Height          =   248
      HelpTag         =   ""
      Hierarchical    =   True
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "Context Name	Color	Back	B	I	U"
      Italic          =   False
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      RequiresSelection=   False
      Scope           =   "0"
      ScrollbarHorizontal=   False
      ScrollBarVertical=   False
      SelectionType   =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "SmallSystem"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   0
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   278
      _ScrollOffset   =   0
      _ScrollWidth    =   -1
   End
   Begin PushButton btnOk
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Ok"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   145
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      Scope           =   "0"
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "SmallSystem"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   260
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin Timer sizeTimer
      Height          =   "32"
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   0
      LockedInPosition=   False
      Mode            =   0
      Period          =   5
      Scope           =   "0"
      TabPanelIndex   =   "0"
      Top             =   309
      Width           =   "32"
   End
   Begin PushButton btnSave
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Save XML"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   53
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      Scope           =   "0"
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "SmallSystem"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   260
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
End
#tag EndWindow

#tag WindowCode
	#tag Method, Flags = &h0
		Function show(definition as highlightdefinition) As boolean
		  self.definition = definition
		  showDefinition
		  me.Title = definition.Name
		  Super.ShowModal
		  Return result
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub ShowContexts(contexts() as highlightcontext, showPlaceholders as Boolean)
		  dim subContext as HighlightContext
		  
		  for each subContext in Contexts
		    if subContext.isPlaceholder and not showPlaceholders then Continue for
		    
		    dim subContexts() as HighlightContext = subContext.contexts
		    if subContexts.Ubound > - 1 and not subContexts(0).isPlaceholder then
		      definitionColors.AddFolder subContext.Name
		      
		    else
		      definitionColors.AddRow subContext.Name
		      
		    end if
		    
		    definitionColors.CellTag(definitionColors.LastIndex, 0) = subContext
		    definitionColors.CellCheck(definitionColors.LastIndex, 3) = subContext.Bold
		    definitionColors.CellCheck(definitionColors.LastIndex, 4) = subContext.Italic
		    definitionColors.CellCheck(definitionColors.LastIndex, 5) = subContext.Underline
		    definitionColors.Expanded(definitionColors.LastIndex) = true //expand all!
		  next
		  
		  Height = (definitionColors.ListCount + 1) * definitionColors.DefaultRowHeight + 52
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub showDefinition()
		  ShowContexts(definition.Contexts, true)
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h1
		Protected definition As highlightdefinition
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected result As boolean
	#tag EndProperty


#tag EndWindowCode

#tag Events definitionColors
	#tag Event
		Sub ExpandRow(row As Integer)
		  dim selectedContext as HighlightContext
		  
		  selectedContext = me.CellTag(row, 0)
		  ShowContexts(selectedContext.Contexts, false)
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  me.ColumnType(3) = Listbox.TypeCheckbox
		  me.ColumnType(4) = Listbox.TypeCheckbox
		  me.ColumnType(5) = Listbox.TypeCheckbox
		End Sub
	#tag EndEvent
	#tag Event
		Function CellBackgroundPaint(g As Graphics, row As Integer, column As Integer) As Boolean
		  if row >= me.ListCount then Return False
		  
		  dim context as HighlightContext = me.CellTag(row, 0)
		  
		  select case column
		  case 1
		    g.ForeColor = Context.HighlightColor
		    g.FillRect (g.Width - 18)/2, 2, 20, 20
		    g.ForeColor = &c0
		    g.drawRect (g.Width - 18)/2, 2, 20, 20
		    
		    Return true
		  case 2
		    if context.HasBackgroundColor then
		      g.ForeColor = Context.BackgroundColor
		      g.FillRect (g.Width - 18)/2, 2, 20, 20
		    else
		      g.DrawLine (g.Width - 18)/2, 2, (g.Width - 18)/2 + 19, 21
		    end if
		    g.ForeColor = &c0
		    g.drawRect (g.Width - 18)/2, 2, 20, 20
		    
		    Return true
		  end select
		  
		End Function
	#tag EndEvent
	#tag Event
		Sub CellAction(row As Integer, column As Integer)
		  dim context as HighlightContext = me.CellTag(row, 0)
		  
		  if Context = nil then Return
		  select case column
		  case 3 'b
		    Context.Bold = me.CellCheck(row, column)
		  case 4 'i
		    Context.Italic = me.CellCheck(row, column)
		  case 5 'u
		    Context.Underline = me.CellCheck(row, column)
		  end select
		  result = true
		End Sub
	#tag EndEvent
	#tag Event
		Function CellClick(row as Integer, column as Integer, x as Integer, y as Integer) As Boolean
		  #pragma unused x
		  #pragma unused y
		  #pragma unused row
		  
		  dim context as HighlightContext = me.CellTag(row, 0)
		  if context = nil then Return true
		  
		  dim newColor as color
		  select case column
		  case 1 'fore
		    newColor = context.HighlightColor
		    if SelectColor(newColor, context.Name + " highlight color") then
		      context.HighlightColor = newColor
		      result = true
		    end if
		  case 2 'back
		    newColor = context.BackgroundColor
		    if SelectColor(newColor, context.Name + " back color") then
		      context.BackgroundColor = newColor
		      result = true
		    end if
		    
		  end select
		End Function
	#tag EndEvent
	#tag Event
		Sub CollapseRow(row As Integer)
		  #pragma unused row
		  sizeTimer.Mode = timer.ModeSingle
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnOk
	#tag Event
		Sub Action()
		  Close
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events sizeTimer
	#tag Event
		Sub Action()
		  Height = (definitionColors.ListCount + 1) * definitionColors.DefaultRowHeight + 52
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnSave
	#tag Event
		Sub Action()
		  dim file as FolderItem = GetSaveFolderItem("Text", definition.Name+"_sdef.xml")
		  if file = nil then Return
		  
		  if not definition.saveAsXml(file) Then
		    MsgBox "Error saving"
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
			"11 - Modeless Dialog"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="400"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="600"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
