#tag Class
Protected Class ControlCloseTimer
Inherits Timer
	#tag Event
		Sub Action()
		  //close all of the ContainerControls within the list
		  for each c as ContainerControl in ContainerControls
		    c.Close
		  next
		  
		  RaiseEvent AfterClose()
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub CloseAll()
		  me.Mode = Timer.ModeSingle
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CloseContainerControl(control as ContainerControl)
		  me.ContainerControls.Append(control)
		  me.Mode = Timer.ModeSingle
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1000
		Sub Constructor()
		  me.Mode = Timer.ModeOff
		  me.Period = 1
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event AfterClose()
	#tag EndHook


	#tag Property, Flags = &h0
		ContainerControls() As ContainerControl
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			Type="Integer"
			EditorType="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Mode"
			Visible=true
			Group="Behavior"
			InitialValue="2"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Off"
				"1 - Single"
				"2 - Multiple"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Period"
			Visible=true
			Group="Behavior"
			InitialValue="1000"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
