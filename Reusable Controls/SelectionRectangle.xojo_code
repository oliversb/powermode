#tag Class
Protected Class SelectionRectangle
Inherits Rect
	#tag Method, Flags = &h0
		Function Intersects(rect as Rect) As boolean
		  return rect.Intersects(SelectionRectangle)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MouseDown(x as integer, y as integer)
		  SelectionRectangle = new Rect
		  SelectionRectangle.Width = 0
		  SelectionRectangle.Height = 0
		  SelectionPoint = new Point(X, Y)
		  SelectionRectangle.Left = X
		  SelectionRectangle.Top = Y
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MouseDrag(x as integer, y as integer)
		  //multiselection box
		  if SelectionRectangle <> nil then
		    for i as integer = 0 to 1
		      SelectionRectangle.Top = Min(SelectionPoint.Y, Y)
		      SelectionRectangle.Bottom = Max(SelectionPoint.Y, Y)
		      
		      SelectionRectangle.Left = Min(SelectionPoint.X, X)
		      SelectionRectangle.Right = Max(SelectionPoint.X, X)
		    next
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MouseUp()
		  SelectionRectangle = nil
		  SelectionPoint = nil
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Paint(g as graphics)
		  if SelectionRectangle <> nil then
		    g = g.Clip(SelectionRectangle.Left, SelectionRectangle.Top, SelectionRectangle.Width, SelectionRectangle.Height)
		    g.ForeColor = &c7B95E9
		    g.Transparency = 50
		    g.FillRect( 1,  1, SelectionRectangle.Width - 1, SelectionRectangle.Height - 1)
		    g.ForeColor = &c2249c9
		    g.Transparency = 0
		    g.DrawRect(0, 0, SelectionRectangle.Width, SelectionRectangle.Height)
		  end if
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private SelectionPoint As Point
	#tag EndProperty

	#tag Property, Flags = &h0
		SelectionRectangle As Rect
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Bottom"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Opacity"
			Group="Behavior"
			Type="double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Right"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
