#tag Class
Protected Class ResizableObject
Inherits Pixmap
	#tag Method, Flags = &h1000
		Sub Constructor()
		  Super.Constructor
		  Grouper = new Group
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1000
		Sub Constructor(p as LiveResizeCanvas)
		  //only called if parent is passed in as a parameter
		  
		  Constructor
		  
		  me.CanvasParent = p
		  
		  me.width = p.width
		  me.height = p.height
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1000
		Function DraggingBigRect(x as integer, y as integer) As Boolean
		  return me.Contains(x, y)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1000
		Function MouseDown(X As Integer, Y As Integer, IgnoreBigRect As Boolean = false) As Boolean
		  if me.CanvasParent <> nil then
		    me.CanvasParent.WinLastWidth = me.CanvasParent.window.width
		    me.CanvasParent.WinLastHeight = me.CanvasParent.window.height
		  end if
		  
		  Pos = 0
		  for each ms as MouseRect in MouseRects
		    if ms.Contains(X, Y) then
		      Pos = ms.Pos + 1
		    end if
		  next
		  
		  if Pos > 0 or (DraggingBigRect(X, Y) or IgnoreBigRect) then
		    MouseRect.myLastX = X
		    MouseRect.myLastY = Y
		    
		    SetDimensions
		    return true
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1000
		Sub MouseDrag(x as integer, y as integer)
		  dim vert, horiz as Boolean
		  'add one to Pos because Pos 0 means N/A
		  select case Pos
		  case 1
		    vert = true
		  case 2
		    horiz = true
		  case else
		    vert = true
		    horiz = true
		  end select
		  
		  if horiz then
		    dim PendingWidth as UInt32 = x - MouseRect.myLastX + LiveResizeLastWidth
		    if PendingWidth > MouseRect.Size then
		      me.Width = PendingWidth
		    else
		      me.Width = MouseRect.Size + 1 //one pixel drawing canvas
		    end if
		  end if
		  
		  if vert then
		    dim PendingHeight as UInt32 = y - MouseRect.myLastY + LiveResizeLastHeight
		    if PendingHeight > MouseRect.Size then
		      me.Height = PendingHeight
		    else
		      me.Height = MouseRect.Size + 1 //one pixel drawing canvas
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MouseExit()
		  App.MouseCursor = nil
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MouseMove(X As Integer, Y As Integer)
		  if DraggingBigRect(x,y) then
		    App.MouseCursor = System.Cursors.ArrowNorthwestSoutheast
		    return
		  else
		    for each ms as MouseRect in MouseRects
		      if ms.Contains(X,Y) then
		        select case ms.pos
		        case 0
		          App.MouseCursor = System.Cursors.ArrowNorthSouth
		          return
		        case 1
		          App.MouseCursor = System.Cursors.ArrowEastWest
		          return
		        case 2
		          App.MouseCursor = System.Cursors.ArrowNorthwestSoutheast
		          return
		        end select
		      end if
		    next
		  end if
		  
		  App.MouseCursor = nil
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1000
		 Shared Sub MouseUp()
		  MouseRect.myLastX = 0
		  MouseRect.myLastY = 0
		  
		  App.MouseCursor = nil
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1000
		Sub Paint(g As Graphics)
		  #pragma StackOverflowChecking false
		  #pragma NilObjectChecking false
		  #pragma BackgroundTasks false
		  
		  '#pragma BreakOnExceptions false
		  
		  if me.Parent <> nil then
		    try
		      g.DrawPicture PWindow.DrawCheckerboard(g.width - MouseRect.Size,g.height - MouseRect.Size), me.Left, me.Top
		    catch OutOfBoundsException
		    end try
		  end if
		  
		  if me.Parent <> nil then
		    //
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetDimensions()
		  LiveResizeLastWidth = me.width
		  LiveResizeLastHeight = me.height
		End Sub
	#tag EndMethod


	#tag Note, Name = Untitled
		This is the pixmapshape for the image.
		
		The Grouper property contains the group that contains the resize handles and this object.
		
	#tag EndNote


	#tag Property, Flags = &h0
		CanvasParent As LiveResizeCanvas
	#tag EndProperty

	#tag Property, Flags = &h0
		Grouper As Group
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return Grouper.Left
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  Grouper.Left = value
			End Set
		#tag EndSetter
		Left As Integer
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		LiveResizeLastHeight As UInt32
	#tag EndProperty

	#tag Property, Flags = &h0
		LiveResizeLastWidth As UInt32
	#tag EndProperty

	#tag Property, Flags = &h0
		MouseRects() As MouseRect
	#tag EndProperty

	#tag Property, Flags = &h0
		Pos As UInt32
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return Grouper.Top '+ (me.Height / 2) - (MouseRect.Size / 2)
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  Grouper.Top = value
			End Set
		#tag EndSetter
		Top As Integer
	#tag EndComputedProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Height"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Image"
			Group="Behavior"
			InitialValue="0"
			Type="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Opacity"
			Group="Behavior"
			Type="double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
