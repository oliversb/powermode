#tag Class
Protected Class LVToolBar
Inherits Canvas
	#tag Event
		Function ConstructContextualMenu(base as MenuItem, x as Integer, y as Integer) As Boolean
		  If EnableContextualMenu Then
		    Dim mnuBigLabel As New MenuItem
		    Dim mnuSmallLabel As New MenuItem
		    Dim mnuBig As New MenuItem
		    Dim mnuSmall As New MenuItem
		    Dim mnuLabel As New MenuItem
		    Dim mnuSpacer As New MenuItem
		    Dim mnuCustomize As New MenuItem
		    
		    mnuBigLabel.Text = TEXT_BIG_ICONS_WITH_LABELS
		    mnuBigLabel.Tag = "BigLabel"
		    mnuBigLabel.Checked = False
		    
		    mnuSmallLabel.Text = TEXT_SMALL_ICONS_WITH_LABELS
		    mnuSmallLabel.Tag = "SmallLabel"
		    mnuSmallLabel.Checked = False
		    
		    mnuBig.Text = TEXT_BIG_ICONS
		    mnuBig.Tag = "Big"
		    mnuBig.Checked = False
		    
		    mnuSmall.Text = TEXT_SMALL_ICONS
		    mnuSmall.Tag = "Small"
		    mnuSmall.Checked = False
		    
		    mnuLabel.Text = TEXT_LABELS_ONLY
		    mnuLabel.Tag = "Label"
		    mnuLabel.Checked = False
		    
		    mnuSpacer.Text = "-"
		    
		    mnuCustomize.Text = TEXT_CUSTOMIZE
		    mnuCustomize.Tag = "Customize"
		    
		    Select Case ToolbarStyle
		    Case STYLE_BIG_ICONS_LABEL
		      mnuBigLabel.Checked = True
		    Case STYLE_SMALL_ICONS_LABEL
		      mnuSmallLabel.Checked = True
		    Case STYLE_BIG_ICONS
		      mnuBig.Checked = True
		    Case STYLE_SMALL_ICONS
		      mnuSmall.Checked = True
		    Case STYLE_LABELS_ONLY
		      mnuLabel.Checked = True
		    End Select
		    
		    'base.Append(mnuBigLabel)
		    'base.Append(mnuSmallLabel)
		    'base.Append(mnuBig)
		    'base.Append(mnuSmall)
		    'base.Append(mnuLabel)
		    'base.Append(mnuSpacer)
		    base.Append(mnuCustomize)
		  End If
		  
		  Return ConstructContextualMenu(base, x, y)
		End Function
	#tag EndEvent

	#tag Event
		Function ContextualMenuAction(hitItem as MenuItem) As Boolean
		  Dim changed As Boolean = False
		  
		  Select Case hitItem.Tag
		  Case "BigLabel"
		    If ToolBarStyle <> STYLE_BIG_ICONS_LABEL Then
		      ToolBarStyle = STYLE_BIG_ICONS_LABEL
		      changed = True
		    End If
		  Case "SmallLabel"
		    If ToolBarStyle <> STYLE_SMALL_ICONS_LABEL Then
		      ToolBarStyle = STYLE_SMALL_ICONS_LABEL
		      changed = True
		    End If
		  Case "Big"
		    If ToolBarStyle <> STYLE_BIG_ICONS Then
		      ToolBarStyle = STYLE_BIG_ICONS
		      changed = True
		    End If
		  Case "Small"
		    If ToolBarStyle <> STYLE_SMALL_ICONS Then
		      ToolBarStyle = STYLE_SMALL_ICONS
		      changed = True
		    End If
		  Case "Label"
		    If ToolBarStyle <> STYLE_LABELS_ONLY Then
		      ToolBarStyle = STYLE_LABELS_ONLY
		      changed = True
		    End If
		  Case "Customize"
		    Customize
		  End Select
		  
		  Call ContextualMenuAction(hitItem)
		  
		  If changed Then
		    Invalidate
		    ToolBarChanged
		  End If
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function MouseDown(X As Integer, Y As Integer) As Boolean
		  Dim rval As Boolean = True
		  
		  If IsContextualClick Then
		    ' Don't process this event for right-clicks
		    Return MouseDown(x, y)
		  End If
		  
		  Dim btnPushed As Integer
		  
		  btnPushed = GetButtonUnderMouse(x, y)
		  
		  If btnPushed > 0 Then
		    If mButtons(btnPushed-1) IsA LVToolBarItem Then
		      Dim myButton As LVToolBarItem
		      myButton = LVToolBarItem(mButtons(btnPushed-1))
		      
		      If myButton.Enabled Then
		        
		        myButton.Selected = True
		        
		        'If the toolbaritem is a menu, then display the menu now
		        If myButton.BaseMenu <> nil And LastSelectedButton <> btnPushed Then
		          'LastSelectedButton = btnPushed
		          
		          Invalidate(false)
		          
		          Dim selectedMenu As MenuItem
		          selectedMenu = myButton.ShowMenu
		          LastSelectedButton = -1
		          
		          If Not (selectedMenu Is Nil) Then
		            ActionMenu(selectedMenu)
		          End If
		          
		          rval = False
		        End If
		        
		        If mybutton.baseMenu = nil Then LastSelectedButton = btnPushed
		        
		      End If
		      
		    End If
		  End If
		  
		  Invalidate(false)
		  
		  Call MouseDown(x, y)
		  
		  Return rval
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub MouseDrag(X As Integer, Y As Integer)
		  Dim btnPushed As Integer = GetButtonUnderMouse(x, y)
		  
		  If LastSelectedButton > 0 Then
		    Dim thisButton As LVIToolBarItem = mButtons(LastSelectedButton-1)
		    
		    If thisButton IsA LVToolBarItem Then
		      If btnPushed <> LastSelectedButton Then
		        LVToolBarItem(thisButton).Selected = False
		        LVToolBarItem(thisButton).Hover = False
		      Else
		        LVToolBarItem(thisButton).Selected = True
		      End If
		    End If
		    
		    Invalidate
		  End If
		  
		  MouseDrag(x, y)
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub MouseExit()
		  For i As Integer = 1 to ButtonCount
		    If mButtons(i-1) IsA LVToolBarItem Then
		      Dim thisButton As LVToolBarItem = LVToolBarItem(mButtons(i-1))
		      thisButton.Selected = False
		      thisButton.Hover = False
		    End If
		  Next
		  
		  StatusBarText = ""
		  
		  LastHoverButton = -1
		  
		  Invalidate
		  
		  MouseExit
		End Sub
	#tag EndEvent

	#tag Event
		Sub MouseMove(X As Integer, Y As Integer)
		  Dim btnPushed as Integer
		  
		  btnPushed = GetButtonUnderMouse(x, y)
		  
		  If btnPushed > 0 Then
		    If mButtons(btnPushed-1) IsA LVToolBarItem Then
		      Dim thisButton As LVToolBarItem = LVToolBarItem(mButtons(btnPushed-1))
		      
		      If thisButton.Enabled Then
		        If btnPushed <> LastHoverButton Then
		          'mButtons(btnPushed-1).Selected = False
		          thisButton.Hover = True
		        End If
		      End If
		      
		      Dim statusText As String = thisButton.Caption
		      If thisButton.HelpTag <> "" Then
		        statusText = statusText + ": " +  thisButton.HelpTag
		      End If
		      StatusBarText = statusText
		      
		    End If
		  Else
		    StatusBarText = ""
		  End If
		  
		  If LastHoverButton <> btnPushed Then
		    LastHoverButton = btnPushed
		    
		    For i As Integer = 1 to ButtonCount
		      If i <> btnPushed Then
		        If mButtons(i-1) IsA LVToolBarItem Then
		          Dim thisButton As LVToolBarItem = LVToolBarItem(mButtons(i-1))
		          
		          thisButton.Selected = False
		          thisButton.Hover = False
		        End IF
		      End If
		    Next
		    
		    Invalidate
		  End If
		  
		  MouseMove(x, y)
		  
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub MouseUp(X As Integer, Y As Integer)
		  If LastSelectedButton <> - 1 Then
		    If mButtons(LastSelectedButton-1) IsA LVToolBarItem Then
		      Dim thisButton As LVToolBarItem = LVToolBarItem(mButtons(LastSelectedButton-1))
		      
		      If thisButton.Enabled Then
		        
		        Dim btnPushed As Integer = GetButtonUnderMouse(x, y)
		        
		        thisButton.Selected = False
		        'thisButton.Hover = False
		        thisButton.Hover = true
		        If btnPushed = LastSelectedButton Then
		          Invalidate
		          Action(thisButton)
		        End If
		      End If
		    End If
		    LastSelectedButton = -1
		  End If
		  
		  MouseUp(x, y)
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Initialize
		  
		  Open
		  
		  ToolBarChanged
		  
		  Me.DoubleBuffer = true
		  Me.EraseBackground = false
		  
		  if BackColourDefault then
		    BackColor = REALbasic.FillColor
		  end if
		End Sub
	#tag EndEvent

	#tag Event
		Sub Paint(g As Graphics, areas() As REALbasic.Rect)
		  #pragma unused areas
		  
		  If Not RefreshLock Then
		    If mOldWidth <> Me.Width Then mDoubleBuffer = Nil
		    
		    Draw(g)
		    
		    Paint(g)
		    
		    mOldWidth = Me.Width
		  End If
		  
		  Return
		  
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub AddButton(myToolBarItem As LVIToolBarItem)
		  If myToolBarItem Is Nil Then
		    Dim myException As New NilObjectException
		    myException.Message = "AddButton called with a ToolBarItem that has not been instantiated."
		    Raise myException
		  End If
		  
		  If mLockRight And myToolBarItem.LockRight Then
		    Dim myException As New OutOfBoundsException
		    
		    Dim myCaption As String
		    
		    If myToolBarItem IsA LVToolBarItem Then
		      myCaption = "'" + LVToolBarItem(myToolBarItem).Caption + "'"
		    End If
		    
		    myException.Message = "ToolBarItem " + myCaption + " cannot have LockRight = True because " + _
		    "another ToolBaritem already has LockRight = True."
		    Raise myException
		  End If
		  
		  If myToolBarItem.LockRight Then mLockRight = True
		  
		  myToolBarItem.ToolBarParent = Me
		  
		  If myToolBarItem IsA LVToolBarItem Then
		    Dim thisButton As LVToolBarItem = LVToolBarItem(myToolBarItem)
		    
		    If thisButton.Image Is Nil Then
		      thisButton.Image = MissingIcon
		    End If
		  End If
		  
		  mButtons.Append(myToolBarItem)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Button(buttonNum As Integer) As LVIToolBarItem
		  Return mButtons(buttonNum)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Button(buttonNum As Integer, Assigns button AS LVIToolBarItem)
		  If buttonNum > ButtonCount-1 Then
		    Dim myException As New OutOfBoundsException
		    myException.Message = "The specified buttonNum (" + Str(buttonNum) + ") is not an available button.  " + _
		    "Use AddButton() if you want to add a new button to the toolbar."
		    Raise myException
		  End If
		  
		  ' Remove this button from mButtons
		  For i As Integer = 0 To ButtonCount-1
		    If mButtons(i) Is button Then
		      mButtons.Remove(i)
		      Exit For
		    End If
		  Next
		  
		  ' Insert this button at the specified position
		  mButtons.Insert(buttonNum, button)
		  
		  ' Refresh the toolbar
		  Redraw
		  
		  ToolBarChanged
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ButtonCount() As Integer
		  Return UBound(mButtons)+1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Customize()
		  Dim tbCustomize As New LVCustomize
		  
		  tbCustomize.ToolBar = mButtons
		  tbCustomize.ToolBarStyle = ToolbarStyle
		  
		  tbCustomize.ShowModalWithin(Me.TrueWindow)
		  
		  If tbCustomize.OK Then
		    ToolbarStyle = tbCustomize.ToolBarStyle
		    
		    mButtons = tbCustomize.mCurrent
		    
		    tbCustomize.Close
		    
		    Invalidate
		    
		    ToolBarChanged
		    
		    Return
		  Else
		    tbCustomize.Close
		    
		    Return
		  End If
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub Draw(g As Graphics)
		  If g Is Nil Then Return
		  
		  RefreshLock = True
		  
		  If mDoubleBuffer = Nil Then
		    mDoubleBuffer = New Picture(Width, 75, 32)
		  End If
		  
		  ' Draw the toolbar to the buffer
		  DrawBackground(mDoubleBuffer.Graphics)
		  DrawButtons(mDoubleBuffer.Graphics)
		  
		  ' We don't know the height of the buttons until after they are drawn.
		  ' So we need to create a new buffer of the correct height and use that instead
		  Dim realHeight As Integer = ButtonHeight+1
		  Dim bufferPicture As Picture
		  
		  bufferPicture = New Picture(Width, realHeight, 32)
		  Me.Height = realHeight
		  
		  bufferPicture.Graphics.DrawPicture(mDoubleBuffer, 0, 0)
		  bufferPicture.Graphics.ForeColor = DarkBevelColor
		  bufferPicture.Graphics.DrawLine(0, realHeight-1, g.Width, realHeight-1)
		  
		  g.DrawPicture(bufferPicture, 0, 0)
		  
		  If mToolBarStyleChanged Then
		    ResizeWindow
		    ToolBarChanged
		  Else
		    #If TargetCarbon Then
		      ToolBarChanged
		    #Endif
		  End If
		  
		  #If TargetWin32 Then
		    // Refresh locked controls that have just been hidden by the toolbar
		    For i As Integer = 0 To UBound(mLockControls)
		      // The act of refreshing a control on the toolbar is causing the toolbar to
		      // want to refresh itself, which causes an infinite loop here.
		      // Need to stop refreshes
		      mLockControls(i).Refresh
		    Next
		    
		    RefreshLock = False
		    
		  #Endif
		  
		  #If TargetCarbon Then
		    If UBound(mLockControls) < 0 Then RefreshLock = False
		    
		    If Me.Window.Composite Then
		      RefreshLock = False
		    End If
		  #Endif
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DrawBackground(g As Graphics)
		  #If TargetCarbon
		    If AquaBackground Then
		      Dim lite As Color
		      Dim white As Color
		      
		      lite = RGB(247, 247, 247)
		      'lite = RGB(100,100,100) ' Higher contrast to make it easier to see the lines for unit testing
		      white = RGB(250, 250, 250)
		      
		      For z As Integer = 0 To g.Height Step 4
		        ' Pixel fails on out of bounds exception for some values of z
		        g.ForeColor = lite
		        g.DrawLine(0, z, g.Width, z)
		        If z < g.Height Then g.Pixel(0, z) = lite
		        
		        g.ForeColor = lite
		        g.DrawLine(0, z+1, g.Width, z+1)
		        If z+1 < g.Height Then g.Pixel(0, z+1) = lite
		        
		        g.ForeColor = white
		        g.DrawLine(0, z+2, g.Width, z+2)
		        If z+2 < g.Height Then g.Pixel(0, z+2) = white
		        
		        g.ForeColor = white
		        g.DrawLine(0, z+3, g.Width, z+3)
		        If z+3 < g.Height Then g.Pixel(0, z+3) = white
		      Next
		    Else
		      g.ForeColor = RGB(245, 245, 245)
		      g.FillRect(0, 0, g.Width, g.Height)
		    End If
		  #ElseIf TargetWin32 Or TargetLinux
		    g.ForeColor = BackColor
		    g.FillRect(0, 0, g.Width, g.Height)
		  #Endif
		  
		  
		  
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub DrawButtons(g As Graphics)
		  If ButtonCount > 0 Then
		    Dim leftTotal As Integer
		    Dim skipPos As Integer = -1
		    Dim button As Picture
		    
		    For i As Integer = 0 To ButtonCount-1
		      button = mButtons(i).DrawButton
		      
		      If i > 0 Then
		        If i-1 <> skipPos Then leftTotal = leftTotal + mButtons(i-1).Width
		      End If
		      
		      If Not mButtons(i).LockRight Then
		        mButtons(i).Left = leftTotal
		        mButtons(i).Right = leftTotal + mButtons(i).Width
		      Else
		        skipPos = i
		        
		        mButtons(i).Left = Me.Width - mButtons(i).Width
		        mButtons(i).Right = Me.Width
		      End If
		      
		      g.DrawPicture(button, mButtons(i).Left, 0)
		    Next
		    
		    #If Not TargetCarbon
		      If Not (g Is Nil) Then
		        'g.ForeColor = RGB(160, 160, 160)
		        g.ForeColor = DarkBevelColor
		        g.DrawLine(0, g.Height-1, g.Width, g.Height-1)
		      End If
		    #Endif
		    
		  End If
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function GetButtonUnderMouse(x As Integer, y As Integer) As Integer
		  If y >= 0 And y <= Me.Height Then
		    For i As Integer = 1 to ButtonCount
		      If mButtons(i-1).Left < x And mButtons(i-1).Right > x _
		      And mButtons(i-1).Visible Then Return i
		    Next
		  End If
		  
		  Return -1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub Hide()
		  Me.Visible = False
		  mHideHeight = Me.Height
		  Me.Height = 0
		  
		  ResizeWindow
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub Initialize()
		  Me.Top = 0
		  Me.Left = 0
		  Me.Width = Me.Window.Width
		  
		  Me.LockTop = True
		  Me.LockLeft = True
		  Me.LockRight = True
		  
		  Me.LastHoverButton = -1
		  Me.LastSelectedButton = -1
		  
		  #If TargetCarbon
		    MoveTimer = New LVMoveControls
		    MoveTimer.Enabled = False
		  #Endif
		  
		  RefreshLock = False
		  
		  
		  RegisterToolBarButtonCarbonEvent
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub LockControl(myControl As RectControl)
		  mLockControls.Append(myControl)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function MissingIcon() As Picture
		  ' Draw a red X for a missing icon
		  Dim icon As Picture
		  
		  icon = New Picture(32, 32, 32)
		  icon.Graphics.ForeColor = &cff0000
		  icon.Graphics.PenWidth = 5
		  icon.Graphics.DrawLine(0, 0, 27, 27)
		  icon.Graphics.DrawLine(0, 27, 27, 0)
		  
		  Return icon
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function MoveControl(item As RectControl) As Boolean
		  If UBound(mLockControls) < 0 Then
		    'If item Is mStatusBar Or Not (item.Parent Is Nil) Then
		    If item Is mStatusBar Or item.Parent Isa PagePanel Then
		      Return False
		    End If
		  Else
		    For i As Integer = 0 To UBound(mLockControls)
		      If Item Is mLockControls(i) And Not Me.Visible Then
		        Item.Visible = False
		      ElseIf Item is mLockControls(i) And Me.Visible Then
		        Item.Visible = True
		      End If
		      
		      'If item Is mLockControls(i) Or item Is mStatusBar Or Not (item.Parent Is Nil) Then
		      If item Is mLockControls(i) Or item Is mStatusBar Then
		        Return False
		      End If
		    Next
		  End If
		  
		  Return True
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MoveControls(adjust As Integer)
		  Dim item As RectControl
		  Dim newTop As Integer
		  
		  ' Move Top
		  ' Set all LockBottom back to original value
		  For ctr As Integer = 0 To Me.Window.ControlCount-1
		    If Me.Window.Control(ctr) IsA RectControl Then
		      item = RectControl(Me.Window.Control(ctr))
		      
		      If MoveControl(item) And Not (item Is Me) Then
		        newTop = item.Top + adjust
		        item.Top = newTop
		        item.LockBottom = thisControl(ctr)
		      Else
		        item.Invalidate
		      End If
		    End If
		  Next
		  
		  
		  //invalidate after small interval
		  OneStepTimer = new timer
		  OneStepTimer.Period = 1
		  OneStepTimer.Mode = Timer.ModeMultiple
		  AddHandler OneStepTimer.Action, AddressOf OneStepRedraw
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub OneStepRedraw(sender as timer)
		  sender.Enabled = false
		  OneStepTimer = nil
		  Redraw
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Redraw()
		  mDoubleBuffer = Nil
		  
		  Invalidate
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub RegisterToolBarButtonCarbonEvent()
		  #If TargetCarbon
		    ' This function registers that we want to receive a CarbonEvent when the user clicks on the ToolBarButton in the 
		    ' titlebar.
		    
		    Declare Function InstallEventHandler Lib Carbon (eventTarget as integer, handler as Ptr, numTypes as integer, types as ptr, userdata as integer, something as integer) as Integer
		    Declare Function GetApplicationEventTarget Lib Carbon () as Integer
		    Declare Function NewEventHandlerUPP Lib Carbon (address as Ptr) as Ptr
		    Declare Function GetWindowEventTarget Lib Carbon (inWindow as WindowPtr) as Integer
		    
		    Dim err, eventRef As Integer
		    Dim eventType As MemoryBlock
		    //Dim attributes As Integer
		    
		    Const kEventClassWindow = "wind"
		    Const kEventWindowToolbarSwitchMode = 150
		    
		    #Pragma StackOverflowChecking False
		    
		    // Event Types List
		    eventType = NewMemoryBlock(8)
		    eventType.StringValue(0, 4) = kEventClassWindow
		    eventType.Long(4) = kEventWindowToolbarSwitchMode
		    
		    Dim winHash As Variant
		    
		    winHash = Me.Window
		    'MsgBox str(winhash.hash)
		    // Install Event Handler
		    Dim eventTarget As Integer
		    
		    eventTarget = GetWindowEventTarget(Me.Window)
		    
		    err = InstallEventhandler(eventTarget, NewEventHandlerUPP(AddressOf ToolBarButtonClicked), 1, eventType, winHash.Hash, 0)
		    If err = 0 Then
		      ' We've successfully registered for the event, so
		      ' create the ToolBarButton in the titlebar
		      Dim OSError As Integer
		      Declare Function ChangeWindowAttributes Lib Carbon (window as WindowPtr, setAttributes as Integer, clearAttributes as Integer) as Integer
		      
		      OSError = ChangeWindowAttributes(Me.Window, 64, 0)
		    Else
		      #If DebugBuild Then
		        MsgBox "RegisterToolBarButtonCarbonEvent()" + EndOfLine + EndOfLine + str(err)
		      #Endif
		    End If
		    
		    ' Looks like we need to register something additional in order to get clicks on this button
		    ' when modifier keys are pressed.
		    
		  #Endif
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveButton(buttonNum As Integer)
		  // Remove the button from the array, making it not available to display in the toolbar
		  // or be added with the customization window.
		  mButtons.Remove(buttonNum)
		  
		  Redraw
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ResizeWindow()
		  Dim myHeight As Integer
		  Dim adjust As Integer
		  
		  mToolBarStyleChanged = False
		  
		  myHeight = Me.Height
		  
		  If mOldHeight = Me.Height Then Return
		  
		  adjust = myHeight - mOldHeight
		  
		  ReDim thisControl(Me.Window.ControlCount-1)
		  
		  Dim item As RectControl
		  
		  For ctr As Integer = 0 To Me.Window.ControlCount-1
		    If Me.Window.Control(ctr) IsA RectControl Then
		      item = RectControl(Me.Window.Control(ctr))
		      
		      If MoveControl(item) And Not (item Is Me) Then
		        thisControl(ctr) = item.LockBottom
		        item.LockBottom = False
		      End If
		    End If
		  Next
		  
		  Me.Window.Height = Me.Window.Height + adjust
		  mOldHeight = myHeight
		  
		  #If TargetCarbon
		    ' Start timer to move controls
		    moveTimer.ToolBar = Me
		    moveTimer.Adjust = adjust
		    moveTimer.Mode = 1
		    moveTimer.Period = 100
		    moveTimer.Enabled = True
		  #Else
		    MoveControls(adjust)
		  #Endif
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub Show()
		  Me.Height = mHideHeight
		  Me.Visible = True
		  Me.Refresh(False)
		  
		  #If TargetWin32
		    Me.Refresh(False)
		  #Endif
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ShowHide()
		  If Me.Visible Then
		    Hide
		  Else
		    Show
		  End If
		  
		  ToolBarChanged
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Version() As String
		  Return App.ShortVersion
		End Function
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Action(selectedToolBarItem As LVToolBarItem)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ActionMenu(selectedMenuItem As MenuItem)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ConstructContextualMenu(base As MenuItem, x As Integer, y As Integer) As Boolean
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ContextualMenuAction(hitItem As MenuItem) As Boolean
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MouseDown(x As Integer, y As Integer) As Boolean
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MouseDrag(x As Integer, y As Integer)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MouseExit()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MouseMove(x As Integer, y As Integer)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MouseUp(x As Integer, y As Integer)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Paint(g As Graphics)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ToolBarChanged()
	#tag EndHook


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mAquaBackground
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  #pragma unused value
			  
			  #If TargetCarbon Then
			    mAquaBackground = value
			    Redraw
			    ToolBarChanged
			  #Else
			    mAquaBackground = False
			  #EndIf
			End Set
		#tag EndSetter
		AquaBackground As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		Shared BackColor As Color
	#tag EndProperty

	#tag Property, Flags = &h0
		Shared BackColourDefault As boolean = true
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  If ButtonCount > 0 Then
			    Return mButtons(0).Height
			  End If
			End Get
		#tag EndGetter
		Private ButtonHeight As Integer
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		EnableContextualMenu As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h0
		FontColourDefault As Boolean
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mHideHeight
			End Get
		#tag EndGetter
		HideHeight As Integer
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private LastHoverButton As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private LastSelectedButton As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mAquaBackground As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mButtons() As LVIToolbarItem
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDoubleBuffer As Picture
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mHideHeight As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		mLockControls() As RectControl
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLockRight As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mOldHeight As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mOldWidth As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private MoveTimer As LVMoveControls
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStatusBar As LVIStatusBar
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mToolBarStyle As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		mToolBarStyleChanged As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private OneStepTimer As Timer
	#tag EndProperty

	#tag Property, Flags = &h0
		RefreshLock As Boolean
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Setter
			Set
			  mStatusBar = value
			  
			End Set
		#tag EndSetter
		StatusBar As LVIStatusBar
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h21
		#tag Setter
			Set
			  If Not (mStatusBar Is Nil) Then
			    mStatusBar.Text = value
			  End If
			  
			End Set
		#tag EndSetter
		Private StatusBarText As String
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private thisControl() As Boolean
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  If mToolBarStyle <= 0 Then mToolBarStyle = 1
			  
			  Return mToolBarStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  If value = 0 Then value = 1
			  
			  If value >= 1 And value <= 5 Then
			    If mToolBarStyle <> value Then
			      mToolBarStyle = value
			      mToolBarStyleChanged = True
			    End If
			  Else
			    Dim myException As New OutOfBoundsException
			    
			    myException.Message = "ToolBarStyle " + Str(value) + " outside of allowable range."
			    Raise myException
			  End If
			End Set
		#tag EndSetter
		ToolBarStyle As Integer
	#tag EndComputedProperty


	#tag Constant, Name = Carbon, Type = String, Dynamic = False, Default = \"Carbon", Scope = Private
		#Tag Instance, Platform = Mac Carbon PEF, Language = Default, Definition  = \"CarbonLib"
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Carbon"
	#tag EndConstant

	#tag Constant, Name = STYLE_BIG_ICONS, Type = Double, Dynamic = False, Default = \"3", Scope = Public
	#tag EndConstant

	#tag Constant, Name = STYLE_BIG_ICONS_LABEL, Type = Double, Dynamic = False, Default = \"1", Scope = Public
	#tag EndConstant

	#tag Constant, Name = STYLE_LABELS_ONLY, Type = Double, Dynamic = False, Default = \"5", Scope = Public
	#tag EndConstant

	#tag Constant, Name = STYLE_SMALL_ICONS, Type = Double, Dynamic = False, Default = \"4", Scope = Public
	#tag EndConstant

	#tag Constant, Name = STYLE_SMALL_ICONS_LABEL, Type = Double, Dynamic = False, Default = \"2", Scope = Public
	#tag EndConstant

	#tag Constant, Name = TEXT_BIG_ICONS, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Big Icons"
		#Tag Instance, Platform = Any, Language = ja, Definition  = \"\xE5\xA4\xA7\xE3\x81\x8D\xE3\x81\xAA\xE3\x82\xA2\xE3\x82\xA4\xE3\x82\xB3\xE3\x83\xB3\xE3\x81\xAE\xE3\x81\xBF"
	#tag EndConstant

	#tag Constant, Name = TEXT_BIG_ICONS_WITH_LABELS, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Big Icons with Labels"
		#Tag Instance, Platform = Any, Language = ja, Definition  = \"\xE5\xA4\xA7\xE3\x81\x8D\xE3\x81\xAA\xE3\x82\xA2\xE3\x82\xA4\xE3\x82\xB3\xE3\x83\xB3\xE3\x81\xA8\xE3\x83\x86\xE3\x82\xAD\xE3\x82\xB9\xE3\x83\x88"
	#tag EndConstant

	#tag Constant, Name = TEXT_CUSTOMIZE, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Customize..."
		#Tag Instance, Platform = Any, Language = ja, Definition  = \"\xE3\x82\xAB\xE3\x82\xB9\xE3\x82\xBF\xE3\x83\x9E\xE3\x82\xA4\xE3\x82\xBA..."
	#tag EndConstant

	#tag Constant, Name = TEXT_LABELS_ONLY, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Labels Only"
		#Tag Instance, Platform = Any, Language = ja, Definition  = \"\xE3\x83\x86\xE3\x82\xAD\xE3\x82\xB9\xE3\x83\x88\xE3\x81\xAE\xE3\x81\xBF"
	#tag EndConstant

	#tag Constant, Name = TEXT_SMALL_ICONS, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Small Icons"
		#Tag Instance, Platform = Any, Language = ja, Definition  = \"\xE5\xB0\x8F\xE3\x81\x95\xE3\x81\xAA\xE3\x82\xA2\xE3\x82\xA4\xE3\x82\xB3\xE3\x83\xB3\xE3\x81\xAE\xE3\x81\xBF"
	#tag EndConstant

	#tag Constant, Name = TEXT_SMALL_ICONS_WITH_LABELS, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Small Icons with Labels"
		#Tag Instance, Platform = Any, Language = ja, Definition  = \"\xE5\xB0\x8F\xE3\x81\x95\xE3\x81\xAA\xE3\x82\xA2\xE3\x82\xA4\xE3\x82\xB3\xE3\x83\xB3\xE3\x81\xA8\xE3\x83\x86\xE3\x82\xAD\xE3\x82\xB9\xE3\x83\x88"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="AcceptFocus"
			Visible=true
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AcceptTabs"
			Visible=true
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AquaBackground"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AutoDeactivate"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Backdrop"
			Visible=true
			Group="Appearance"
			Type="Picture"
			EditorType="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DoubleBuffer"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="EnableContextualMenu"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="EraseBackground"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="FontColourDefault"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Position"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Appearance"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HideHeight"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			Type="Integer"
			EditorType="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="InitialParent"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="mToolBarStyleChanged"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RefreshLock"
			Group="Behavior"
			InitialValue="0"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabIndex"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabPanelIndex"
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabStop"
			Visible=true
			Group="Position"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ToolbarStyle"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Transparent"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="UseFocusRing"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Position"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
