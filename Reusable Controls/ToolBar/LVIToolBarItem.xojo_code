#tag Interface
Protected Interface LVIToolBarItem
	#tag Method, Flags = &h0
		Function AvailablePosition() As Integer
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AvailablePosition(Assigns value As Integer)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CurrentPosition() As Integer
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CurrentPosition(Assigns value As Integer)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function DrawButton() As Picture
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Height() As Integer
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Height(Assigns value As Integer)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Left() As Integer
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Left(Assigns value As Integer)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function LockRight() As Boolean
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub LockRight(Assigns value As Boolean)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Right() As Integer
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Right(Assigns value As Integer)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ToolBarParent(Assigns value AS LVToolBar)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Visible() As Boolean
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Visible(Assigns value As Boolean)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Width() As Integer
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Width(Assigns value As Integer)
		  
		End Sub
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Interface
#tag EndInterface
