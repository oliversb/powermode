#tag Module
Protected Module LVCarbonEvents
	#tag Method, Flags = &h0
		Sub ToolBarButtonClicked(EventHandlerCallRef As Integer, EventRef As Integer, UserData As Integer)
		  #pragma unused EventHandlerCallRef
		  #pragma unused EventRef
		  #pragma unused UserData
		  
		  ' We'll look at all the open windows and compare their hash value to userData.
		  
		  ' If we got this event, we know the window has a toolbar on it.  We
		  ' loop through the controls on the window and once we find the toolbar control,
		  ' call its showhide method
		  
		  ' No click should toggle hide/show of toolbar
		  ' Command-click should toggle toolbar display styles
		  ' Option-Command-click should open customize window
		  #If TargetCarbon
		    Dim winHash As Variant
		    
		    For i As Integer = 0 To WindowCount-1
		      winHash = Window(i)
		      
		      If winHash.Hash = UserData Then
		        For j As Integer = 0 To Window(i).ControlCount-1
		          If Window(i).Control(j) IsA LVToolBar Then
		            If KeyBoard.MenuShortcutKey Then
		              'LVToolBar(Window(i).Control(j)).ShowHide
		            ElseIf Keyboard.MenuShortcutKey And Keyboard.AltKey Then
		              LVToolBar(Window(i).Control(j)).Customize
		            Else
		              LVToolBar(Window(i).Control(j)).ShowHide
		            End If
		          End If
		        Next
		      End If
		    Next
		  #Endif
		End Sub
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
