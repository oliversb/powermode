#tag Class
Protected Class LVStandardToolBarItem
Implements LVIToolBarItem
	#tag Method, Flags = &h0
		Function AvailablePosition() As Integer
		  Return mAvailablePosition
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AvailablePosition(Assigns value As Integer)
		  mAvailablePosition = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor()
		  Me.Type = Me.STYLE_SEPARATOR
		  Me.Visible = True
		  Me.Width = 32
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CurrentPosition() As Integer
		  Return mCurrentPosition
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CurrentPosition(Assigns value As Integer)
		  mCurrentPosition = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function DrawButton() As Picture
		  Dim tbHeight As Integer
		  
		  If mToolBarParent.Visible Then
		    tbHeight = mToolBarParent.Height
		  Else
		    tbHeight = mToolBarParent.HideHeight
		  End If
		  
		  If Me.Type = STYLE_SEPARATOR Then
		    Dim btnSeparator As Picture
		    
		    btnSeparator = New Picture(8, tbHeight - 1, 32)
		    
		    mToolBarParent.DrawBackground(btnSeparator.Graphics)
		    Height = btnSeparator.Height
		    
		    If mToolBarParent.AquaBackground Then
		      For i As Integer =  3 To tbHeight-4 Step 3
		        btnSeparator.Graphics.Pixel(3, i) = &c010101
		      Next
		    Else
		      btnSeparator.Graphics.ForeColor = RGB(170, 170, 170)
		      btnSeparator.Graphics.FillRect(3, 2, 1, tbHeight-5)
		    End If
		    
		    Me.Width = btnSeparator.Width
		    
		    Return btnSeparator
		  ElseIf Me.Type = STYLE_SPACE Then
		    Dim btnSpace As Picture
		    
		    btnSpace = New Picture(Me.Width, tbHeight-1, 32)
		    Height = btnSpace.Height
		    
		    mToolBarParent.DrawBackground(btnSpace.Graphics)
		    
		    Return btnSpace
		  End If
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Height() As Integer
		  Return mHeight
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Height(Assigns value As Integer)
		  mHeight = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Left() As Integer
		  Return mLeft
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Left(Assigns value As Integer)
		  mLeft = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function LockRight() As Boolean
		  Return mLockRight
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub LockRight(Assigns value As Boolean)
		  mLockRight = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Right() As Integer
		  Return mRight
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Right(Assigns value As Integer)
		  mRight = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ToolBarParent(Assigns value As LVToolBar)
		  mToolBarParent = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Visible() As Boolean
		  Return mVisible
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Visible(Assigns value As Boolean)
		  mVisible = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Width() As Integer
		  Return mWidth
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Width(Assigns value As Integer)
		  mWidth = value
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private mAvailablePosition As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mCurrentPosition As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mHeight As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLeft As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLockRight As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mOrder As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mRight As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mToolBarParent As LVToolBar
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mType As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mVisible As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mWidth As Integer
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mType
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  If value >= 1 And value <= 3 Then
			    mType = value
			  Else
			    Raise New OutOfBoundsException
			  End If
			End Set
		#tag EndSetter
		Type As Integer
	#tag EndComputedProperty


	#tag Constant, Name = STYLE_FLEXIBLE_SPACE, Type = Double, Dynamic = False, Default = \"3", Scope = Private
	#tag EndConstant

	#tag Constant, Name = STYLE_SEPARATOR, Type = Double, Dynamic = False, Default = \"1", Scope = Public
	#tag EndConstant

	#tag Constant, Name = STYLE_SPACE, Type = Double, Dynamic = False, Default = \"2", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Type"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
