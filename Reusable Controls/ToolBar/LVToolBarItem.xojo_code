#tag Class
Protected Class LVToolBarItem
Implements LVIToolBarItem
	#tag Method, Flags = &h0
		Sub AddMenuItem(myMenu As MenuItem)
		  If mBaseMenu Is Nil Then
		    mBaseMenu = New MenuItem
		  End If
		  
		  mBaseMenu.Append(myMenu)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AvailablePosition() As Integer
		  Return mAvailablePosition
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AvailablePosition(Assigns value As Integer)
		  mAvailablePosition = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor()
		  Visible = True
		  mEnabled = True
		  AvailablePosition = 0
		  CurrentPosition = -1
		  LockRight = False
		  Selected = False
		  Hover = False
		  AutoMask = True
		  mShadowColor =  RGB(170, 170, 170)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function CreateMask(icon As Picture) As Picture
		  #pragma unused icon
		  
		  'Dim mask As Picture
		  'Dim iconColor As Color
		  '
		  'mask = New Picture(icon.Width, icon.Height)', Screen(0).Depth)
		  '
		  'mask.Graphics.ForeColor = RGB(255, 255, 255)
		  'mask.Graphics.FillRect(0, 0, icon.Width, icon.Height)
		  '
		  'For x As Integer = 0 To mask.Width-1
		  'For y As Integer = 0 to mask.Height-1
		  'iconColor = icon.RGBSurface.Pixel(x, y)
		  'If iconColor = &cffffff Or iconColor = &c000000 Then
		  'Else
		  'mask.RGBSurface.Pixel(x, y) = &c000000
		  'End If
		  'Next
		  'Next
		  '
		  'Return mask
		  
		  'return me.image.mask
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CurrentPosition() As Integer
		  Return mCurrentPosition
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CurrentPosition(Assigns value As Integer)
		  mCurrentPosition = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub DisableIcon(byref btnIcon As Picture)
		  dim disabled as picture
		  if disabled = nil then
		    disabled = new picture(btnIcon.Width, btnIcon.Height)
		    disabled.Graphics.DrawPicture(btnIcon, 0, 0)
		    Brightness(disabled, 40)
		  end if
		  
		  btnIcon = disabled
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function DrawButton() As Picture
		  If ToolBarParent Is Nil Then Return Nil
		  If Not Me.Visible Then Return Nil
		  
		  Dim iconBuffer, btnBuffer, tmpPicture As Picture
		  Dim btnIcon, btnMask As Picture
		  Dim btnWidth, btnHeight, btnOffset As Integer
		  
		  ' This is used to calculate the size of the caption text in pixels
		  tmpPicture = New Picture(128, 128, Screen(0).Depth)
		  
		  If mTextSize > 0 Then tmpPicture.Graphics.TextSize = mTextSize
		  
		  #If TargetCarbon
		    btnOffset = 5
		    If mTextSize = 0 Then
		      tmpPicture.Graphics.TextSize = 11
		    End If
		  #Endif
		  
		  Select Case ToolBarParent.ToolbarStyle
		  Case ToolbarParent.STYLE_BIG_ICONS_LABEL
		    'btnIcon = ScalePicture(Me.Image, 32, 32)
		    btnIcon = Me.Image
		    
		    'If AutoMask Then
		    btnMask = Me.Image.CopyMask
		    'btnMask = CreateMask(btnIcon)
		    'Else
		    'btnMask = ScalePicture(Me.Image.Mask, 32, 32)
		    'End If
		    btnHeight = btnIcon.Height + 23
		    
		    btnWidth = Max(49+btnOffset, tmpPicture.Graphics.StringWidth(Me.Caption) + 10 + btnOffset)
		  Case ToolbarParent.STYLE_SMALL_ICONS_LABEL
		    btnIcon = ScalePicture(Me.Image, 16, 16)
		    'If AutoMask Then
		    'btnMask = CreateMask(btnIcon)
		    'Else
		    btnMask = ScalePicture(Me.Image.CopyMask, 16, 16)
		    'End If
		    
		    btnHeight = btnIcon.Height + 8
		    btnWidth = btnIcon.Width + _
		    tmpPicture.Graphics.StringWidth(Me.Caption) + 20
		    
		    If MinimumWidth > 0 And btnWidth < MinimumWidth Then btnWidth = MinimumWidth
		    
		  Case ToolbarParent.STYLE_BIG_ICONS
		    'btnIcon = ScalePicture(Me.Image, 32, 32)
		    btnIcon = Me.Image
		    'If AutoMask Then
		    btnMask = Me.Image.CopyMask
		    'btnMask = CreateMask(btnIcon)
		    'Else
		    'btnMask = ScalePicture(Me.Image.CopyMask, 32, 32)
		    'End If
		    
		    btnHeight = btnIcon.Height + 10
		    btnWidth = 49 + btnOffset
		  Case ToolbarParent.STYLE_SMALL_ICONS
		    btnIcon = ScalePicture(Me.Image, 16, 16)
		    'If AutoMask Then
		    'btnMask = Me.Image.CopyMask
		    ''btnMask = CreateMask(btnIcon)
		    'Else
		    btnMask = ScalePicture(Me.Image.CopyMask, 16, 16)
		    'End If
		    
		    btnHeight = btnIcon.Height + 4
		    btnWidth = btnIcon.Width + 10 + btnOffset
		  Case ToolbarParent.STYLE_LABELS_ONLY
		    btnIcon = Me.Image
		    btnHeight = tmpPicture.Graphics.StringHeight(Me.Caption, 100) + 6
		    btnWidth = tmpPicture.Graphics.StringWidth(Me.Caption) + 10 + btnOffset
		  End Select
		  
		  Me.Height = btnHeight + 1
		  
		  Me.Width = btnWidth
		  
		  iconBuffer = New Picture(btnIcon.Width, btnIcon.Height, btnIcon.Depth)
		  iconBuffer.Mask.Graphics.DrawPicture(btnMask, 0, 0)
		  iconBuffer.Graphics.TextFont = TextFont
		  iconBuffer.Graphics.TextSize = TextSize
		  
		  btnBuffer = New Picture(btnWidth, btnHeight, Screen(0).Depth)
		  'btnBuffer.Graphics.TextFont = Me.TextFont
		  'btnBuffer.Graphics.TextSize = Me.TextSize
		  
		  #If TargetCarbon
		    btnBuffer.Graphics.TextSize = 11
		  #Endif
		  
		  If btnBuffer = Nil Or iconBuffer = Nil Then
		    Return Nil
		  End If
		  
		  ToolBarParent.DrawBackground(btnBuffer.Graphics)
		  'btnBuffer.Transparent = 1
		  If Me.Enabled Then
		    If Selected Then
		      DrawSelectedBackground(btnBuffer.Graphics, btnWidth)
		      btnIcon = SelectIcon(btnIcon)
		    ElseIf Hover Then
		      DrawHoverBackground(btnBuffer.Graphics, btnWidth)
		    End If
		    
		    iconBuffer.Graphics.DrawPicture(btnIcon, 0, 0)
		    
		    btnBuffer.Graphics.ForeColor = BLACK
		  Else
		    If ToolbarParent.ToolbarStyle <> ToolbarParent.STYLE_LABELS_ONLY Then DisableIcon(btnIcon) Else EnableIcon(btnIcon)
		    
		    iconBuffer.Graphics.DrawPicture(btnIcon, 0, 0)
		    btnBuffer.Graphics.ForeColor = RGB(160, 160, 160) // Disabled text color
		  End If
		  
		  If mTextSize > 0 Then btnBuffer.Graphics.TextSize = mTextSize
		  
		  Select Case ToolBarParent.ToolbarStyle
		  Case ToolbarParent.STYLE_BIG_ICONS_LABEL
		    // Draw button caption underneath the button
		    #If TargetCarbon
		      If Selected And Me.Enabled Then
		        btnBuffer.Graphics.ForeColor = mShadowColor
		        btnBuffer.Graphics.DrawString(Me.Caption, _
		        ((btnWidth - (btnBuffer.Graphics.StringWidth(Me.Caption)))/2)+1,_
		        btnHeight - 4)
		        btnBuffer.Graphics.ForeColor = BLACK
		      End If
		    #Endif
		    
		    btnBuffer.Graphics.ForeColor = FontColour
		    btnBuffer.Graphics.DrawString(Me.Caption, _
		    ((btnWidth - (btnBuffer.Graphics.StringWidth(Me.Caption)))/2),_
		    btnHeight - 5)
		    
		    btnBuffer.Graphics.DrawPicture(iconBuffer, (btnWidth-btnIcon.Width)/2, 7)
		    DrawMenuTriangle(btnBuffer)
		    
		  Case ToolbarParent.STYLE_SMALL_ICONS_LABEL
		    // Draw the caption to the right of the button
		    #If TargetCarbon
		      If Selected And Me.Enabled Then
		        btnBuffer.Graphics.ForeColor = mShadowColor
		        btnBuffer.Graphics.DrawString(Me.Caption, _
		        btnIcon.Width + 11, _
		        btnHeight*.7+1)
		        'btnBuffer.Graphics.ForeColor = BLACK
		      End If
		    #Endif
		    
		    btnBuffer.Graphics.ForeColor = FontColour
		    
		    btnBuffer.Graphics.DrawString(Me.Caption, btnIcon.Width + 10, btnHeight*.7)
		    
		    btnBuffer.Graphics.DrawPicture(iconBuffer, 5, (btnHeight-btnIcon.Height) / 2)
		    
		    DrawMenuTriangle(btnBuffer)
		    
		  Case ToolbarParent.STYLE_BIG_ICONS
		    btnBuffer.Graphics.DrawPicture(iconBuffer, (btnWidth-btnIcon.Width)/2, (btnHeight-btnIcon.Height) / 2)
		    DrawMenuTriangle(btnBuffer)
		    
		  Case ToolbarParent.STYLE_SMALL_ICONS
		    btnBuffer.Graphics.DrawPicture(iconBuffer, 5, (btnHeight-btnIcon.Height) / 2)
		    DrawMenuTriangle(btnBuffer)
		    
		  Case ToolbarParent.STYLE_LABELS_ONLY
		    #If TargetCarbon
		      If Selected And Me.Enabled Then
		        btnBuffer.Graphics.ForeColor = mShadowColor
		        btnBuffer.Graphics.DrawString(Me.Caption, 5 + 1, btnHeight*.7 + 1)
		        'btnBuffer.Graphics.ForeColor = BLACK
		      End If
		    #Endif
		    
		    btnBuffer.Graphics.ForeColor = FontColour
		    btnBuffer.Graphics.DrawString(Me.Caption, 5, btnHeight*.7)
		    DrawMenuTriangle(btnBuffer)
		    
		  End Select
		  
		  Return btnBuffer
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub DrawHoverBackground(g as graphics, btnWidth As Integer)
		  if me.Enabled then
		    Dim myHeight As Integer = Me.Height-2
		    Dim myWidth As Integer = btnWidth-1
		    
		    g.ForeColor = HoverColour
		    
		    DrawHoverSelectedBackground(g, myWidth, myHeight, false)
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub DrawHoverSelectedBackground(g as graphics, w as integer, h as integer, selected as boolean)
		  select case HoverStyle
		  case "Round"
		    g.FillRoundRect(0, 0, w, h, 4, 4)
		  case "Flat"
		    g.FillRect(0, 0, w, h)
		  case "Classic"
		    if selected = false then
		      'g.ForeColor = RGB(160,160,160)
		      g.ForeColor = DarkBevelColor
		      
		      g.DrawLine(0, h, w, h)
		      g.DrawLine(w, h, w, 0)
		      
		      g.ForeColor = LightBevelColor
		      g.DrawLine(0, 0, 0, h - 1)
		      g.DrawLine(0, 0, w, 0)
		    else
		      g.ForeColor = DarkBevelColor
		      
		      g.DrawLine(0, 0, w, 0)
		      g.DrawLine(0, 0, 0, h)
		      
		      g.ForeColor = LightBevelColor
		      g.DrawLine(w, 0, w, h)
		      g.DrawLine(0, h, w, h)
		    end if
		  else
		    MsgBox("No theme of name """ + HoverStyle + """")
		    HoverStyle = "Round"
		  end select
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub DrawMenuTriangle(btnBuffer As Picture)
		   If BaseMenu <> nil Then
		    Dim triangle(6) As Integer
		    Dim triangleWidth As Integer = 4
		    Dim triangleHeight As Integer = 2
		    
		    triangle(1) = btnBuffer.Graphics.Width - 3 - triangleWidth
		    triangle(2) = btnBuffer.Graphics.Height - 3 - triangleHeight
		    triangle(3) = btnBuffer.Graphics.Width - 3
		    triangle(4) = btnBuffer.Graphics.Height - 3 - triangleHeight
		    triangle(5) = btnBuffer.Graphics.Width - 3 - triangleWidth/2
		    triangle(6) = btnBuffer.Graphics.Height - 3
		    
		    btnBuffer.Graphics.ForeColor = &c000000
		    btnBuffer.Graphics.FillPolygon(triangle)
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub DrawSelectedBackground(g As Graphics, btnWidth As Integer)
		  '#If TargetCarbon
		  '// Change the font to shadow
		  '#Else
		  'g.ForeColor = DarkBevelColor
		  '
		  'Dim myHeight As Integer = Me.Height - 3
		  'Dim myWidth As Integer = btnWidth - 1
		  '
		  'g.DrawLine(0, 0, myWidth, 0)
		  'g.DrawLine(0, 0, 0, myHeight)
		  '
		  'g.ForeColor = LightBevelColor
		  'g.DrawLine(myWidth, 0, myWidth, myHeight)
		  'g.DrawLine(0, myHeight, myWidth, myHeight)
		  '#Endif
		  
		  if me.Enabled then
		    Dim myHeight As Integer = Me.Height-2
		    Dim myWidth As Integer = btnWidth-1
		    
		    g.ForeColor = HoverColour.Darker(20)
		    
		    DrawHoverSelectedBackground(g, myWidth, myHeight, true)
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Enabled() As Boolean
		  Return mEnabled
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Enabled(Assigns value As Boolean)
		  mEnabled = value
		  
		  ToolBarParent.Redraw
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub EnableIcon(byref btnIcon As Picture)
		  btnIcon = Me.Image
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Height() As Integer
		  Return mHeight
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Height(Assigns value As Integer)
		  mHeight = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Hover() As Boolean
		  Return mHover
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Hover(Assigns value As Boolean)
		  mHover = value
		  
		  If mHover Then mSelected = False
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Image() As Picture
		  Return mImage
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Image(Assigns value As Picture)
		  If Not (value Is Nil ) Then
		    mImage = New Picture(value.Height, value.Width)//, Screen(0).Depth)
		    
		    mImage.Graphics.DrawPicture(value, 0, 0)
		  Else
		    mImage = Nil
		  End If
		  
		  ImageChanged = true
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Left() As Integer
		  Return mLeft
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Left(Assigns value As Integer)
		  mLeft = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function LockRight() As Boolean
		  Return mLockRight
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub LockRight(Assigns value As Boolean)
		  mLockRight = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Right() As Integer
		  Return mRight
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Right(Assigns value As Integer)
		  mRight = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ScalePicture(myPicture As Picture, scaleHeight As Integer, scaleWidth As Integer) As Picture
		  'Dim scalePicture As Picture
		  '
		  'scalePicture = New Picture(scaleHeight, scaleWidth, Screen(0).Depth)
		  '
		  'scalePicture.Graphics.DrawPicture(myPicture, 0, 0, scalePicture.Width, scalePicture.Height, 0, 0, myPicture.Width, myPicture.Height)
		  '
		  'Return scalePicture
		  
		  return ModGraphics.ScalePicture(myPicture, scaleWidth, scaleHeight, false)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Selected() As Boolean
		  Return mSelected
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Selected(Assigns value As Boolean)
		  mSelected = value
		  
		  If mSelected Then mHover = False
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function SelectIcon(btnIcon As Picture) As Picture
		  If True Then
		    Static drk As Picture
		    
		    If (drk Is Nil) Then
		      drk = New Picture(1, 1, 32)
		      drk.Mask.Graphics.Pixel(0, 0) = &cc0c0c0
		      drk.Graphics.Pixel(0, 0) = &c000000
		    End If
		    
		    dim nIcon as picture
		    nIcon = new picture(btnIcon.width, btnIcon.height)
		    nIcon.Graphics.DrawPicture(btnIcon, 0, 0)
		    'nIcon.ApplyMask(drk.CopyMask)
		    'btnIcon.Mask.Graphics.DrawPicture(drk, 0, 0, btnIcon.Width, btnIcon.Height, 0, 0, 1, 1)
		    
		    'return btnIcon
		    Return nIcon
		    
		  Else
		    Dim map(255), i as Integer
		    
		    For i = 0 To 255
		      map(i) = Max(0, i-100)
		    Next
		    
		    If btnIcon.Depth >= 24 Then btnIcon.RGBSurface.Transform(map)
		    
		    Return btnIcon
		  End If
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ShowMenu() As MenuItem
		  Dim selectedMenuItem As MenuItem
		  
		  Dim offset As Integer
		  
		  #If TargetWin32 Then
		    offset = 3
		  #Endif
		  #If TargetMacOS Then
		    offset = -3
		  #Endif
		  
		  selectedMenuItem = BaseMenu.PopUp(ToolBarParent.Window.Left + Left, _
		  ToolBarParent.Window.Top + Height - offset)
		  
		  Me.Selected = False
		  
		  Return selectedMenuItem
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ToolBarParent() As LVToolBar
		  Return mToolBarParent
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ToolBarParent(Assigns value As LVToolBar)
		  mToolBarParent = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Visible() As Boolean
		  Return mVisible
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Visible(Assigns value As Boolean)
		  mVisible = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Width() As Integer
		  Return mWidth
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Width(Assigns value As Integer)
		  mWidth = value
		End Sub
	#tag EndMethod


	#tag Note, Name = Enhancements
		Add LargeImage and SmallImage Properties
		
		If both are not supplied, scale one for the other.  Use this permanent image rather than scaling it
		each time as we do now.
		
		
	#tag EndNote


	#tag Property, Flags = &h0
		AutoMask As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		BaseMenu As MenuItem
	#tag EndProperty

	#tag Property, Flags = &h0
		Caption As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Shared FontColour As Color
	#tag EndProperty

	#tag Property, Flags = &h0
		HelpTag As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Shared HoverColour As Color = &c9F0500
	#tag EndProperty

	#tag Property, Flags = &h0
		Shared HoverStyle As String = "Round"
	#tag EndProperty

	#tag Property, Flags = &h21
		Private ImageChanged As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		ItemID As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mAvailablePosition As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mBaseMenu As MenuItem
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mCurrentPosition As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mEnabled As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mHeight As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mHover As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mImage As Picture
	#tag EndProperty

	#tag Property, Flags = &h0
		MinimumWidth As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLeft As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLockRight As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mOrder As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mRight As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSelected As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mShadowColor As Color
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTextSize As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mToolBarParent As LVToolbar
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mVisible As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mWidth As Integer
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mTextSize
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTextSize = value
			End Set
		#tag EndSetter
		TextSize As Integer
	#tag EndComputedProperty


	#tag Constant, Name = BLACK, Type = Color, Dynamic = False, Default = \"&c000000", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="AutoMask"
			Group="Behavior"
			InitialValue="0"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Caption"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ItemID"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MinimumWidth"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TextSize"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
