#tag Window
Begin Window LVCustomize
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   BalloonHelp     =   ""
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   8
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   330
   ImplicitInstance=   True
   LiveResize      =   False
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   False
   MaxWidth        =   32000
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   False
   Title           =   "Customize Toolbar"
   Visible         =   True
   Width           =   575
   Begin ListBox lbAvailable
      AutoDeactivate  =   True
      AutoHideScrollbars=   True
      Bold            =   False
      Border          =   True
      ColumnCount     =   1
      ColumnsResizable=   False
      ColumnWidths    =   ""
      DataField       =   ""
      DataSource      =   ""
      DefaultRowHeight=   -1
      Enabled         =   True
      EnableDrag      =   False
      EnableDragReorder=   False
      GridLinesHorizontal=   0
      GridLinesVertical=   0
      HasHeading      =   False
      HeadingIndex    =   -1
      Height          =   190
      HelpTag         =   ""
      Hierarchical    =   False
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      RequiresSelection=   False
      Scope           =   "0"
      ScrollbarHorizontal=   False
      ScrollBarVertical=   True
      SelectionType   =   0
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   34
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   200
      _ScrollOffset   =   0
      _ScrollWidth    =   -1
   End
   Begin ListBox lbCurrent
      AutoDeactivate  =   True
      AutoHideScrollbars=   True
      Bold            =   False
      Border          =   True
      ColumnCount     =   1
      ColumnsResizable=   False
      ColumnWidths    =   ""
      DataField       =   ""
      DataSource      =   ""
      DefaultRowHeight=   -1
      Enabled         =   True
      EnableDrag      =   False
      EnableDragReorder=   True
      GridLinesHorizontal=   0
      GridLinesVertical=   0
      HasHeading      =   False
      HeadingIndex    =   -1
      Height          =   190
      HelpTag         =   ""
      Hierarchical    =   False
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   331
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      RequiresSelection=   False
      Scope           =   "0"
      ScrollbarHorizontal=   False
      ScrollBarVertical=   True
      SelectionType   =   0
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   34
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   200
      _ScrollOffset   =   0
      _ScrollWidth    =   -1
   End
   Begin PopupMenu pmView
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "#LVToolBar.TEXT_BIG_ICONS_WITH_LABELS\r#LVToolBar.TEXT_SMALL_ICONS_WITH_LABELS\r#LVToolBar.TEXT_BIG_ICONS\r#LVToolBar.TEXT_SMALL_ICONS\r#LVToolBar.TEXT_LABELS_ONLY"
      Italic          =   False
      Left            =   312
      ListIndex       =   -2147483648
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      Scope           =   "0"
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   247
      Underline       =   False
      Visible         =   True
      Width           =   218
   End
   Begin Label StaticText1
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   242
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "#TEXT_VIEW_AS"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   247
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   62
   End
   Begin PushButton pbReset
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "#TEXT_RESET_DEFAULTS"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      Scope           =   "0"
      TabIndex        =   6
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   291
      Underline       =   False
      Visible         =   True
      Width           =   120
   End
   Begin PushButton pbAdd
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "#TEXT_ADD"
      Default         =   True
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   230
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      Scope           =   "0"
      TabIndex        =   7
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   57
      Underline       =   False
      Visible         =   True
      Width           =   90
   End
   Begin PushButton pbRemove
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "#TEXT_REMOVE"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   230
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      Scope           =   "0"
      TabIndex        =   8
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   89
      Underline       =   False
      Visible         =   True
      Width           =   90
   End
   Begin PushButton pbMoveUp
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "#TEXT_MOVEUP"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   230
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      Scope           =   "0"
      TabIndex        =   9
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   146
      Underline       =   False
      Visible         =   True
      Width           =   90
   End
   Begin PushButton pbMoveDown
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "#TEXT_MOVEDOWN"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   230
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      Scope           =   "0"
      TabIndex        =   10
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   178
      Underline       =   False
      Visible         =   True
      Width           =   90
   End
   Begin Label StaticText2
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   11
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "#TEXT_AVAILABLE_TOOLBAR_BUTTONS"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   14
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   200
   End
   Begin Label StaticText3
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   331
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   12
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "#TEXT_CURRENT_TOOLBAR_BUTTONS"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   14
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   199
   End
   Begin OkCancelButton Listbox1
      AcceptFocus     =   False
      AcceptTabs      =   True
      ActionButtonText=   "OK"
      AutoDeactivate  =   True
      BackColor       =   &cFFFFFF00
      Backdrop        =   0
      CancelButtonText=   "Cancel"
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   51
      HelpTag         =   ""
      Index           =   "-2147483648"
      InitialParent   =   ""
      Left            =   331
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   13
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   279
      Transparent     =   True
      UseFocusRing    =   True
      Visible         =   True
      Width           =   199
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  
		  
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Sub Add()
		  Dim selButton As Integer
		  Dim origPosition As Integer
		  
		  selButton = lbAvailable.ListIndex
		  
		  If selButton >= 0 Then
		    origPosition = mAvailable(selButton).CurrentPosition
		    
		    If origPosition < 0 Then origPosition = lbCurrent.ListCount
		    If origPosition > lbCurrent.ListCount Then origPosition = lbCurrent.ListCount
		    
		    If mAvailable(selButton) IsA LVToolBarItem Then
		      lbCurrent.InsertRow(origPosition, LVToolBarItem(mAvailable(selButton)).Caption)
		      lbCurrent.RowPicture(origPosition) = ScalePicture(LVToolBarItem(mAvailable(selButton)).Image, 16, 16)
		    Else
		      lbCurrent.InsertRow(origPosition, "Separator")
		      lbCurrent.RowPicture(origPosition) = mAvailable(selButton).DrawButton
		    End If
		    
		    mCurrent.Insert(origPosition, mAvailable(selButton))
		    
		    mAvailable.Remove(selButton)
		    lbAvailable.RemoveRow(selButton)
		    
		    lbCurrent.ListIndex = origPosition
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub FillAvailable()
		  lbAvailable.DeleteAllRows
		  
		  For i As Integer = 0 To UBound(mAvailable)
		    
		    If mAvailable(i) IsA LVToolBarItem Then
		      lbAvailable.AddRow(LVToolBarItem(mAvailable(i)).Caption)
		      lbAvailable.RowPicture(lbAvailable.LastIndex) = ScalePicture(LVToolBarItem(mAvailable(i)).Image, 16, 16)
		    Else
		      lbAvailable.AddRow("Separator")
		      lbAvailable.RowPicture(lbAvailable.LastIndex) = mAvailable(i).DrawButton
		    End If
		    mAvailable(i).AvailablePosition = lbAvailable.LastIndex
		  Next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub FillCurrent()
		  lbCurrent.DeleteAllRows
		  
		  For i As Integer = 0 To UBound(mCurrent)
		    If mCurrent(i) IsA LVToolBarItem Then
		      lbCurrent.AddRow(LVToolBarItem(mCurrent(i)).Caption)
		      lbCurrent.RowPicture(lbCurrent.LastIndex) = ScalePicture(LVToolBarItem(mCurrent(i)).Image, 16, 16)
		    Else
		      lbCurrent.AddRow("Separator")
		      lbCurrent.RowPicture(lbCurrent.LastIndex) = mCurrent(i).DrawButton
		    End If
		    
		    mCurrent(i).CurrentPosition = lbCurrent.LastIndex
		  Next
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub Move(fromPos As Integer, toPos As Integer, drag As Boolean)
		  Dim thisButton As LVIToolBarItem
		  
		  If toPos >= lbCurrent.ListCount Or toPos < 0 Then Return
		  
		  thisButton = mCurrent(fromPos)
		  
		  If Not drag Then
		    ' Remove original
		    lbCurrent.RemoveRow(fromPos)
		    
		    ' Add at new position
		    If thisButton IsA LVToolBarItem Then
		      lbCurrent.InsertRow(toPos, LVToolBarItem(thisButton).Caption)
		      lbCurrent.RowPicture(toPos) = ScalePicture(LVToolBarItem(thisButton).Image, 16, 16)
		    Else
		      lbCurrent.InsertRow(toPos, "Separator")
		      lbCurrent.RowPicture(toPos) = thisButton.DrawButton
		    End If
		  End If
		  
		  mCurrent.Remove(fromPos)
		  mCurrent.Insert(toPos, thisButton)
		  
		  lbCurrent.ListIndex = toPos
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub Remove()
		  Dim selButton As Integer
		  Dim origPosition As Integer
		  
		  selButton = lbCurrent.ListIndex
		  
		  If selButton >= 0 Then
		    
		    origPosition = mCurrent(selButton).AvailablePosition
		    
		    If origPosition > lbAvailable.ListCount Then origPosition = lbAvailable.ListCount
		    
		    If mCurrent(selButton) IsA LVToolBarItem Then
		      lbAvailable.InsertRow(origPosition, LVToolBarItem(mCurrent(selButton)).Caption)
		      lbAvailable.RowPicture(origPosition) = ScalePicture(LVToolBarItem(mCurrent(selButton)).Image, 16, 16)
		    Else
		      lbAvailable.InsertRow(origPosition, "Separator")
		      lbAvailable.RowPicture(origPosition) = mCurrent(selButton).DrawButton
		    End If
		    
		    mAvailable.Insert(origPosition, mCurrent(selButton))
		    mAvailable(origPosition).Visible = False
		    
		    mCurrent.Remove(selButton)
		    lbCurrent.RemoveRow(selButton)
		    
		    lbAvailable.ListIndex = origPosition
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ScalePicture(myPicture As Picture, scaleHeight As Integer, scaleWidth As Integer) As Picture
		  Dim scalePicture As Picture
		  
		  scalePicture = New Picture(scaleHeight, scaleWidth, Screen(0).Depth)
		  
		  scalePicture.Graphics.DrawPicture(myPicture, 0, 0, scalePicture.Width, scalePicture.Height, 0, 0, myPicture.Width, myPicture.Height)
		  
		  Return scalePicture
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ToolBar(Assigns ByVal myToolBar() As LVIToolBarItem)
		  Static defaultAvailable() As LVIToolBarItem
		  Static defaultCurrent() As LVIToolBarItem
		  
		  Dim saveDefault As Boolean = False
		  
		  If UBound(defaultCurrent) < 0 Then
		    saveDefault = True
		  End If
		  
		  // Populate toolbar buttons lists
		  For i As Integer = 0 To UBound(myToolBar)
		    If myToolBar(i).Visible Then
		      mCurrent.Append(myToolBar(i))
		      
		      If saveDefault Then defaultCurrent.Append(myToolBar(i))
		    Else
		      mAvailable.Append(myToolBar(i))
		      
		      If saveDefault Then defaultAvailable.Append(myToolBar(i))
		    End If
		  Next
		  
		  FillCurrent
		  FillAvailable
		  
		  mDefaultCurrent = defaultCurrent
		  mDefaultAvailable = defaultAvailable
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ToolBarStyle() As Integer
		  Return mToolbarStyle
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ToolBarStyle(Assigns myStyle As Integer)
		  mToolbarStyle = myStyle
		  
		  pmView.ListIndex = myStyle-1
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private mAvailable() As LVIToolBarItem
	#tag EndProperty

	#tag Property, Flags = &h0
		mCurrent() As LVIToolBarItem
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDefaultAvailable() As LVIToolBarItem
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDefaultCurrent() As LVIToolBarItem
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDragRow As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mToolbarStyle As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		OK As Boolean
	#tag EndProperty


	#tag Constant, Name = TEXT_ADD, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Add ->"
		#Tag Instance, Platform = Any, Language = ja, Definition  = \"-> \xE8\xBF\xBD\xE5\x8A\xA0"
	#tag EndConstant

	#tag Constant, Name = TEXT_AVAILABLE_TOOLBAR_BUTTONS, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Available toolbar buttons:"
		#Tag Instance, Platform = Any, Language = ja, Definition  = \"\xE4\xBD\xBF\xE7\x94\xA8\xE5\x8F\xAF\xE8\x83\xBD\xE3\x83\x84\xE3\x83\xBC\xE3\x83\xAB\xE3\x83\x90\xE3\x83\xBC\xE3\x83\x9C\xE3\x82\xBF\xE3\x83\xB3\xEF\xBC\x9A"
	#tag EndConstant

	#tag Constant, Name = TEXT_CANCEL, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Cancel"
		#Tag Instance, Platform = Any, Language = ja, Definition  = \"\xE3\x82\xAD\xE3\x83\xA3\xE3\x83\xB3\xE3\x82\xBB\xE3\x83\xAB"
	#tag EndConstant

	#tag Constant, Name = TEXT_CURRENT_TOOLBAR_BUTTONS, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Current toolbar buttons:"
		#Tag Instance, Platform = Any, Language = ja, Definition  = \"\xE3\x82\xAB\xE3\x83\xAC\xE3\x83\xB3\xE3\x83\x88\xE3\x83\xBB\xE3\x83\x84\xE3\x83\xBC\xE3\x83\xAB\xE3\x83\x90\xE3\x83\xBC\xE3\x83\x9C\xE3\x82\xBF\xE3\x83\xB3\xEF\xBC\x9A"
	#tag EndConstant

	#tag Constant, Name = TEXT_MOVEDOWN, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Move Down"
		#Tag Instance, Platform = Any, Language = ja, Definition  = \"\xE4\xB8\x8B\xE3\x81\xB8"
	#tag EndConstant

	#tag Constant, Name = TEXT_MOVEUP, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Move Up"
		#Tag Instance, Platform = Any, Language = ja, Definition  = \"\xE4\xB8\x8A\xE3\x81\xB8"
	#tag EndConstant

	#tag Constant, Name = TEXT_OK, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"OK"
		#Tag Instance, Platform = Any, Language = ja, Definition  = \"OK"
	#tag EndConstant

	#tag Constant, Name = TEXT_REMOVE, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"<- Remove"
		#Tag Instance, Platform = Any, Language = ja, Definition  = \"<- \xE5\x8F\x96\xE9\x99\xA4"
	#tag EndConstant

	#tag Constant, Name = TEXT_RESET_DEFAULTS, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Reset Defaults"
		#Tag Instance, Platform = Any, Language = ja, Definition  = \"\xE3\x83\x87\xE3\x83\x95\xE3\x82\xA9\xE3\x83\xAB\xE3\x83\x88\xE8\xA8\xAD\xE5\xAE\x9A\xE3\x81\xB8"
	#tag EndConstant

	#tag Constant, Name = TEXT_VIEW_AS, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"View As:"
		#Tag Instance, Platform = Any, Language = ja, Definition  = \"\xE8\xA1\xA8\xE7\xA4\xBA\xEF\xBC\x9A"
	#tag EndConstant


#tag EndWindowCode

#tag Bindings
	#tag BeginBinding
		SourceItem = lbAvailable
		DestinationItem = pbAdd
		ItemType = enablingBinder
		ItemDescription = Enable pbAdd when lbAvailable has a selection
		SourceBindData = getSelectionProvider
	#tag EndBinding
	#tag BeginBinding
		SourceItem = lbCurrent
		DestinationItem = pbRemove
		ItemType = enablingBinder
		ItemDescription = Enable pbRemove when lbCurrent has a selection
		SourceBindData = getSelectionProvider
	#tag EndBinding
	#tag BeginBinding
		SourceItem = lbCurrent
		DestinationItem = pbMoveUp
		ItemType = enablingBinder
		ItemDescription = Enable pbMoveUp when lbCurrent has a selection
		SourceBindData = getSelectionProvider
	#tag EndBinding
	#tag BeginBinding
		SourceItem = lbCurrent
		DestinationItem = pbMoveDown
		ItemType = enablingBinder
		ItemDescription = Enable pbMoveDown when lbCurrent has a selection
		SourceBindData = getSelectionProvider
	#tag EndBinding
#tag EndBindings
#tag Events lbAvailable
	#tag Event
		Sub DoubleClick()
		  Add()
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  #If TargetWin32
		    Me.DefaultRowHeight = 20
		  #Endif
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events lbCurrent
	#tag Event
		Sub DoubleClick()
		  Remove()
		End Sub
	#tag EndEvent
	#tag Event
		Function DragRow(drag As DragItem, row As Integer) As Boolean
		  #pragma unused drag
		  
		  mDragRow = row
		End Function
	#tag EndEvent
	#tag Event
		Function DragReorderRows(newPosition as Integer, parentRow as Integer) As Boolean
		  #pragma unused parentRow
		  
		  Move(mDragRow, newPosition, True)
		  
		End Function
	#tag EndEvent
	#tag Event
		Sub Open()
		  #If TargetWin32
		    Me.DefaultRowHeight = 20
		  #Endif
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbReset
	#tag Event
		Sub LostFocus()
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Action()
		  mCurrent = mDefaultCurrent
		  mAvailable = mDefaultAvailable
		  
		  FillCurrent
		  FillAvailable
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbAdd
	#tag Event
		Sub Action()
		  Add()
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbRemove
	#tag Event
		Sub Action()
		  Remove()
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbMoveUp
	#tag Event
		Sub Action()
		  Dim selButton As Integer
		  
		  selButton = lbCurrent.ListIndex
		  
		  Move(selButton, selButton-1, False)
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbMoveDown
	#tag Event
		Sub Action()
		  Dim selButton As Integer
		  
		  selButton = lbCurrent.ListIndex
		  
		  Move(selButton, selButton+1, False)
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Listbox1
	#tag Event
		Sub Action()
		  ToolbarStyle = pmView.ListIndex+1
		  
		  For i As Integer = 0 To UBound(mCurrent)
		    mCurrent(i).Visible = True
		  Next
		  
		  For i As Integer = 0 To UBound(mAvailable)
		    mAvailable(i).Visible = False
		    mCurrent.Append(mAvailable(i))
		  Next
		  
		  OK = True
		  
		  Self.Hide
		End Sub
	#tag EndEvent
	#tag Event
		Sub CancelAction()
		  Self.Hide
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="OK"
		Group="Behavior"
		InitialValue="0"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
