#tag Class
Protected Class MouseRect
Inherits Pixmap
	#tag Method, Flags = &h1000
		Sub Constructor(group as Group, theResizableObject as ResizableObject)
		  Super.Constructor
		  
		  Grouper = group
		  
		  Grouper.append(me)
		  
		  ResizableObjectParent = theResizableObject
		  
		  
		  //set the colour to black and then draw a small rectangle
		  static p as picture
		  
		  if p = nil then
		    const roundness = 4
		    p = new picture(MouseRect.Size, MouseRect.Size)
		    p.graphics.ForeColor = &CFFFFFF
		    p.graphics.FillRoundRect(0, 0, MouseRect.Size, MouseRect.Size,   roundness, roundness)
		    p.graphics.ForeColor = &c000000
		    p.graphics.DrawRoundRect(0, 0, MouseRect.Size, MouseRect.Size,   roundness, roundness)
		  end if
		  
		  me.Image = p
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1000
		Sub Update()
		  dim width as integer = ResizableObjectParent.Width
		  dim height as integer = ResizableObjectParent.Height
		  
		  dim staticTop as integer = height
		  dim staticLeft as integer = width
		  
		  select case Pos
		  case 1
		    //middle right
		    me.Top = (height / 2)
		    me.Left = staticLeft
		  case 2
		    //bottom right
		    me.Top = staticTop
		    me.Left = staticLeft
		  case else
		    //bottom centre
		    me.Top = staticTop
		    me.Left = (width / 2)
		  end select
		  
		  me.Top = me.Top - MouseRect.Size
		  me.Left = me.Left - MouseRect.Size
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private Grouper As Group
	#tag EndProperty

	#tag Property, Flags = &h21
		Private hvar As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		Shared myLastX As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		Shared myLastY As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		Pos As UInt32
	#tag EndProperty

	#tag Property, Flags = &h0
		ResizableObjectParent As ResizableObject
	#tag EndProperty

	#tag Property, Flags = &h21
		Private wvar As Integer
	#tag EndProperty


	#tag Constant, Name = PosCount, Type = Double, Dynamic = False, Default = \"2", Scope = Public
	#tag EndConstant

	#tag Constant, Name = Size, Type = Double, Dynamic = False, Default = \"10", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Height"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Image"
			Group="Behavior"
			InitialValue="0"
			Type="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Opacity"
			Group="Behavior"
			Type="double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
