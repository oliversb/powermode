#tag Class
Protected Class GSPopoverWindow
Inherits Window
	#tag Event
		Sub Deactivate()
		  //If we lose focus, we want the popover to close (just like in iOS)
		  self.Close
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  //Set some basic definitions here.  The window shaping will come during the popover call.
		  
		  //Make sure these don't show, although I don't think it makes a difference
		  self.CloseButton = False
		  self.MaximizeButton = false
		  self.MinimizeButton = False
		  self.FullScreenButton = false
		  
		  //If running on Windows, if the screen isn't set to Resizeable=false it looks awful
		  //Unfortunately it has to be set in the IDE
		  #if TargetWin32 then
		    if self.Resizeable then
		      MsgBox "You must make the popover window property 'Resizeable' false in the IDE. If you don't, the window shape looks bad."
		    end if
		  #endif
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub Paint(g As Graphics, areas() As REALbasic.Rect)
		  #pragma unused areas
		  
		  //Draw the popoever shape
		  g.DrawObject windowFigureShape, 0, 0
		  
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h1000
		Sub displayPopoverAt(xScreen as Integer, yScreen as Integer, forceDisplaySide as Integer = - 1, tag as variant)
		  // Calling the overridden superclass constructor.
		  Super.Constructor
		  //Display the popover window at screen coordinates (not window coordinates) xScreen, yScreen
		  //You can force the pointer to show up on a specific side by setting forceDisplaySide
		  //0 top, 1 right, 2 bottom, 3 left
		  //
		  //Returns the variable result()
		  //result(0) is the upper left of everything
		  //result(1,2,3) are the corners of the pointer triangle
		  //result(4,5) are the corners of the roundrect
		  //
		  //result(0) is used to place the window on the screen so the pointer lines up with the xScreen, yScreen
		  //the rest are used in the windows dll calls to shape the window
		  
		  //First set the popover color based on the system FillColor
		  //If the fill color is already very dark, lighten the popover, otherwise darken it.
		  windowColor = FillColor
		  if FillColor.Value > 0.6 then
		    windowColor = RGB(FillColor.Red / colorChange, FillColor.Green / colorChange, FillColor.Blue / colorChange)
		  ElseIf FillColor.Value = 0 then
		    windowColor = RGB(25, 25, 25)
		  else
		    windowColor = RGB(FillColor.Red / colorChange, FillColor.Green / colorChange, FillColor.Blue / colorChange)
		  end if
		  
		  
		  //Set the FigureShape of the window background
		  dim result() as REALbasic.Point
		  
		  if windowFigureShape <> nil then
		    //We've already been through this routine, no need to reset the window. We're just moving.
		    
		    //Position the window
		    self.Left = xScreen + upperLeftOffset.x
		    self.Top = yScreen + upperLeftOffset.y
		    
		    return
		  end if
		  
		  result = setWindowShape(xScreen,yScreen, forceDisplaySide)
		  
		  upperLeftOffset = new REALbasic.Point(result(0).x - xScreen, result(0).y - yScreen)
		  
		  #if TargetWin32
		    //The original method made a picture from windowFigureShape and went pixel by pixel to create the transparency
		    //using createrectrgn and combinergn.  This was VERY slow.
		    //We are only using a triangle and a roundrect, so we can just create those regions instead.
		    Const SWP_NOMOVE = &H2
		    Const SWP_FRAMECHANGED = &H20
		    Const HWND_TOP = 0
		    Const GWL_STYLE = -16
		    Const WS_POPUPWINDOW = &H80880000
		    const WS_MINIMIZEBOX = &h00020000
		    
		    Declare Function CreateRoundRectRgn Lib "gdi32" (Left As Integer, top As Integer, Right As Integer, bottom As Integer, widthEllipse as integer, heightEllipse as Integer) As Integer
		    Declare Function CreatePolygonRgn Lib "gdi32" (points as Ptr, cPoints As Integer, fnPolyFillMode As Integer) As Integer
		    Declare Function CombineRgn Lib "gdi32" (rgnDest As Integer, rgnSrc1 As Integer, rgnSrc2 As Integer, combineMode As Integer) As Integer
		    Declare Function SetWindowRgn Lib "user32" (hWnd As Integer, hRgn As Integer, bRedraw As Boolean) As Integer
		    Declare Function DeleteObject Lib "gdi32" (hObject As Integer) As Integer
		    Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongW" (hwnd As Integer, nIndex As Integer, dwNewLong As Integer) As Integer
		    Declare Function SetWindowPos Lib "user32" (hwnd as Integer, hWndInstertAfter as Integer, x as Integer, y as Integer, cx as Integer, cy as Integer, flags as Integer) as Integer
		    
		    Dim styleFlags As Integer
		    styleFlags = SetWindowLong( self.Handle, GWL_STYLE, WS_POPUPWINDOW )
		    styleFlags = SetWindowLong( self.Handle, GWL_STYLE, WS_MINIMIZEBOX)
		    styleFlags = BitwiseOr( SWP_FRAMECHANGED, SWP_NOMOVE )
		    styleFlags = SetWindowPos( self.handle, HWND_TOP, 0, 0, self.width, self.height, styleFlags )
		    
		    
		    //Create the main rountrect shape
		    dim r1 as integer = CreateRoundRectRgn(result(4).X,result(4).Y,result(5).X, result(5).Y, radius, radius)
		    
		    //shift the triangle toward the roundrect if it's on the right or bottom
		    if result(2).x > result(5).x then
		      result(1).Offset -1, 0
		      result(2).Offset -1, 0
		      result(3).Offset -1, 0
		    elseif result(2).y > result(5).y then
		      result(1).Offset 0, -1
		      result(2).Offset 0, -1
		      result(3).Offset 0, -1
		    end if
		    
		    //Create the triangle
		    dim points as new MemoryBlock(32)
		    points.long(0) = result(1).X
		    points.long(4) = result(1).Y
		    points.long(8) = result(2).X
		    points.long(12) = result(2).Y
		    points.long(16) = result(3).X
		    points.long(20) = result(3).Y
		    points.long(24) = result(1).X
		    points.long(28) = result(1).Y
		    
		    dim r2 as Integer = CreatePolygonRgn(points, 4, 1)
		    
		    //Combine the RoundRect (r1) and triangle (r2) into r1
		    dim i as Integer = CombineRgn(r1, r1, r2, 2)
		    
		    //Delete the triangle
		    i = DeleteObject(r2)
		    
		    //Set the window
		    i = SetWindowRgn(Handle, r1, True)
		    
		  #elseif TargetCocoa
		    //Just set the whole thing transparent and in the paint event draw the background on it.
		    declare function NSClassFromString lib "Cocoa" (aClassName as CFStringRef) as Ptr
		    Declare Function colorWithCalibratedWhite Lib "AppKit" Selector "colorWithCalibratedWhite:alpha:" (NSColorClass As Ptr, white As Single, alpha As Single) As Ptr
		    Declare Sub setBackgroundColor Lib "AppKit" Selector "setBackgroundColor:" (NSWindow As Ptr, backgroundColor As Ptr)
		    Declare Sub setOpaque Lib "AppKit" Selector "setOpaque:" (NSWindow As Ptr, flag As Byte)
		    setOpaque(Ptr(self.Handle), 0) // Zero for NO in Objective C
		    setBackgroundColor(Ptr(self.Handle), colorWithCalibratedWhite(NSClassFromString("NSColor"), 1, 0))
		  #endif
		  
		  //Position the window
		  self.Left = result(0).x
		  self.Top = result(0).y
		  
		  //set tag
		  self.tag = tag
		  
		  //Show ourselves
		  self.Show
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub offsetControls(deltaX as Integer, deltaY as Integer)
		  //Only offset the "root level" controls.  If something is inside another control (like in a tabpanel),
		  //only move the tabpanel.
		  for i as Integer = 0 to ControlCount - 1
		    dim c as RectControl = RectControl(Control(i))
		    if c.Parent = nil then
		      if c.LockLeft = false or c.lockright = false then
		        c.Left = c.Left + deltaX
		      end if
		      
		      if c.LockTop = false or c.LockBottom = false then
		        c.Top = c.Top + deltaY
		      Else
		        c.Top = c.Top + deltaY/2
		      end if
		      
		    end if
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ScreenFromXY(x as Integer, y as Integer) As Integer
		  Dim rect As New REALbasic.Rect
		  Dim pt As New REALbasic.Point
		  pt.X = X
		  pt.Y = Y
		  For i As Integer = 0 To ScreenCount - 1
		    rect.Top = Screen(i).Top
		    rect.Left = Screen(i).Left
		    rect.Width = Screen(i).Width
		    rect.Height = Screen(i).Height
		    If rect.Contains(pt) Then
		      Return i
		    End If
		  Next
		  
		  Return -1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function setWindowShape(xScreen as Integer, yScreen as Integer, forceDisplaySide as Integer = - 1) As REALbasic.Point()
		  //This routine sets the property windowFigureShape, used during the paint event as well as for window transparency in Windows.
		  //
		  //Input is (xScreen,yScreen), the point at which the pointer should point. Probably from a mouse click, but perhaps the center of
		  //a control.
		  //IMPORTANT: (xScreen,yScreen) are overall screen coordinates, not window coordinates.
		  
		  //It returns "result", an array of type REALbasic.Point.  The array contains:
		  //0: the upper left coordinates of the window, to be used when we Show the window (otherwise we don't know how to line up the pointer)
		  //1,2,3: the coordinates of the triangle that makes up the pointer. Used for Windows transparency routine.
		  //4,5: the coordinates of the upper left/lower right corners of the roundrect. Used for Windows transparency routine.
		  dim result() as REALbasic.Point
		  
		  //Get information about the display that was clicked
		  //This should let it be used on multi-display systems with different resolutions
		  dim thisScreen as Integer = ScreenFromXY(xScreen, yScreen)
		  dim screenWidth as Integer = Screen(thisScreen).AvailableWidth
		  dim screenHeight as Integer = Screen(thisScreen).AvailableHeight
		  
		  //Position the window based on where it's showing from (xScreen,yScreen)
		  //If yScreen is in the middle part of the screen (vertically), we will use a left or right pointer.
		  //If yScreen is within self.Height/2 of the top, we use a pointer on the top edge.
		  //If yScreen is within self.Height/2 of the bottom, we use a pointer on the bottom edge.
		  dim pointerOnSide(4) as Boolean //0 top, 1 right, 2 bottom, 3 left
		  if yScreen < self.Height / 2 then
		    pointerOnSide(0) = true
		  elseif yScreen > screenHeight - self.Height / 2 then
		    pointerOnSide(2) = true
		  else
		    if xScreen < screenWidth/2 then
		      pointerOnSide(3) = true
		    else
		      pointerOnSide(1) = true
		    end if
		  end if
		  
		  if forceDisplaySide >= 0 then
		    for i as Integer = 0 to pointerOnSide.Ubound
		      pointerOnSide(i) = false
		    next i
		    pointerOnSide(forceDisplaySide) = True
		  end if
		  
		  //By default, the pointer will show in the middle of the side it's on.
		  //But if we are near an edge, we may need to offset it to keep the window on the screen.
		  //A negative offset moves it to the left, a positive offset moves it to the right
		  dim pointerOffset as Integer = 0
		  if pointerOnSide(1) or pointerOnSide(3) then //check up/down positioning
		    if yScreen < self.Height/2 then
		      pointerOffset = max(yScreen - self.Height/2, -self.Height/2.15)
		    elseif yScreen > screenHeight - self.Height/2 then
		      pointerOffset = min(yScreen - self.Height/2, self.Height/2.15)
		    end if
		  else //check left/right positioning
		    if xScreen < self.Width/2 then
		      pointerOffset = max(xScreen - self.Width/2 - 50, -self.Width/2.15)
		    elseif xScreen > screenWidth - self.Width/2 then
		      pointerOffset = min(xScreen - (screenWidth - self.Width/2) + 50, self.Width / 2.15)
		    end if
		  end if
		  
		  //Now that the pointer side and offset are known, we can set the corners of the RoundRect
		  dim corners(4) as REALbasic.Point
		  
		  //First set the corners as if there were no pointer. The roundrect is the size of the window.
		  corners(0) = new REALbasic.Point(0,0)
		  corners(1) = new REALbasic.Point(self.Width,0)
		  corners(2) = new REALbasic.Point(self.Width, self.Height)
		  corners(3) = new REALbasic.Point(0, self.Height)
		  
		  //Offset the corners of the RoundRect to make room for the pointer.
		  //Also set the upper left corner of the entire thing so we know where to display the window
		  if pointerOnSide(0) then
		    //top side pointer, offset things downward
		    for i as Integer = 0 to 3
		      corners(i).Offset 0, pointerSize
		    next i
		    self.Height = self.Height + pointerSize //make the window bigger
		    offsetControls 0, pointerSize //offset the controls to keep them in the window
		    result.Append new REALbasic.Point(xScreen - self.Width/2 - pointerOffset, yScreen) //append the upper left corner to result
		    
		  ElseIf pointerOnSide(1) then
		    //right side pointer, no offset required
		    self.Width = self.Width + pointerSize //make the window bigger
		    result.Append new REALbasic.Point(xScreen - self.Width, yScreen - self.Height/2 - pointerOffset) //append the upper left corner to result
		    
		  ElseIf pointerOnSide(2) then
		    //bottom side pointer, no offset required
		    self.Height = self.Height + pointerSize //make the window bigger
		    result.Append new REALbasic.Point(xScreen - self.Width/2 - pointerOffset, yScreen - self.Height) //append the upper left corner to result
		    
		  else
		    //left side pointer, offset things to the right
		    for i as Integer = 0 to 3
		      corners(i).Offset pointerSize, 0
		    next i
		    self.Width = self.Width + pointerSize //make the window bigger
		    offsetControls pointerSize, 0 //offset the controls to keep them in the window
		    result.Append new REALbasic.Point(xScreen, yScreen - self.Height/2 - pointerOffset) //append the upper left corner to result
		  end if
		  
		  //Create the FigureShape beginning with the upper left corner.
		  //On each straight side, see if there is a pointer there.  If so, break the side and draw it.
		  dim fs as new GSFigureShape //GSFigureShape has AddLineTo and AddQuadTo to make my life easier.
		  
		  //Set the colors
		  fs.BorderColor = windowColor
		  fs.FillColor = windowColor
		  
		  //Top left roundrect
		  fs.AddQuad corners(0).x, corners(0).y + radius, corners(0).x + radius, corners(0).y, corners(0).x, corners(0).y
		  
		  //Top side, left to right
		  if pointerOnSide(0) then
		    //append the pointer vertices to result, then add them to fs
		    result.Append new REALbasic.Point((corners(0).x + corners(1).x)/2 - pointerSize/2 + pointerOffset, corners(0).y)
		    result.Append new REALbasic.Point((corners(0).x + corners(1).x)/2 + pointerOffset, corners(0).y - pointerSize)
		    result.Append new REALbasic.Point((corners(0).x + corners(1).x)/2 + pointerSize/2 + pointerOffset, corners(0).y)
		    fs.AddLineTo result(1).x, result(1).y
		    fs.AddLineTo result(2).x, result(2).y
		    fs.AddLineTo result(3).x, result(3).y
		  end if
		  //Continue the line to the next corner
		  fs.AddLineTo corners(1).x - radius, corners(1).y
		  
		  //Top right corner
		  fs.AddQuadTo corners(1).x, corners(1).y + radius, corners(1).x, corners(1).y
		  
		  //Right side, top to bottom
		  if pointerOnSide(1) then
		    //append the pointer vertices to result, then add them to fs
		    result.Append new REALbasic.Point(corners(1).x, (corners(1).y + corners(2).y)/2 - pointerSize/2 + pointerOffset)
		    result.Append new REALbasic.Point(corners(1).x + pointerSize, (corners(1).y + corners(2).y)/2 + pointerOffset)
		    result.Append new REALbasic.Point(corners(1).x, (corners(1).y + corners(2).y)/2 + pointerSize/2 + pointerOffset)
		    fs.AddLineTo result(1).x, result(1).y
		    fs.AddLineTo result(2).x, result(2).y
		    fs.AddLineTo result(3).x, result(3).y
		  end if
		  //Continue the line to the next corner
		  fs.AddLineTo corners(2).x, corners(2).y - radius
		  
		  //Lower right corner
		  fs.AddQuadTo corners(2).x - radius, corners(2).y, corners(2).x, corners(2).y
		  
		  //Lower side, right to left
		  if pointerOnSide(2) then
		    //append the pointer vertices to result, then add them to fs
		    result.Append new REALbasic.Point((corners(2).x + corners(3).x)/2 + pointerSize/2 + pointerOffset, corners(2).y)
		    result.Append new REALbasic.Point((corners(2).x + corners(3).x)/2 + pointerOffset, corners(2).y + pointerSize)
		    result.Append new REALbasic.Point((corners(2).x + corners(3).x)/2 - pointerSize/2 + pointerOffset, corners(2).y)
		    fs.AddLineTo result(1).x, result(1).y
		    fs.AddLineTo result(2).x, result(2).y
		    fs.AddLineTo result(3).x, result(3).y
		  end if
		  //Continue the line to the lower left
		  fs.AddLineTo corners(3).x + radius, corners(3).y
		  
		  //Lower left corner
		  fs.AddQuadTo corners(3).x, corners(3).y - radius, corners(3).x, corners(3).y
		  
		  //Left side, bottom to top
		  if pointerOnSide(3) then
		    //append the pointer vertices to result, then add them to fs
		    result.Append new REALbasic.Point(corners(3).x, (corners(3).y + corners(0).y)/2 + pointerSize/2 + pointerOffset)
		    result.Append new REALbasic.Point(corners(3).x - pointerSize, (corners(3).y + corners(0).y)/2 + pointerOffset)
		    result.Append new REALbasic.Point(corners(3).x, (corners(3).y + corners(0).y)/2 - pointerSize/2 + pointerOffset)
		    fs.AddLineTo result(1).x, result(1).y
		    fs.AddLineTo result(2).x, result(2).y
		    fs.AddLineTo result(3).x, result(3).y
		  end if
		  //Continue the line to the upper left corner (already drawn)
		  fs.AddLineTo corners(0).x, corners(0).y + radius
		  
		  //Append the corners of the just-drawn roundrect to result. These will be used in a Windows only dll call.
		  result.Append corners(0)
		  result.Append corners(2)
		  
		  //Store the shape in a property so we don't have to recalculate it during paint
		  windowFigureShape = new FigureShape
		  windowFigureShape = fs
		  
		  Return result
		  
		End Function
	#tag EndMethod


	#tag Note, Name = License
		Copyright (c) 2013, Gookin Software LLC
		
		Permission is hereby granted, free of charge, to any person obtaining a copy
		of this software and associated documentation files (the "Software"), to deal
		in the Software without restriction, including without limitation the rights
		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
		copies of the Software, and to permit persons to whom the Software is
		furnished to do so, subject to the following conditions:
		
		The above copyright notice and this permission notice shall be included in all
		copies or substantial portions of the Software.
		
		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
		SOFTWARE.
	#tag EndNote

	#tag Note, Name = Request
		1. Please make any improvements to this public - not required but appreciated.
		2. If you'd like to donate, you can send me money through PayPal at public@gookinsoftware.com - again, not required but appreciated.
	#tag EndNote


	#tag Property, Flags = &h0
		colorChange As Double = 1.1
	#tag EndProperty

	#tag Property, Flags = &h0
		pointerSize As Integer = 15
	#tag EndProperty

	#tag Property, Flags = &h0
		radius As Integer = 10
	#tag EndProperty

	#tag Property, Flags = &h0
		tag As Variant
	#tag EndProperty

	#tag Property, Flags = &h0
		upperLeftOffset As REALbasic.Point
	#tag EndProperty

	#tag Property, Flags = &h0
		windowColor As Color = &cCCCCCC
	#tag EndProperty

	#tag Property, Flags = &h0
		windowFigureShape As FigureShape
	#tag EndProperty

	#tag Property, Flags = &h0
		winShape As Picture
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="BackColor"
			Visible=true
			Group="Appearance"
			InitialValue="&hFFFFFF"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Backdrop"
			Visible=true
			Group="Appearance"
			Type="Picture"
			EditorType="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CloseButton"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="colorChange"
			Group="Behavior"
			InitialValue="1.1"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Composite"
			Visible=true
			Group="Appearance"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Frame"
			Visible=true
			Group="Appearance"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Document"
				"1 - Movable Modal"
				"2 - Modal Dialog"
				"3 - Floating Window"
				"4 - Plain Box"
				"5 - Shadowed Box"
				"6 - Rounded Window"
				"7 - Global Floating Window"
				"8 - Sheet Window"
				"9 - Metal Window"
				"10 - Drawer Window"
				"11 - Modeless Dialog"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="FullScreen"
			Group="Appearance"
			InitialValue="False"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="FullScreenButton"
			Visible=true
			Group="Appearance"
			InitialValue="False"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HasBackColor"
			Visible=true
			Group="Appearance"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Position"
			InitialValue="400"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ImplicitInstance"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Interfaces"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LiveResize"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MacProcID"
			Visible=true
			Group="Appearance"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MaxHeight"
			Visible=true
			Group="Position"
			InitialValue="32000"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MaximizeButton"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MaxWidth"
			Visible=true
			Group="Position"
			InitialValue="32000"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MenuBar"
			Visible=true
			Group="Appearance"
			Type="MenuBar"
			EditorType="MenuBar"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MenuBarVisible"
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MinHeight"
			Visible=true
			Group="Position"
			InitialValue="64"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MinimizeButton"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MinWidth"
			Visible=true
			Group="Position"
			InitialValue="64"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Placement"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Default"
				"1 - Parent Window"
				"2 - Main Screen"
				"3 - Parent Window Screen"
				"4 - Stagger"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="pointerSize"
			Group="Behavior"
			InitialValue="15"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="radius"
			Group="Behavior"
			InitialValue="10"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Resizeable"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Title"
			Visible=true
			Group="Appearance"
			InitialValue="Untitled"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Position"
			InitialValue="600"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="windowColor"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="winShape"
			Group="Behavior"
			Type="Picture"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
