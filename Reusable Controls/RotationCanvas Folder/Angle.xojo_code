#tag Class
Protected Class Angle
	#tag Method, Flags = &h0
		Sub Clone(toClone as Angle)
		  me.Value = toClone.Value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor()
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(val as double, inDegrees as boolean)
		  if inDegrees = true then
		    Value = val
		  else
		    Radians = val
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		 Shared Sub CorrectRotationBounds(byref r as double)
		  if r = 360 then
		    r = 0
		  elseif r > 360 then
		    while r > 360
		      r = r - 360
		    wend
		  elseif r < 0 then
		    while r < 0
		      r = r + 360
		    wend
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Radians() As Double
		  return (pi / 180) * value
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Radians(assigns val as double)
		  Value = (180.0 / Math.PI) * val
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetRawValue(value as double)
		  mValue = value
		  RaiseEvent AngleChanged()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		 Shared Function ToRadians(degrees as double) As double
		  dim a as new angle(degrees, true)
		  return a.Radians
		End Function
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event AngleChanged()
	#tag EndHook


	#tag Property, Flags = &h0
		#tag Note
			Either refer to this value from getter/setter or refer to this to save the processing of the angle.
		#tag EndNote
		mValue As Double
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  CorrectRotationBounds(value)
			  SetRawValue(value)
			End Set
		#tag EndSetter
		Value As Double
	#tag EndComputedProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="mValue"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Value"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
