#tag Class
Protected Class ScrollCanvas
Inherits BufferCanvas
	#tag Event
		Function MouseDown(X As Integer, Y As Integer) As Boolean
		  return MouseDown(x, y, CanvasToContentX(x), CanvasToContentY(y))
		End Function
	#tag EndEvent

	#tag Event
		Sub MouseDrag(X As Integer, Y As Integer)
		  MouseDrag x, y, CanvasToContentX(x), CanvasToContentY(y)
		End Sub
	#tag EndEvent

	#tag Event
		Sub MouseUp(X As Integer, Y As Integer)
		  MouseUp x, y, CanvasToContentX(x), CanvasToContentY(y)
		End Sub
	#tag EndEvent

	#tag Event
		Sub Paint(g as Graphics)
		  //update scrollbars
		  UpdateScrollRange
		  UpdateScrollbars
		  
		  //calculate computed width and height
		  dim w as integer = ComputedContentWidth
		  dim h as integer = ComputedContentHeight
		  
		  //store clipped g to keep reference of unclipped g
		  dim clippedg as graphics = g
		  
		  //parameters
		  dim sx, sy as integer //source top/left
		  dim sw, sh as integer //source width/height
		  dim cx, cy as integer //clip top/left
		  dim cw, ch as integer //clip width/height
		  dim bx, by as integer //border top/left
		  
		  dim clip as boolean = false
		  
		  if Width >= w then
		    
		    //whole content width visible
		    bx = (Width - w)/2
		    sx = 0
		    sw = ContentWidth
		    cx = bx
		    cw = w
		    clip = true
		    
		  else
		    
		    //part of content width visible
		    bx = - private_scrollx
		    cx = 0
		    cw = Width
		    sx = private_scrollx
		    sw = cw
		    
		  end
		  
		  if Height >= h then
		    
		    //whole content height visible
		    by = (Height - h)/2
		    sy = 0
		    sh = ContentHeight
		    cy = by
		    ch = h
		    clip = true
		    
		  else
		    
		    //part of content height visible
		    by = - private_scrolly
		    cy = 0
		    ch = Height
		    sy = private_scrolly
		    sh = ch
		    
		  end
		  
		  if clip then
		    
		    //paint border
		    PaintBorder g, bx, by, w, h
		    
		    //clip to visible area
		    clippedg= g.Clip(cx,cy,cw,ch)
		    
		  end
		  
		  //draw content
		  Paint clippedg, sx,sy,sw,sh
		  
		  UnclippedPaint g, CanvasToContentX(MouseX), CanvasToContentY(MouseY)
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Function CanvasToContentX(X As Integer) As Double
		  //map canvas coordinate to content region
		  dim w as integer = ComputedContentWidth
		  if width >= w then
		    return x - (width - w)/2
		  else
		    return x + private_scrollx
		  end
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CanvasToContentY(Y As Integer) As Double
		  //map canvas coordinate to content region
		  dim h as integer = ComputedContentHeight
		  if Height >= h then
		    return y - (Height - h)/2
		  else
		    return y + private_scrolly
		  end
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function ComputedContentHeight() As integer
		  //overloaded for ZoomCanvas
		  return private_height
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function ComputedContentWidth() As integer
		  //overloaded for ZoomCanvas
		  return private_width
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ContentToCanvasX(X As Integer) As Double
		  //map content coordinate to canvas
		  dim w as integer = ComputedContentWidth
		  if width >= w then
		    return x + (width - w)/2
		  else
		    return x - private_scrollx
		  end
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ContentToCanvasY(Y As Integer) As Double
		  //map content coordinate to canvas
		  dim h as integer = ComputedContentHeight
		  if height >= h then
		    return y + (height - h)/2
		  else
		    return y - private_scrolly
		  end
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function InCanvas(X As Integer, Y As Integer) As Boolean
		  if x < 0 or x > Width then return false
		  if y < 0 or y > Width then return false
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Scroll(DeltaX As Integer, DeltaY As Integer, Left As Integer = 0, Top As Integer = 0, Width As Integer = - 10000, Height As Integer = - 10000, ScrollControls As Boolean = True)
		  #pragma unused Left
		  #pragma unused Top
		  #pragma unused Width
		  #pragma unused Height
		  #pragma unused ScrollControls
		  
		  //scroll
		  private_scrollx = private_scrollx + DeltaX
		  private_scrolly = private_scrolly + DeltaY
		  UpdateScrollRange
		  UpdateScrollbars
		  Refresh false
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1001
		Protected Sub UpdateScrollbars()
		  //horizontal scroll
		  if HScroll <> nil then
		    if width >= ComputedContentWidth then
		      HScroll.Enabled = false
		      HScroll.SetRange 0, 0, 0
		    else
		      HScroll.Enabled = true
		      HScroll.SetRange 0, ComputedContentWidth - width, private_scrollx
		      HScroll.PageStep = width
		    end
		  end
		  
		  //vertical scroll
		  if VScroll <> nil then
		    if Height >= ComputedContentHeight then
		      VScroll.Enabled = false
		      VScroll.SetRange 0, 0, 0
		    else
		      VScroll.Enabled = true
		      VScroll.SetRange 0, ComputedContentHeight - Height, private_scrolly
		      VScroll.PageStep = Height
		    end
		  end
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub UpdateScrollRange()
		  private_scrollx = max(0,min(ComputedContentWidth - width,private_scrollx))
		  private_scrolly = max(0,min(ComputedContentHeight - Height,private_scrolly))
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event MouseDown(X As Integer, Y As Integer, ContentX As Double, ContentY As Double) As Boolean
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MouseDrag(X As Integer, Y As Integer, ContentX As Double, ContentY As Double)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MouseUp(X As Integer, Y As Integer, ContentX As Double, ContentY As Double)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Paint(g as Graphics, sx as Integer, sy as Integer, sw as Integer, sh as Integer)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event PaintBorder(g as Graphics, contentLeft as Integer, contentTop as Integer, contentWidth as Integer, contentHeight as Integer)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event UnclippedPaint(g as graphics, ContentX as integer, ContentY as integer)
	#tag EndHook


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return private_height
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  private_height = value
			  UpdateScrollbars
			  Refresh false
			End Set
		#tag EndSetter
		ContentHeight As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return private_width
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  private_width = value
			  UpdateScrollbars
			  Refresh false
			End Set
		#tag EndSetter
		ContentWidth As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  if private_hscroll = nil then return nil
			  return CanvasScrollBar(private_hscroll.Value)
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  private_hscroll = new WeakRef(value)
			  HScroll.Canvas = me
			  UpdateScrollbars
			End Set
		#tag EndSetter
		HScroll As CanvasScrollBar
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private private_height As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private private_hscroll As WeakRef
	#tag EndProperty

	#tag Property, Flags = &h21
		Private private_scrollx As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private private_scrolly As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private private_vscroll As WeakRef
	#tag EndProperty

	#tag Property, Flags = &h21
		Private private_width As Integer
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return private_scrollx
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  private_scrollx = value
			  UpdateScrollRange
			  Refresh false
			End Set
		#tag EndSetter
		ScrollX As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return private_scrolly
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  private_scrolly = value
			  UpdateScrollRange
			  Refresh false
			End Set
		#tag EndSetter
		ScrollY As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  if private_vscroll = nil then return nil
			  return CanvasScrollBar(private_vscroll.Value)
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  private_vscroll = new WeakRef(value)
			  VScroll.Canvas = me
			  UpdateScrollbars
			End Set
		#tag EndSetter
		VScroll As CanvasScrollBar
	#tag EndComputedProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="AcceptFocus"
			Visible=true
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AcceptTabs"
			Visible=true
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AutoDeactivate"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Backdrop"
			Visible=true
			Group="Appearance"
			Type="Picture"
			EditorType="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ContentHeight"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ContentWidth"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DoubleBuffer"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="EraseBackground"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Position"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Appearance"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			Type="Integer"
			EditorType="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="InitialParent"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ScrollX"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ScrollY"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabIndex"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabPanelIndex"
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabStop"
			Visible=true
			Group="Position"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Transparent"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="UseFocusRing"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Position"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
