#tag Class
Protected Class ImageCanvas
Inherits ZoomCanvas2
	#tag Event
		Sub Paint(g as Graphics, x as Integer, y as Integer, w as Integer, h as Integer, sx as Integer, sy as Integer, sw as Integer, sh as Integer)
		  #pragma BoundsChecking false
		  #pragma StackOverflowChecking false
		  #pragma NilObjectChecking false
		  '#pragma DisableAutoWaitCursor
		  
		  //drop out if no image
		  if private_image = nil then
		    return
		  end
		  
		  'dim hasMask as Boolean
		  'hasMask = image.Mask(false) <> nil
		  'if hasMask then
		  
		  g.DrawPicture PWindow.DrawCheckerboard(g.width,g.height), 0, 0
		  
		  'end
		  
		  //draw image
		  'g.UseOldRenderer = False
		  
		  g.DrawPicture private_image,x,y,w,h,sx,sy,sw,sh
		  
		  //this routine is very slow on current hardware but may be needed if support is
		  //dropped for useoldrenderer in a future version of realbasic
		  'DrawScaled g,private_image,x,y,w,h,sx,sy,sw,sh
		End Sub
	#tag EndEvent

	#tag Event
		Sub PaintBorder(g as Graphics, contentLeft as Integer, contentTop as Integer, contentWidth as Integer, contentHeight as Integer)
		  //clear background
		  g.ForeColor = &cAAAAAA
		  g.FillRect 0,0,g.Width,g.Height
		  
		  //draw border
		  g.ForeColor = &c000000
		  g.DrawRect contentLeft - 1, contentTop -1, contentWidth + 2, contentHeight + 2
		  
		  ContentPoint = new REALbasic.Point(contentLeft, contentTop)
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h1
		Protected Sub DrawScaled(g as Graphics, p as Picture, dx as integer, dy as integer, dw as integer, dh as integer, sx as integer, sy as integer, sw as integer, sh as integer)
		  //this routine is very slow on current hardware but may be needed if support is
		  //dropped for useoldrenderer in a future version of realbasic
		  
		  #pragma BoundsChecking false
		  #pragma StackOverflowChecking false
		  #pragma NilObjectChecking false
		  #pragma BackgroundTasks false
		  '#pragma DisableAutoWaitCursor
		  
		  //assumes square pixels, i.e. uniform scaling - won't work correctly if not
		  dim blockSize as Double = dw/sw
		  dim ceilBlock as integer = ceil(blockSize)
		  
		  if blockSize >= 2 then
		    //get image pixels
		    dim ir as RGBSurface = p.RGBSurface
		    
		    dim hasMask as Boolean
		    hasMask = image.Mask(false) <> nil
		    if hasMask then
		      
		      //get mask pixels
		      dim mr as RGBSurface = p.Mask.RGBSurface
		      
		      //create pixel stamp
		      static block as new Picture(1,1,32)
		      
		      //scale up without blurring
		      for i as integer = sh - 1 downto 0
		        for j as integer = sw - 1 downto 0
		          //source coordinates
		          dim x as integer = sx + j
		          dim y as integer = sy + i
		          dim maskc as color = mr.Pixel(x,y)
		          //optimisation if mask is opaque for this pixel
		          if maskc = &cFFFFFF then
		            //set colour
		            g.ForeColor = ir.Pixel(sx + j,sy + i)
		            //draw block
		            g.FillRect ceil(dx + j*blockSize), ceil(dy + i*blockSize), ceilBlock, ceilBlock
		          else
		            //set colour
		            block.Graphics.pixel(0,0) = ir.Pixel(x,y)
		            //set mask
		            block.mask.Graphics.pixel(0,0) = maskc
		            //draw block
		            g.drawpicture block, ceil(dx + j*blockSize), ceil(dy + i*blockSize), ceilBlock, ceilBlock, 0,0,1,1
		          end
		        next
		      next
		      
		    else
		      
		      //scale up without blurring
		      for i as integer = sh - 1 downto 0
		        for j as integer = sw - 1 downto 0
		          //set colour
		          g.ForeColor = ir.Pixel(sx + j,sy + i)
		          //draw block
		          g.FillRect ceil(dx + j*blockSize), ceil(dy + i*blockSize), ceilBlock, ceilBlock
		        next
		      next
		      
		    end
		  else
		    
		    //use built-in drawing to downscale or upscale < 2x
		    g.DrawPicture p, dx, dy, dw, dh, sx, sy, sw, sh
		    
		  end
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		ContentPoint As REALbasic.Point
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return private_image
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  private_image = value
			  ContentWidth = value.Width
			  ContentHeight = value.Height
			End Set
		#tag EndSetter
		Image As Picture
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private private_image As Picture
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="AcceptFocus"
			Visible=true
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AcceptTabs"
			Visible=true
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AutoDeactivate"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Backdrop"
			Visible=true
			Group="Appearance"
			Type="Picture"
			EditorType="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ContentHeight"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ContentWidth"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DoubleBuffer"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="EraseBackground"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Position"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Appearance"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Image"
			Group="Behavior"
			InitialValue="0"
			Type="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			Type="Integer"
			EditorType="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="InitialParent"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ScrollX"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ScrollY"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabIndex"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabPanelIndex"
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabStop"
			Visible=true
			Group="Position"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Transparent"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="UseFocusRing"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Position"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Zoom"
			Group="Behavior"
			InitialValue="0"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
