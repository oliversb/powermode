#tag Class
Protected Class ZoomCanvas2
Inherits ScrollCanvas
	#tag Event
		Sub Paint(g as Graphics, sx as Integer, sy as Integer, sw as Integer, sh as Integer)
		  //calculate zoomed width and height
		  dim w as integer = ComputedContentWidth
		  dim h as integer = ComputedContentHeight
		  
		  //parameters
		  dim dx, dy as integer //destination top/left
		  dim dw, dh as integer //destination width/height
		  
		  if Width >= w then
		    
		    //whole content width visible
		    dx = 0
		    dw = w
		    
		  else
		    
		    //part of content width visible
		    sx = floor(ScrollX/private_zoom)
		    dx = floor(sx*private_zoom) - ScrollX
		    sw = ceil((Width-dx)/private_zoom)
		    dw = ceil(sw*private_zoom)
		    
		  end
		  
		  if Height >= h then
		    
		    //whole content height visible
		    
		    
		    dy = 0
		    dh = h
		    
		  else
		    
		    //part of content height visible
		    sy = floor(ScrollY/private_zoom)
		    dy = floor(sy*private_zoom) - ScrollY
		    sh = ceil((Height-dy)/private_zoom)
		    dh = ceil(sh*private_zoom)
		    
		  end
		  
		  //draw content
		  Paint g, dx,dy,dw,dh,sx,sy,sw,sh
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Function CanvasToContentX(X As Integer) As Double
		  return super.CanvasToContentX(x)/private_zoom
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CanvasToContentY(Y As Integer) As Double
		  return super.CanvasToContentY(y)/private_zoom
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function ComputedContentHeight() As integer
		  return round(ContentHeight*private_zoom)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function ComputedContentWidth() As integer
		  return round(ContentWidth*private_zoom)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ContentToCanvasX(X As Integer) As Double
		  return super.ContentToCanvasX(x*private_zoom)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ContentToCanvasY(Y As Integer) As Double
		  return super.ContentToCanvasY(y*private_zoom)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function InContentRegion(X As Integer, Y As Integer) As Boolean
		  x = CanvasToContentX(x)
		  if x < 0 or x > ComputedContentWidth then return false
		  
		  y = CanvasToContentX(y)
		  if y < 0 or y > ComputedContentHeight then return false
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Zoom(X As Integer, Y As Integer, Zoom as Double)
		  //zoom in at a particular set of coordinates
		  
		  //get content coordinates of click
		  dim cx as integer = CanvasToContentX(x)
		  dim cy as integer = CanvasToContentY(y)
		  
		  //update zoom level
		  private_zoom = private_zoom * Zoom
		  
		  //get canvas coordinates after zoom
		  dim wx as integer = ContentToCanvasX(cx)
		  dim wy as integer = ContentToCanvasY(cy)
		  
		  //get scroll distance required to centre those coordinates
		  dim sx as integer = wx - width/2
		  dim sy as integer = wy - Height/2
		  
		  //scroll and refresh view
		  Scroll sx, sy
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Paint(g as Graphics, x as Integer, y as Integer, w as Integer, h as Integer, sx as Integer, sy as Integer, sw as Integer, sh as Integer)
	#tag EndHook


	#tag Property, Flags = &h21
		Private private_zoom As double = 1.0
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return private_zoom
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  private_zoom = value
			  UpdateScrollbars
			  Refresh false
			End Set
		#tag EndSetter
		Zoom As Double
	#tag EndComputedProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="AcceptFocus"
			Visible=true
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AcceptTabs"
			Visible=true
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AutoDeactivate"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Backdrop"
			Visible=true
			Group="Appearance"
			Type="Picture"
			EditorType="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ContentHeight"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ContentWidth"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DoubleBuffer"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="EraseBackground"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Position"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Appearance"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			Type="Integer"
			EditorType="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="InitialParent"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ScrollX"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ScrollY"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabIndex"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabPanelIndex"
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabStop"
			Visible=true
			Group="Position"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Transparent"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="UseFocusRing"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Position"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Zoom"
			Group="Behavior"
			InitialValue="0"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
