#tag Class
Protected Class CanvasObject
	#tag Method, Flags = &h0
		Function Bottom() As Integer
		  Return Top + theResizableObject.Height
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(x as integer = 0, y as integer = 0)
		  Grouper = new Group
		  Grouper.Left = x
		  Grouper.Top = y
		  
		  AddHandler Grouper.GetWidth, AddressOf GetGrouperWidth
		  AddHandler Grouper.GetHeight, AddressOf GetGrouperHeight
		  
		  AddHandler Grouper.GetContains, AddressOf GetGrouperContains
		  
		  me.Selected = true
		  //add resize handle
		  theResizableObject = new ResizableObject
		  Grouper.Append(theResizableObject)
		  
		  ScaleX = new ValueChangeTracker(1)
		  ScaleY = new ValueChangeTracker(1)
		  Visible = new ValueChangeTracker(true)
		  Opacity = new ValueChangeTracker(100)
		  Layer = new ValueChangeTracker(0)
		  Rotation = new ValueChangeTracker(new Angle)
		  Presistant = new ValueChangeTracker(false)
		  
		  AddHandler Rotation.Value.AngleChanged, AddressOf RotationChanged
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Copy(toCopy as CanvasObject)
		  dim index as integer
		  dim vals() as ValueChangeTracker = toCopy.Values
		  //copy all properties from passed CanvasObject
		  for each v as ValueChangeTracker in Values
		    v.Copy = vals(index)
		    index = index + 1
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CopyChanges(toCopy as CanvasObject)
		  dim index as integer
		  dim vals() as ValueChangeTracker = toCopy.Values
		  //only copy changed properties
		  for each v as ValueChangeTracker in Values
		    if vals(index).Changed then
		      v.Copy = vals(index)
		    end if
		    index = index + 1
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DisableAllChangedValues()
		  for each v as ValueChangeTracker in Values
		    v.Changed = false
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function EmptySprite() As boolean
		  //see if there is a sprite attatched and then see if there are frames attatched to the sprite
		  return ((me.theObject.Sprite <> nil and me.theObject.Sprite.Frames.Ubound <> -1) = false)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetGrouperContains(sender as Group, x as integer, y as integer) As MyVariant
		  #pragma unused sender
		  
		  dim value as new MyVariant
		  debug sender.left
		  theResizableObject.Left = sender.Left
		  theResizableObject.Top = sender.Top
		  value.BooleanValue = theResizableObject.Contains(x, y)//x - sender.Left, y - sender.Top)
		  value.BooleanValue = theResizableObject.Contains(x - sender.Left, y - sender.Top)
		  theResizableObject.Left = 0
		  theResizableObject.Top = 0
		  return value
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetGrouperHeight(sender as Group) As MyVariant
		  #pragma unused sender
		  
		  dim value as new MyVariant
		  value.IntValue = me.Height
		  return value
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetGrouperWidth(sender as Group) As MyVariant
		  #pragma unused sender
		  
		  dim value as new MyVariant
		  value.IntValue = me.Width
		  return value
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function OriginalPicture() As Picture
		  return me.theObject.Sprite.Frames(0).SourceImage
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ReadNativeXml(node as XmlNode)
		  node.SetAttribute("Index", str(docfile.Resources.IndexOf(me.theObject)))
		  node.SetAttribute("X", str(me.Left))
		  node.SetAttribute("Y", str(me.Top))
		  node.SetAttribute("ScaleX", me.ScaleX.Value.StringValue)
		  node.SetAttribute("ScaleY", me.ScaleY.Value.StringValue)
		  node.SetAttribute("Opacity", me.Opacity.Value.StringValue)
		  node.SetAttribute("Visible", me.Visible.Value.StringValue)
		  node.SetAttribute("Rotation", me.Rotation.Value.StringValue)
		  node.SetAttribute("Layer", me.Layer.Value.StringValue)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Rescale()
		  #if not DebugBuild then
		    #pragma BackgroundTasks false
		    #pragma BoundsChecking false
		    #pragma NilObjectChecking false
		    #pragma StackOverflowChecking false
		  #endif
		  
		  //check if sprite and first frame are not nil
		  if me.theObject.Sprite <> nil and me.theObject.Sprite.Frames(0) <> nil then
		    dim width as integer = Abs(OriginalPicture.Width * ScaleX.Value.DoubleValue)
		    dim height as integer = Abs(OriginalPicture.Height * ScaleY.Value.DoubleValue)
		    
		    me.SourceImage = me.theObject.Sprite.Frames(0).SourceImageFlipped
		    
		    //scale the image
		    dim newPic as picture = new picture(width, height)
		    newPic.Graphics.DrawPicture(SourceImage, 0, 0, width, height, _
		    0, 0, SourceImage.Width, SourceImage.Height)
		    
		    theResizableObject.Image = newPic
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function reutnr(sender as Group) As Group
		  #pragma unused sender
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Right() As Integer
		  Return Left + theResizableObject.Width
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RotationChanged(sender as MyVariant = nil)
		  dim angle as Angle
		  
		  if sender <> nil then
		    angle = sender.AngleValue
		  else
		    angle = Rotation.Value.AngleValue
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetOpacity(value as double)
		  Opacity.Value.DoubleValue = value
		  theResizableObject.Opacity = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetRotation(value as double)
		  Rotation.Value.AngleValue.Value = value
		  Grouper.Angle.Value = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetScale(x as single, y as single)
		  if me.theObject.Sprite <> nil and me.Visible.Value.BooleanValue = true then
		    if x <> ScaleX.Value.DoubleValue or y <> ScaleY.Value.DoubleValue then
		      dim ScaleXNegativeBefore as boolean = (ScaleX.Value.DoubleValue < 0)
		      dim ScaleYNegativeBefore as boolean = (ScaleY.Value.DoubleValue < 0)
		      
		      ScaleX.Value.DoubleValue = x
		      ScaleY.Value.DoubleValue = y
		      if ScaleX.Value.DoubleValue = 0 then
		        ScaleX.Value.DoubleValue = 1
		      end if
		      if ScaleY.Value.DoubleValue = 0 then
		        ScaleY.Value.DoubleValue = 1
		      end if
		      
		      //check if sprite and first frame are not nil
		      if me.theObject.Sprite <> nil and me.theObject.Sprite.Frames.Ubound <> -1  then
		        Dim NewPic As Picture
		        
		        //if either scalex or scaley are less than 0 then the image shall be flipped
		        if (ScaleXNegativeBefore <> (ScaleX.Value.DoubleValue < 0) or ScaleYNegativeBefore <> (ScaleY.Value.DoubleValue < 0)) then
		          if ScaleX.Value.DoubleValue < 0 then
		            NewPic = OriginalPicture.MirrorH
		          end if
		          if ScaleY.Value.DoubleValue < 0 then
		            //check if it has already been mirrored horizontally
		            if NewPic = nil then
		              NewPic = OriginalPicture.MirrorV
		            else
		              NewPic = NewPic.MirrorV
		            end if
		          end if
		          me.theObject.Sprite.Frames(0).SourceImageFlipped = NewPic
		        end if
		        
		        Rescale
		      end if
		      
		    end if
		  else
		    ScaleX.Value.DoubleValue = x
		    ScaleY.Value.DoubleValue = y
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetScaleX(x as single)
		  SetScale(x, ScaleY.Value.DoubleValue)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetScaleY(y as single)
		  SetScale(ScaleX.Value.DoubleValue, y)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateProperty(toCopy as CanvasObject, propertyToUpdate as string)
		  //only copy a specific property
		  select case propertyToUpdate
		  case "Scale X"
		    ScaleX.Copy = toCopy.ScaleX
		  case "Scale Y"
		    ScaleY.Copy = toCopy.ScaleY
		  case "Visible"
		    Visible.Copy = toCopy.Visible
		  case "Opacity"
		    Opacity.Copy = toCopy.Opacity
		  case "Layer"
		    Layer.Copy = toCopy.Layer
		  case "Rotation"
		    Rotation.Copy = toCopy.Rotation
		  end select
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Values() As ValueChangeTracker()
		  if mValues.Ubound = -1 then
		    mValues.Append ScaleX
		    mValues.Append ScaleY
		    mValues.Append Visible
		    mValues.Append Opacity
		    mValues.Append Layer
		    mValues.Append Rotation
		    mValues.Append Presistant
		  end if
		  
		  return mValues
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub WriteNativeXml(ByRef node as XmlNode)
		  node.SetAttribute("Index", str(docfile.Resources.IndexOf(me.theObject)))
		  node.SetAttribute("X", str(me.Left))
		  node.SetAttribute("Y", str(me.Top))
		  node.SetAttribute("ScaleX", me.ScaleX.Value.StringValue)
		  node.SetAttribute("ScaleY", me.ScaleY.Value.StringValue)
		  node.SetAttribute("Opacity", me.Opacity.Value.StringValue)
		  node.SetAttribute("Visible", me.Visible.Value.StringValue)
		  node.SetAttribute("Rotation", me.Rotation.Value.StringValue)
		  node.SetAttribute("Layer", me.Layer.Value.StringValue)
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		backupObjectIndex As Integer = -1
	#tag EndProperty

	#tag Property, Flags = &h0
		Caption As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Grouper As Group
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return theResizableObject.Height
			End Get
		#tag EndGetter
		Height As Integer
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		Layer As ValueChangeTracker
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return Grouper.Left
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  Grouper.Left = value
			  
			End Set
		#tag EndSetter
		Left As Integer
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		Locked As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private Moved As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mValues() As ValueChangeTracker
	#tag EndProperty

	#tag Property, Flags = &h0
		OffsetX As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		OffsetY As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		Opacity As ValueChangeTracker
	#tag EndProperty

	#tag Property, Flags = &h0
		Presistant As ValueChangeTracker
	#tag EndProperty

	#tag Property, Flags = &h0
		Rotation As ValueChangeTracker
	#tag EndProperty

	#tag Property, Flags = &h0
		ScaleX As ValueChangeTracker
	#tag EndProperty

	#tag Property, Flags = &h0
		ScaleY As ValueChangeTracker
	#tag EndProperty

	#tag Property, Flags = &h0
		Selected As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		SetOffset As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		SourceImage As Picture
	#tag EndProperty

	#tag Property, Flags = &h0
		theObject As ResourceObject
	#tag EndProperty

	#tag Property, Flags = &h0
		theResizableObject As ResizableObject
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return Grouper.Top
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  Grouper.Top = value
			  Moved = true
			End Set
		#tag EndSetter
		Top As Integer
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		#tag Note
			Indicates wether the CanvasObject belongs visually on the scene
		#tag EndNote
		Visible As ValueChangeTracker
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return theResizableObject.Width
			End Get
		#tag EndGetter
		Width As Integer
	#tag EndComputedProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="backupObjectIndex"
			Group="Behavior"
			InitialValue="-1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Caption"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="OffsetX"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="OffsetY"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Selected"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="SetOffset"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="SourceImage"
			Group="Behavior"
			Type="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
