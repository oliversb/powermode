#tag Class
Protected Class SceneLayer
	#tag Method, Flags = &h0
		Sub Append(value as CanvasObject)
		  CanvasObjects.Append(value)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(layer as integer)
		  me.Layer = layer
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		CanvasObjects() As CanvasObject
	#tag EndProperty

	#tag Property, Flags = &h0
		Layer As Integer
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Layer"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
