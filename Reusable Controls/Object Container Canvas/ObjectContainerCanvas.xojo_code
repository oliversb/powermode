#tag Class
Protected Class ObjectContainerCanvas
Inherits ZoomCanvas
	#tag Event
		Function MouseDown(X As Integer, Y As Integer) As Boolean
		  MouseDownOnNothing = true
		  
		  // Find the object that is where the user just clicked
		  
		  Dim theX As Integer
		  Dim theY As Integer
		  
		  For Each m As CanvasObject In CanvasObjects
		    m.SetOffset = false
		  Next
		  
		  for i as integer = CanvasObjects.Ubound downto 0
		    dim m as CanvasObject = CanvasObjects(i)
		    if m.Visible.Value.BooleanValue then
		      theX = x - m.Left
		      theY =  y - m.Top
		      if m.Grouper.Contains(X, Y) Then
		        //only add to selected objects if not already selected
		        if m.Selected = false then
		          DeselectAll
		          AppendSelectedObject m
		          m.Selected = true
		          ObjectSelected(m, false)
		        end if
		        
		        MouseDownOnNothing = false
		        if m.theResizableObject.MouseDown(theX, theY, true) then
		          exit for
		        end if
		      end if
		    end if
		  next
		  
		  
		  if IsContextualClick then
		    ContextualClickOnObject
		  end if
		  
		  if MouseDownOnNothing then
		    theSelectionRectangle.MouseDown(x, y)
		    Invalidate(false)
		  end if
		  return true
		End Function
	#tag EndEvent

	#tag Event
		Sub MouseDrag(X As Integer, Y As Integer)
		  dim theX, theY as integer
		  
		  if MouseDownOnNothing then
		    theSelectionRectangle.MouseDrag(x, y)
		    DeselectAll
		    
		    for each co as CanvasObject in CanvasObjects
		      if theSelectionRectangle.SelectionRectangle.Intersects(co.Grouper) then
		        AppendSelectedObject co
		        co.Selected = true
		        ObjectSelected(co, true)
		      else
		        co.Selected = false
		      end if
		    next
		  else
		    for each co as CanvasObject in SelectedObjects
		      //if dragging but not dragging small rectangle
		      if co.locked = false then
		        if AllowManipulations then
		          if co.theResizableObject.Pos <> 0 and co.theObject.Sprite <> nil and co.theObject.Sprite.Frames.Ubound <> -1 then
		            co.Rescale
		            
		            theX = x - co.Left
		            theY =  y - co.Top
		            
		            co.theResizableObject.MouseDrag(theX, theY)
		            
		            dim ScaleXNegative as boolean = (co.ScaleX.Value.DoubleValue < 0)
		            dim ScaleYNegative as boolean = (co.ScaleY.Value.DoubleValue < 0)
		            
		            co.ScaleX.Value.DoubleValue = co.theResizableObject.Width / co.theObject.Sprite.Frames(0).SourceImage.Width
		            co.ScaleY.Value.DoubleValue = co.theResizableObject.Height / co.theObject.Sprite.Frames(0).SourceImage.Height
		            //if it was originally a negative value, make it an negative value
		            if ScaleXNegative then
		              co.ScaleX.Value.DoubleValue = -(co.ScaleX.Value.DoubleValue)
		            end if
		            if ScaleYNegative then
		              co.ScaleY.Value.DoubleValue = -(co.ScaleY.Value.DoubleValue)
		            end if
		            exit for
		            
		          else
		            if co.SetOffset = false then
		              co.SetOffset = true
		              co.OffsetX = X - co.Left
		              co.OffsetY = Y - co.Top
		            elseif (LastX <> X Or LastY <> Y ) Then
		              if res.SnapEnabled then
		                co.Left = Round((X - co.OffsetX) / res.SnapX) * res.SnapY
		                co.Top = Round((Y - co.OffsetY) / res.SnapY) * res.SnapY
		              else
		                co.Left = X - co.OffsetX
		                co.Top = Y - co.OffsetY
		              end if
		            end if
		          end if
		        end if
		      end if
		    next
		  end if
		  
		  LastX = X
		  LastY = Y
		  
		  Invalidate(False)
		End Sub
	#tag EndEvent

	#tag Event
		Sub MouseMove(X As Integer, Y As Integer)
		  'for each co as canvasobject in selectedobjects
		  'if co.theObject.Sprite <> nil and co.theObject.Sprite.Frames.Ubound <> -1 then
		  'dim theX as integer = x - co.Left
		  'dim theY as integer = y - co.Top
		  '
		  'if( x >= co.Left) and (x <= co.Left + co.Width) and _
		  '(y >= co.Top) and (y <= co.Top + co.Width) then
		  'else
		  'co.theResizableObject.MouseExit
		  'end if
		  
		  'if co.theResizableObject.DraggingBigRect(theX,theY) = false then
		  'for each ms as MouseRect in co.theResizableObject.MouseRects
		  'if ms.Contains(theX,theY) then
		  'select case ms.pos
		  'case 0
		  'App.MouseCursor = System.Cursors.ArrowNorthSouth
		  'return
		  'case 1
		  'App.MouseCursor = System.Cursors.ArrowEastWest
		  'return
		  'case 2
		  'App.MouseCursor = System.Cursors.ArrowNorthwestSoutheast
		  'return
		  'end select
		  'end if
		  'next
		  '
		  'end if
		  'end if
		  'next
		  
		  'App.MouseCursor = nil
		End Sub
	#tag EndEvent

	#tag Event
		Sub MouseUp(X As Integer, Y As Integer)
		  #pragma unused x
		  #pragma unused y
		  
		  theSelectionRectangle.MouseUp
		  
		  if MouseDownOnNothing then
		    if SelectedObjects.Ubound = -1 then
		      DeselectAll
		      MouseDownOnNothing = false
		      MouseUpOnNothing(x, y)
		      me.Invalidate
		      return
		    end if
		    for each co as canvasobject in SelectedObjects
		      ObjectMoved(co)
		    next
		  End If
		  
		  ResizableObject.MouseUp
		  
		  me.Invalidate
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  theSelectionRectangle = new SelectionRectangle
		  Open()
		End Sub
	#tag EndEvent

	#tag Event
		Sub Paint(g As Graphics, areas() As REALbasic.Rect)
		  #pragma unused areas
		  
		  //draw checkerboard
		  g.DrawPicture DrawCheckerboard(me.width, me.height),  0, 0,  me.width, me.height
		  
		  
		  //draw zoomed
		  dim p as new picture(g.Width, g.Height)
		  //draw scene
		  p.graphics.ForeColor = res.Colour
		  p.graphics.FillRect 0, 0, res.SceneWidth, res.SceneHeight
		  if res.Background <> nil and res.Background.Frames.Ubound <> -1 then
		    p.graphics.DrawPicture(res.Background.Frames(0).SourceImage)
		  end if
		  
		  //draw grid
		  if res.SnapEnabled then
		    p.graphics.DrawPicture res.GridPic, 0, 0
		  end if
		  
		  Paint(p.Graphics)
		  
		  //draw the background if there is one
		  If Background <> nil then
		    g.DrawPicture(Background, 0, 0)
		  end if
		  
		  CanvasObjects.SortClassArrayByValue("Layer")
		  
		  //draw all CanvasObjects
		  for each co as CanvasObject in CanvasObjects
		    Draw(p.Graphics, co)
		  Next
		  
		  if res.Zoom = 100 then
		    g.DrawPicture(p, 0, 0)
		  else
		    //set zoom to minimum if below it
		    if res.zoom < popoverZoom.MinZoom then
		      res.zoom = popoverZoom.MinZoom
		    end if
		    #pragma BreakOnExceptions false
		    try
		      dim resizedPic as picture = ImagePlayEffectsLibrary.Resize(p, p.width * (res.Zoom / 100), p.height * (res.Zoom / 100), ImagePlayEffectsLibrary.kResizeModeByPixel)
		      
		      g.DrawPicture(resizedPic, 0, 0)
		    catch OutOfBoundsException
		    end try
		  end if
		  
		  theSelectionRectangle.Paint(g)
		End Sub
	#tag EndEvent

	#tag Event
		Sub ZoomComplete(zoom as integer)
		  res.Zoom = zoom
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub AddObject(m as CanvasObject)
		  // Adds a new object to the canvas and selects it.
		  
		  dim sceneProperties1 as scenePropertiesContainer = SceneEditor(me.window).sceneProperties1.scenePropertiesContainer1
		  dim p as picture
		  
		  //check if there is an object selected for adding
		  if sceneProperties1.CurrentObject <> nil then
		    //check if there is a sprite assigned to the currentobject
		    if sceneProperties1.CurrentObject.Sprite <> nil then
		      //check if there is anything contained within the sprite
		      if sceneProperties1.CurrentObject.Sprite.Frames.Ubound <> -1 then
		        //add the object (using original image)
		        p = sceneProperties1.CurrentObject.Sprite.Frames(0).SourceImage
		      end if
		    end if
		  end if
		  
		  if p = nil then
		    m.theResizableObject.Image = Icon("Sprite")
		  else
		    m.SourceImage = p
		    m.theResizableObject.Image = p
		  end if
		  
		  dim ms as MouseRect
		  
		  m.Grouper.Append(m.theResizableObject.Grouper)
		  
		  for i as integer= 0 to MouseRect.PosCount
		    ms = new MouseRect(m.theResizableObject.Grouper, m.theResizableObject)
		    ms.Pos = i
		    m.theResizableObject.MouseRects.Append ms
		  next
		  
		  CanvasObjects.Append(m)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1000
		Sub AddObject(caption As String = "", x as integer, y as integer, CurrentObject As ResourceObject)
		  dim m as new CanvasObject(x, y)
		  m.Caption = caption
		  m.theObject = CurrentObject
		  
		  //apply defaults brought from the object
		  dim co as CanvasObject = m.theObject.DefaultCanvasObject
		  
		  m.Visible.Value.BooleanValue = co.Visible.Value.BooleanValue
		  m.Presistant.Value.BooleanValue = co.Presistant.Value.BooleanValue
		  
		  m.Layer.Value.IntValue = co.Layer.Value.IntValue
		  m.Opacity.Value.IntValue = co.Opacity.Value.DoubleValue
		  
		  m.ScaleX.Value.DoubleValue = co.ScaleX.Value.DoubleValue
		  m.ScaleY.Value.DoubleValue = co.ScaleY.Value.DoubleValue
		  
		  m.DisableAllChangedValues
		  
		  AddObject(m)
		  SelectObject m
		  
		  m.SetRotation co.Rotation.Value.AngleValue.Value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AppendSelectedObject(co as canvasobject)
		  if co.Visible.Value.BooleanValue then
		    SelectedObjects.Append co
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DeselectAll(triggerEvent as boolean = true)
		  //Deselects all the objects on the canvas.
		  
		  if SelectedObjects.Ubound <> -1 then
		    For Each m As CanvasObject In SelectedObjects
		      m.Selected = False
		    Next
		  else
		    For Each m As CanvasObject In CanvasObjects
		      m.Selected = False
		    Next
		  end if
		  
		  redim SelectedObjects(-1)
		  
		  if triggerEvent then
		    RaiseEvent DeselectedAll()
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub Draw(g As Graphics, co As CanvasObject)
		  //ignore visuals if CanvasObject is not visible or not placed on the scene
		  if co.Visible.Value.BooleanValue then
		    // Draws an object on the canvas
		    
		    //Draw its image
		    if co.locked = false then
		      if co.EmptySprite then
		        co.theResizableObject.Image = Icon("Sprite")
		      end if
		    end if
		    
		    if co.Locked then
		      //set locked icon
		      co.theResizableObject.Image = Icon("Lock")
		    elseif co.EmptySprite then
		      //set empty icon
		      co.theResizableObject.Image = Icon("Sprite")
		    end if
		    
		    for each ms as MouseRect in co.theResizableObject.MouseRects
		      ms.Update
		    next
		    
		    //set visibility of the resize handles depending on wether the object instance is selected or not
		    co.theResizableObject.Grouper.Visible = co.Selected
		  end if 
		  
		  co.RotationChanged
		  g.DrawObject(co.Grouper)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MoveObject(co As CanvasObject, left As Integer, top As Integer)
		  If co <> Nil Then
		    co.Left = left
		    co.Top = top
		    
		    ObjectMoved(co)
		    
		    Invalidate(False)
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MoveObject(index As Integer, left As Integer, top As integer)
		  // Moves the specified object to the specified position
		  
		  If index >= 0 And index <= CanvasObjects.Ubound Then
		    MoveObject(CanvasObjects(index), left, top)
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SelectObject(value as CanvasObject)
		  DeselectAll
		  
		  'For Each m As CanvasObject In CanvasObjects
		  'If m Is value Then
		  'm.Selected = True
		  'End If
		  'Next
		  
		  value.Selected = true
		  
		  AppendSelectedObject value
		  
		  ObjectSelected(value, false)
		  
		  Invalidate(False)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SelectObject(index As Integer)
		  // Select the specified object
		  
		  If index >= 0 And index <= CanvasObjects.Ubound Then
		    DeselectAll
		    AppendSelectedObject CanvasObjects(index)
		  End If
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event ContextualClickOnObject()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event DeselectedAll()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MouseUpOnNothing(x as integer, y as integer)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ObjectMoved(co As CanvasObject)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ObjectSelected(co As CanvasObject, selecting As Boolean)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Paint(g as graphics)
	#tag EndHook


	#tag Property, Flags = &h0
		AllowManipulations As Boolean = true
	#tag EndProperty

	#tag Property, Flags = &h0
		Background As Picture
	#tag EndProperty

	#tag Property, Flags = &h0
		CanvasObjects() As CanvasObject
	#tag EndProperty

	#tag Property, Flags = &h21
		Private LastX As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private LastY As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private MouseDownOnNothing As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private Moved As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private PrevSprite As PhotoThumbnail
	#tag EndProperty

	#tag Property, Flags = &h0
		res As ResourceScene
	#tag EndProperty

	#tag Property, Flags = &h0
		SelectedObjects() As CanvasObject
	#tag EndProperty

	#tag Property, Flags = &h21
		Private theSelectionRectangle As SelectionRectangle
	#tag EndProperty

	#tag Property, Flags = &h0
		WithinDraggingArea As Boolean
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="AcceptFocus"
			Visible=true
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AcceptTabs"
			Visible=true
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AllowManipulations"
			Visible=true
			Group="Behavior"
			InitialValue="true"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AutoDeactivate"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Backdrop"
			Visible=true
			Group="Appearance"
			Type="Picture"
			EditorType="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Background"
			Group="Behavior"
			Type="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DoubleBuffer"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="EraseBackground"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Position"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Appearance"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			Type="Integer"
			EditorType="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="InitialParent"
			Group="Position"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabIndex"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabPanelIndex"
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabStop"
			Visible=true
			Group="Position"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Transparent"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="UseFocusRing"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Position"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="WithinDraggingArea"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
