#tag Class
Protected Class LiveResizeCanvas
Inherits Canvas
	#tag Event
		Function MouseDown(X As Integer, Y As Integer) As Boolean
		  if theResizableObject.MouseDown(x, y) then
		    MouseDown()
		    return true
		  end if
		End Function
	#tag EndEvent

	#tag Event
		Sub MouseDrag(X As Integer, Y As Integer)
		  #pragma unused x
		  #pragma unused y
		  
		  me.width = theResizableObject.width
		  me.height = theResizableObject.height
		  Resize
		End Sub
	#tag EndEvent

	#tag Event
		Sub MouseExit()
		  theResizableObject.MouseExit()
		End Sub
	#tag EndEvent

	#tag Event
		Sub MouseMove(X As Integer, Y As Integer)
		  theResizableObject.MouseMove(x,y)
		End Sub
	#tag EndEvent

	#tag Event
		Sub MouseUp(X As Integer, Y As Integer)
		  #pragma unused x
		  #pragma unused y
		  
		  theResizableObject.MouseUp
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  DoubleBuffer = true
		  EraseBackground = false
		  
		  me.width =  FrameDialog.size + MouseRect.Size
		  me.height =  FrameDialog.size + MouseRect.Size
		  
		  theResizableObject = new ResizableObject(me)
		End Sub
	#tag EndEvent

	#tag Event
		Sub Paint(g As Graphics, areas() As REALbasic.Rect)
		  #pragma unused areas
		  
		  theResizableObject.Paint(g)
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub Resize()
		  dim PendingFrameDialogWidth as UInt32 = me.left+me.Width
		  if dragged and PendingFrameDialogWidth > me.window.MinWidth then
		    me.window.Width =  PendingFrameDialogWidth
		  else
		    me.window.Width = me.window.MinWidth
		  end if
		  
		  dim PendingFrameDialogHeight as UInt32 = me.WinLastHeight+me.Height-theResizableObject.LiveResizeLastHeight
		  if dragged and PendingFrameDialogHeight > me.window.MinHeight then
		    me.window.Height = PendingFrameDialogHeight
		  else
		    me.window.Height = me.window.MinHeight
		    dragged = true
		  end if
		  'SetTextboxes
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetDimensions()
		  //from resize of the window
		  'me.Width = me.window.Width-me.Left
		  'me.Height = me.window.Height-me.Top
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetTextboxes()
		  if val(FrameDialog.WidthInput.Text) <> me.width then
		    FrameDialog.WidthInput.Text = str(me.width - MouseRect.Size)
		  end if
		  if val(FrameDialog.HeightInput.Text) <> me.height then
		    FrameDialog.HeightInput.Text = str(me.height - MouseRect.Size)
		  end if
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event MouseDown()
	#tag EndHook


	#tag Property, Flags = &h0
		Shared dragged As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		theResizableObject As ResizableObject
	#tag EndProperty

	#tag Property, Flags = &h0
		WinLastHeight As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		WinLastWidth As Integer
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="AcceptFocus"
			Visible=true
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AcceptTabs"
			Visible=true
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AutoDeactivate"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Backdrop"
			Visible=true
			Group="Appearance"
			Type="Picture"
			EditorType="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DoubleBuffer"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="EraseBackground"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Position"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Appearance"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			Type="Integer"
			EditorType="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="InitialParent"
			Group="Position"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabIndex"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabPanelIndex"
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabStop"
			Visible=true
			Group="Position"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Transparent"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="UseFocusRing"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Position"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="WinLastHeight"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="WinLastWidth"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
