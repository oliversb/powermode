#tag Class
Protected Class PhotoThumbnail
Inherits FGThumbnailClass
Implements FGThumbnail
	#tag Method, Flags = &h1000
		Sub Constructor(Image as Picture, mText as String)
		  // Calling the overridden superclass constructor.
		  Super.Constructor
		  
		  Text = mText
		  if image <> nil then
		    SourceImage = Image
		    return
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Image(Selected as Boolean = False) As Picture
		  // Part of the FGThumbnail interface.
		  
		  // Return the graphical representation of this thumbnail for display in the canvas.
		  
		  if not selected then
		    return MyImage
		  else
		    return SelectedImage
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub Invert(byref p as picture)
		  #pragma BackgroundTasks false
		  #pragma BoundsChecking false
		  #pragma NilObjectChecking false
		  #pragma StackOverflowChecking false
		  
		  dim i, m(255) as Integer
		  dim s as RGBSurface
		  
		  if p = nil then return
		  
		  s = p.RGBSurface
		  if s = nil then return
		  
		  for i = 0 to 255
		    m(i) = 255-i
		  next i
		  
		  s.Transform(m)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Update(CellSize as Integer, ForceUpdate as Boolean = False)
		  // Part of the FGThumbnail interface.
		  // Our thumbnail canvas' dimensions have changed.  We need to scale our current image to fit within the maximum size specified.
		  
		  dim p as Picture
		  dim textW as integer
		  
		  // Do we need to redraw the image?
		  if not forceUpdate and MyImage <> nil and MyImage.Width = CellSize and MyImage.Height = CellSize then return ' we have a valid image and nothing has changed
		  
		  // Create a new MyImage
		  MyImage = new Picture(CellSize, CellSize, 32)
		  MyImage.Transparent = Picture.TransparentWhite
		  
		  // Fill with our background colour
		  MyImage.Graphics.ForeColor = &c676767
		  MyImage.Graphics.FillRect(0, 0, CellSize, CellSize)
		  
		  // Get the image to draw and scale it as necessary
		  if MyImage = nil then
		    return
		  end if
		  p = ScalePicture(SourceImage, CellSize, CellSize-20)
		  
		  // Draw the scaled picture, centred on MyImage
		  if p <> nil then
		    MyImage.Graphics.DrawPicture(p, (CellSize/2)-(p.Width/2), ((CellSize-20)/2)-(p.Height/2))
		    MyImage.Mask.Graphics.FillRect((CellSize/2)-(p.Width/2), ((CellSize-20)/2)-(p.Height/2), p.Width, p.Height)
		    p = nil
		  end if
		  
		  // Draw the text
		  MyImage.Graphics.TextSize = 11
		  textw = MyImage.Graphics.StringWidth(Text)
		  if textW > CellSize then textW = CellSize
		  MyImage.Graphics.ForeColor = &c676767 ' background colour
		  MyImage.Graphics.FillRect(0, CellSize-20, CellSize, 20)
		  MyImage.Graphics.ForeColor = &cFFFFFF
		  MyImage.Graphics.DrawString(Text, (MyImage.Width/2)-(textW/2), CellSize-5, CellSize, true)
		  
		  // Store this cell size
		  LastCellSize = CellSize
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private LastCellSize As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSelectedImage As Picture
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSourceImageFlipped As Picture
	#tag EndProperty

	#tag Property, Flags = &h0
		mVisible As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private MyImage As Picture
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  // Construct the image we want to use when selected
			  // If you like - you can return a different image when a thumbnail is selected.  For this demo we'll invert the picture
			  
			  dim p as Picture
			  dim textW as integer
			  
			  // Does the user want us to invert selected images?
			  return MyImage
			  
			  // Do we need to redraw the image?
			  if mSelectedImage <> nil and mSelectedImage.Width = LastCellSize and mSelectedImage.Height = LastCellSize then ' we've a valid image - nothing has changed
			    return mSelectedImage
			  end if
			  
			  // Create a new selected image
			  mSelectedImage = new Picture(LastCellSize, LastCellSize, 32)
			  
			  // Fill with our background colour
			  mSelectedImage.Graphics.ForeColor = &c676767
			  mSelectedImage.Graphics.FillRect(0, 0, LastCellSize, LastCellSize)
			  
			  // Get the image to draw and scale it as necessary
			  p = ScalePicture(SourceImage, LastCellSize, LastCellSize-20)
			  
			  // Invert p
			  Invert(p)
			  
			  // Draw the scaled picture, centred on mSelectedImage
			  if p <> nil then
			    mSelectedImage.Graphics.DrawPicture(p, (LastCellSize/2)-(p.Width/2), ((LastCellSize-20)/2)-(p.Height/2))
			    mSelectedImage.Mask.Graphics.FillRect((LastCellSize/2)-(p.Width/2), ((LastCellSize-20)/2)-(p.Height/2), p.Width, p.Height)
			    p = nil
			  end if
			  
			  // Draw the text
			  mSelectedImage.Graphics.TextSize = 11
			  textw = mSelectedImage.Graphics.StringWidth(Text)
			  if textW > LastCellSize then textW = LastCellSize
			  mSelectedImage.Graphics.ForeColor = &c676767 ' background colour
			  mSelectedImage.Graphics.FillRect(0, LastCellSize-20, LastCellSize, 20)
			  mSelectedImage.Graphics.ForeColor = &cFFFFFF
			  mSelectedImage.Graphics.DrawString(Text, (mSelectedImage.Width/2)-(textW/2), LastCellSize-5, LastCellSize, true)
			  
			  return mSelectedImage
			  
			End Get
		#tag EndGetter
		Private SelectedImage As Picture
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		SourceImage As Picture
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  if mSourceImageFlipped <> nil then
			    return mSourceImageFlipped
			  else
			    return SourceImage
			  end if
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mSourceImageFlipped = value
			End Set
		#tag EndSetter
		SourceImageFlipped As Picture
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		Text As String
	#tag EndProperty

	#tag Property, Flags = &h0
		x As Integer
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="mVisible"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="SourceImage"
			Group="Behavior"
			Type="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="SourceImageFlipped"
			Group="Behavior"
			Type="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Text"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="UnderMouse"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="x"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
