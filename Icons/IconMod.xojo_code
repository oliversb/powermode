#tag Module
Protected Module IconMod
	#tag Method, Flags = &h0
		Function Icon(name as string) As picture
		  if MyDict.HasKey(name) then
		    return MyDict.Value(name)
		  else
		    select case name
		    case "Zoom"
		      return iconZoom
		    case "ZoomOut"
		      return iconZoomOut
		    case "ZoomIn"
		      return iconZoomIn
		    case "Snap"
		      return iconSnap
		    case "Stop"
		      return iconStop
		    case "Undo"
		      return iconUndo
		    case "Forum"
		      return iconForum
		    case "Object"
		      return iconObject
		    case "Text"
		      return iconText
		    case "Settings"
		      return iconSettings
		    case "NewFolder"
		      return iconNewFolder
		    case "Build"
		      return iconBuild
		    case "SaveAs"
		      return iconSaveAs
		    case "Save"
		      return iconSave
		    case "Redo"
		      return iconRedo
		    case "Scene"
		      return iconScene
		    case "Draw"
		      return iconDraw
		    case "Online"
		      return iconOnline
		    case "Help"
		      return iconHelp
		    case "Open"
		      return iconOpen
		    case "New"
		      return iconNew
		    case "Delete"
		      return iconDelete
		    case "Cut"
		      return iconCut
		    case "Paste"
		      return iconPaste
		    case "Copy"
		      return iconCopy
		    case "Sprite"
		      return iconSprite
		    case "Update"
		      return iconUpdate
		    case "Sound"
		      return iconSound
		    case "About"
		      return iconAbout
		    case "Play"
		      return iconPlay
		    case "Lock"
		      return iconLock
		    case "InstanceList"
		      return iconInstanceList
		    case "Right"
		      return iconRight
		    case "Left"
		      return iconLeft
		    case "Unlock"
		      return iconUnlock
		    end select
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Icon(name as string, assigns icon as picture)
		  MyDict.Value(name) = icon
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Setup()
		  MyDict = new Dictionary
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		MyDict As Dictionary
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
