#tag Class
Protected Class AppTimer
Inherits Timer
	#tag Event
		Sub Action()
		  for each key as ShortcutKey in ShortcutKeys
		    select case ShortcutKeys.IndexOf(key)
		    case 0
		      //CTRL + * (show command box)
		      if key.TestPress then
		        PopoverQuickCommand.displayPopoverAt(0, 0, nil)
		      end if
		    case 1
		      //CTRL + NUMBER (change tab)
		      if key.TestPress then
		        //use this method
		        'theNumKeys.CurrentKey
		      end if
		    end select
		  next
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h1000
		Sub Constructor()
		  theNumKeys = new NumKeys
		  
		  ShortcutKeys.Append(new ShortcutKey(KeyCodes.TimesSymbol) )
		  
		  dim numShortcutKey as new ShortcutKey
		  AddHandler numShortcutKey.TestIndividualKeyPressEvent, AddressOf NumKeyPress
		  
		  ShortcutKeys.Append(numShortcutKey)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function NumKeyPress(sender as ShortcutKey) As boolean
		  #pragma unused sender
		  
		  return theNumKeys.AnyKeyDown
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		ShortcutKeys() As ShortcutKey
	#tag EndProperty

	#tag Property, Flags = &h0
		theNumKeys As NumKeys
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			Type="Integer"
			EditorType="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Mode"
			Visible=true
			Group="Behavior"
			InitialValue="2"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Off"
				"1 - Single"
				"2 - Multiple"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Period"
			Visible=true
			Group="Behavior"
			InitialValue="1000"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
