#tag Module
Protected Module ModProjectChecker
	#tag Method, Flags = &h0
		Function CheckProjectErrors() As Boolean
		  //check there is at least one scene
		  dim sceneExists as boolean
		  for each res as resource in resources
		    if res isa ResourceScene then
		      sceneExists = true
		    end if
		  next
		  if not sceneExists then
		    MsgBox "Please create at least one scene and then try running/building again."
		    return false
		  end if
		  
		  //variable name checking is done when the name gets typed
		  //Loop through all the variables in the project and check the name
		  'for each res as resource in docfile.resources
		  'if res isa ResourceObject then
		  'for each var as ObjectVariable in ResourceObject(res).Variables
		  'if isVariableNameOK(var.name) then
		  '//good variable naming
		  'else
		  '//bad variable naming
		  'Return false
		  'end if
		  'next
		  'end if
		  'next
		  
		  // Loop through each frame
		  for each res as resource in docfile.resources
		    if res isa ResourceObject then
		      for each frame as contentFrame in ResourceObject(res).Blocks
		        dim checkFrameAndContainer as boolean = ResourceObject(res).CodeContainer <> nil and frame <> nil
		        // Make sure each variable that is required is set
		        if BlockBase(frame.tag).RequireVariablePath1 then
		          if BlockBase(frame.tag).VariablePath1 = nil then
		            MsgBox "Please set the required values for this " + BlockBase(frame.tag).DisplayName + " block and then try running/building again."
		            if checkFrameAndContainer then ResourceObject(res).CodeContainer.SelectFrame(frame)
		            Return false
		          end if
		        end if
		        
		        if BlockBase(frame.tag).RequireVariablePath2 then
		          if  BlockBase(frame.tag).VariablePath2 = nil then
		            MsgBox "Please set the required values for this " + BlockBase(frame.tag).DisplayName + " block and then try running/building again."
		            if checkFrameAndContainer then ResourceObject(res).CodeContainer.SelectFrame(frame)
		            Return false
		          end if
		        end if
		        
		        if BlockBase(frame.tag).RequireVariablePath3 then
		          if  BlockBase(frame.tag).VariablePath3 = nil then
		            MsgBox "Please set the required values for this " + BlockBase(frame.tag).DisplayName + " block and then try running/building again."
		            if checkFrameAndContainer then ResourceObject(res).CodeContainer.SelectFrame(frame)
		            Return false
		          end if
		        end if
		        
		        if BlockBase(frame.tag).RequireVariablePath4 then
		          if  BlockBase(frame.tag).VariablePath4 = nil then
		            MsgBox "Please set the required values for this " + BlockBase(frame.tag).DisplayName + " block and then try running/building again."
		            if checkFrameAndContainer then ResourceObject(res).CodeContainer.SelectFrame(frame)
		            Return false
		          end if
		        end if
		      next
		    end if
		  next
		  
		  Return true // No Errors
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function IsVariableNameOK(theVariable as String) As Boolean
		  
		  if theVariable.InStr("-") > 0 then
		    MsgBox "Variable '" + theVariable + " ' contans a '-' which is not allowed.  Please fix this variable name  and then try running/building again."
		    Return false
		  end if
		  
		  if theVariable.InStr("/") > 0 then
		    MsgBox "Variable '" + theVariable + " ' contans a '/' which is not allowed.  Please fix this variable name  and then try running/building again."
		    Return false
		  end if
		  
		  if theVariable.InStr("\") > 0 then
		    MsgBox "Variable '" + theVariable + " ' contans a '\' which is not allowed.  Please fix this variable name  and then try running/building again."
		    Return false
		  end if
		  
		  if theVariable.InStr(".") > 0 then
		    MsgBox "Variable '" + theVariable + " ' contans a '.' which is not allowed.  Please fix this variable name  and then try running/building again."
		    Return false
		  end if
		  
		  if theVariable.InStr("*") > 0 then
		    MsgBox "Variable '" + theVariable + " ' contans a '*' which is not allowed.  Please fix this variable name  and then try running/building again."
		    Return false
		  end if
		  
		  if theVariable.InStr("+") > 0 then
		    MsgBox "Variable '" + theVariable + " ' contans a '+' which is not allowed.  Please fix this variable name  and then try running/building again."
		    Return false
		  end if
		  
		  if theVariable.InStr("%") > 0 then
		    MsgBox "Variable '" + theVariable + " ' contans a '%' which is not allowed.  Please fix this variable name  and then try running/building again."
		    Return false
		  end if
		  
		  if theVariable.InStr("!") > 0 then
		    MsgBox "Variable '" + theVariable + " ' contans a '!' which is not allowed.  Please fix this variable name  and then try running/building again."
		    Return false
		  end if
		  
		  if theVariable.InStr(" ") > 0 then
		    MsgBox "Variable '" + theVariable + " ' contans a space which is not allowed.  Please fix this variable name  and then try running/building again."
		    Return false
		  end if
		  
		  if theVariable.InStr("#") > 0 then
		    MsgBox "Variable '" + theVariable + " ' contans a '#' which is not allowed.  Please fix this variable name  and then try running/building again."
		    Return false
		  end if
		  
		  if theVariable.InStr("@") > 0 then
		    MsgBox "Variable '" + theVariable + " ' contans a '@' which is not allowed.  Please fix this variable name  and then try running/building again."
		    Return false
		  end if
		  
		  if theVariable.InStr("<") > 0 then
		    MsgBox "Variable '" + theVariable + " ' contans a '>' which is not allowed.  Please fix this variable name  and then try running/building again."
		    Return false
		  end if
		  
		  if theVariable.InStr(">") > 0 then
		    MsgBox "Variable '" + theVariable + " ' contans a '>' which is not allowed.  Please fix this variable name  and then try running/building again."
		    Return false
		  end if
		  
		  Return true
		End Function
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
