#tag Module
Protected Module CodeGenerator
	#tag Method, Flags = &h0
		Sub BeginCodeGeneration(forTarget as uint32)
		  'MoveAppToTopFrame
		  
		  select case TargetInProcess
		  case app.kpPHPJavascriptWeb
		    BeginCodeGenerationJavascript(forTarget)
		  end select
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub BeginCodeGenerationJavascript(forTarget as integer)
		  #pragma unused forTarget
		  
		  SetHTML5BaseCode
		  
		  //pass 1
		  for each res as resource in DocFile.resources
		    select case res
		    case isa ResourceObject //loop through frames in resources
		      dim mRes as ResourceObject = ResourceObject(res)
		      
		      for each frame as ContentFrame in mRes.Blocks
		        dim s as string
		        dim ss as string
		        
		        BlockBase(frame.tag).CallGenerateHTML5Code(mRes, ss)
		        
		        if ss <> "" then
		          s = s + mRes.GetFunctionStarter + BlockBase(frame.tag).GetCodeName + " = function() {" + EndOfLine
		          s = s + ss + "}" + EndOfLine + EndOfLine
		        end if
		        BlockBase(frame.tag).HTML5InsertCode(s, EndOfFunctions)
		      next
		    case isa ResourceSound
		      if ResourceSound(res).Sound <> nil then
		        HTML5InsertCodeAtMarker("preloadSound('" + ResourceSound(res).name + "', " + str(ResourceSound(res).Sound.Volume) + ", " + str(ResourceSound(res).Sound.Pan) + ");", EndOfPreload)
		      end if
		      HTML5InsertCodeAtMarker("function " + ResourceSound(res).GetCodeName + "() { return getSound('" + ResourceSound(res).name + "'); }", EndOfFunctions)
		    end select
		  next
		  
		  //pass 2
		  //loop through outputs in ContentObjects frames in resources
		  for each res as resource in DocFile.resources
		    if res isa ResourceObject then
		      for each frame as ContentFrame in ResourceObject(res).Blocks
		        for each co as ContentObject in frame.contents
		          for each output as ContentObject in co.outputs
		            BlockBase.HelperGenerateHTML5CodePhase2(co, output)
		          next
		        next
		      next
		    end if
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ExportAllImages(ToFolder as FolderItem, res as resource, combined as Boolean = false)
		  select case res
		  case isa ResourceSprite
		    dim castedRes as ResourceSprite = ResourceSprite(res)
		    
		    //create folder for this sprite on the file system
		    ToFolder.CreateAsFolder
		    
		    //save each frame of animation
		    if not combined then
		      for i as Integer = 0 to castedRes.frames.ubound
		        dim p as picture = castedRes.frames(i).SourceImage
		        p.Save(ToFolder.Child(str(i) + ".png"), Picture.SaveAsPNG, Picture.QualityMax)
		      next
		    else
		      dim pwidth as uint32 = 0
		      dim pheight as uint32 = 0
		      for i as Integer = 0 to castedRes.frames.ubound
		        pwidth = pwidth + castedRes.frames(i).SourceImage.width
		        if pheight < castedRes.frames(i).SourceImage.height then
		          pheight = castedRes.frames(i).SourceImage.height
		        end if
		      next
		      
		      if pwidth <> 0 then
		        //create picture then reset pwidth and pheight to loop through sprite frames
		        dim p as new picture(pwidth, pheight)
		        pwidth = 0
		        for i as Integer = 0 to castedRes.frames.ubound
		          castedRes.frames(i).x = pwidth
		          
		          p.Graphics.DrawPicture castedRes.frames(i).SourceImage, pwidth, 0
		          
		          pwidth = pwidth + castedRes.frames(i).SourceImage.width
		        next
		        
		        p.Save(ToFolder.Child(ResourceSprite(res).GetCodeName + ".png"),Picture.SaveAsPNG,Picture.QualityMax)
		      end if
		    end if
		  case isa ResourceObject
		    dim castedRes as ResourceObject = ResourceObject(res)
		    
		    //create folder for the object's notes on the file system
		    toFolder.CreateAsFolder
		    
		    //save picture for each note
		    for each b as ContentFrame in castedRes.Blocks
		      if b.tag isa BlockPictureNote then
		        dim p as picture = b.OriginalPicture
		        p.Save(ToFolder.Child(str(castedRes.Blocks.IndexOf(b)) + ".png"), Picture.SaveAsPNG, Picture.QualityMax)
		      end if
		    next
		  case isa ResourceSound
		    dim castedRes as ResourceSound = ResourceSound(res)
		    
		    ToFolder.CreateAsFolder
		    
		    //save sound
		    if castedRes.SoundFile <> nil then
		      castedRes.SoundFile.CopyFileTo(ToFolder)
		      toFolder.Child(castedRes.SoundFile.name).Name = res.name + ".mp3"
		    end if
		  end select
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub HTML5InsertCodeAtMarker(theCode as string, theMarker as string)
		  theCode = theCode + EndOfLine + theMarker
		  CodeGenerator.HTML5Code = CodeGenerator.HTML5Code.Replace(theMarker, theCode)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MoveAppToTopFrame()
		  'for each frame as contentFrame in frmProject.chartProject.frames
		  '
		  'for each content as contentObject in frame.contents
		  '
		  'if content.tag isa ilObjectApp then
		  'ilObjectApp(content.tag).UniqueID = "1"
		  'end if
		  '
		  'next
		  '
		  'next
		  '
		  '
		  '
		  'dim frame as ContentFrame
		  'dim tempframe as contentframe
		  'dim objectIndex as integer
		  '
		  '// find the selected object
		  'for objectIndex = frmProject.chartProject.frames.Ubound DownTo 0
		  'frame = frmProject.chartProject.frames(objectIndex)
		  'if frame.tag isa ilObjectApp then
		  'tempframe = contentFrame(frame)
		  'frmProject.chartProject.frames.Remove(objectIndex)
		  'frmProject.chartProject.frames.Insert(0,tempframe)
		  'exit for
		  'end if
		  '
		  'next
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetHTML5BaseCode()
		  dim s as string
		  
		  dim FirstScene as ResourceScene
		  //find the first scene
		  for each scene as resource in DocFile.Resources
		    if scene isa ResourceScene then
		      FirstScene = ResourceScene(scene)
		      //exit for loop on the first scene found
		      exit for
		    end if
		  next
		  
		  // The Header of the HTML file
		  
		  s = "<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Strict//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"">" + EndOfLine + _
		  "<html xmlns=""http://www.w3.org/1999/xhtml"" lang=""en"">" + EndOfLine + _
		  "<head>" + EndOfLine + _
		  "<meta http-equiv=""content-type"" content=""text/html;charset=utf-8""/>" + EndOfLine + _
		  "<title>" + DocFile.ProjectName + "</title>" + EndOfLine + EndOfLine
		  
		  //import scripts
		  s = s + ConstHTMLCode + EndOfLine + EndOfLine
		  
		  // The CSS
		  
		  s = s + "<style type=""text/css"">" + EndOfLine
		  s = s + "body {" + EndOfLine
		  s = s + "background-color: #3B3B3B;" + EndOfLine
		  s = s + "font-family: arial, verdana, sans-serif;" + EndOfLine
		  s = s + "font-size: 12px;" + EndOfLine
		  s = s + "}" + EndOfLine + EndOfLine
		  
		  s = s + "#canvas"
		  s = s + "{" + EndOfLine
		  s = s + "display: block;" + EndOfLine
		  s = s + "margin: 0 auto;" + EndOfLine
		  s = s + "}" + EndOfLine
		  s = s + "</style>" + EndOfLine + EndOfLine
		  
		  //import css from external files
		  s = s + "<link rel='stylesheet' type='text/css' href='src\SimpleModal\css\confirm.css'>" + EndOfLine
		  
		  
		  // The Script section
		  
		  s = s + "<script type=""text/javascript"">" + EndOfLine + EndOfLine
		  
		  s = s + EndOfVariables + EndOfLine + EndOfLine
		  
		  //load sprites
		  s = s + "function loadSprites() {" + EndOfLine
		  for each res as resource in resources
		    if res isa ResourceSprite and ResourceSprite(res) <> nil and ResourceSprite(res).Frames.Ubound <> -1 then
		      s = s + "loadSpriteSheet('" + ResourceSprite(res).GetCodeName + "');" + EndOfLine
		    end if
		  next
		  s = s + "}" + EndOfLine
		  
		  s = s + "function init() {" + EndOfLine
		  s = s + StartOfInit + EndOfLine
		  s = s + "setupGame(" + str(FirstScene.SceneWidth) + ", " + str(FirstScene.SceneHeight) + ");" + EndOfLine
		  for each res as resource in resources
		    if res isa ResourceScene then
		      s = s + "addScene('" + ResourceScene(res).GetCodeName + "', " + ResourceScene(res).GetCodeName + ");" + EndOfLine
		    end if
		  next
		  
		  //add preload function
		  s = s + "loaderState.prototype.preload2 = function () {" + EndOfLine
		  s = s + EndOfPreload + EndOfLine
		  s = s + "}" + EndOfLine + EndOfLine
		  s = s + "startFirstState();" + EndOfLine
		  s = s + "}" + EndOfLine + EndOfLine
		  
		  s = s + "function handleLoadComplete2(event) {" + EndOfLine
		  s = s + "startScene('" + FirstScene.GetCodeName + "');" + EndOfLine
		  s = s + AfterStartScene + EndOfLine
		  s = s + "}" + EndOfLine
		  
		  for each res as resource in DocFile.resources
		    select case res
		    case isa ResourceObject
		      dim mRes as ResourceObject = ResourceObject(res)
		      
		      s = s + "function " + mRes.GetCodeName + "(" + ObjectInstanceParameters + ") {" + EndOfLine
		      //insert all variables into the object
		      for each var as ObjectVariable in mRes.Variables
		        if not var isa EngineVariable then
		          s = s + var.GetHTML5CodeName + " = "
		          select case var.Type
		          case "Text"
		            s = s + """" + var.CodedValue + """"
		          case "Number"
		            s = s + var.Value.StringValue
		          case "Switch"
		            //has to be lowercase true or false
		            s = s + Lowercase(var.Value.StringValue)
		          case "Colour"
		            s = s + "'#" + toPureHexString(var.value.ColorValue) + "'"
		          case "Sprite"
		            if var.value.ResourceValue <> nil then
		              s = s + "'" + ResourceSprite(var.value.ResourceValue).GetCodeName + "'"
		            else
		              s = s + "null"
		            end if
		          case "Sound"
		            if var.value.ResourceValue <> nil then
		              s = s + resourceInterface(var.value.ResourceValue).GetCodeName + "()"
		            else
		              s = s + "null"
		            end if
		          case "Object"
		            if var.value.ResourceValue <> nil then
		              s = s + resourceInterface(var.value.ResourceValue).GetCodeName
		            else
		              s = s + "null"
		            end if
		          end select
		          //adding to the end of this line of code anyway
		          s = s + ";" + EndOfLine
		        end if
		      next
		      
		      //set sprite key
		      if mRes.Sprite <> nil and mRes.Sprite.Frames.Ubound <> -1 then
		        s = s + "this.spriteKey = '" + mRes.Sprite.GetCodeName + "';"
		      end if
		      
		      //set visibility and presistance
		      if mRes.Visible = false then
		        s = s + "setVisible(this, " + ToLowercaseString(mRes.Visible) + ");" + EndOfLine
		      end if
		      if mRes.Presistant = true then
		        s = s + "setPresistant(this, " + ToLowercaseString(mRes.Presistant) + ");" + EndOfLine
		      end if
		      
		      //call parent's constructor
		      if mRes isa ResourceObjectDefaultParent then
		        s = s + EndOfLine + "PowerModeDefaultObject.call(this, " + ObjectInstanceParameters + ", this.spriteKey);" + EndOfLine
		      elseif mRes.Parent <> nil then
		        s = s + EndOfLine + mRes.Parent.GetCodeName + ".call(this, " + ObjectInstanceParameters + ", this.spriteKey);" + EndOfLine
		      end if
		      
		      //cleanup sprite key
		      s = s + "this.spriteKey = '';"
		      
		      //place a marker for the object to add variables
		      s = s + EndOfLine + mRes.GetCodeComment + EndOfLine
		      
		      s = s + "}" + EndOfLine
		      if mRes isa ResourceObjectDefaultParent then
		        s = s + mRes.GetCodeName + ".prototype = Object.create(PowerModeDefaultObject.prototype);"
		      elseif mRes.Parent <> nil then 
		        s = s + mRes.GetCodeName + ".prototype = Object.create(" + mRes.Parent.GetCodeName + ".prototype);" + EndOfLine
		      else
		        //no inheritance (not even from the default parent)
		        //boneless object
		      end if
		      s = s + mRes.GetCodeName + ".prototype.constructor = " + mRes.GetCodeName + ";" + EndOfLine
		      s = s + mRes.GetCodeName + ".prototype.theConstructor = function () {" + EndOfLine
		      s = s + mRes.GetCodeCommentConstructor + EndOfLine
		      s = s + "}" + EndOfLine
		      
		    case isa ResourceScene
		      s = s + "function " + ResourceScene(res).GetCodeName + "(game) {" + EndOfLine
		      
		      s = s + "this.create = function() {" + EndOfLine
		      
		      s = s + "this.stage.backgroundColor = '#" + toPureHexString(ResourceScene(res).Colour) + "';" + EndOfLine
		      if ResourceScene(res).Background <> nil then
		        s = s + "setBackground('" + ResourceScene(res).Background.GetCodeName + "');" + EndOfLine
		      end if
		      if ResourceScene(res).Scene.Ubound <> -1 then
		        //add container with all objects from the scene
		        for each co as CanvasObject in ResourceScene(res).Scene
		          if not co.theObject.Boneless then
		            //use for reference on parameters
		            'dim constant as string = ObjectInstanceParameters
		            
		            s = s + "addObject(new " + co.theObject.GetCodeName + "(" + str(co.Left) + ", " + str(co.Top) + ", " + co.Opacity.Value.StringValue + ", " + str(co.Rotation.Value.AngleValue.Radians) + ", " + co.ScaleX.Value.StringValue + ", " + co.ScaleY.Value.StringValue +", " + Lowercase(co.Visible.Value.StringValue) + "));" + EndOfLine
		          else
		            s = s + "objects.push(new " + co.theObject.GetCodeName + "());"
		          end if
		        next
		      end if
		      
		      s = s + "sceneCreateHandler();" + EndOfLine
		      
		      s = s + "}" + EndOfLine
		      s = s + "}" + EndOfLine
		    end select
		  next
		  s = s + EndOfFunctions + EndOfLine + EndOfLine
		  
		  
		  // End the script block
		  s = s + "</script>" + EndOfLine
		  s = s + "</head>" + EndOfLine
		  
		  s = s + "<body onload='engineInit()'>" + EndOfLine
		  
		  s = s + SimpleModalDiv + EndOfLine
		  
		  s = s + "</body>" + EndOfLine
		  s = s + "</html>" + EndOfLine
		  
		  CodeGenerator.HTML5Code = s
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		AndroidLayoutFolder As FolderItem
	#tag EndProperty

	#tag Property, Flags = &h0
		AndroidMainJavaCode As string
	#tag EndProperty

	#tag Property, Flags = &h0
		BuildingForiPad As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		FlexWebCode As string
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  dim s as string
			  
			  s = "package components{" + EndOfLine
			  s = s + "import flash.events.Event;" + EndOfLine
			  s = s + "import flash.events.MouseEvent;" + EndOfLine
			  
			  s = s + "import mx.containers.Panel;" + EndOfLine
			  s = s + "import mx.controls.LinkButton;" + EndOfLine
			  
			  s = s + "public class DraggablePanel extends Panel{" + EndOfLine
			  s = s + "private var _closer:LinkButton=new LinkButton();" + EndOfLine
			  s = s + "[Embed(source=""assets/img/close.png"")] [Bindable] public var closePng:Class;" + EndOfLine
			  
			  s = s + "public function DraggablePanel(){" + EndOfLine
			  s = s + "super();" + EndOfLine
			  s = s + "_closer.addEventListener(MouseEvent.CLICK, closeMe);" + EndOfLine
			  s = s + "_closer.setStyle('icon', closePng);" + EndOfLine
			  s = s + "_closer.useHandCursor = false;" + EndOfLine
			  s = s + "}" + EndOfLine
			  
			  s = s + "override protected function createChildren():void{" + EndOfLine
			  s = s + "super.createChildren();" + EndOfLine
			  s = s + "super.titleBar.addEventListener(MouseEvent.MOUSE_DOWN,handleDown);" + EndOfLine
			  s = s + "super.titleBar.addEventListener(MouseEvent.MOUSE_UP,handleUp);" + EndOfLine
			  s = s + "super.titleBar.addChild(_closer);" + EndOfLine
			  s = s + "}" + EndOfLine
			  s = s + "override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{" + EndOfLine
			  s = s + "super.updateDisplayList(unscaledWidth,unscaledHeight);" + EndOfLine
			  s = s + "_closer.width = 16;" + EndOfLine
			  s = s + "_closer.height = 16;" + EndOfLine
			  s = s + "_closer.x = super.titleBar.width - _closer.width - 8;" + EndOfLine
			  s = s + "_closer.y = 8;" + EndOfLine
			  s = s + "}" + EndOfLine
			  
			  s = s + "private function handleDown(e:Event):void{" + EndOfLine
			  s = s + "this.startDrag();" + EndOfLine
			  s = s + "}" + EndOfLine
			  s = s + "private function handleUp(e:Event):void{" + EndOfLine
			  s = s + "this.stopDrag();" + EndOfLine
			  s = s + "}" + EndOfLine
			  
			  s = s + "private function closeMe(e:MouseEvent):void{" + EndOfLine
			  s = s + "this.visible = false;" + EndOfLine
			  s = s + "}" + EndOfLine
			  s = s + "}" + EndOfLine
			  s = s + "}" + EndOfLine
			  
			  Return s
			End Get
		#tag EndGetter
		GetFlexDragPanelString As string
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		HTML5Code As string
	#tag EndProperty

	#tag Property, Flags = &h0
		iosAppDelegateHCode As string
	#tag EndProperty

	#tag Property, Flags = &h0
		iosAppDelegateMCode As string
	#tag EndProperty

	#tag Property, Flags = &h0
		iosMainCode As string
	#tag EndProperty

	#tag Property, Flags = &h0
		iosPBXProj As string
	#tag EndProperty

	#tag Property, Flags = &h0
		iosPList As string
	#tag EndProperty

	#tag Property, Flags = &h0
		iosPrefix As string
	#tag EndProperty

	#tag Property, Flags = &h0
		PythonDesktopCode As string
	#tag EndProperty

	#tag Property, Flags = &h0
		PythonMaemoCode As string
	#tag EndProperty

	#tag Property, Flags = &h0
		TargetInProcess As Integer
	#tag EndProperty


	#tag Constant, Name = AfterStartScene, Type = String, Dynamic = False, Default = \"//After StartScene", Scope = Public
	#tag EndConstant

	#tag Constant, Name = ConstHTMLCode, Type = String, Dynamic = False, Default = \"<script src\x3D\"src/jQuery/jquery.js\" type\x3D\"text/javascript\"></script>\r<script src\x3D\"src/jQuery/jquery-ui.js\"></script>\r<link rel\x3D\"stylesheet\" href\x3D\"src/jQuery/jquery-ui.css\">\r\r<script src\x3D\"src/prototype.js\" type\x3D\"text/javascript\"></script>\r<script src\x3D\"src/phaser.js\" type\x3D\"text/javascript\"></script>\r<script src\x3D\"src/preloadjs.js\" type\x3D\"text/javascript\"></script>\r<script src\x3D\"src/SimpleModal/simplemodal.js\" type\x3D\"text/javascript\"></script>\r\r<script src\x3D\"src/PowerModeEngine.js\" type\x3D\"text/javascript\"></script>\r\r<!-- SoundJS -->\r<script type\x3D\"text/javascript\" src\x3D\"src/soundjs/Sound.js\"></script>\r<script type\x3D\"text/javascript\" src\x3D\"src/soundjs/WebAudioPlugin.js\"></script>\r<script type\x3D\"text/javascript\" src\x3D\"src/soundjs/HTMLAudioPlugin.js\"></script>\r\r<!-- Note: FlashPlugin is only needed to support older browsers -->\r<script type\x3D\"text/javascript\" src\x3D\"src/swfobject.js\"></script>\r<script type\x3D\"text/javascript\" src\x3D\"src/soundjs/FlashPlugin.js\"></script> ", Scope = Public
	#tag EndConstant

	#tag Constant, Name = EndOfFunctions, Type = String, Dynamic = False, Default = \"//End Of PowerMode Functions", Scope = Public
	#tag EndConstant

	#tag Constant, Name = EndOfPreload, Type = String, Dynamic = False, Default = \"//End Of Preload", Scope = Public
	#tag EndConstant

	#tag Constant, Name = EndOfVariables, Type = String, Dynamic = False, Default = \"//End Of Variables", Scope = Public
	#tag EndConstant

	#tag Constant, Name = ObjectInstanceParameters, Type = String, Dynamic = False, Default = \"x \x2Cy\x2C opacity\x2C rotation\x2C scaleX\x2C scaleY\x2C visible", Scope = Public
	#tag EndConstant

	#tag Constant, Name = SimpleModalDiv, Type = String, Dynamic = False, Default = \"<div id\x3D\'content\'>\r\t\t<!-- modal content -->\r\t\t<div id\x3D\'confirm\'>\r\t\t\t<div class\x3D\'header\'><span>Confirm</span></div>\r\t\t\t<div class\x3D\'message\'></div>\r\t\t\t<div class\x3D\'buttons\'>\r\t\t\t\t<div class\x3D\'no simplemodal-close\'>No</div><div class\x3D\'yes\'>Yes</div>\r\t\t\t</div>\r\t\t</div>\r\t\t<!-- preload the images -->\r\t\t<div style\x3D\'display:none\'>\r\t\t\t<img src\x3D\'src/SimpleModal/img/confirm/header.gif\' alt\x3D\'\' />\r\t\t\t<img src\x3D\'src/SimpleModal/img/confirm/button.gif\' alt\x3D\'\' />\r\t\t</div>\r\t</div>", Scope = Public
	#tag EndConstant

	#tag Constant, Name = SimpleModalDivBackup, Type = String, Dynamic = False, Default = \"<div id\x3D\'confirm-overlay\' class\x3D\'simplemodal-overlay\' style\x3D\'opacity: 0.5; height: 100%; width: 100%; position: fixed; left: 0px; top: 0px; z-index: 1001;\'></div>\r<div id\x3D\'confirm-container\' class\x3D\'simplemodal-container\' style\x3D\'position: fixed; z-index: 1002; height: 140px; width: 420px; left: 739.5px; top: 20%;\'><a href\x3D\'#\' title\x3D\'Close\' class\x3D\'modal-close simplemodal-close\'>x</a><div tabindex\x3D\'-1\' class\x3D\'simplemodal-wrap\' style\x3D\'height: 100%; outline: 0px; width: 100%; overflow: visible;\'><div id\x3D\'confirm\' class\x3D\'simplemodal-data\' style\x3D\'display: block;\'>\r\t\t\t<div class\x3D\'header\'><span>Question Dialog</span></div>\r\t\t\t<div class\x3D\'message\'></div>\r\t\t\t<div class\x3D\'buttons\'>\r\t\t\t\t<div class\x3D\'no simplemodal-close\'>No</div><div class\x3D\'yes\'>Yes</div>\r\t\t\t</div>\r\t\t</div></div></div>", Scope = Public
	#tag EndConstant

	#tag Constant, Name = StartOfInit, Type = String, Dynamic = False, Default = \"//Start Of Init", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="AndroidMainJavaCode"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="BuildingForiPad"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="FlexWebCode"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="GetFlexDragPanelString"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HTML5Code"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="iosAppDelegateHCode"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="iosAppDelegateMCode"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="iosMainCode"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="iosPBXProj"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="iosPList"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="iosPrefix"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="PythonDesktopCode"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="PythonMaemoCode"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TargetInProcess"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
