#tag Module
Protected Module BuildProject
	#tag Method, Flags = &h0
		Function BuildProjectHTML5(debugProject as boolean = false) As FolderItem
		  if ModProjectChecker.CheckProjectErrors = false then
		    Return nil
		  end if
		  
		  //check if project is saved and take action if not saved
		  if DocFile.ProjectFileItem = nil then
		    MainEditor.SaveProjectAs
		  else
		    MainEditor.SaveProject
		  end if
		  
		  
		  //check again
		  if DocFile.ProjectFileItem = nil then
		    ReportError(3)
		    Return nil
		  end if
		  
		  app.DoEvents
		  app.YieldToNextThread
		  
		  CodeGenerator.TargetInProcess = app.kpPHPJavascriptWeb
		  CodeGenerator.BeginCodeGeneration(app.kpPHPJavascriptWeb)
		  
		  
		  Dim t as TextOutputStream
		  Dim homeFile, f, mediaFolder, srcFolder as FolderItem
		  
		  if debugProject then
		    //debug from localhost
		    f = GetFolderItem("C:\wamp\www\PowerMode Debug")
		  else
		    //build files to be put on to a web server
		    f = DocFile.ProjectFolder.Child("HTML Build")
		  end if
		  
		  // Make a build folder
		  if f.DeleteEntireFolder = 0 then
		    f.CreateAsFolder
		    
		    //copy files
		    try
		      CopyFileOrFolder(DocFile.AppFolder.Child("webjs"), f)
		      srcFolder = f.Child("webjs")
		      srcFolder.Name = "src"
		    catch NilObjectException
		      ReportError(2)
		      return nil
		    end try
		    
		    mediaFolder = f.Child("media")
		    mediaFolder.CreateAsFolder
		    
		    for each res as resource in docfile.resources
		      select case res
		      case isa ResourceSprite
		        //save images directly to images folder
		        ExportAllImages(mediaFolder, res, true)
		        
		        //place the images into a spritesheet
		        dim s as string
		        s = "{""frames"": ["
		        //define the frames
		        for each frame as PhotoThumbnail in ResourceSprite(res).Frames
		          s = s + "{""frame"": {""x"": " + str(frame.x) + ",""y"": 0,""w"": " + str(frame.SourceImage.width) + ",""h"": " + str(frame.SourceImage.Height) + "}}"
		          //add comma if this is not the last item
		          if ResourceSprite(res).Frames.IndexOf(frame) <> ResourceSprite(res).Frames.Ubound then
		            s = s + ","
		          end if
		        next
		        s = s + "]}" + EndOfLine
		        
		        //write string a JSON file
		        dim tos as TextOutputStream = TextOutputStream.Create(mediaFolder.Child(ResourceSprite(res).GetCodeName + ".json"))
		        tos.Write(s)
		        tos.Close
		      case isa ResourceSound
		        ExportAllImages(mediaFolder, res, true)
		      end select
		    next
		    
		    if mediaFolder = nil then
		      mediaFolder.CreateAsFolder
		    end if
		    
		    //place splashscreen in media folder
		    DocFile.SplashScreen.Save(mediaFolder.Child("splashscreen.png"), Picture.SaveAsPNG)
		    
		    homeFile = f.Child("index.html")
		    t = TextOutputStream.Create(homeFile)
		    t.Write(CodeGenerator.HTML5Code)
		    t.Close
		  end if
		  
		  if not debugProject then
		    f.Launch
		  end if
		  Return homeFile
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CopyFileOrFolder(source as FolderItem, destination as FolderItem)
		  Dim i as Integer
		  Dim newFolder as FolderItem
		  If source.directory then //it's a folder
		    newFolder=destination.child(source.name)
		    newFolder.CreateAsFolder
		    For i=1 to source.count //go through each item
		      If source.item(i).directory then
		        //it's a folder
		        CopyFileOrFolder source.item(i), newFolder
		        //recursively call this
		        //routine passing it the folder
		      else
		        source.item(i).CopyFileTo newFolder
		        //it's a file so copy it
		      end if
		    next
		  else //it's not a folder
		    source.CopyFileTo destination
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RunProjectHTML5()
		  dim f as FolderItem
		  f = BuildProjectHTML5(true)
		  
		  if f <> nil then
		    if f.exists then
		      ShowURL("http://localhost/PowerMode Debug")
		    end if
		  end if
		End Sub
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
