#tag Class
Protected Class ResourceSound
Inherits Resource
Implements ResourceInterface
	#tag Method, Flags = &h0
		Sub Create(newProject as boolean)
		  #pragma unused newProject
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetCodeName() As string
		  return "pmSND" + name
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ReadNativeXml(ByRef x as XmlDocument, ByRef n as XmlNode)
		  #pragma unused x
		  
		  backupVolume = val(n.GetAttribute("Volume"))
		  backupPan = val(n.GetAttribute("Pan"))
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub WriteNativeXml(ByRef resource as XmlNode, ByRef xml as XmlDocument)
		  #pragma unused resource
		  #pragma unused xml
		  
		  if Sound <> nil then
		    resource.SetAttribute("Volume", str(Sound.Volume))
		    resource.SetAttribute("Pan", str(Sound.Pan))
		  end if
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		backupPan As uint32
	#tag EndProperty

	#tag Property, Flags = &h0
		backupVolume As Uint32 = 100
	#tag EndProperty

	#tag Property, Flags = &h0
		Sound As Sound
	#tag EndProperty

	#tag Property, Flags = &h0
		SoundFile As FolderItem
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="BackupOpenTabValue"
			Group="Behavior"
			InitialValue="-1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="icon"
			Group="Behavior"
			Type="picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ListIndex"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="OpenTabValue"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Sound"
			Group="Behavior"
			Type="Sound"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="typeTab"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="WrittenXml"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
