#tag Class
Protected Class ResourceObjectDefaultParent
Inherits ResourceObject
	#tag Method, Flags = &h0
		Sub Create2()
		  //add variables needed for the default parent object
		  dim var as ObjectVariable
		  
		  //General
		  var = new EngineVariable("Visible", me)
		  var.Type = "Switch"
		  Variables.Append var
		  
		  'var = new EngineVariable("Presistant", me)
		  'var.Type = "Switch"
		  'Variables.Append var
		  
		  //Numbers
		  var = new EngineVariable("PI", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = Math.PI
		  var.ReadOnly = true
		  Variables.Append var
		  
		  //Physics
		  var = new EngineVariable("X", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 0
		  Variables.Append var
		  
		  var = new EngineVariable("Y", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 0
		  Variables.Append var
		  
		  var = new EngineVariable("VelocityX", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 0
		  Variables.Append var
		  
		  var = new EngineVariable("VelocityY", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 0
		  Variables.Append var
		  
		  var = new EngineVariable("AccelerationX", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 0
		  Variables.Append var
		  
		  var = new EngineVariable("AccelerationY", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 0
		  Variables.Append var
		  
		  var = new EngineVariable("MaxVelocityX", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 0
		  Variables.Append var
		  
		  var = new EngineVariable("MaxVelocityY", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 0
		  Variables.Append var
		  
		  var = new EngineVariable("Gravity", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 0
		  Variables.Append var
		  
		  var = new EngineVariable("Mass", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 0
		  Variables.Append var
		  
		  var = new EngineVariable("MaxAngularVelocity", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 0
		  Variables.Append var
		  
		  var = new EngineVariable("Rotation", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 0
		  Variables.Append var
		  
		  var = new EngineVariable("CollisionMovement", me)
		  var.Type = "Switch"
		  var.Value.BooleanValue = false
		  Variables.Append var
		  
		  //Graphics
		  var = new EngineVariable("IsPenDown", me)
		  var.Type = "Switch"
		  var.Value.BooleanValue = false
		  Variables.Append var
		  
		  var = new EngineVariable("DrawPointX", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 0
		  Variables.Append var
		  
		  var = new EngineVariable("DrawPointY", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 0
		  Variables.Append var
		  
		  var = new EngineVariable("DrawColour", me)
		  var.Type = "Colour"
		  var.Value.ColorValue = &c000000
		  Variables.Append var
		  
		  var = new EngineVariable("DrawOpacity", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 0
		  Variables.Append var
		  
		  var = new EngineVariable("DrawThickness", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 1
		  Variables.Append var
		  
		  var = new EngineVariable("Sprite", me)
		  var.Type = "Sprite"
		  var.Value.ResourceValue = me.Sprite
		  Variables.Append var
		  
		  var = new EngineVariable("SpriteOpacity", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 100
		  Variables.Append var
		  
		  'var = new EngineVariable("SpriteRotation", me)
		  'var.Type = "Number"
		  'var.Value.DoubleValue = 0
		  'Variables.Append var
		  
		  var = new EngineVariable("AnimationSpeed", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 0
		  Variables.Append var
		  
		  var = new EngineVariable("AnimationFrame", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 0
		  Variables.Append var
		  
		  //at the moment, sprites are the only built-in way of defining animation
		  'var = new EngineVariable("CurrentAnimationPlaying", me)
		  'var.Type = "Switch"
		  'var.Value.BooleanValue = true
		  'Variables.Append var
		  
		  var = new EngineVariable("AnimationFrameCount", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 0
		  Variables.Append var
		  
		  var = new EngineVariable("SpriteCentrePointX", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 0
		  Variables.Append var
		  
		  var = new EngineVariable("SpriteCentrePointY", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 0
		  Variables.Append var
		  
		  var = new EngineVariable("ScaleX", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 1
		  Variables.Append var
		  
		  var = new EngineVariable("ScaleY", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 1
		  Variables.Append var
		  
		  var = new EngineVariable("Width", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 0
		  var.ReadOnly = true
		  Variables.Append var
		  
		  var = new EngineVariable("Height", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 0
		  Variables.Append var
		  
		  //UI
		  
		  //Sound
		  var = new EngineVariable("CurrentMusicPlaying", me)
		  var.Type = "Sound"
		  var.Value = nil
		  var.ReadOnly = true
		  Variables.Append var
		  
		  var = new EngineVariable("CurrentSoundsPlaying", me)
		  var.Type = "List"
		  var.Value = nil
		  var.ReadOnly = true
		  Variables.Append var
		  
		  var = new EngineVariable("MusicVolume", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 100
		  Variables.Append var
		  
		  var = new EngineVariable("MusicPan", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 0
		  Variables.Append var
		  
		  //Input
		  var = new EngineVariable("MouseX", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 0
		  var.ReadOnly = true
		  Variables.Append var
		  
		  var = new EngineVariable("MouseY", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 0
		  var.ReadOnly = true
		  Variables.Append var
		  
		  var = new EngineVariable("LastKeyPressed", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = -1
		  var.ReadOnly = true
		  Variables.Append var
		  
		  var = new EngineVariable("LastScrollAmount", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 0
		  var.ReadOnly = true
		  Variables.Append var
		  
		  //Misc
		  var = new EngineVariable("TotalObjectCount", me)
		  var.Type = "Number"
		  var.Value.DoubleValue = 0
		  var.ReadOnly = true
		  Variables.Append var
		End Sub
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="BackupOpenTabValue"
			Group="Behavior"
			InitialValue="-1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="icon"
			Group="Behavior"
			Type="picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IndexParent"
			Group="Behavior"
			InitialValue="-1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IndexSprite"
			Group="Behavior"
			InitialValue="-1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="InObjectList"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ListIndex"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockColour"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockIcon"
			Group="Behavior"
			Type="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="OpenTabValue"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Presistant"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="typeTab"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="WrittenXml"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
