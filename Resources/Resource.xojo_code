#tag Class
Protected Class Resource
	#tag Method, Flags = &h0
		 Shared Sub ConfirmedDeletion(SResource() as integer)
		  for index as integer = SResource.Ubound downto 0
		    if MainEditor.SelectedResourceTree.RowTag(SResource(index)) <> nil then
		      dim mass as boolean = (SResource.Ubound > 0)
		      Resource(MainEditor.SelectedResourceTree.RowTag(SResource(index))).Delete(false, mass)
		      MainEditor.SelectedResourceTree.RemoveRow(SResource(index))
		      //start from first selected item
		      for i as uint32 = 0 to MainEditor.SelectedResourceTree.ListCount - 1
		        if MainEditor.SelectedResourceTree.RowTag(i) <> nil then
		          //reassign listindex
		          Resource(MainEditor.SelectedResourceTree.RowTag(i)).ListIndex = i
		        end if
		      next
		    else
		      MainEditor.SelectedResourceTree.RemoveRow(SResource(index))
		    end if
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Delete(handleRemoveRow as boolean = true, massImport as boolean = false)
		  //tell the save system that we have got information to save
		  DocFile.ModifiedDoc = true
		  
		  if handleRemoveRow then
		    //remove instance from the listbox
		    UsedResourceTree.RemoveRow ListIndex
		  end if
		  
		  //remove instance from the tabs if there is a tab assigned
		  if OpenTabValue <> -1 then
		    MainEditor.OpenResourceTabs.removeTab OpenTabValue, massImport
		  end if
		  
		  //remove this instance from the array
		  DocFile.resources.Remove(DocFile.resources.IndexOf(me))
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Export()
		  //export this resource to a file
		  DocFile.Export(name,WriteXmlString,ProjectFile.Project)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		 Shared Sub Institute(string as string, typeTab as ResourceType, massImport as boolean = false, optional x as XmlDocument, optional n as XmlNode, editCell as boolean = true, openTab as boolean = true, customResource as Resource = nil, newProject as boolean)
		  DocFile.ModifiedDoc = true
		  
		  dim resource as resource
		  
		  if customResource = nil then
		    select case typeTab
		    case resourceType.object
		      resource = new ResourceObject
		    case resourceType.sprite
		      resource = new ResourceSprite
		    case resourceType.scene
		      resource = new ResourceScene
		    case resourceType.sound
		      resource = new ResourceSound
		    end select
		  else
		    resource = customResource
		  end if
		  
		  select case typeTab
		  case resourceType.object
		    resource.icon =Icon("Object")
		  case resourceType.sprite
		    resource.icon = Icon("Sprite")
		  case resourceType.scene
		    resource.icon = Icon("Scene")
		  case resourceType.sound
		    resource.icon = Icon("Sound")
		  end select
		  
		  resourceInterface(resource).Create(newProject)
		  
		  resource.name = string
		  
		  resource.typeTab = integer(typeTab)
		  
		  if x <> nil then
		    resourceInterface(resource).ReadNativeXml(x,n)
		  end if
		  
		  resource.Make(massImport, editCell, openTab)
		  
		  DocFile.resources.Append resource
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Make(massImport as boolean, editCell as boolean = true, openTab as boolean = true)
		  if massImport = false then
		    //if we are creating a new resource, we want to assign a tab
		    //NOTE: WE HAVE TO SET THE TAB BEFORE DETERMINING THE SELECTED RESOURCE
		    MainEditor.resourceTreeTabs.value = typeTab
		  end if
		  
		  dim SResource() as integer = MainEditor.SelectedResource
		  //assign instance with listbox
		  //nil CellTag means it's a folder
		  if SResource.Ubound <> -1 then
		    ListIndex = SResource(SResource.Ubound) + 1
		    UsedResourceTree.InsertRow(ListIndex, name)
		    UsedResourceTree.RowTag(ListIndex) = me
		    
		    //refresh list indices so they are correct in all of the assets
		    for i as integer = 0 to UsedResourceTree.ListCount - 1
		      Resource(UsedResourceTree.RowTag(i)).ListIndex = i
		    next
		  else
		    ListIndex = UsedResourceTree.ListCount
		    UsedResourceTree.AddRow(name)
		    UsedResourceTree.RowTag(ListIndex) = me
		  end if
		  
		  //tag this resource to the row in the listbox
		  UsedResourceTree.RowTag(ListIndex) = me
		  
		  if massImport = false then
		    //have the name the resource
		    if editCell then
		      UsedResourceTree.EditCell(ListIndex, 0)
		    end if
		    //open tab and assign editor
		    if openTab then
		      MakeOpenTab
		    end if
		  else
		    //if we are importing a mass or multiple resources then we are not assigning a tab (default is -1)
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MakeName(string as string = "", performChecks as boolean = true, changeList as boolean = true)
		  if string = "" then string = me.name
		  'MainEditor.SelectedResourceTree.SetFocus
		  
		  if performChecks then
		    //////////////////////////////////////////////////////////MakeUniqueName
		    string = ReplaceAll(string," ","")
		    
		    dim itemNo as integer = 0
		    dim pendingString as string = string
		    dim uniqueName as boolean = false
		    
		    while not uniqueName
		      for each res as resource in DocFile.resources
		        MakeName.MakeName(itemNo,pendingString,me,res,res.name,string)
		      next
		      
		      MakeName.CheckName(name,itemNo,string)
		      
		      uniqueName = true
		      //loop through resources and check if names are unique
		      for each res as resource in DocFile.resources
		        if res = me then continue
		        if res.name = name then
		          uniqueName = false
		          exit for
		        end if
		      next
		      MakeName.FinalizeUniqueName(uniqueName, name)
		    wend
		    //////////////////////////////////////////////////////////
		  end if
		  
		  //update names for different controls displaying this text
		  MainEditor.OpenResourceTabs.caption(OpenTabValue) = name
		  if changeList then
		    UsedResourceTree.Cell(ListIndex,0) = name
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MakeOpenTab()
		  //assign me to the last tab which will be created
		  OpenTabValue = MainEditor.OpenResourceTabs.tabCount
		  //create the tab assigned to this resource
		  MainEditor.OpenResourceTabs.Append(name,icon,true)
		  select case typeTab
		  case int32(ResourceType.object)
		    EmbeddedEditor = new ObjectEditor(ResourceObject(me))
		  case int32(ResourceType.sprite)
		    EmbeddedEditor = new SpriteEditor(ResourceSprite(me))
		  case int32(ResourceType.scene)
		    EmbeddedEditor = new SceneEditor(ResourceScene(me))
		  case int32(ResourceType.sound)
		    EmbeddedEditor = new SoundEditor(ResourceSound(me))
		  end select
		  
		  
		  //set the visible property to false, while embedding so the user cannot see the control being resized
		  EmbeddedEditor.Visible = false
		  //embed the editor
		  EmbeddedEditor.EmbedWithinPanel(MainEditor.resourceEditor, OpenTabValue)
		  EmbeddedEditor.Tab = MainEditor.OpenResourceTabs.tabs(OpenTabValue)
		  MainEditor.EmbeddedEditors.Append(EmbeddedEditor)
		  //set the visible property back to true, once the control has been embedded
		  EmbeddedEditor.Visible = true
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function UsedResourceTree() As listbox
		  return MainEditor.resourceTree(typeTab +1)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub WriteToXML(ByRef Xml As XmlDocument, Resources As XmlNode = nil)
		  //write to the xmlnode, instead of directly to the top level of the xml
		  WriteXml(Xml, Resources)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub WriteXML(Xml As XmlDocument, Resources As XmlNode = nil)
		  //used to name the xmlelement being created
		  const ResourceObjectName = "resource"
		  
		  Dim resourceNode As XmlNode
		  //append directly to xml if there is no xmlnode to append to
		  if resources = nil then
		    resourceNode = xml.AppendChild(Xml.CreateElement(ResourceObjectName))
		    //append to xmlnode if there is multiple instances of xmlnodes being appended
		  else
		    resourceNode = resources.AppendChild(Xml.CreateElement(ResourceObjectName))
		  end if
		  
		  //set top level attributes
		  resourceNode.SetAttribute("Name", name)
		  resourceNode.SetAttribute("TypeID", str(typeTab))
		  
		  //set native attributes
		  ResourceInterface(me).WriteNativeXml(resourceNode,xml)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function WriteXmlString() As String
		  //create the xml and convert it to a string
		  dim xml as new XmlDocument
		  WriteXml(xml)
		  return xml.ToString
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function XQLResource(x as XmlDocument, ending as string) As XmlNodeList
		  return  x.XQL("//resource[@Name='" + name + "']" + ending)
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		BackupOpenTabValue As Integer = -1
	#tag EndProperty

	#tag Property, Flags = &h0
		EmbeddedEditor As EmbeddedControl
	#tag EndProperty

	#tag Property, Flags = &h0
		Icon As picture
	#tag EndProperty

	#tag Property, Flags = &h0
		ListIndex As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		name As String
	#tag EndProperty

	#tag Property, Flags = &h0
		OpenTabValue As Integer = -1
	#tag EndProperty

	#tag Property, Flags = &h0
		typeTab As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		WrittenXml As Boolean
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="BackupOpenTabValue"
			Group="Behavior"
			InitialValue="-1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Icon"
			Group="Behavior"
			Type="picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ListIndex"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="OpenTabValue"
			Group="Behavior"
			InitialValue="-1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="typeTab"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="WrittenXml"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
