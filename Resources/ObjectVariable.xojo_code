#tag Class
Protected Class ObjectVariable
	#tag Method, Flags = &h0
		Function CodedValue() As string
		  //replace ' " '  with  ' \" '
		  //this is so that each occurance of a speachmark will not interfere with the ending of the string
		  return value.StringValue.ReplaceAll("""", "\""")
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor()
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(r as ResourceObject, c as VariableManager)
		  res = r
		  dim cc as new conVariable
		  cc.Show(me, nil, -1, c)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(n as string, r as ResourceObject)
		  res = r
		  name = n
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(n as string, txtVar as VariableTextfield, r as ResourceObject, type as integer = - 1)
		  //get name of variable if global
		  if DocFile.VariableScope = 1 then
		    n = n.Right(n.InStr(" > "))
		  end if
		  Constructor(n, r)
		  
		  //make new variable dialog and show
		  dim cc as new conVariable
		  cc.Show(me,txtVar,type)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		 Shared Function FilterToString(filter as integer) As string
		  select case filter
		  case -1
		    return ""
		  case 0
		    return "Text"
		  case 1
		    return "Number"
		  case 2
		    return "Switch"
		  case 3
		    return "Colour"
		  case 4
		    return "Sound"
		  case 5
		    return "Object"
		  case 6
		    return "Sprite"
		  case 7
		    return "List"
		  end select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetCodeName() As string
		  return "pmVAR" + Name
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function GetGlobalName(path as ObjectVariablePath) As String
		  dim text as string
		  
		  dim v as ObjectVariable
		  for i as integer = 0 to path.History.Ubound
		    v = path.History(i)
		    
		    text = text + v.Name
		    if i <> path.History.Ubound then //check if it's not the last item
		      text = text + " > "
		    end if
		  next
		  return text
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetHTML5CodeName() As string
		  return "this." + GetCodeName
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetName(nocheck as boolean, path as ObjectVariablePath = nil) As string
		  //if nocheck then just return plainly the name
		  if nocheck = true then
		    return name
		  end if
		  
		  return GetGlobalName(path)
		  
		  'if DocFile.VariableScope = 1 then //if global
		  'return GetGlobalName
		  'else
		  'if res = r then
		  'return name
		  'else
		  'return GetGlobalName
		  'end if
		  'end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MakeName(n as string)
		  //////////////////////////////////////////////////////////MakeUniqueName
		  dim itemNo as integer = 0
		  dim pendingString as string = n
		  dim uniqueName as boolean = false
		  
		  while not uniqueName
		    for each v as ObjectVariable in res.Variables
		      MakeName.MakeName(itemNo,pendingString,me,v,v.name,n)
		    next
		    
		    MakeName.CheckName(name,itemNo,n)
		    
		    uniqueName = true
		    //loop through resources and check if names are unique
		    for each v as ObjectVariable in res.Variables
		      if v = me then continue
		      if v.name = name then
		        uniqueName = false
		        exit for
		      end if
		    next
		    MakeName.FinalizeUniqueName(uniqueName, name)
		  wend
		  //////////////////////////////////////////////////////////
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function OfType(filter as integer) As Boolean
		  return OfType(ObjectVariable.FilterToString(filter))
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function OfType(myType as string) As boolean
		  //create a list of types that the variable could potentially be if it is of that type
		  dim possibleTypes() as string
		  
		  select case myType
		  case ""
		    //any type
		    return true
		  case "Resource"
		    possibleTypes = Array("Object", "Sprite", "Sound", "Scene")
		  case "Object", "Sprite", "Sound", "Scene", _
		    "Text", "Number", "Switch", "Colour", "List"
		    possibleTypes = Array(myType)
		  end select
		  
		  //loop through possible types to find if this is of the type and return true if that is the case
		  for each possibleType as string in possibleTypes
		    if Type = possibleType then
		      return true
		    end if
		  next
		  
		  //non of the types matched
		  return false
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		#tag Note
			Backup of all of the list items indices (if there are any).
		#tag EndNote
		backupList() As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		#tag Note
			Backup of all of the indices of the resources of the list items (if there is a list).
		#tag EndNote
		backupResList() As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		backupValue As integer = -1
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mType As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mValue As MyVariant
	#tag EndProperty

	#tag Property, Flags = &h0
		Name As String
	#tag EndProperty

	#tag Property, Flags = &h0
		ReadOnly As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		res As ResourceObject
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mType
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mType = value
			End Set
		#tag EndSetter
		Type As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  if mValue = nil then
			    mValue = new MyVariant
			    mValue.IntValue = 0
			  end if
			  return mValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mValue = value
			End Set
		#tag EndSetter
		Value As MyVariant
	#tag EndComputedProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="backupValue"
			Group="Behavior"
			InitialValue="-1"
			Type="integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ReadOnly"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Type"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
