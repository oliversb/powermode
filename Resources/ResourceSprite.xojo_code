#tag Class
Protected Class ResourceSprite
Inherits Resource
Implements ResourceInterface
	#tag Method, Flags = &h0
		Sub Create(newProject as boolean)
		  #pragma unused newProject
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetCodeName() As String
		  return "pmSPRITE" + me.name
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Path() As String
		  return me.GetCodeName + ".png"
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ReadNativeXml(ByRef x as XmlDocument, ByRef n as XmlNode)
		  #pragma unused x
		  #pragma unused n
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub WriteNativeXml(ByRef resource as XmlNode, ByRef xml as XmlDocument)
		  //only using file system to save data
		  #pragma unused resource
		  #pragma unused xml
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		Frames() As PhotoThumbnail
	#tag EndProperty

	#tag Property, Flags = &h0
		LockColour As Color = &C000000
	#tag EndProperty

	#tag Property, Flags = &h0
		LockIcon As Picture
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="BackupOpenTabValue"
			Group="Behavior"
			InitialValue="-1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="icon"
			Group="Behavior"
			Type="picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ListIndex"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockColour"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockIcon"
			Group="Behavior"
			Type="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="OpenTabValue"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="typeTab"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="WrittenXml"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
