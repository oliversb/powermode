#tag Interface
Protected Interface ResourceInterface
	#tag Method, Flags = &h0
		Sub Create(newProject as boolean)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetCodeName() As string
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ReadNativeXml(ByRef x as XmlDocument, ByRef n as XmlNode)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub WriteNativeXml(ByRef resource as XmlNode, ByRef xml as XmlDocument)
		  
		End Sub
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Interface
#tag EndInterface
