#tag Class
Protected Class EngineVariable
Inherits ObjectVariable
	#tag Method, Flags = &h0
		Function GetHTML5CodeName() As string
		  return "get" + name + "(this)"
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetHTML5Setter(value as string) As string
		  return "set" + name + "(this, " + value + ")"
		End Function
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="backupValue"
			Group="Behavior"
			InitialValue="-1"
			Type="integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ReadOnly"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Type"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
