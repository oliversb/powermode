#tag Class
Protected Class VariableContainer
Inherits Window
	#tag Event
		Sub Open()
		  StartHeight = self.height
		  OKCancelButton1Top = OKCancelButton1.Top
		  
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Function OKCancelButton1() As OKCancelButton
		  select case self
		  case isa conVariable
		    return conVariable(self).OKCancelButton1
		  case isa EditVariable
		    return EditVariable(self).OKCancelButton1
		  end select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub SetValueContainer(type as uint32, x as uint32, y as uint32)
		  if ValueContainer <> nil then
		    ValueContainer.Close
		  end if
		  
		  if SetDefPos = false then
		    dim h as integer = self.height
		    dim OKCancelButton1Top as integer = OKCancelButton1.Top
		    self.height = h
		    OKCancelButton1.Top = OKCancelButton1Top
		  end if
		  
		  select case type
		  case 0
		    Variable.Type = "Text"
		    ValueContainer = new conTextValue
		  case 1
		    Variable.Type = "Switch"
		    ValueContainer = new conSwitchValue
		  case 2
		    Variable.Type = "Colour"
		    ValueContainer = new conColourValue
		  case 3
		    Variable.Type = "Sound"
		    ValueContainer = new conSoundValue
		  case 4
		    Variable.Type = "Object"
		    ValueContainer = new conObjectValue
		  case 5
		    Variable.Type = "Sprite"
		    ValueContainer = new conSpriteValue
		  case 6
		    Variable.Type = "List"
		    ValueContainer = new conListValue(Variable, res)
		    static heightWithList as integer = StartHeight + 390
		    static OKCancelButton1TopWithList as integer = OKCancelButton1.Top + 400
		    self.height = heightWithList
		    OKCancelButton1.Top = OKCancelButton1TopWithList
		  end select
		  
		  ValueContainer.EmbedWithin(self, x, y)
		  ValueContainer.TabIndex = 3
		  
		  select case ValueContainer
		  case isa MinimalArrayMainContainer
		    MinimalArrayMainContainer(ValueContainer).CallOpen
		  end select
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function VariableEditDone(txtName as TextField, txtType as TextField, Variable as ObjectVariable) As boolean
		  dim txtValueBad as boolean = false
		  if TypeNotDefined then
		    txtValueBad = true
		  else
		    select case Variable.Type
		    case "Number"
		      if conTextValue(ValueContainer).txtValue <> nil then
		        txtValueBad = (conTextValue(ValueContainer).txtValue.TextIsOk = false)
		      end if
		    end select
		  end if
		  
		  if (txtValueBad or txtName.Text = "") then
		    Dim d as New MessageDialog //declare the MessageDialog object
		    d.icon=MessageDialog.GraphicCaution //display warning icon
		    d.Title=""
		    d.Message="The name and/or type fields are blank."
		    d.Explanation="Please fill in the required fields."
		    call d.ShowModal //display the dialog
		    return true
		  else
		    Variable.Name = txtName.Text
		    if txtType <> nil then
		      Variable.Type = txtType.Text
		    end if
		    select case Variable.Type
		    case "Text"
		      Variable.Value.StringValue = conTextValue(ValueContainer).txtValue.Text
		    case "Number"
		      Variable.Value.DoubleValue = conTextValue(ValueContainer).txtValue.Text
		    case "Switch"
		      Variable.Value.BooleanValue = conSwitchValue(ValueContainer).CheckValue.Value
		    case "Colour"
		      Variable.Value.ColorValue = conColourValue(ValueContainer).Colour
		    case "Sound", "Sprite", "Object"
		      Variable.Value.ResourceValue = conAssetValue(ValueContainer).Asset
		    case "List"
		      Variable.Value.ObjectVariablePathArrayValue = conListValue(ValueContainer).List
		    end select
		    return false
		  end if
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		conSetVarReference As VariableManager
	#tag EndProperty

	#tag Property, Flags = &h0
		OKCancelButton1Top As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		Path As ObjectVariablePath
	#tag EndProperty

	#tag Property, Flags = &h0
		res As ResourceObject
	#tag EndProperty

	#tag Property, Flags = &h0
		SetDefPos As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		StartHeight As Uint32
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected TypeNotDefined As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		ValueContainer As ContainerControl
	#tag EndProperty

	#tag Property, Flags = &h0
		Variable As ObjectVariable
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="BackColor"
			Visible=true
			Group="Appearance"
			InitialValue="&hFFFFFF"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Backdrop"
			Visible=true
			Group="Appearance"
			Type="Picture"
			EditorType="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CloseButton"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Composite"
			Visible=true
			Group="Appearance"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Frame"
			Visible=true
			Group="Appearance"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Document"
				"1 - Movable Modal"
				"2 - Modal Dialog"
				"3 - Floating Window"
				"4 - Plain Box"
				"5 - Shadowed Box"
				"6 - Rounded Window"
				"7 - Global Floating Window"
				"8 - Sheet Window"
				"9 - Metal Window"
				"10 - Drawer Window"
				"11 - Modeless Dialog"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="FullScreen"
			Group="Appearance"
			InitialValue="False"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="FullScreenButton"
			Visible=true
			Group="Appearance"
			InitialValue="False"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HasBackColor"
			Visible=true
			Group="Appearance"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Position"
			InitialValue="400"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ImplicitInstance"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Interfaces"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LiveResize"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MacProcID"
			Visible=true
			Group="Appearance"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MaxHeight"
			Visible=true
			Group="Position"
			InitialValue="32000"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MaximizeButton"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MaxWidth"
			Visible=true
			Group="Position"
			InitialValue="32000"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MenuBar"
			Visible=true
			Group="Appearance"
			Type="MenuBar"
			EditorType="MenuBar"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MenuBarVisible"
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MinHeight"
			Visible=true
			Group="Position"
			InitialValue="64"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MinimizeButton"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MinWidth"
			Visible=true
			Group="Position"
			InitialValue="64"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="OKCancelButton1Top"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Placement"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Default"
				"1 - Parent Window"
				"2 - Main Screen"
				"3 - Parent Window Screen"
				"4 - Stagger"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Resizeable"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="SetDefPos"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Title"
			Visible=true
			Group="Appearance"
			InitialValue="Untitled"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Position"
			InitialValue="600"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
