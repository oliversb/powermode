#tag Class
Protected Class ResourceObject
Inherits Resource
Implements ResourceInterface
	#tag Method, Flags = &h0
		Function AllInstances(scene as ResourceScene = nil) As CanvasObject()
		  dim instances() as CanvasObject
		  
		  if scene = nil then
		    for each res as Resource in DocFile.Resources
		      if res isa ResourceScene then
		        for each co as CanvasObject in AllInstances_SceneCheck(ResourceScene(res))
		          instances.append(co)
		        next
		      end if
		    next
		  else
		    instances = AllInstances_SceneCheck(scene)
		  end if
		  
		  return instances
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function AllInstances_SceneCheck(res as ResourceScene) As CanvasObject()
		  dim instances() as CanvasObject
		  
		  for each co as CanvasObject in ResourceScene(res).Scene
		    if co.theObject.HasParent(self) then
		      instances.append(co)
		    end if
		  next
		  
		  return instances
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AllVariables() As ObjectVariable()
		  //get all variables including super's variables
		  
		  //no parent means we can directly use this object's 'Variables'
		  if me.Parent = nil then return me.Variables
		  
		  dim vars() as ObjectVariable
		  dim o as ResourceObject = me
		  
		  while o <> nil
		    //append every variable in this object
		    for each v as ObjectVariable in o.Variables
		      vars.append v
		    next
		    //loop up the hierarchy with parent relationships
		    o = o.Parent
		  wend
		  
		  return vars
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Boneless() As boolean
		  //no parent and not being the default parent object means no bones
		  if me.Parent = nil and (not me isa ResourceObjectDefaultParent) then return true
		  
		  dim o as ResourceObject = me
		  while o <> nil
		    //reached default parent object so this is not boneless
		    if o isa ResourceObjectDefaultParent then return false
		    //move up parent relationship hierarchy
		    o = o.Parent
		  wend
		  
		  //reached nil parent without default parent object
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Create(newProject as boolean)
		  if me isa ResourceObjectDefaultParent then
		    //only perform these actions if the variables have not yet been loaded
		    if newProject then
		      ResourceObjectDefaultParent(me).Create2
		    end if
		  else
		    me.Parent = ResourceObject( DocFile.GetResource(DEFAULT_PARENT_NAME) )
		  end if
		  
		  DefaultCanvasObject = new CanvasObject
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetCodeComment() As String
		  return "//" + GetCodeName
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetCodeCommentConstructor() As string
		  return "//" + GetCodeName + "Constructor"
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetCodeName() As String
		  return "pmOBJECT" + me.name
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetFunctionStarter() As string
		  return me.GetCodeName + ".prototype."
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function HasParent(checkObject as ResourceObject) As boolean
		  dim obj as ResourceObject = me
		  
		  while obj <> nil
		    if obj = checkObject then
		      return true
		    else
		      obj = obj.Parent
		    end if
		  wend
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ReadNativeXml(ByRef x as XmlDocument, ByRef n as XmlNode)
		  dim v as string
		  
		  
		  v = n.GetAttribute("Parent")
		  if v <> "" then
		    IndexParent = val(v) //load parent from index
		  end if
		  
		  v = n.GetAttribute("Sprite")
		  if v <> "" then
		    IndexSprite = val(v) //load sprite from index
		  end if
		  
		  
		  
		  //Load arrays
		  dim NodeList as XMLNodeList
		  //load variables
		  NodeList = XQLResource(x,"/Variable")
		  for i as integer = 0 to NodeList.Length - 1
		    dim var as ObjectVariable
		    dim theName as string = NodeList.Item(i).GetAttribute("Name")
		    if NodeList.Item(i).GetAttribute("BuiltIn") = "true" then
		      var = new EngineVariable
		    else
		      var = new ObjectVariable
		    end if
		    var.Constructor(theName, me)
		    
		    select case var.Type
		    case "Sound", "Object", "Sprite"
		      var.backupValue = val(NodeList.Item(i).GetAttribute("Value"))
		    case "List"
		      for each backupResListItem as string in NodeList.Item(i).GetAttribute("ResIndices").Split(",")
		        var.backupResList.Append val(backupResListItem)
		      next
		      for each backupListItem as string in NodeList.Item(i).GetAttribute("Indices").Split(",")
		        var.backupList.Append val(backupListItem)
		      next
		    else
		      var.Value.StringValue = NodeList.Item(i).GetAttribute("Value")
		    end select
		    
		    var.Type = NodeList.Item(i).GetAttribute("Type")
		    
		    Variables.Append var
		  next
		  
		  
		  //load codeblocks
		  NodeList = XQLCodeBlock(x)
		  dim block as ContentFrame
		  dim curItem as XmlNode
		  for i as integer = 0 to NodeList.Length - 1 //loop through codeblocks
		    curItem = NodeList.Item(i)
		    //only parse type specific content to save processing time
		    //declare parsed items
		    dim _
		    parsedType, _
		    parsedNote as string
		    
		    dim parsedX, parsedY as integer
		    
		    dim parsedPic as picture
		    ////////////////////////////////////////////////////////////////////
		    parsedType = curItem.GetAttribute("Type")
		    select case parsedType
		    case "Picture Note"
		      parsedPic = Picture.Open(GetFolderItem(curItem.GetAttribute("Picture")))
		    case "Note"
		      parsedNote = curItem.GetAttribute("Note")
		    end select
		    parsedX = val(curItem.GetAttribute("X"))
		    parsedY = val(curItem.GetAttribute("Y"))
		    
		    block = ObjectEditor.AddNewObject(parsedType, parsedX, parsedY, parsedPic, parsedNote, me)
		    //parse extra information
		    dim blockInfo as BlockBase = BlockBase(block.tag)
		    
		    dim getMath1, getMath2, getMath3, getMathArray() as string
		    
		    getMath1 = curItem.GetAttribute("Math1")
		    if getMath1 <> "" then
		      blockInfo.backupVariablePath1 = getMath1
		      'blockInfo.backupVariablePath1Parent = val(curItem.GetAttribute("Math1Par"))
		    end if
		    getMathArray = curItem.GetAttribute("MathArray").Split(",")
		    if getMathArray.Ubound <> -1 then
		      for each s as string in getMathArray
		        //add VariablePath array item and convert it to an integer
		        blockInfo.backupVariablePathArray.append(s)
		      next
		      '//seperate and loop through each parent
		      'for each s as string in curItem.GetAttribute("MathArrayPar").Split(",")
		      '//add VariablePath array item and convert it to an integer
		      'blockInfo.backupVariablePathArrayParent.append val(s)
		      'next
		    else
		      //checking the getMathArray ubound gives an oppertunity to check wether math2 and math3 should be checked
		      
		      getMath2 = curItem.GetAttribute("Math2")
		      if getMath2 <> "" then
		        blockInfo.backupVariablePath2 = getMath2
		        'blockInfo.backupVariablePath2Parent = val(curItem.GetAttribute("Math1Par"))
		      end if
		      getMath3 = curItem.GetAttribute("Math3")
		      if getMath3 <> "" then
		        blockInfo.backupVariablePath3 = getMath3
		        'blockInfo.backupVariablePath3Parent = val(curItem.GetAttribute("Math3Par"))
		      end if
		      blockInfo.CustomMessage = curItem.GetAttribute("Custom")
		      
		    end if
		    
		    
		    if curItem.GetAttribute("IsOriginalSize") <> "" then
		      //when size is set, the custom property is changed to the opposite of this
		      blockInfo.CodeFrame.custom = bool(curItem.GetAttribute("IsOriginalSize"))
		    end if
		    
		    
		    Blocks.Append block
		    
		    //load outputs for codeblocks
		    dim OutputNodeList as XmlNode = XQLCodeBlock(x).Item(i)
		    for i2 as integer = 0  to OutputNodeList.ChildCount-1 //loop through output containers
		      dim OutputContentObject as integer = val(OutputNodeList.Child(i2).GetAttribute("ID"))
		      for i3 as integer = 0 to OutputNodeList.Child(i2).ChildCount-1 //loop through outputs
		        //store only the index because not all of the code blocks have been loaded
		        block.contents(OutputContentObject).outputsBackupIndex.Append ( val(OutputNodeList.Child(i2).Child(i3).GetAttribute("ConnectedBlock")) )
		      next
		    next
		  next
		  
		  //now load references from indexes stored
		  for each block1 as ContentFrame in me.Blocks //loop through all of the blocks (in this object)
		    for co as integer = 0 to block1.contents.Ubound //loop through all of the contentframes in this block
		      for output as integer = 0 to block1.contents(co).outputsBackupIndex.Ubound //loop through all of the pending outputs in the contentframe
		        'MakeOpenTab //used to debug changes
		        //block -> contents -> outputs ->    parent -> contentobject
		        //outofbounds if we do not load all of the blocks first
		        block1.contents(co).outputs.Append ( me.blocks(block1.contents(co).outputsBackupIndex(output)).contents(0) )
		      next
		    next
		  next
		  
		  //open default canvas object
		  dim Node as XmlNode = n.GetNode("DefSceneObjectInstance") 
		  me.DefaultCanvasObject = new CanvasObject
		  me.DefaultCanvasObject.ReadNativeXml(Node)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Variable(name as string) As ObjectVariable
		  //find which variable has this name
		  for each v as ObjectVariable in me.AllVariables
		    if v.name = name then
		      return v
		    end if
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub WriteNativeXml(ByRef resource as XmlNode, ByRef xml as XmlDocument)
		  if Parent <> nil then 
		    resource.SetAttribute("Parent", str(resources.IndexOf(Parent))) //save index of object
		  end if
		  if Sprite <> nil then
		    resource.SetAttribute("Sprite", str(resources.IndexOf(Sprite))) //save index of sprite
		  end if
		  
		  
		  dim Node as XmlNode
		  
		  for each v as ObjectVariable in Variables
		    Node = resource.AppendChild(xml.CreateElement("Variable"))
		    Node.SetAttribute("Name",v.Name)
		    Node.SetAttribute("Type",v.Type)
		    if v isa EngineVariable then
		      Node.SetAttribute("BuiltIn", "true")
		    end if 
		    select case v.Value.Type
		    case "Resource"
		      Node.SetAttribute("Value", str(resources.IndexOf(v.Value.ResourceValue)))
		    case "Array"
		      dim listValue() as ObjectVariablePath = v.Value.ObjectVariablePathArrayValue
		      'dim itemsResString() as string
		      dim itemsString() as string
		      
		      Node = resource.AppendChild(xml.CreateElement("ListItems"))
		      for each listItem as ObjectVariablePath in listValue
		        if listItem <> nil then
		          dim r as ResourceObject = me
		          #pragma unused r
		          '//record index of resource
		          'itemsResString.Append (str(DocFile.resources.IndexOf(r)))
		          //record index of variable from the resource
		          'itemsString.Append(str(r.Variables.IndexOf(listItem)))
		          itemsString.Append(listItem.StringValue)
		        else
		          'itemsResString.Append str(-1)
		          itemsString.Append str(-1)
		        end if
		      next
		      'Node.SetAttribute("ResIndices", Join(itemsResString, ","))
		      Node.SetAttribute("Indices", Join(itemsString, ","))
		    case else
		      Node.SetAttribute("Value",v.Value.StringValue)
		    end select
		  next
		  
		  for each b as ContentFrame in Blocks
		    //casting tag
		    dim block as BlockBase = b.tag
		    
		    Node = resource.AppendChild(xml.CreateElement("CodeBlock"))
		    if not b.tag isa BlockObject then
		      //object holding the variables
		      'dim math1par, math2par, math3par, mathArrayPar() as ResourceObject
		      
		      if block.VariablePath1 <> nil then
		        'math1par = block.VariablePath1.res
		        '//store index of the object
		        'Node.SetAttribute("Math1Par",str(DocFile.resources.IndexOf(math1par)))
		        //store index of the variable
		        Node.SetAttribute("Math1", block.VariablePath1.StringValue)
		        'Node.SetAttribute("Math1",str(math1par.Variables.IndexOf(block.VariablePath1)))
		      end if
		      
		      if block.VariablePathArray.Ubound <> -1 then
		        
		        dim items() as string
		        
		        //loop through each VariablePath in the VariablePatharray
		        for each path as ObjectVariablePath in block.VariablePathArray
		          if path <> nil then
		            #pragma BreakOnExceptions false
		            try
		              dim indexOfItem as integer = block.VariablePathArray.IndexOf(path)
		              #pragma unused indexOfItem
		              
		              '//append parent of current item in loop
		              'mathArrayPar.append item.res
		              '//store index of current object in loop
		              'itemsPar.append str(DocFile.resources.IndexOf(mathArrayPar(indexOfItem)))
		              //store index of current variable in loop
		              items.append str(path.StringValue)
		            catch OutOfBoundsException
		            end try
		            #pragma BreakOnExceptions true
		          end if
		        next
		        
		        //put arrays into string format (seperated by commas)
		        'Node.SetAttribute("MathArrayPar", Join(itemsPar, ","))
		        Node.SetAttribute("MathArray", Join(items, ","))
		      else
		        //checking the if the VariablePatharray is empty gives the opportunity to check wether we should look out for the other VariablePaths
		        
		        if block.VariablePath2 <> nil then
		          'math2par = block.VariablePath2.res
		          '//store index of the object
		          'Node.SetAttribute("Math2Par", str(DocFile.resources.IndexOf(math2par)))
		          //store the index of the variable
		          Node.SetAttribute("Math2", block.VariablePath2.StringValue)
		        end if
		        if block.VariablePath3 <> nil then
		          'math3par = block.VariablePath3.res
		          '//store index of the object
		          'Node.SetAttribute("Math3Par", str(DocFile.resources.IndexOf(math3par)))
		          //store the index of the variable
		          Node.SetAttribute("Math3", block.VariablePath3.StringValue)
		        end if
		        if BlockBase(b.tag).CustomMessage <> "" then
		          Node.SetAttribute("Custom", block.CustomMessage)
		        end if
		      end if
		    end if
		    
		    //ContentObject class shows properties which may need to be saved here
		    Node.SetAttribute("X", str(b.X))
		    Node.SetAttribute("Y", str(b.Y))
		    Node.SetAttribute("Type", BlockBase(b.tag).DisplayName)
		    if b.tag isa BlockPictureNote then
		      Node.SetAttribute("IsOriginalSize", str(not b.custom))
		    end if
		    
		    for each c as ContentObject in b.contents
		      if c.outputs.Ubound <> -1 then //see if there is outputs to save
		        dim OutputsNode as XmlNode = Node.AppendChild(xml.CreateElement("OutputContainer"))
		        OutputsNode.SetAttribute("ID",str(b.contents.IndexOf(c)))
		        for each output as ContentObject in c.outputs
		          dim OutputNode as XmlNode = OutputsNode.AppendChild(xml.CreateElement("Output"))
		          OutputNode.SetAttribute("ConnectedBlock",str(Blocks.IndexOf(output.parent))) //save the index of the output ContentObject's parent
		          //not using this line of code because there is only one input available for all of the blocks anyway
		          'OutputNode.SetAttribute("ConnectedOutput",str(output.parent.contents.IndexOf(output))) //save the index of the output ContentObject
		        next
		      end if
		    next
		  next
		  
		  //save default canvas object
		  Node = resource.AppendChild(xml.CreateElement("DefSceneObjectInstance"))
		  me.DefaultCanvasObject.WriteNativeXml(Node)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function XQLCodeBlock(x as XmlDocument, ending as string = "") As XmlNodeList
		  return XQLResource(x,"/CodeBlock" + ending)
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		Blocks() As ContentFrame
	#tag EndProperty

	#tag Property, Flags = &h0
		CodeContainer As RelationalFrames.RelationalFramesCanvas
	#tag EndProperty

	#tag Property, Flags = &h0
		DefaultCanvasObject As CanvasObject
	#tag EndProperty

	#tag Property, Flags = &h0
		IndexParent As Integer = -1
	#tag EndProperty

	#tag Property, Flags = &h0
		IndexSprite As Integer = -1
	#tag EndProperty

	#tag Property, Flags = &h0
		InObjectList As Boolean
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return DefaultCanvasObject.Layer.Value.IntValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  DefaultCanvasObject.Layer.Value.IntValue = value
			End Set
		#tag EndSetter
		Layer As UInt32
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  if me.Sprite = nil then
			    return me.mLockColour
			  else
			    return me.Sprite.LockColour
			  end if
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if me.Sprite = nil then
			    me.mLockColour = value
			  else
			    me.Sprite.LockColour = value
			  end if
			End Set
		#tag EndSetter
		LockColour As Color
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  if me.Sprite = nil then
			    if me.mLockIcon = nil then return iconLock
			    return me.mLockIcon
			  else
			    return me.Sprite.LockIcon
			  end if
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if me.Sprite = nil then
			    me.mLockIcon = value
			  else
			    me.Sprite.LockIcon = value
			  end if
			End Set
		#tag EndSetter
		LockIcon As Picture
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mLockColour As Color = &C000000
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLockIcon As Picture
	#tag EndProperty

	#tag Property, Flags = &h0
		Parent As ResourceObject
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return DefaultCanvasObject.Presistant.Value.BooleanValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  DefaultCanvasObject.Presistant.Value.BooleanValue = value
			End Set
		#tag EndSetter
		Presistant As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		Sprite As ResourceSprite
	#tag EndProperty

	#tag Property, Flags = &h0
		Variables() As ObjectVariable
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return DefaultCanvasObject.Visible.Value.BooleanValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  DefaultCanvasObject.Visible.Value.BooleanValue = value
			End Set
		#tag EndSetter
		Visible As Boolean
	#tag EndComputedProperty


	#tag Constant, Name = DEFAULT_PARENT_NAME, Type = String, Dynamic = False, Default = \"DEFAULT_PARENT", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="BackupOpenTabValue"
			Group="Behavior"
			InitialValue="-1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="icon"
			Group="Behavior"
			Type="picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IndexParent"
			Group="Behavior"
			InitialValue="-1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IndexSprite"
			Group="Behavior"
			InitialValue="-1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="InObjectList"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ListIndex"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockColour"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockIcon"
			Group="Behavior"
			Type="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="OpenTabValue"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Presistant"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="typeTab"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="WrittenXml"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
