#tag Class
Protected Class ResourceScene
Inherits Resource
Implements ResourceInterface
	#tag Method, Flags = &h0
		Sub Create(newProject as boolean)
		  #pragma unused newProject
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		 Shared Function DrawGrid(SnapX As Uint32, SnapY As Uint32, Width As Uint32, Height As Uint32) As Picture
		  try
		    dim p as new picture(width, height)
		    dim g as graphics = p.Graphics
		    
		    for x as uint32 = 0 to Width step SnapX
		      for y as uint32 = 0 to Height step SnapY
		        g.DrawRect x, y, SnapX, SnapY
		      next
		    next
		    
		    return p
		  catch OutOfBoundsException
		  end try
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetCodeName() As string
		  return "pmSCENE" + me.name
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ReadNativeXml(ByRef x as XmlDocument, ByRef n as XmlNode)
		  SceneWidth = val(n.GetAttribute("Width"))
		  SceneHeight = val(n.GetAttribute("Height"))
		  
		  Colour = toColour(n.GetAttribute("Colour"))
		  BackgroundIndex = val(n.GetAttribute("Image"))
		  
		  Presistant = bool(n.GetAttribute("Presistant"))
		  
		  Dim NodeList as XmlNodeList
		  
		  //load arrays
		  NodeList = XQLResource(x,"/Object")
		  
		  Dim co As CanvasObject
		  for i as integer = 0 to NodeList.Length - 1
		    co = new CanvasObject
		    co.ReadNativeXml(NodeList.Item(i))
		    co.backupObjectIndex = val(NodeList.Item(i).GetAttribute("Index"))
		    Scene.Append co
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RedrawGrid()
		  GridPic = DrawGrid(SnapX, SnapY, SceneWidth, SceneHeight)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub WriteNativeXml(ByRef resource as XmlNode, ByRef xml as XmlDocument)
		  resource.SetAttribute("Width", str(me.SceneWidth))
		  resource.SetAttribute("Height", str(me.SceneHeight))
		  
		  resource.SetAttribute("Colour", str(me.Colour))
		  resource.SetAttribute("Image", str(docfile.Resources.IndexOf(me.Background)))
		  
		  resource.SetAttribute("Presistant", str(me.Presistant))
		  
		  dim node as XmlNode = resource.AppendChild(xml.CreateElement("Object"))
		  
		  for each co as CanvasObject in Scene
		    co.WriteNativeXml(node)
		  next
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		Background As ResourceSprite
	#tag EndProperty

	#tag Property, Flags = &h0
		BackgroundIndex As Integer = -1
	#tag EndProperty

	#tag Property, Flags = &h0
		Colour As Color = &cFFFFFF
	#tag EndProperty

	#tag Property, Flags = &h0
		GridPic As Picture
	#tag EndProperty

	#tag Property, Flags = &h0
		Presistant As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		Scene() As CanvasObject
	#tag EndProperty

	#tag Property, Flags = &h0
		SceneHeight As Uint32
	#tag EndProperty

	#tag Property, Flags = &h0
		SceneWidth As Uint32
	#tag EndProperty

	#tag Property, Flags = &h0
		SnapEnabled As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		SnapX As Uint32 = 32
	#tag EndProperty

	#tag Property, Flags = &h0
		SnapY As Uint32 = 32
	#tag EndProperty

	#tag Property, Flags = &h0
		Zoom As Integer = 100
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="BackgroundIndex"
			Group="Behavior"
			InitialValue="-1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="BackupOpenTabValue"
			Group="Behavior"
			InitialValue="-1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Colour"
			Group="Behavior"
			InitialValue="&cFFFFFF"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="GridPic"
			Group="Behavior"
			Type="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="icon"
			Group="Behavior"
			Type="picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ListIndex"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="OpenTabValue"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Presistant"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="SnapEnabled"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="typeTab"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="WrittenXml"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Zoom"
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
