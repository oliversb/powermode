#tag Class
Protected Class App
Inherits Application
	#tag Event
		Sub Open()
		  App.UseGDIPlus = true
		  IconMod.Setup
		  ScriptRunnerMod.Setup
		  
		  //handle plugins
		  PluginDev.AssignPluginsFolder
		  
		  if PluginDev.PluginsFolder.Exists = false then
		    PluginDev.PluginsFolder.CreateAsFolder
		  end if
		  
		  PluginDev.PluginFolderItem = PluginDev.PluginsFolder.Child("Startup.pmplugin")
		  
		  if PluginDev.PluginFolderItem.Exists = false then
		    dim t as TextOutputStream = TextOutputStream.Create(PluginDev.PluginFolderItem)
		    t.Write(ConvertEncoding("//This script allows you to determine the behavour of the application on startup" + EndOfLine(2), Encodings.UTF16))
		    t.Close
		  else
		    //open and run script from startup file
		    dim t as TextInputStream = TextInputStream.Open(PluginDev.PluginFolderItem)
		    PluginDev.RunSource(t.ReadAll, false)
		    t.Close
		  end if
		  
		  
		  //KAJU UPDATER
		  'mPrefFolder = SpecialFolder.CurrentWorkingDirectory.Child("Update")
		  'if not mPrefFolder.Exists then
		  'mPrefFolder.CreateAsFolder
		  'end if
		  '
		  '//
		  
		  '// See if this was a launch after an attempted update
		  '//
		  'dim fromVersion as string
		  'if Kaju.DidLastUpdateSucceed( fromVersion ) then
		  'MsgBox "You have successfully updated from version " + fromVersion + "."
		  'elseif Kaju.DidLastUpdateFail then
		  'MsgBox "The last update failed. Please try again."
		  'end if
		  
		  //create timer that runs constantly throughout the running of the app
		  myAppTimer = new AppTimer
		  myAppTimer.Enabled = true
		  myAppTimer.Mode = Timer.ModeMultiple
		  myAppTimer.Period = 1
		End Sub
	#tag EndEvent

	#tag Event
		Function UnhandledException(error As RuntimeException) As Boolean
		  #pragma unused error
		  #If DebugBuild = false then
		    Dim d As New Date
		    Dim f As New FolderItem
		    f  = SpecialFolder.Desktop.Child(App.ExecutableFile.DisplayName+"_ERRORLOG.txt")
		    If f.exists Then f.delete
		    
		    Dim t As TextOutputStream
		    t=textoutputstream.create(f)
		    
		    If t = Nil Then Return False
		    
		    t.WriteLine "This file contains an error log for the PowerMode error." + EndOfLine + EndOfLine
		    t.WriteLine "UnhandledException: " + Introspection.GetType(error).Name + EndOfLine
		    t.WriteLine "Current date: " +  d.SQLDateTime + EndOfLine + EndOfLine
		    t.WriteLine "Executable Name: " + App.ExecutableFile.DisplayName
		    t.WriteLine "Executable Path: " + f.NativePath + EndOfLine
		    t.WriteLine "Executable Size: " + Str( App.ExecutableFile.Length )
		    t.WriteLine "Executable ModificationDate: " + App.ExecutableFile.ModificationDate.SQLDateTime
		    t.WriteLine "Executable CreationDate: " + App.ExecutableFile.CreationDate.SQLDateTime + EndOfLine
		    t.WriteLine "Call Stack: " + EndOfLine
		    t.WriteLine "Stack: " + EndOfLine + Join( error.Stack, EndOfLine)
		    t.Close
		    
		    f.Launch
		    
		    Quit
		  #EndIf
		End Function
	#tag EndEvent


	#tag Method, Flags = &h0
		Function PrefFolder() As FolderItem
		  return mPrefFolder
		End Function
	#tag EndMethod


	#tag Property, Flags = &h21
		Private mPrefFolder As FolderItem
	#tag EndProperty

	#tag Property, Flags = &h0
		myAppTimer As Timer
	#tag EndProperty

	#tag Property, Flags = &h0
		UpdateInitiater As Kaju.UpdateInitiater
	#tag EndProperty


	#tag Constant, Name = kFileQuit, Type = String, Dynamic = False, Default = \"&Quit", Scope = Public
		#Tag Instance, Platform = Windows, Language = Default, Definition  = \"Exit"
	#tag EndConstant

	#tag Constant, Name = kFileQuitShortcut, Type = String, Dynamic = False, Default = \"Alt+F4", Scope = Public
		#Tag Instance, Platform = Mac OS, Language = Default, Definition  = \"Cmd+Q"
		#Tag Instance, Platform = Linux, Language = Default, Definition  = \"Ctrl+Q"
	#tag EndConstant

	#tag Constant, Name = kpAndroid, Type = Double, Dynamic = False, Default = \"3", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kpFlexAIRDesktop, Type = Double, Dynamic = False, Default = \"9", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kpFlexWeb, Type = Double, Dynamic = False, Default = \"2", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kpiOSiPad, Type = Double, Dynamic = False, Default = \"5", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kpiOSiPhone, Type = Double, Dynamic = False, Default = \"4", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kpPHPJavascriptWeb, Type = Double, Dynamic = False, Default = \"6", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kpPythonDesktop, Type = Double, Dynamic = False, Default = \"0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kpPythonMaemo, Type = Double, Dynamic = False, Default = \"1", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kpWinPhone7, Type = Double, Dynamic = False, Default = \"7", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
	#tag EndViewBehavior
End Class
#tag EndClass
